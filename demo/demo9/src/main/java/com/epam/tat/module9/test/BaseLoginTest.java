package com.epam.tat.module9.test;

import com.epam.tat.module9.ui.page.yandexmail.LoginPage;
import com.epam.tat.module9.util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class BaseLoginTest {

    protected WebDriver driver;

    private String userLogin = "tat-test-user@yandex.ru"; // ACCOUNT
    private String userPassword = "tat-123qwe"; // ACCOUNT

//    @BeforeClass
//    public void prepareBrowser() {
//
//    }

    @Test
    public void login() {
        driver = WebDriverHelper.getWebDriver();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin, userPassword);
    }

//    @Test //(dependsOnMethods = "login", priority = 999)
//    public void test1() throws InterruptedException {
//        Thread.sleep(2000);
//    }
//
//
//    @Test //(dependsOnMethods = "test1")
//    public void test2() throws InterruptedException {
//        Thread.sleep(2000);
//    }
}
