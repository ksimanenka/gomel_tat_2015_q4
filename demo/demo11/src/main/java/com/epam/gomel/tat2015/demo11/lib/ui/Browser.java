package com.epam.gomel.tat2015.demo11.lib.ui;

import com.epam.gomel.tat2015.demo11.lib.config.GlobalConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public class Browser implements WrapsDriver {

    public static final int COMMON_ELEMENT_WAIT_TIME_OUT = 7;
    public static final long WEBDRIVER_IMPLICIT_TIME_OUT = 4;
    private WebDriver driver;

    private static Browser instance = null;

    private Browser() {
    }

    public static Browser rise() {
        if (instance != null) {
            instance.quit();
        }
        Browser browser = new Browser();
        browser.createDriver();
        instance = browser;
        return browser;
    }

    public static Browser current() {
        if (instance == null) {
            rise();
        }
        return instance;
    }

    private void quit() {
        try {
            WebDriver wrappedDriver = getWrappedDriver();
            if (wrappedDriver != null) {
                wrappedDriver.quit();
            }
        } catch (Exception ignore) {
        } finally {
            instance = null;
        }
    }

    private void createDriver() {
        // Login of choosing browser uses global configs
        this.driver = localFirefoxDriver();
        switch (BrowserType.valueOf(GlobalConfig.getBrowser().toUpperCase())) {
            default:
            case FIREFOX:
                this.driver = localFirefoxDriver();
                break;
            case CHROME:
                this.driver = localChromeDriver();
                break;
        }
        driver.manage().timeouts().implicitlyWait(WEBDRIVER_IMPLICIT_TIME_OUT, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    private WebDriver localFirefoxDriver() {
        // driver specific
        return new FirefoxDriver();
    }

    private WebDriver localChromeDriver() {
        // driver specific
        return new ChromeDriver();
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void open(String url) {
        System.out.println("Open page : " + url); // Logger
        getWrappedDriver().get(url);
    }

    public void waitForAppear(By locator) {
        // Logger
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void writeText(By locator, String text) {
        System.out.println("Write text to : " + locator + " text = '" + text + "'"); // Logger
        getWrappedDriver().findElement(locator).sendKeys(text);
    }

    public void click(By locator) {
        // Logger
        getWrappedDriver().findElement(locator).click();
    }

    public String getText(By locator) {
        // Logger
        return getWrappedDriver().findElement(locator).getText();
    }

    public static void screenshot() {
        // implement
    }
}
