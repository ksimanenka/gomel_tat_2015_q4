package com.epam.gomel.tat2015.demo11.lib.runner;

import com.epam.gomel.tat2015.demo11.lib.util.ResourceUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class Runner {
    public static void main(String[] args) {
        // args4j
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<String>();
//        testNG.addListener(new CustomTestNgListener());
//        suites.add();
        testNG.setTestSuites(suites);
        testNG.run();
    }
}
