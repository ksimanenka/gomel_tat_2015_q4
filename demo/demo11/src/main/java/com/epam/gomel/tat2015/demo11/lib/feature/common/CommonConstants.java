package com.epam.gomel.tat2015.demo11.lib.feature.common;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public interface CommonConstants {
    String MAILBOX_URL = "http://mail.yandex.ru";

    String DEFAULT_USER_LOGIN = "igorepamtest";
    String DEFAULT_USER_PASSWORD = "qwertyqwerty";
    String DEFAULT_USER_MAIL = "igorepamtest@yandex.ru";

    String DEFAULT_MAIL_TO_SEND = "igorepamtest@yandex.ru";
}
