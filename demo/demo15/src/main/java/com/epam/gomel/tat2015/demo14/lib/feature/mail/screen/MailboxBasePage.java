package com.epam.gomel.tat2015.demo14.lib.feature.mail.screen;

import com.epam.gomel.tat2015.demo14.framework.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public abstract class MailboxBasePage {

    private By composeLetterLink = By.xpath("");
//    private By composeLetterLink = By.xpath("");
//    private By composeLetterLink = By.xpath("");
//    private By composeLetterLink = By.xpath("");

    public MailboxMenu menu() {
        // check if exists
        this.menu().openInbox().getCurrentAddress();
        return new MailboxMenu();
    }

    public WriteLetterPage openWriteLetter() {
        Browser.current().click(composeLetterLink);
        return new WriteLetterPage();
    }

    public InboxPage refresh() {
        // do some check for verify that page has refreshed
        return null;
    }

}
