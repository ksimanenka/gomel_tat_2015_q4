package com.epam.gomel.tat2015.demo14.lib.feature.steps;

import com.epam.gomel.tat2015.demo14.framework.util.Randomizer;
import com.epam.gomel.tat2015.demo14.lib.feature.common.Account;
import com.epam.gomel.tat2015.demo14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.demo14.lib.feature.mail.screen.MailLoginPage;
import com.epam.gomel.tat2015.demo14.lib.feature.mail.service.MailLoginService;
import com.epam.gomel.tat2015.demo14.lib.feature.mail.service.MailService;
import org.jbehave.core.annotations.*;
import org.testng.Assert;


public class MailSteps {

    private Account actor;
    private MailLoginService loginService = new MailLoginService();
    private MailService mailService = new MailService();

    @Given("Actor is mailbox owner")
    public void buildAccount() {
        actor = AccountBuilder.getDefaultAccount();
    }

    @Given("Actor has invalid password")
    public void accountHasInvalidPassword() {
        actor.setPassword(actor.getPassword() + Randomizer.alphabetic());
    }

    @When("Actor open login page")
    public void openLoginPage() {
        MailLoginPage.open();
    }

    @When("Actor enter credentials and click submit")
    @Aliases( values = {"Actor login to mailbox", ""})
    public void login() {
        loginService.loginToMailbox(actor);
    }

    @Then("Actor's Mailbox page is opened")
    public void checkAccountMailboxOpened() {
        Assert.assertTrue(mailService.isAccountMailboxOpened(actor), "Opened mailbox is not actor");
    }

    @Then("List of incoming messages is available")
    public void checkListOfMessages() {
        Assert.assertTrue(mailService.isInboxMessageListIsVisible(), "List on incoming messages not appeared");
    }
}
