package com.epam.tat.module4.test.testng;

import org.testng.annotations.Factory;

/**
 * Created by Aleh_Vasilyeu on 11/19/2015.
 */
public class PositiveNegativeTestFactory {

    @Factory
    public Object [] getTests() {
        return new Object[] {
                new PositiveNegativeTest(10, true, false),
                new PositiveNegativeTest(0, false, false)
        };
    }
}
