package com.epam.tat.module4.test.testng;

import com.epam.tat.module4.Timeout;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * @author Aleh_Vasilyeu
 */
public class TrigonometryTest extends BaseCalculatorTest {

    @Test(groups = "a")
    public void sin0() {
        checkTime();
        Timeout.sleep(1);
        System.out.println("sin0");
        double result = calculator.sin(0);
        assertEquals(result, 0.0);
    }

    @Test()
    public void sin30() {
        System.out.println("sin30");
        double result = round(calculator.sin(Math.PI / 6));
        assertEquals(result, 0.5);
    }

    @Test(groups = "a")
    public void sin45() {
        checkTime();
        Timeout.sleep(1);
        System.out.println("sin45");
        double result = round(calculator.sin(Math.PI / 4));
        assertEquals(result, 0.7071);
    }

    private double round(double value) {
        return (double) Math.round(value * 10000d) / 10000d;
    }
}
