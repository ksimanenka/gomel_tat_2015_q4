package com.epam.tat.module12.util;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.util.concurrent.TimeUnit;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class WebDriverHelper {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;

    private static WebDriver driver;

    public static WebDriver getWebDriver() {
        if (driver == null) {
//            FirefoxProfile profile = new FirefoxProfile();
//            profile.setPreference("browser.download.dir", "C:\\");
//            profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "image/png");
            driver = new FirefoxDriver();
//            try {
//                driver = new RemoteWebDriver( new URL("http://104.155.68.40:4444/wd/hub"), DesiredCapabilities.chrome());
//            } catch (MalformedURLException e) {
//                throw new RuntimeException(e);
//            }

            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }

        return driver;
    }

    // For allure
    @Attachment
    public static byte[] makeScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        }
    }

    public static WebElement waitForElementIsClickable(By locator) {

        new WebDriverWait(driver, DRIVE_IMPL_WAIT_TIMEOUT_SECONDS).until(ExpectedConditions.elementToBeClickable(locator));

        return driver.findElement(locator);
    }

    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, DRIVE_IMPL_WAIT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForVisible(By locator) {
        new WebDriverWait(driver, 20)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
