package com.epam.tat.module12.runner;

import com.epam.tat.module12.util.Logger;
import com.epam.tat.module12.util.WebDriverHelper;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Aleh_Vasilyeu on 1/18/2016.
 */
public class OnFailure extends TestListenerAdapter {

    @Step("Hi, I'm step in your testng listener")
    @Override
    public void onTestFailure(ITestResult tr) {
        Logger.error("Failed test: " + tr.getThrowable().getMessage(), tr.getThrowable());
        WebDriverHelper.makeScreenshot();
        createAttachment();
    }




    @Attachment("Custom attachment")
    public String createAttachment() {
        return "My own attachment body!";
    }
}
