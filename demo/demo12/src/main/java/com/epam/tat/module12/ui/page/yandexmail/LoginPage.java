package com.epam.tat.module12.ui.page.yandexmail;

import com.epam.tat.module12.ui.page.Page;
import com.epam.tat.module12.util.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class LoginPage extends Page {

    public static final String BASE_URL = "http://www.ya.ru";

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        Logger.info("Open " + BASE_URL);
        driver.get(BASE_URL);
        Logger.debug("Click on " + ENTER_BUTTON_LOCATOR);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
    }

    @Step("Login using credentials {0} :: {1}")
    public InboxPage login(String userLogin, String userPassword) {
        Logger.info("Login using credentials " + userLogin + " :: " + userPassword);
        Logger.debug("Type " + userLogin + " into " +  LOGIN_INPUT_LOCATOR);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        Logger.debug("Type " + userPassword + " into " + PASSWORD_INPUT_LOCATOR);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        Logger.debug("Submit login form");
        passInput.submit();
        return new InboxPage(driver);
    }
}
