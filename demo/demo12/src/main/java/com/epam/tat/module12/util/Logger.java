package com.epam.tat.module12.util;

import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Aleh_Vasilyeu on 3/24/2015.
 */
public class Logger {

    private static org.apache.log4j.Logger logger
            = org.apache.log4j.Logger.getLogger("com.epam");

    public static void info(String message) {
        stepLog("INFO " + message);
        logger.info(message);
    }

    public static void error(String message, Throwable e) {
        stepLog("ERROR " + message);
        logger.error(message, e);
    }

    public static void debug(String message) {
        stepLog("DEBUG " + message);
        logger.debug(message);
    }

    @Step("{0}")
    public static void stepLog(String log) {
    }


}
