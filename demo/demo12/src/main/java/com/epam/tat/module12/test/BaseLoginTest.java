package com.epam.tat.module12.test;

import com.epam.tat.module12.ui.page.yandexmail.InboxPage;
import com.epam.tat.module12.ui.page.yandexmail.LoginPage;
import com.epam.tat.module12.util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.epam.tat.module12.util.WebDriverHelper.getWebDriver;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class BaseLoginTest {

    protected WebDriver driver;

    private String userLogin = "tat-test-user@yandex.ru"; // ACCOUNT
    private String userPassword = "tat-123qwe"; // ACCOUNT

    @Test
    @Features("BASE MAIL FUNCTIONALITY")
    @Stories("LOGIN")
    public void login() {
        driver = WebDriverHelper.getWebDriver();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        InboxPage inboxPage = loginPage.login(userLogin, userPassword);

        Assert.assertFalse(inboxPage.isCorrectUserLoggedIn(userLogin), "Invalid user in header present");
    }

    @AfterClass
    public void shutdown() {
        WebDriverHelper.shutdownWebDriver();
    }
}
