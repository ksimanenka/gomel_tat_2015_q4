package com.epam.tat.module12.runner;

import com.epam.tat.module12.util.Logger;
import org.testng.IReporter;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.reporters.EmailableReporter;
import org.testng.xml.XmlSuite;
import ru.yandex.qatools.allure.data.AllureReportGenerator;
import ru.yandex.qatools.allure.testng.AllureTestListener;
import ru.yandex.qatools.allure.utils.AllureResultsUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleh_Vasilyeu on 1/18/2016.
 */
public class Runner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();

        testNG.addListener(new TestListenerAdapter());
        testNG.addListener(new OnFailure());

        XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");

        List<String> files = new ArrayList<String>();
        files.addAll(new ArrayList<String>() {{
            add("./src/main/resources/suites/testng.xml");
        }});
        suite.setSuiteFiles(files);


        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);

        testNG.setXmlSuites(suites);
    }
}
