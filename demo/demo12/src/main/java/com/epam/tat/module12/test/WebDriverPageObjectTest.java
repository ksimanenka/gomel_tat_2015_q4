package com.epam.tat.module12.test;


import com.epam.tat.module12.bo.Letter;
import com.epam.tat.module12.bo.LetterFactory;
import com.epam.tat.module12.ui.page.yandexmail.ComposePage;
import com.epam.tat.module12.ui.page.yandexmail.InboxPage;
import com.epam.tat.module12.ui.page.yandexmail.LoginPage;
import com.epam.tat.module12.util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class WebDriverPageObjectTest extends BaseLoginTest {

    private Letter letter;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }

//    @BeforeClass(dependsOnMethods = "prepareBrowser")
//    public void login() {
//        LoginPage loginPage = new LoginPage(driver);
//        loginPage.open();
//        loginPage.login(userLogin, userPassword);
//    }

    @Test
    @Features("BASE MAIL FUNCTIONALITY")
    @Stories("SEND MAIL")
    public void sendMail() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);

        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresent(letter), "Letter should be present in inbox folder");
    }
}
