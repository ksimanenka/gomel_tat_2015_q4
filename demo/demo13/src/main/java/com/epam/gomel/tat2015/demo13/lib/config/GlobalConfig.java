package com.epam.gomel.tat2015.demo13.lib.config;

import com.epam.gomel.tat2015.demo13.lib.ui.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import org.testng.xml.XmlSuite.ParallelMode;

import java.util.List;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class GlobalConfig {

    private static GlobalConfig instance;

    @Option(name = "-bt", usage = "browser type: firefox or chrome")
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name="-suites", usage = "list of pathes to suites", handler = StringArrayOptionHandler.class, required = true)
    private List<String> suites = null;

    @Option(name = "-pm", usage = "parallel mode: false or tests")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name="-tc", usage = "amount of threads for parallel execution, equal to 0 by default")
    private int threadCount = 0;

    @Option(name = "-hub", usage = "selenium hub")
    private String seleniumHub = "";

    public static GlobalConfig getInstance() {
        if(instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public String getSeleniumHub() {
        return seleniumHub;
    }

    public void setSeleniumHub(String seleniumHub) {
        this.seleniumHub = seleniumHub;
    }



}
