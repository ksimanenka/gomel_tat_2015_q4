package com.epam.gomel.tat2015.demo13.lib.runner;

import com.epam.gomel.tat2015.demo13.lib.config.GlobalConfig;
import com.epam.gomel.tat2015.demo13.lib.util.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class Runner {

    public Runner(String[] args) {
        parseCli(args);
    }

    public static void main(String[] args) {
        new Runner(args).runTests();
    }

    private void parseCli(String[] args){
        Logger.info("Parse cli params...");
        CmdLineParser parser = new CmdLineParser(GlobalConfig.getInstance());
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            Logger.error("Failed to parse cli params: " + e.getMessage(), e);
            parser.printUsage(System.out);
            System.exit(1);
        }
    }

    private void runTests() {
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestListener());
        testNG.addListener(new ParallelSuitesListener());

        XmlSuite suite = new XmlSuite();
        suite.setName("Suite");

        List<String> files = new ArrayList<String>();
        files.addAll(GlobalConfig.getInstance().getSuites());
        suite.setSuiteFiles(files);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        testNG.setXmlSuites(suites);

        Logger.info("Tests will be started");
        testNG.run();
    }
}
