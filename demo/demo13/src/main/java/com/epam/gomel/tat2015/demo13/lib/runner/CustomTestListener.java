package com.epam.gomel.tat2015.demo13.lib.runner;

import com.epam.gomel.tat2015.demo13.lib.ui.Browser;
import com.epam.gomel.tat2015.demo13.lib.util.Logger;
import org.testng.ITestContext;
import org.testng.TestListenerAdapter;

/**
 * @author Aleh_Vasilyeu
 */
public class CustomTestListener extends TestListenerAdapter {

    @Override
    public void onStart(ITestContext testContext) {
        super.onStart(testContext);
        Logger.info("[STARTED TEST] " + testContext.getName());
    }

    @Override
    public void onFinish(ITestContext testContext) {
        super.onFinish(testContext);
        Logger.info("[FINISHED TEST] " + testContext.getName());
        Browser.current().quit();
    }
}
