package com.epam.gomel.tat2015.demo13.lib.ui;

import com.epam.gomel.tat2015.demo13.lib.config.GlobalConfig;
import com.epam.gomel.tat2015.demo13.lib.util.Logger;
import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.security.krb5.internal.crypto.Des;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public class Browser implements WrapsDriver {

    public static final int COMMON_ELEMENT_WAIT_TIME_OUT = 7;
    public static final long WEBDRIVER_IMPLICIT_TIME_OUT = 4;
    private WebDriver driver;

    private static Map<Thread, Browser> instances = new HashMap<>();

    private Browser() {
    }

    public static synchronized Browser rise() {
        if(instances.get(Thread.currentThread()) != null) {
            instances.get(Thread.currentThread()).quit();
        }

        Browser browser = new Browser();
        browser.createDriver();
        instances.put(Thread.currentThread(), browser);

        return browser;
    }



    public static synchronized Browser current() {
        if(instances.get(Thread.currentThread()) == null) {
            rise();
        }
        return instances.get(Thread.currentThread());
    }

    public void quit() {
        Logger.debug("Stop browser");
        try {
            WebDriver wrappedDriver = getWrappedDriver();
            if (wrappedDriver != null) {
                wrappedDriver.quit();
            }
        } catch (Exception ignore) {
        } finally {
            instances.remove(Thread.currentThread());
        }
    }

    private void createDriver() {
        // Login of choosing browser uses global configs
        switch (GlobalConfig.getInstance().getBrowserType()) {
            default:
            case FIREFOX:
                Logger.debug("Start firefox browser");
                this.driver = localFirefoxDriver();
                break;
            case CHROME:
                Logger.debug("Start chrome browser");
                this.driver = localChromeDriver();
                break;
        }
        driver.manage().timeouts().implicitlyWait(WEBDRIVER_IMPLICIT_TIME_OUT, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    private WebDriver localFirefoxDriver() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.dir", "/tmp/download");
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        return new FirefoxDriver(profile);
    }

    private WebDriver localChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver");

        DesiredCapabilities cap = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();

        Map<String, Object> content_setting = new HashMap<>();
        content_setting.put("automatic_downloads", 1);
        content_setting.put("popups", 1);

        Map<String, Object> prefs = new HashMap<>();
        prefs.put("download.default_directory", "/tmp/download");
        prefs.put("download.prompt_for_download", "false");
        prefs.put("profile.default_content_setting_values", content_setting);

        options.setExperimentalOption("prefs", prefs);
        options.addArguments("--disable-web-security");
        options.addArguments("--allow-running-insecure-content");

        cap.setCapability(ChromeOptions.CAPABILITY, options);

        return new ChromeDriver(cap);
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void open(String url) {
        Logger.debug("Open page : " + url); // Logger
        getWrappedDriver().get(url);
    }

    public void waitForAppear(By locator) {
        Logger.debug("Wait for element: " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void writeText(By locator, String text) {
        Logger.debug("Write text to : " + locator + " text = '" + text + "'"); // Logger
        screenshot();
        getWrappedDriver().findElement(locator).sendKeys(text);
    }

    public void click(By locator) {
        Logger.debug("Click on element: " + locator);
        getWrappedDriver().findElement(locator).click();
    }

    public String getText(By locator) {
        Logger.debug("Get text of element: " + locator);
        return getWrappedDriver().findElement(locator).getText();
    }

    public static void screenshot() {
        WebDriver driver = current().getWrappedDriver();
        byte[] screenshotBytes = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        File screenshotFile = new File("./screenshot/"+System.nanoTime()+".png");
        try {
            FileUtils.writeByteArrayToFile(new File("./screenshot/"+System.nanoTime()+".png"), screenshotBytes);
            Logger.debug("Screenshot taken: file://" + screenshotFile.getAbsolutePath());
        } catch (IOException e) {
            Logger.error("Failed to write screenshot: " + e.getMessage(), e);
        }
    }
}
