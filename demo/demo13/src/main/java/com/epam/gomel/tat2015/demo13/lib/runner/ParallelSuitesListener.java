package com.epam.gomel.tat2015.demo13.lib.runner;

import com.epam.gomel.tat2015.demo13.lib.config.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * @author Aleh_Vasilyeu
 */
public class ParallelSuitesListener implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.getInstance().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalConfig.getInstance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {

    }
}
