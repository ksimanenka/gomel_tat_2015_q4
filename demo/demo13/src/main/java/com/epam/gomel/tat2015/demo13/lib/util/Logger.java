package com.epam.gomel.tat2015.demo13.lib.util;

/**
 * @author Aleh_Vasilyeu
 */
public class Logger {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("com.epam.gomel.tat2015.demo13");


    public static void info(String message) {
        logger.info(message);
    }

    public static void debug(String message) {
        logger.debug(message);
    }

    public static void error(String message, Throwable throwable) {
        logger.error(message, throwable);
    }

}
