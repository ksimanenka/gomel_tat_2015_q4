package com.epam.gomel.tat2015.demo13.tests.mail;

import com.epam.gomel.tat2015.demo13.lib.feature.common.Account;
import com.epam.gomel.tat2015.demo13.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.demo13.lib.feature.mail.service.MailLoginService;
import com.epam.gomel.tat2015.demo13.lib.runner.CustomTestListener;
import com.epam.gomel.tat2015.demo13.lib.ui.Browser;
import org.junit.After;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class MailSuccessLoginTest {

    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getDefaultAccount();

    @Test(description = "Login to mailbox as a valid user")
    public void successLoginToMailbox() {
        loginService.checkSuccessLogin(account);
    }
}
