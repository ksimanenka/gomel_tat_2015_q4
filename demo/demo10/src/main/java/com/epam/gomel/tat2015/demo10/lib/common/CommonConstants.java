package com.epam.gomel.tat2015.demo10.lib.common;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public interface CommonConstants {
    String MAILBOX_URL = "";
    String DEFAULT_MAIL_USER_LOGIN = "";
    String DEFAULT_MAIL_USER_PASSWORD = "";
    String DEFAULT_MAIL_TO_SEND = "";
}
