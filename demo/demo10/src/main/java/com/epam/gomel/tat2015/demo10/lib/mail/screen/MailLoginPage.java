package com.epam.gomel.tat2015.demo10.lib.mail.screen;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public class MailLoginPage {

    @FindBy(name = "login")
    WebElement loginInput;

    @FindBy(name = "passwd")
    WebElement passInput;

    @FindBy(name = "")
    WebElement submitButton;

    public void login(String login, String pass) {
        loginInput.sendKeys(login);
        passInput.sendKeys(pass);
        loginInput.submit();
    }
}
