package com.epam.gomel.tat2015.demo10.lib.mail.service;

import com.epam.gomel.tat2015.demo10.lib.common.Account;
import com.epam.gomel.tat2015.demo10.lib.mail.Letter;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public class MailService {

    public void sendLetter(Account account, Letter letter) {
        if(letter.containsAttach()) {
            letter.getAttachment().getAbsolutePath();
        }
    }

    public boolean isLetterExistInInbox(Account account, Letter letter) {
        return false;
    }

}
