package com.home.tat.home5.listeners;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestMethodFailListener implements IInvokedMethodListener {

	public void afterInvocation(IInvokedMethod meth, ITestResult res) {
		final String PATH_TO_LOGFILE = "d:/TAT/TAT_2015_Q4/gomel_tat_2015_q4_h4/log.txt";

		if (!res.isSuccess()) {
			File logFile = new File(PATH_TO_LOGFILE);
			List<String> outputData = new ArrayList<String>();
			String methodName = res.getMethod().getMethodName();
			String exceptionMessage = res.getThrowable().getMessage();
			String stackTrace = res.getThrowable().getStackTrace().toString();
			outputData.add(methodName);
			outputData.add(exceptionMessage);
			outputData.add(stackTrace);
			try {
				FileUtils.writeLines(logFile, outputData);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {

	}

}
