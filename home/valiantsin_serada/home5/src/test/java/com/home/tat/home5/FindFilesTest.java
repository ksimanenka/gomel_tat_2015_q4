package com.home.tat.home5;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.gomel.tat.home5.WordList;

public class FindFilesTest {
	private String wrongBaseDirectory;
	private String fileNamePattern;
	private WordList wordList = new WordList();

	FindFilesTest() {
		super();
	}

	FindFilesTest(String wrongBaseDirectory, String fileNamePattern) {
		super();
		this.wrongBaseDirectory = wrongBaseDirectory;
		this.fileNamePattern = fileNamePattern;
	}

	@Parameters({ "wrongBaseDirectory", "fileNamePattern" })
	@Test(expectedExceptions = IllegalStateException.class, groups = "except")
	public void throwIfBaseDirectoryIsWrong(String wrongBaseDirectory,
			String fileNamePattern) throws IllegalStateException {
		wordList.getWords(wrongBaseDirectory, fileNamePattern);
	}
}
