package com.home.tat.home7.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.home.tat.home7.enums.Browsers;

public class CreateInstanceOfWebDriver {
	public static final String REMOTE_SERVER_ADDRESS = "http://localhost:4444/wd/hub";
	public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
	public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
	public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
	private static WebDriver driver;

	public static WebDriver getDriver(String browser)
			throws MalformedURLException {
		if (browser.equals(Browsers.CHROME.name())) {
			driver = new RemoteWebDriver(new URL(REMOTE_SERVER_ADDRESS),
					DesiredCapabilities.chrome());
		} else
			driver = new RemoteWebDriver(new URL(REMOTE_SERVER_ADDRESS),
					DesiredCapabilities.firefox());
		driver.manage().window().maximize();
		driver.manage()
				.timeouts()
				.pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
						TimeUnit.SECONDS);
		driver.manage()
				.timeouts()
				.implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS,
						TimeUnit.SECONDS);
		return driver;
	}

}
