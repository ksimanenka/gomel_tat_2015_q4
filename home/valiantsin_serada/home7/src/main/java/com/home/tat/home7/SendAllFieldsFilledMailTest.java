package com.home.tat.home7;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SendAllFieldsFilledMailTest extends BaseMailTest {

	@Parameters("browser")
	@Test
	public void sendAllFieldsFilledMailTest(String browser)
			throws MalformedURLException {
		loginMail(browser, userLogin, userPassword);
		sendMail(mailTo, mailSubject, mailContent);
		WebElement inboxMailLink = driver.findElement(By.xpath(String.format(
				MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
		WebElement sentboxLink = driver.findElement(SENTBOX_LINK_LOCATOR);
		sentboxLink.click();
		WebElement sentboxMailLink = driver.findElement(By.xpath(String.format(
				MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
		Assert.assertEquals(sentboxMailLink, inboxMailLink);

	}
}
