package com.home.tat.home7;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class LoginNegativeTest extends BaseMailTest {

	@Parameters("browser")
	@Test
	public void loginNegativeTest(String browser) throws MalformedURLException {
		final String wrongPassword = "wrong password";
		loginMail(browser, userLogin, wrongPassword);
		String url = driver.getCurrentUrl();
		Assert.assertFalse(url.contains(BASE_URL));
	}

}
