package com.home.tat.home7;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

import com.home.tat.home7.utils.CreateInstanceOfWebDriver;

public class BaseMailTest {
	public static final String BASE_URL = "https://mail.yandex.by";
	public static final By LOGIN_INPUT_LOCATOR = By.name("login");
	public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
	public static final String userLogin = "tat-test-user@yandex.ru";
	public static final String userPassword = "tat-123qwe";
	public String mailSubject = "test subject" + Math.random() * 100000000;
	public String mailContent = "mail content" + Math.random() * 100000000;
	// LOCATORS
	public static final By COMPOSE_BUTTON_LOCATOR = By
			.xpath("//a[@href='#compose']");
	public static final By TO_INPUT_LOCATOR = By
			.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
	public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
	public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
	public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
	public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
	public static final By SENTBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#sent']");
	public static final By DRAFTBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#draft']");
	public static final By DELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='compose.delete']");
	public static final By PERMANENTDELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='delete']");
	public static final By CANCEL_LINK_LOCATOR = By
			.xpath("//a[@data-action='dialog.cancel']");
	public static final By BUTTON_SAVE_LOCATOR = By
			.xpath("//button[@data-action='dialog.save']");
	public static final By TRASHBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#trash']");
	public static final By DRAFT_CHECKBOX_LOCATOR = By
			.xpath("//input[@class='b-messages-head__checkbox']");
	public static final By POPUP_TABLE_LOCATOR = By
			.xpath("//a[@data-action='delete']");
	public static final By PERMANENT_DELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='delete']");
	public static final By STATUSLINE_MESSAGE_LOCATOR = By
			.xpath("//div[@class='b-statusline']");
	// FIND_PATTERNS
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
	public static final String CHECKBOX_LOCATOR_PATTERN = "//input[@value='%s']]";
	public static final String mailTo = "tat-test-user@yandex.ru";

	public WebDriver driver;

	@AfterClass(description = "Close browser")
	public void clearBrowser() {
		driver.quit();
	}

	public void loginMail(String browser, String login, String password)
			throws MalformedURLException {
		driver = CreateInstanceOfWebDriver.getDriver(browser);
		driver.get(BASE_URL);
		WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
		loginInput.sendKeys(login);
		WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
		passInput.sendKeys(password);
		passInput.submit();
	}

	public void sendMail(String mailTo, String mailSubject, String mailContent) {
		createDraftMail(mailTo, mailSubject, mailContent);
		WebElement sendMailButton = driver
				.findElement(SEND_MAIL_BUTTON_LOCATOR);
		sendMailButton.click();

	}

	public WebElement waitForElementIsClickable(By locator) {
		new WebDriverWait(driver, 5000).until(ExpectedConditions
				.elementToBeClickable(locator));
		return driver.findElement(locator);
	}

	public WebElement waitForDisappear(By locator) {
		final int TIME_FOR_FIND_ELEMENT = 5000;
		new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT)
				.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		return driver.findElement(locator);
	}

	public void createDraftMail(String mailTo, String mailSubject,
			String mailContent) {
		WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
		composeButton.click();
		WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
		toInput.sendKeys(mailTo);
		WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
		subjectInput.sendKeys(mailSubject);
		WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
		mailContentText.sendKeys(mailContent);

	}

	public boolean isElementExist(By locator) {
		final int TIME_FOR_FIND_ELEMENT = 5000;
		try {
			new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT)
					.until(ExpectedConditions
							.invisibilityOfElementLocated(locator));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
