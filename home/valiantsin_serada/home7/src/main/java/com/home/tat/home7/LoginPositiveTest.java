package com.home.tat.home7;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class LoginPositiveTest extends BaseMailTest {

	@Parameters("browser")
	@Test
	public void loginPositivTest(String browser) throws MalformedURLException {
		loginMail(browser, userLogin, userPassword);
		String url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains(BASE_URL));
	}

}
