package com.home.tat.home9.utils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverHelper {
	public static final String DOWNLOAD_PATH = "D:\\downloads\\";
	public static final String REMOTE_SERVER_ADDRESS = "http://localhost:4444/wd/hub";
	public static final String BROWSER_QUIT_ERROR_MESSAGE = "Problem with shutting down driver:";
	public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
	public static final String CHROME_BROWSER = "chrome";
	public static final String FIREFOX_BROWSER = "firefox";
	public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
	public static final int DOWNLOAD_TIMEOUT_SECONDS = 10;
	public static final int TIME_FOR_FIND_ELEMENT_SECONDS = 10;
	public static DesiredCapabilities browserCapabilities;
	private static WebDriver driver;

	public static WebDriver getWebDriver(String browser)
			throws MalformedURLException {
		if (driver == null) {
			if (browser.equals(CHROME_BROWSER)) {
				ChromeOptions options = ChromeBrowserOptions
						.setChromeBrowserOptions();
				browserCapabilities = DesiredCapabilities.chrome();
				browserCapabilities.setCapability(ChromeOptions.CAPABILITY,
						options);
			} else {

				FirefoxProfile firefoxProfile = FirefoxBrowserProfile
						.setFireFoxBrowserProfile();
				browserCapabilities = DesiredCapabilities.firefox();
				browserCapabilities.setCapability(FirefoxDriver.PROFILE,
						firefoxProfile);
			}
			driver = new RemoteWebDriver(new URL(REMOTE_SERVER_ADDRESS),
					browserCapabilities);
			driver.manage().window().maximize();
			driver.manage()
					.timeouts()
					.pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
							TimeUnit.SECONDS);
			driver.manage()
					.timeouts()
					.implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS,
							TimeUnit.SECONDS);
		}
		return driver;
	}

	public static String getBrowserName() {
		return browserCapabilities.getBrowserName();
	}

	public static void shutdownWebDriver() {
		try {
			driver.quit();
			driver = null;
		} catch (Exception e) {
			System.err.println(BROWSER_QUIT_ERROR_MESSAGE + e.getMessage());
		}
	}

	public static WebElement waitForElementIsClickable(By locator) {
		new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT_SECONDS)
				.until(ExpectedConditions.elementToBeClickable(locator));
		return driver.findElement(locator);
	}

	public static WebElement waitForDisappear(By locator) {
		new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT_SECONDS)
				.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		return driver.findElement(locator);
	}

	public static WebElement waitForAppearence(By locator) {
		new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT_SECONDS)
				.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return driver.findElement(locator);
	}

	public static void waitForFileDownloading(final String fileName) {
		(new WebDriverWait(driver, DOWNLOAD_TIMEOUT_SECONDS))
				.until(new ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver driver) {

						return new File(DOWNLOAD_PATH.concat(fileName))
								.exists();
					}
				});

	}
}
