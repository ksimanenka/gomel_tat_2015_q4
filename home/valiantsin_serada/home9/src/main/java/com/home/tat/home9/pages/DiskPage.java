package com.home.tat.home9.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.home.tat.home9.utils.WebDriverHelper;

public class DiskPage extends Page {
	public static final String FILE_ELEMENT_LOCATOR_PATTERN = "//*[@class='nb-resource__name']//div[contains(., '%s')]";
	public static final int TIME_FOR_FIND_ELEMENT_SEC = 10;

	public DiskPage(WebDriver driver) {
		super(driver);
	}

	public boolean isFilePresent(String fileName) {
		try {
			WebDriverHelper.waitForAppearence(By.xpath(String.format(
					FILE_ELEMENT_LOCATOR_PATTERN, fileName)));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static void findAndClickFileElement(final String fileName) {
		(new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT_SEC))
				.until(new ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver driver) {

						try {
							WebElement fileElement = driver.findElement(By
									.xpath(String.format(
											FILE_ELEMENT_LOCATOR_PATTERN,
											fileName)));
							fileElement.click();
						} catch (Exception e) {
							return false;
						}
						return true;
					}
				});
	}
}
