package com.home.tat.home9.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {
	public static final String BASE_URL = "https://www.disk.yandex.ru";
	public static final By LOGIN_INPUT_LOCATOR = By.name("login");
	public static final By PASSWORD_INPUT_LOCATOR = By.name("password");

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		driver.get(BASE_URL);
	}

	public LoginPage typeUsername(String username) {
		driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(username);
		return this;
	}

	public LoginPage typePassword(String password) {
		driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
		return this;
	}

	public MainDiskPage submitLogin() {
		driver.findElement(LOGIN_INPUT_LOCATOR).submit();
		return new MainDiskPage(driver);
	}

	public MainDiskPage loginAs(String username, String password) {
		typeUsername(username);
		typePassword(password);
		return submitLogin();
	}

}
