package com.home.tat.home9.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home9.pages.MainDiskPage;
import com.home.tat.home9.pages.TrashPage;

public class SeveralFilesToTrashTest extends BaseTest {
	public static final String UPLOAD_FILE_PATH = "D:\\uploads\\";
	public static final String FOLDER_NAME = "TextFiles";
	public static final String FILE_NAME1 = "testFile1.txt";
	public static final String FILE_NAME2 = "testFile2.txt";
	public static final String FILE_NAME3 = "testFile3.txt";
	MainDiskPage diskPage;
	TrashPage trashPage;

	@BeforeClass
	public void severalFilesUpload() {
		diskPage = login();
		diskPage.openFolder(FOLDER_NAME);
		diskPage.uploadFile(UPLOAD_FILE_PATH.concat(FILE_NAME1));
		diskPage.uploadFile(UPLOAD_FILE_PATH.concat(FILE_NAME2));
		diskPage.uploadFile(UPLOAD_FILE_PATH.concat(FILE_NAME3));
	}

	@Test
	public void FilesToTrashTest() {
		diskPage.sendFilesToTrash(FILE_NAME1, FILE_NAME2, FILE_NAME3);
		trashPage = new TrashPage(driver);
		trashPage.open();
		Assert.assertTrue(trashPage.isFilesPresent(FILE_NAME1, FILE_NAME2,
				FILE_NAME3));
	}

	@AfterClass
	public void severalFilesDeleteelete() {
		trashPage.deleteFilePermanently(FILE_NAME1);
		trashPage.deleteFilePermanently(FILE_NAME2);
		trashPage.deleteFilePermanently(FILE_NAME3);
	}

}
