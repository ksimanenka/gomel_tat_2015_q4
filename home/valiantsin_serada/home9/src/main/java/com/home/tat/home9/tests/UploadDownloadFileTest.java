package com.home.tat.home9.tests;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.home.tat.home9.pages.MainDiskPage;
import com.home.tat.home9.pages.TrashPage;

public class UploadDownloadFileTest extends BaseTest {
	public static final String UPLOAD_FILE_PATH = "D:\\uploads\\testFile.txt";
	public static final String DOWNLOAD_DIRECTORY = "D:\\downloads\\";
	public static final String FOLDER_NAME = "TextFiles";
	public static final String FILE_NAME = "testFile.txt";
	MainDiskPage diskPage;
	TrashPage trashPage;

	@Test
	public void uploadTest() {
		diskPage = login();
		diskPage.openFolder(FOLDER_NAME);
		diskPage.uploadFile(UPLOAD_FILE_PATH);
		Assert.assertTrue(diskPage.isFilePresent(FILE_NAME));
	}

	@Test(dependsOnMethods = { "uploadTest" })
	public void downloadTest() throws IOException {
		diskPage.downloadFile(FILE_NAME);
		File uploadFile = new File(UPLOAD_FILE_PATH);
		File downloadFile = new File(DOWNLOAD_DIRECTORY.concat(FILE_NAME));
		Assert.assertTrue(FileUtils.contentEquals(uploadFile, downloadFile));
	}

	@AfterClass
	public void deleteTestFile() {
		diskPage.sendFilesToTrash(FILE_NAME);
		trashPage = new TrashPage(driver);
		trashPage.open();
		trashPage.deleteFilePermanently(FILE_NAME);
	}

}
