package com.home.tat.home10.lib.mail.screens.bloks;

import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;

@FindBy(xpath = "//form[@class='compose-form']")
public class ComposeForm extends HtmlElement {
	public static final String TO_INPUT_XPATH = "//*[@data-params='field=to']//ancestor::tr//input[@type='text']";
	public static final String SUBJECT_INPUT_NAME = "subj";
	public static final String MAIL_TEXT_ID = "compose-send";
	public static final String BUTTON_SUBMIT_ID = "compose-submit";

	@FindBy(xpath = TO_INPUT_XPATH)
	private TextInput addressInput;

	@FindBy(name = SUBJECT_INPUT_NAME)
	private TextInput subjectInput;

	@FindBy(id = MAIL_TEXT_ID)
	private TextInput contentMailInput;

	@FindBy(id = BUTTON_SUBMIT_ID)
	private Button submitButton;

	public Button getSubmitButton() {
		return submitButton;
	}

	public static String getButtonSubmitId() {
		return BUTTON_SUBMIT_ID;
	}

	public TextInput getAddressInput() {
		return addressInput;
	}

	public TextInput getSubjectInput() {
		return subjectInput;
	}

	public TextInput getContentMailInput() {
		return contentMailInput;
	}

}
