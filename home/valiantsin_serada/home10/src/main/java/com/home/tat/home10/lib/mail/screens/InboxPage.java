package com.home.tat.home10.lib.mail.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.home.tat.home10.lib.common.models.Account;
import com.home.tat.home10.lib.common.screens.Page;
import com.home.tat.home10.lib.common.utils.WebDriverHelper;
import com.home.tat.home10.lib.mail.models.Letter;
import com.home.tat.home10.lib.mail.services.LoginService;
import com.home.tat.home10.lib.mail.services.MailService;

public class InboxPage extends Page {
	public static final By USER_NAME_ELEMENT_LOCATOR = By
			.xpath("//span[@class='header-user-name js-header-user-name']");
	public static final By INPOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(@href, '%s')]";
	public static final By COMPOSE_BUTTON_LOCATOR = By
			.xpath("//a[@href='#compose']");
	public static final By DELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='compose.delete']");

	public InboxPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		WebDriverHelper.waitForElementIsClickable(INPOX_LINK_LOCATOR).click();
	}

	public boolean isUserNameDisplayed(Account account) {
		return LoginService.checkSuccessLogin(driver,
				USER_NAME_ELEMENT_LOCATOR, account);
	}

	public boolean isLetterPresent(Letter letter) {
		return MailService.isLetterPresent(MAIL_LINK_LOCATOR_PATTERN, letter);
	}

}
