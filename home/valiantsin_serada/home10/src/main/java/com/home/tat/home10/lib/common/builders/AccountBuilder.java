package com.home.tat.home10.lib.common.builders;

import static com.home.tat.home10.lib.common.constants.CommonConstants.DEFAULT_MAIL_TO_SEND;
import static com.home.tat.home10.lib.common.constants.CommonConstants.DEFAULT_MAIL_USER_LOGIN;
import static com.home.tat.home10.lib.common.constants.CommonConstants.DEFAULT_MAIL_USER_PASSWORD;

import org.apache.commons.lang3.RandomStringUtils;

import com.home.tat.home10.lib.common.models.Account;

public class AccountBuilder {
	public final static int NUMBER_ADDED_SYMBOLS = 3;

	public static Account getDefaultAccount() {
		Account account = new Account();
		account.setLogin(DEFAULT_MAIL_USER_LOGIN);
		account.setPassword(DEFAULT_MAIL_USER_PASSWORD);
		account.setEmail(DEFAULT_MAIL_TO_SEND);
		return account;
	}

	public static Account getAccountWithWrongLogin() {
		Account account = getDefaultAccount();
		account.setLogin(account.getLogin().concat(
				RandomStringUtils.randomAlphabetic(NUMBER_ADDED_SYMBOLS)));
		return account;
	}

}
