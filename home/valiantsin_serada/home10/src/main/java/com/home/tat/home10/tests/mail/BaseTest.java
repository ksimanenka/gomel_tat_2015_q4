package com.home.tat.home10.tests.mail;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.home.tat.home10.lib.common.builders.AccountBuilder;
import com.home.tat.home10.lib.common.models.Account;
import com.home.tat.home10.lib.common.utils.WebDriverHelper;
import com.home.tat.home10.lib.mail.screens.InboxPage;
import com.home.tat.home10.lib.mail.screens.MailLoginPage;

public class BaseTest {
	protected WebDriver driver;

	@Parameters("browser")
	@BeforeClass
	public void prepareBrowser(String browser) throws MalformedURLException {
		driver = WebDriverHelper.getWebDriver(browser);
	}

	public InboxPage login() {
		MailLoginPage loginPage = new MailLoginPage(driver);
		loginPage.open();
		Account account = AccountBuilder.getDefaultAccount();
		return loginPage.loginAs(account);
	}

	@AfterClass
	public void closeBrowser() {
		WebDriverHelper.shutdownWebDriver();
	}
}
