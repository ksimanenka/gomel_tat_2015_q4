package com.home.tat.home10.lib.mail.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

import com.home.tat.home10.lib.common.screens.Page;
import com.home.tat.home10.lib.common.utils.WebDriverHelper;
import com.home.tat.home10.lib.mail.models.Letter;
import com.home.tat.home10.lib.mail.screens.bloks.ComposeForm;
import com.home.tat.home10.lib.mail.services.MailService;

public class ComposePage extends Page {
	public static final By COMPOSE_BUTTON_LOCATOR = By
			.xpath("//a[@href='#compose']");
	public static final By BUTTON_SAVE_LOCATOR = By
			.xpath("//button[@data-action='dialog.save']");
	public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
	public static final By NEW_MESSAGE_LOCATOR = By
			.xpath("//a[@class='b-statusline__link']");
	public static final By POPUP_TABLE_LOCATOR = By
			.xpath("//table[@class='b-popup__box']");
	public static final String UNFILLED_ADDRESS_FIELD_ALERT_LOCATOR_PATTERN = "//span[@class='b-notification__i' and contains(., '%s')]";

	private ComposeForm composeForm;

	public ComposePage(WebDriver driver) {
		super(driver);
		HtmlElementLoader.populatePageObject(this, driver);
	}

	public InboxPage sendLetter(Letter letter) {
		MailService.sendMail(composeForm, letter);
		WebDriverHelper.waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
		setIdLetter(letter);
		return PageFactory.initElements(driver, InboxPage.class);
	}

	public void open() {
		WebDriverHelper.waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR)
				.click();
	}

	public void skipAlertMessage() {
		MailService.skipAlert(driver, POPUP_TABLE_LOCATOR, BUTTON_SAVE_LOCATOR);
	}

	public void setIdLetter(Letter letter) {
		MailService.setIdLetter(driver, NEW_MESSAGE_LOCATOR, letter);
	}

	public ComposePage sendExpectedUnaddressedLetter(Letter letter) {
		MailService.sendMail(composeForm, letter);
		return this;
	}

	public boolean isAlertOnEmptyAddressExist(String errorMessage) {
		return MailService.checkErronOnEmptyAddress(
				UNFILLED_ADDRESS_FIELD_ALERT_LOCATOR_PATTERN, errorMessage);
	}

}
