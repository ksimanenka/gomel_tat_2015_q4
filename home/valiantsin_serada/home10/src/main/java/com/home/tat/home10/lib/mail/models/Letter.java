package com.home.tat.home10.lib.mail.models;

public class Letter {
	private String id;
	private String address;
	private String subject;
	private String body;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Letter(String address, String subject, String body) {
		this.address = address;
		this.subject = subject;
		this.body = body;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getAddress() {
		return address;
	}

	public String getSubject() {
		return subject;
	}

	public String getBody() {
		return body;
	}

}
