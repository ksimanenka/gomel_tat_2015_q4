package com.home.tat.home10.lib.mail.builders;

import static com.home.tat.home10.lib.common.constants.CommonConstants.DEFAULT_MAIL_TO_SEND;

import org.apache.commons.lang3.RandomStringUtils;

import com.home.tat.home10.lib.mail.models.Letter;

public class LetterBuilder {
	public final static int LENTH_OF_WORD = 8;
	public final static String EMPTY_FIELD = "";

	public static Letter getRandomLetter() {
		String mailTo = DEFAULT_MAIL_TO_SEND;
		String mailSubject = RandomStringUtils.randomAlphabetic(LENTH_OF_WORD);
		String mailContent = RandomStringUtils.randomAlphabetic(LENTH_OF_WORD);
		return new Letter(mailTo, mailSubject, mailContent);
	}

	public static Letter getLetterWithoutAddress() {
		String mailTo = EMPTY_FIELD;
		String mailSubject = RandomStringUtils.randomAlphabetic(LENTH_OF_WORD);
		String mailContent = RandomStringUtils.randomAlphabetic(LENTH_OF_WORD);
		return new Letter(mailTo, mailSubject, mailContent);
	}

	public static Letter getLetterWitEmptySubjectAndBody() {
		String mailTo = DEFAULT_MAIL_TO_SEND;
		String mailSubject = EMPTY_FIELD;
		String mailContent = EMPTY_FIELD;
		return new Letter(mailTo, mailSubject, mailContent);
	}

}
