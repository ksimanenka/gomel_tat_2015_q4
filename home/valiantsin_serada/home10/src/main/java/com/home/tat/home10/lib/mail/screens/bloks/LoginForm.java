package com.home.tat.home10.lib.mail.screens.bloks;

import org.openqa.selenium.support.FindBy;

import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;

@FindBy(xpath = "//form[@class='new-auth-form js-new-auth-form']")
public class LoginForm extends HtmlElement {

	public static final String TO_INPUT_XPATH = "//*[@data-params='field=to']//ancestor::tr//input[@type='text']";
	public static final String LOGIN_INPUT_NAME = "login";
	public static final String PASSWORD_INPUT_NAME = "passwd";
	public static final String BUTTON_SUBMIT_XPATH = "//button[@class=' nb-button _nb-action-button nb-group-start']";

	@FindBy(name = LOGIN_INPUT_NAME)
	private TextInput loginInput;

	@FindBy(name = PASSWORD_INPUT_NAME)
	private TextInput passwordInput;

	@FindBy(xpath = BUTTON_SUBMIT_XPATH)
	private Button submitButton;

	public TextInput getLoginInput() {
		return loginInput;
	}

	public TextInput getPasswordInput() {
		return passwordInput;
	}

	public Button getSubmitButton() {
		return submitButton;
	}

}
