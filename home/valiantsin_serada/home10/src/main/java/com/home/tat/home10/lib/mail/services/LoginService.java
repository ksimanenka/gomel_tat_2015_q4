package com.home.tat.home10.lib.mail.services;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.home.tat.home10.lib.common.models.Account;
import com.home.tat.home10.lib.mail.screens.bloks.LoginForm;

public class LoginService {

	public static void loginMailbox(LoginForm loginForm, Account account) {
		loginForm.getLoginInput().sendKeys(account.getLogin());
		loginForm.getPasswordInput().sendKeys(account.getPassword());
		loginForm.getSubmitButton().click();
	}

	public static boolean checkSuccessLogin(WebDriver driver,
			By userNameElementLocator, Account account) {
		return driver.findElement(userNameElementLocator).getText()
				.equals(account.getLogin());
	}

	public static boolean checkErrorOnFailedLogin(WebDriver driver,
			By userNameElementLocator, String message) {
		return driver.findElement(userNameElementLocator).getText()
				.contains(message);
	}

}
