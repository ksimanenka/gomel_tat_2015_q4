package com.home.tat.home10.tests.mail;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.home.tat.home10.lib.common.builders.AccountBuilder;
import com.home.tat.home10.lib.common.models.Account;
import com.home.tat.home10.lib.mail.screens.InboxPage;
import com.home.tat.home10.lib.mail.screens.MailLoginPage;

public class LoginPositiveTest extends BaseTest {

	@Test
	public void loginPositiveTest() {
		MailLoginPage loginPage = new MailLoginPage(driver);
		loginPage.open();
		Account account = AccountBuilder.getDefaultAccount();
		InboxPage inboxPage = loginPage.loginAs(account);
		Assert.assertTrue(inboxPage.isUserNameDisplayed(account));
	}
}
