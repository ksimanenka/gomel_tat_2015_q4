package com.home.tat.home10.tests.mail;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home10.lib.common.constants.CommonConstants;
import com.home.tat.home10.lib.mail.builders.LetterBuilder;
import com.home.tat.home10.lib.mail.models.Letter;
import com.home.tat.home10.lib.mail.screens.ComposePage;

public class SendUnfilledAddressFieldTest extends BaseTest {
	private Letter letter;

	@BeforeClass
	public void prepareData() {
		letter = LetterBuilder.getLetterWithoutAddress();
	}

	@Test
	public void sendUnAddressedMail() {
		login();
		ComposePage composePage = new ComposePage(driver);
		composePage.open();
		composePage.sendExpectedUnaddressedLetter(letter);
		Assert.assertTrue(composePage
				.isAlertOnEmptyAddressExist(CommonConstants.EMPTY_ADDRESS_ERROR_MESSAGE));
	}

}
