package com.home.tat.home8.tests;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.home.tat.home8.pages.LoginPage;

public class LoginNegativeTest extends BaseLoginTest {

	final public String PASSPORT_MAIL_URL = "https://passport.yandex.ru";
	private String userLogin = "tat-test-user@yandex.ru";
	private String wrongUserPassword = "tut-123qwe";

	@Test
	public void loginNegativeTest() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		loginPage.submitLoginExpectingFailure(userLogin, wrongUserPassword);
		String currentUrl = driver.getCurrentUrl();
		Assert.assertTrue((currentUrl.contains(PASSPORT_MAIL_URL)));
	}

}
