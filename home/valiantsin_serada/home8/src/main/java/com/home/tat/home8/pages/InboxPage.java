package com.home.tat.home8.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.home.tat.home8.entities.Letter;
import com.home.tat.home8.utils.WebDriverHelper;

public class InboxPage extends Page {
	public static final By INPOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(@href, '%s')]";

	public InboxPage(WebDriver driver) {
		super(driver);

	}

	public void open() {
		WebDriverHelper.waitForElementIsClickable(INPOX_LINK_LOCATOR).click();
	}

	public boolean isLetterPresent(Letter letter) {
		try {
			WebDriverHelper.waitForAppearence(By.xpath(String.format(
					MAIL_LINK_LOCATOR_PATTERN, letter.getId())));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
