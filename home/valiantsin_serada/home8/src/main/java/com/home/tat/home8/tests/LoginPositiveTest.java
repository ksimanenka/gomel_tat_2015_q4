package com.home.tat.home8.tests;

import org.junit.Assert;
import org.testng.annotations.Test;

public class LoginPositiveTest extends BaseLoginTest {

	public final String YANDEX_MAIL_URL = "https://mail.yandex.by";
	public final String userLogin = "tat-test-user@yandex.ru";
	public final String userPassword = "tat-123qwe";

	@Test
	public void loginPositiveTest() {
		login();
		String currentUrl = driver.getCurrentUrl();
		Assert.assertTrue((currentUrl.contains(YANDEX_MAIL_URL)));
	}

}
