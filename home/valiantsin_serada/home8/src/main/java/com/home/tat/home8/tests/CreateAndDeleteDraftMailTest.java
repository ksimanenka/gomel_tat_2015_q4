package com.home.tat.home8.tests;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home8.entities.Letter;
import com.home.tat.home8.factories.LetterFactory;
import com.home.tat.home8.pages.ComposePage;
import com.home.tat.home8.pages.DeletedLetterPage;
import com.home.tat.home8.pages.DraftboxPage;
import com.home.tat.home8.pages.TrashboxPage;

public class CreateAndDeleteDraftMailTest extends BaseLoginTest {

	private Letter letter;

	@BeforeClass
	public void prepareData() {
		letter = LetterFactory.getRandomLetter();
	}

	@Test
	public void createAndDeleteDraftMail() {
		login();
		ComposePage composePage = new ComposePage(driver);
		composePage.open();
		DraftboxPage draftboxPage = composePage.createDraft(letter);
		composePage = draftboxPage.findAndShowCreatedDraft(letter);
		draftboxPage = composePage.deleteDraft();
		TrashboxPage trashboxPage = draftboxPage
				.switchToTrashboxAfterDeleting(letter);
		DeletedLetterPage deletedLetterPage = trashboxPage
				.findAndShowCreatedDraft(letter);
		trashboxPage = deletedLetterPage.deleteLetter();
		Assert.assertFalse(trashboxPage.isLetterPresent(letter));
	}

}
