package com.home.tat.home8.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home8.entities.Letter;
import com.home.tat.home8.factories.LetterFactory;
import com.home.tat.home8.pages.ComposePage;
import com.home.tat.home8.pages.InboxPage;

public class SendUnfilledSubjectAndBodyFieldsMailTest extends BaseLoginTest {
	private final String EMPTY_FIELD = " ";
	private Letter letter;

	@BeforeClass
	public void prepareData() {
		letter = LetterFactory.getRandomLetter();
	}

	@Test
	public void sendMail() {
		login();
		letter.setBody(EMPTY_FIELD);
		letter.setSubject(EMPTY_FIELD);
		ComposePage composePage = new ComposePage(driver);
		composePage.open();
		composePage.sendLetter(letter);
		InboxPage inboxPage = new InboxPage(driver);
		inboxPage.open();
		Assert.assertTrue(inboxPage.isLetterPresent(letter));
	}

}
