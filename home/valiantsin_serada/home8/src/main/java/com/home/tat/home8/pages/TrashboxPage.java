package com.home.tat.home8.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.home.tat.home8.entities.Letter;
import com.home.tat.home8.utils.WebDriverHelper;

public class TrashboxPage extends Page {
	public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
	public static final By TRASHBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#trash']");
	public static final By DELETED_LETTER_MESSAGE_LOCATOR = By
			.xpath("//a[@class='b-statusline__link']");
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
	public static final String HREF_ATRIBUTE = "href";

	public TrashboxPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		WebDriverHelper.waitForElementIsClickable(TRASHBOX_LINK_LOCATOR)
				.click();
	}

	public DeletedLetterPage findAndShowCreatedDraft(Letter letter) {
		WebDriverHelper.waitForElementIsClickable(By.xpath(String.format(
				MAIL_LINK_LOCATOR_PATTERN, letter.getBody())));
		WebElement draftboxMailLink = driver.findElement(By.xpath(String
				.format(MAIL_LINK_LOCATOR_PATTERN, letter.getBody())));
		driver.get(draftboxMailLink.getAttribute(HREF_ATRIBUTE));
		return new DeletedLetterPage(driver);
	}

	public boolean isLetterPresent(Letter letter) {
		try {
			WebDriverHelper.waitForAppearence(By.xpath(String.format(
					MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean isDeletedLetterAlertExist() {
		try {
			WebDriverHelper.waitForDisappear(DELETED_LETTER_MESSAGE_LOCATOR);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
