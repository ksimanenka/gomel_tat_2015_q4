package com.home.tat.home8.pages;

import org.openqa.selenium.WebDriver;

public class PassportPage extends Page {
	public static final String PASSPORTPAGE_BASE_URL = "https://passport.yandex.ru";

	public PassportPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		driver.get(PASSPORTPAGE_BASE_URL);
	}
}
