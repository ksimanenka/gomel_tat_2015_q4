package com.home.tat.home8.tests;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.home.tat.home8.pages.LoginPage;
import com.home.tat.home8.utils.WebDriverHelper;

public class BaseLoginTest {

	protected WebDriver driver;

	public final String YANDEX_MAIL_URL = "https://mail.yandex.by";
	public final String userLogin = "tat-test-user@yandex.ru";
	public final String userPassword = "tat-123qwe";

	@Parameters("browser")
	@BeforeClass
	public void prepareBrowser(String browser) throws MalformedURLException {
		driver = WebDriverHelper.getWebDriver(browser);
	}

	public void login() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		loginPage.loginAs(userLogin, userPassword);
	}

	@AfterClass
	public void closeBrowser() {
		WebDriverHelper.shutdownWebDriver();
	}

}
