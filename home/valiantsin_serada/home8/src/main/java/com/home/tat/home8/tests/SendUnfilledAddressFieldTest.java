package com.home.tat.home8.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home8.entities.Letter;
import com.home.tat.home8.factories.LetterFactory;
import com.home.tat.home8.pages.ComposePage;

public class SendUnfilledAddressFieldTest extends BaseLoginTest {

	private Letter letter;

	@BeforeClass
	public void prepareData() {
		letter = LetterFactory.getRandomLetter();
	}

	@Test
	public void sendUnAddressedMail() {
		login();
		ComposePage composePage = new ComposePage(driver);
		composePage.open();
		composePage = composePage.sendUnAddressedLetter(letter);
		Assert.assertTrue(composePage.isUnfilledAddressFieldAlertExist());
	}

}
