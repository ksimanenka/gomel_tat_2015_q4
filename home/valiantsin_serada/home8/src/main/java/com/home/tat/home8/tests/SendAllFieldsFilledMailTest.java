package com.home.tat.home8.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home8.entities.Letter;
import com.home.tat.home8.factories.LetterFactory;
import com.home.tat.home8.pages.ComposePage;
import com.home.tat.home8.pages.InboxPage;

public class SendAllFieldsFilledMailTest extends BaseLoginTest {
	private Letter letter;

	@BeforeClass
	public void prepareData() {
		letter = LetterFactory.getRandomLetter();
	}

	@Test
	public void sendMail() {
		login();
		ComposePage composePage = new ComposePage(driver);
		composePage.open();
		InboxPage inboxPage = composePage.sendLetter(letter);
		inboxPage.open();
		Assert.assertTrue(inboxPage.isLetterPresent(letter));
	}

}
