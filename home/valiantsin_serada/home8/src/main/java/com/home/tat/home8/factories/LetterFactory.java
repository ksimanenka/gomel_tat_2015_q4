package com.home.tat.home8.factories;

import com.home.tat.home8.entities.Letter;

public class LetterFactory {
	final static String DEFAULT_ADDRESS = "tat-test-user@yandex.ru";
	final static String DEFAULT_SUBJECT = "test subject";
	final static String DEFAULT_MAIL_CONTENT = "mail content";
	final static int RANDOM_MULTIPLIER = 100000;

	public static Letter getRandomLetter() {
		String mailTo = DEFAULT_ADDRESS;
		String mailSubject = DEFAULT_SUBJECT + Math.random()
				* RANDOM_MULTIPLIER;
		String mailContent = DEFAULT_MAIL_CONTENT + Math.random()
				* RANDOM_MULTIPLIER;
		return new Letter(mailTo, mailSubject, mailContent);
	}
}
