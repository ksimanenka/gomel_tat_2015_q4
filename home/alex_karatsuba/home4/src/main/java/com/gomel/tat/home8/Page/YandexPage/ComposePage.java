package com.gomel.tat.home8.Page.YandexPage;


import com.gomel.tat.home8.Letter.Letter;
import com.gomel.tat.home8.Page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static com.gomel.tat.home8.Utilities.WebDriverHelper.waitForDisappear;

public class ComposePage extends Page {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//span[@class='b-toolbar__item__label js-toolbar-item-title-compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.id("compose-subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
    }

    public void sendLetter(Letter letter) {
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

}
