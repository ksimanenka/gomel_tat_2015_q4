package com.gomel.tat.home8.Page.YandexPage;

import com.gomel.tat.home8.Page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends Page{

    public static final String URL = "https://ya.ru/";
    public static final By ENTER_BUTTON_LOCATOR= By.className("b-inline");
    public static final String USER_LOGIN = "mr.einshtein2016@yandex.ru";
    public static final String USER_PASSWD = "mreintat2015";
    public static final String USER_INCORRECT_LOGIN = "notamr.einshtein2016@yandex.ru";
    public static final String USER_INCORRECT_PASSWD = "notamreintat2015";
    public static final By LOGIN_INPUT = By.xpath("//*[@class='_nb-input-controller' and @name='login']");
    public static final By PASSWD_INPUT = By.xpath("//*[@class='_nb-input-controller' and @name='passwd']");



    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
    }

    public InboxPage login(String userLogin, String userPassword) {
        WebElement loginInput = driver.findElement(LOGIN_INPUT);
        loginInput.sendKeys(USER_LOGIN);
        WebElement passwdInput = driver.findElement(PASSWD_INPUT);
        passwdInput.sendKeys(USER_PASSWD);
        passwdInput.submit();
        return new InboxPage(driver);
    }

    public InboxPage negativelogin(String userIncorrectLogin, String userIncorrectPassword) {
        WebElement loginInput = driver.findElement(LOGIN_INPUT);
        loginInput.sendKeys(USER_INCORRECT_LOGIN);
        WebElement passwdInput = driver.findElement(PASSWD_INPUT);
        passwdInput.sendKeys(USER_INCORRECT_PASSWD);
        passwdInput.submit();
        return new InboxPage(driver);
    }
}
