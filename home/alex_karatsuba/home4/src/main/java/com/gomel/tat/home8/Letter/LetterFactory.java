package com.gomel.tat.home8.Letter;

public class LetterFactory {

    public static Letter getRandomLetter() {
        String mailTo = "mr.einshtein2016@yandex.ru";
        String mailSubject = "test subject" + Math.random() * 100000000;
        String mailContent = "mail content" + Math.random() * 100000000;
        return new Letter(mailTo, mailSubject, mailContent);
    }
}
