package com.gomel.tat.home8.Page.YandexPage;


import com.gomel.tat.home8.Page.Page;
import com.gomel.tat.home8.Letter.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;
import static com.gomel.tat.home8.Utilities.WebDriverHelper.waitForElementIsClickable;

public class InboxPage extends Page{

    public static final By INCOMING = By.xpath("//a[contains(@title,'Ctrl + i')]");
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(INCOMING).click();
    }

    public boolean isLetterPresent(Letter letter) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
