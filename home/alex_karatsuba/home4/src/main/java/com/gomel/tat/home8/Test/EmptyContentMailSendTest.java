package com.gomel.tat.home8.Test;


import com.gomel.tat.home8.Letter.Letter;
import com.gomel.tat.home8.Page.YandexPage.ComposePage;
import com.gomel.tat.home8.Page.YandexPage.InboxPage;
import com.gomel.tat.home8.Page.YandexPage.LoginPage;
import com.gomel.tat.home8.Utilities.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EmptyContentMailSendTest {
    protected WebDriver driver;

    private String USER_LOGIN = "mr.einshtein2016@yandex.ru";
    private String USER_PASSWD = "mreintat2015";
    String ENTER_ADRESS = "mr.einshtein2016@yandex.ru";
    String ENTER_SUBJECT = "";
    String ENTER_CONTENT = "";

    @BeforeClass
    public void prepareBrowser() {
        driver = WebDriverHelper.getWebDriver();
    }

    @Test
    public void sendEmptyContentMail() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(USER_LOGIN, USER_PASSWD);
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        Letter letter = new Letter(ENTER_ADRESS, ENTER_SUBJECT, ENTER_CONTENT);
        composePage.sendLetter(letter);
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresent(letter), "Letter should be present in inbox folder");
    }
}
