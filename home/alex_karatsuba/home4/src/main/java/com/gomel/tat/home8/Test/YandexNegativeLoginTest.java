package com.gomel.tat.home8.Test;

import com.gomel.tat.home8.Page.YandexPage.LoginPage;
import com.gomel.tat.home8.Utilities.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class YandexNegativeLoginTest {

    protected WebDriver driver;

    private String USER_INCORRECT_LOGIN = "notamr.einshtein2016@yandex.ru";
    private String USER_INCORRECT_PASSWD = "notamreintat2015";

    @BeforeClass
    public void prepareBrowser() {
        driver = WebDriverHelper.getWebDriver();
    }

    @Test
    public void negativelogin() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.negativelogin(USER_INCORRECT_LOGIN, USER_INCORRECT_PASSWD);
        driver.findElement(By.className("error-msg"));
    }

}
