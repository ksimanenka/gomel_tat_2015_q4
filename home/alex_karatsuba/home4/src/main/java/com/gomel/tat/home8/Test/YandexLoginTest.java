package com.gomel.tat.home8.Test;

import com.gomel.tat.home8.Page.YandexPage.LoginPage;
import com.gomel.tat.home8.Utilities.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Администратор on 07.01.2016.
 */
public class YandexLoginTest {


        protected WebDriver driver;

        private String USER_LOGIN = "mr.einshtein2016@yandex.ru";
        private String USER_PASSWD = "mreintat2015";

        @BeforeClass
        public void prepareBrowser() {
            driver = WebDriverHelper.getWebDriver();
        }

        @Test
        public void login() {
            LoginPage loginPage = new LoginPage(driver);
            loginPage.open();
            loginPage.login(USER_LOGIN, USER_PASSWD);
        }

}
