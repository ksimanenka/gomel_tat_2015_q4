package com.gomel.tat.home7;


import org.apache.tools.ant.taskdefs.WaitFor;
import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class YandexMailTest {


    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;
    public static final String URL = "https://ya.ru/";
    public static final By MAIL_LINK = By.className("b-inline");
    public static final By LOGIN_INPUT = By.xpath("//*[@class='_nb-input-controller' and @name='login']");
    public static final String USER_LOGIN = "mr.einshtein2016@yandex.ru";
    public static final String USER_NLOGIN = "martvothrydgkaivh@yandex.ru";
    public static final By PASSWD_INPUT = By.xpath("//*[@class='_nb-input-controller' and @name='passwd']");
    public static final String USER_PASSWD = "mreintat2015";
    public static final String USER_NPASSWD = "hwioycjlakuih";
    public static final By WRITE_NEW_BUTTON = By.xpath("//span[@class='b-toolbar__item__label js-toolbar-item-title-compose']");
    public static final By ENTER_ADDRESS = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final String MAIL_ADDRESS = "mr.einshtein2016@yandex.ru";
    public static final By ENTER_SUBJECT = By.id("compose-subj");
    public static final String SOME_SUBJECT = "test subject";
    public static final By ENTER_TEXT = By.id("compose-send");
    public static final String SOME_TEXT = "test text";
    public static final By SEND_BUTTON = By.id("nb-6");
    public static final By INCOMING = By.xpath("//a[contains(@title,'Ctrl + i')]");
    public static final By OUTCOMING = By.xpath("//*[@href='#sent']");
    public static final By TRASH = By.xpath("//*[@href='#draft']");
    public static final By LETTER_FOR_KILLING = By.xpath("//*[@class='b-messages__from__text' and @type='checkbox']");
    //public static final By ENTER_SEND_AND_COME = By.className("b-inline");
    //public static final By DELETE_MAIL = By.className("b-toolbar__item b-toolbar__item_delete js-toolbar-item-delete daria-action");




    private WebDriver driver;

    @BeforeClass(description = "Prepare Firefox browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    /*@BeforeClass(description = "Prepare Chrome browser")
    public void prepareBrowser() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }*/

       @Test(description = "Yandex positive login test")
        public void login(){
           driver.get(URL);
           WebElement mailLink = driver.findElement(MAIL_LINK);
           mailLink.click();
           WebElement loginInput = driver.findElement(LOGIN_INPUT);
           loginInput.sendKeys(USER_LOGIN);
           WebElement passwdInput = driver.findElement(PASSWD_INPUT);
           passwdInput.sendKeys(USER_PASSWD);
           passwdInput.submit();
           driver.quit();
       }

        @Test(description = "Yandex negative login test")
        public void nlogin(){
            driver.get(URL);
            WebElement mailLink = driver.findElement(MAIL_LINK);
            mailLink.click();
            WebElement loginInput = driver.findElement(LOGIN_INPUT);
            loginInput.sendKeys(USER_NLOGIN);
            WebElement passwdInput = driver.findElement(PASSWD_INPUT);
            passwdInput.sendKeys(USER_NPASSWD);
            passwdInput.submit();
            driver.findElement(By.className("error-msg"));
            driver.quit();
        }

        @Test(description = "Yandex send mail")
        public void sendMail(){
            driver.get(URL);
            WebElement mailLink = driver.findElement(MAIL_LINK);
            mailLink.click();
            WebElement loginInput = driver.findElement(LOGIN_INPUT);
            loginInput.sendKeys(USER_LOGIN);
            WebElement passwdInput = driver.findElement(PASSWD_INPUT);
            passwdInput.sendKeys(USER_PASSWD);
            passwdInput.submit();
            WebElement writeNewButton = driver.findElement(WRITE_NEW_BUTTON);
            writeNewButton.click();
            WebElement enterAddress = driver.findElement(ENTER_ADDRESS);
            enterAddress.sendKeys(MAIL_ADDRESS);
            WebElement enterSubject = driver.findElement(ENTER_SUBJECT);
            enterSubject.sendKeys(SOME_SUBJECT);
            WebElement enterText = driver.findElement(ENTER_TEXT);
            enterText.sendKeys(SOME_TEXT);
            enterText.submit();
            WebElement incoming = driver.findElement(INCOMING);
            incoming.click();
            driver.findElement(By.className("b-messages__from__text"));
            WebElement outcoming = driver.findElement(OUTCOMING);
            outcoming.click();
            driver.findElement(By.className("b-messages__from__text"));
            driver.quit();
        }

        @Test(description = "Send mail with empty fields")
        public void sendIncorrectMail(){
            driver.get(URL);
            WebElement mailLink = driver.findElement(MAIL_LINK);
            mailLink.click();
            WebElement loginInput = driver.findElement(LOGIN_INPUT);
            loginInput.sendKeys(USER_LOGIN);
            WebElement passwdInput = driver.findElement(PASSWD_INPUT);
            passwdInput.sendKeys(USER_PASSWD);
            passwdInput.submit();
            WebElement writeNewButton = driver.findElement(WRITE_NEW_BUTTON);
            writeNewButton.click();
            WebElement sendButton = driver.findElement(SEND_BUTTON);
            sendButton.click();
            driver.findElement(By.className("b-notification__i"));
            driver.quit();
        }

        @Test(description = "Send mail with only address")
        public void sendNotFullMail(){
            driver.get(URL);
            WebElement mailLink = driver.findElement(MAIL_LINK);
            mailLink.click();
            WebElement loginInput = driver.findElement(LOGIN_INPUT);
            loginInput.sendKeys(USER_LOGIN);
            WebElement passwdInput = driver.findElement(PASSWD_INPUT);
            passwdInput.sendKeys(USER_PASSWD);
            passwdInput.submit();
            WebElement writeNewButton = driver.findElement(WRITE_NEW_BUTTON);
            writeNewButton.click();
            WebElement enterAddress = driver.findElement(ENTER_ADDRESS);
            enterAddress.sendKeys(MAIL_ADDRESS);
            WebElement sendButton = driver.findElement(SEND_BUTTON);
            sendButton.click();
            WebElement incoming = driver.findElement(INCOMING);
            incoming.click();
            driver.findElement(By.className("b-messages__from__text"));
            WebElement outcoming = driver.findElement(OUTCOMING);
            outcoming.click();
            driver.findElement(By.className("b-messages__from__text"));
            driver.quit();
        }

        @Test(description = "Creat and delete draft mail")
        public void draftMail() {
            driver.get(URL);
            WebElement mailLink = driver.findElement(MAIL_LINK);
            mailLink.click();
            WebElement loginInput = driver.findElement(LOGIN_INPUT);
            loginInput.sendKeys(USER_LOGIN);
            WebElement passwdInput = driver.findElement(PASSWD_INPUT);
            passwdInput.sendKeys(USER_PASSWD);
            passwdInput.submit();
            WebElement writeNewButton = driver.findElement(WRITE_NEW_BUTTON);
            writeNewButton.click();
            WebElement enterAddress = driver.findElement(ENTER_ADDRESS);
            enterAddress.sendKeys(MAIL_ADDRESS);
            WebElement enterSubject = driver.findElement(ENTER_SUBJECT);
            enterSubject.sendKeys(SOME_SUBJECT);
            WebElement enterText = driver.findElement(ENTER_TEXT);
            enterText.sendKeys(SOME_TEXT);
            //WebElement trash = driver.findElement(TRASH);
            //trash.click();
            //тут у нас косяк - если мы заполняем поля и дальше лезем по тесту в trash то у нас вылазит табличка "хотите сохранить и перейти?"
            //а если мы по тесту идем с готовым решением по этой табличке то она ,censored, не выскакивает(((
            //WebElement enterSaveAndCome = driver.findElement(ENTER_SEND_AND_COME);
            //enterSaveAndCome.click();
            //WebElement letterForKilling = driver.findElement(LETTER_FOR_KILLING);
            //letterForKilling.click();
            //WebElement delete = driver.findElement(DELETE_MAIL);
            //delete/click();
            //а дальше заходим в удаленные и удаляем его вроде ничего сложного но как убрать "косяк" не пойму никак



        }


}
