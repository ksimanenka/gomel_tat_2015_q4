package com.gomel.tat.home8.Page;


import org.openqa.selenium.WebDriver;

    public abstract class Page {

        protected WebDriver driver;

        public Page(WebDriver driver) {
            this.driver = driver;
        }
    }
