package com.gomel.tat.home5;

import org.testng.TestNG;
import org.testng.collections.Lists;
import org.testng.xml.XmlSuite;

import java.util.List;


public class TestRunner {
    public static void main(String[] args) {
        TestNG testng = new TestNG();
        testng.addListener(new WordListTestListener());
        List<String> suites = Lists.newArrayList();
        suites.add("src\\test\\resources\\suite.xml");
        testng.setTestSuites(suites);
        testng.run();
    }
}
