package com.gomel.tat.home5;

import com.gomel.tat.home4.WordList;
import org.testng.annotations.*;
import static org.testng.Assert.*;

public class WordListTest {
    private WordList wordList;

    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        wordList = new WordList();
    }

    @Test(expectedExceptions = IllegalArgumentException.class,dataProvider = "dProvider")
    public void checkIncorrectDirectory(String directory,
                                       String fileNamePattern) {
        wordList.getWords(directory, fileNamePattern);
     }

    @DataProvider(name = "dProvider")
    public Object[][] pathsProvider() {
        return new Object[][] {
                new Object[] { "D:\\nonexistedfolder",
                        ".+\\.txt" },
                new Object[] {"D:\\hello.txt",
                        ".+\\.txt" }, };
    }


    @Test
    public void testPrintStatistics() {
        wordList.getWords("d:\\home\\test_folder", null);
        assertEquals(wordList.getStatistics(), "apple : 3\n" +"melom : 1\n" + "melon : 4\n");
    }
}