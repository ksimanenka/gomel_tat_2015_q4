package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import java.net.MalformedURLException;


public class LoginTest extends PrepareBrowser {

    @Test
    public void login() throws MalformedURLException {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

    }
}