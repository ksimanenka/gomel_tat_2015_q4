package com.gomel.tat.home7;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import java.net.MalformedURLException;

public class EmptySubjectAndBodyFieldsTest extends LoginTest {
    @Test
    public void emptySubjectAndBodyFields() throws MalformedURLException {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }
}
