package com.gomel.tat.home7.tests;

import com.gomel.tat.home7.essence.Letter;
import com.gomel.tat.home7.pages.ComposePage;
import org.testng.annotations.Test;

public class DeleteDraftMailTest extends LoginTest {

    String mailTo = "test-USER-trololo@yandex.ru";
    String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @Test
    public void createDraft() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        Letter letter = new Letter(mailTo, mailSubject, mailContent);
        composePage.sendLetter(letter);

    }
}
