package com.gomel.tat.home7.tests;

import com.gomel.tat.home7.pages.LoginPage;
import com.gomel.tat.home7.util.PrepareBrowser;
import com.gomel.tat.home7.util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTest extends PrepareBrowser{

    protected WebDriver driver;

    private String userLogin = "test-USER-trololo"; // ACCOUNT
    private String userPassword = "hopHEYlalaley2016"; // ACCOUNT

        @BeforeClass
        public void prepareBrowser() {
            driver = WebDriverHelper.getWebDriver();
        }

        @Test
        public void login() {
            LoginPage loginPage = new LoginPage(driver);
            loginPage.login(userLogin, userPassword);

            Assert.assertTrue(driver.findElement(COMPOSE_BUTTON_LOCATOR).isDisplayed());
            System.out.println("Login successful");
        }

}
