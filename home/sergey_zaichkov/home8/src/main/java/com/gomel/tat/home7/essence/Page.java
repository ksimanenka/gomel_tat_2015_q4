package com.gomel.tat.home7.essence;

import org.openqa.selenium.WebDriver;


public abstract class Page {

    protected WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }
}