package com.gomel.tat.home7.pages;

import com.gomel.tat.home7.essence.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends Page{

    public static final String BASE_URL = "mail.yandex.by";
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public InboxPage login(String userLogin, String userPassword) {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        return new InboxPage(driver);
    }
}
