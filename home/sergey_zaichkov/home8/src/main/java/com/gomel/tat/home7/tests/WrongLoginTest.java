package com.gomel.tat.home7.tests;


import com.gomel.tat.home7.pages.LoginPage;
import com.gomel.tat.home7.util.PrepareBrowser;
import com.gomel.tat.home7.util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.net.MalformedURLException;

public class WrongLoginTest extends PrepareBrowser {

    protected WebDriver driver;

    private String userLogin = "test-USER-trololo";
    private String userPassword = "hopHEYlalaley2016";
    private String wrongUserLogin = "qwerty";
    private String wrongUserPassword = "qwerty123";

    @BeforeClass
    public void prepareBrowser() {
        driver = WebDriverHelper.getWebDriver();
    }

    @Test
    public void falseLoginAndPass() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(wrongUserLogin, wrongUserPassword);
        WebElement error = driver.findElement(By.className("error-msg"));
        Assert.assertTrue(error.isDisplayed(), "False Login or Pass");
    }

    @Test
    public void faleLoginAndTruePass() throws MalformedURLException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(wrongUserLogin, userPassword);
        WebElement error = driver.findElement(By.className("error-msg"));
        Assert.assertTrue(error.isDisplayed(), "False Login or Pass");

    }
    @Test
     public void TrueLoginAndFalsePass() throws MalformedURLException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(userLogin, wrongUserPassword);
        WebElement error = driver.findElement(By.className("error-msg"));
        Assert.assertTrue(error.isDisplayed(), "False Login or Pass");

    }

    @Test
    public void EmptyLoginAndPass() throws MalformedURLException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("", "");
        WebElement error = driver.findElement(By.className("error-msg"));
        Assert.assertTrue(error.isDisplayed(), "False Login or Pass");

    }

}
