package com.gomel.tat.home7.pages;

import com.gomel.tat.home7.essence.Letter;
import com.gomel.tat.home7.essence.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class DraftPage extends Page {


    public static final By DRAFT_SAVE_LOCATOR = By.xpath("//button[@data-action='dialog.save']");
    public  static final By DRAFT_BUTTON_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebElement draftButton = driver.findElement(DRAFT_BUTTON_LOCATOR);
        draftButton.click();
    }


    public boolean isLetterPresent(Letter letter) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void deleteFromDraft(Letter letter) {

    }

}
