package com.gomel.tat.home7.tests;

import com.gomel.tat.home7.essence.Letter;
import com.gomel.tat.home7.pages.ComposePage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class WrongAddressFieldTest extends LoginTest {

    String mailTo = "";
    String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @Test
    public void wrongAddressField() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        Letter letter = new Letter(mailTo, mailSubject, mailContent);
        composePage.sendLetter(letter);

        Assert.assertFalse(driver.findElement(COMPOSE_BUTTON_LOCATOR).isDisplayed());
        System.out.println("Message was not sent! Wrong email address");


    }

}
