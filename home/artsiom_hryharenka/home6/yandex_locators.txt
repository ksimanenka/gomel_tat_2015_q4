﻿mainInputSearchLocator //input[@id='lst-ib']     input#lst-ib
searchButtonLocator    //input[@name='btnK']     input[name=btnK]
imLuckyButtonLocator   //input[@name='btnI']     input[name=btnI]
allTenSerchResultLocator   //*[@id='imagebox_bigimages']/div/a|//h3/a     ._Icb._kk._wI>a, .r>a
oLetterLocator   (//table[@id='nav']/tbody/tr/td)[6]  table#nav tbody tr td:nth-child(6)

loginInputLocator     //input[@name='login']    input[name=login]
passwordInputLocator  //input[@name='passwd']   input[name=passwd]
enterButtonLocator    //span/button[@type='submit']   span button[type=submit] 

inboxLinkLocator //div[@class='block-left-box']//a[@href='#inbox']       .block-left-box a[href='#inbox']
sentLinkLocator  //div[@class='block-left-box']//a[@href='#sent']        .block-left-box a[href='#sent']
deletedLinkLocator  //div[@class='block-left-box']//a[@href='#trash']    .block-left-box a[href='#trash']
spamLinkLocator  //div[@class='block-left-box']//a[@href='#spam']        .block-left-box a[href='#spam']
draftLinkLocator  //div[@class='block-left-box']//a[@href='#draft']      .block-left-box a[href='#draft']

composeLinkLocator //div[@class='block-toolbar']//a[@href='#compose']    div.block-toolbar a[href='#compose']
refreshLinkLocator //div[@class='block-toolbar']//a[@data-action='mailbox.check']  div.block-toolbar a[data-action='mailbox.check']
forwardLinkLocator //div[@class='block-toolbar']//a[@data-action='forward']   div.block-toolbar a[data-action='forward']
deleteLinkLocator //div[@class='block-toolbar']//a[@data-action='delete']   div.block-toolbar a[data-action='delete']
markAsSpamLinkLocator //div[@class='block-toolbar']//a[@data-action='tospam']   div.block-toolbar a[data-action='tospam']
markNotSpamLinkLocator //div[@class='block-toolbar']//a[@data-action='notspam']   div.block-toolbar a[data-action='notspam']
markAsReadLinkLocator //div[@class='block-toolbar']//a[@data-action='mark']   div.block-toolbar a[data-action='mark']
moveToFolderLinkLocator (//div[@class='block-toolbar']//a//span[@class='b-toolbar__item__label'])[2]  .b-toolbar__item.daria-action.js-item-move.js-kbd-folders

uploadButtonLocator  //div[@class='header']//span[text()='Загрузить'] 
downloadButtonLocator  //div[@class='nb-panel__footer']//button[@data-click-action='resource.download']  button[data-click-action='resource.download']
deleteButtonLocator  //div[@class='nb-panel__footer']//button[@data-click-action='resource.delete']   button[data-click-action='resource.delete']
restoreButtonLocator  //button[@data-click-action='resource.restore']   button[data-click-action='resource.restore']
