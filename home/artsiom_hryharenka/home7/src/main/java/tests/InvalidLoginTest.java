package tests;

import framework.YandexMail;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class InvalidLoginTest extends BaseTest{

    @Test
    public void wrongLogin() {
        driver.get(YandexMail.YANDEX_START_PAGE);
        driver.findElement(YandexMail.INPUT_LOGIN_LOCATOR).sendKeys("USER_NAME");
        driver.findElement(YandexMail.INPUT_PASSWORD_LOCATOR).sendKeys("PASSWORD");
        driver.findElement(YandexMail.BUTTON_SUBMIT_LOCATOR).click();
        WebElement errorMessage = driver.findElement(YandexMail.ERROR_LOGIN_MESSAGE_LOCATOR);
        Assert.assertTrue(errorMessage.isDisplayed());
    }
}
