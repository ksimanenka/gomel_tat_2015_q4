package tests;

import framework.YandexMail;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest{

    @Test
    public void correctLogin(){
        driver.get(YandexMail.YANDEX_START_PAGE);
        driver.findElement(YandexMail.INPUT_LOGIN_LOCATOR).sendKeys(YandexMail.USER_NAME);
        driver.findElement(YandexMail.INPUT_PASSWORD_LOCATOR).sendKeys(YandexMail.PASSWORD);
        driver.findElement(YandexMail.BUTTON_SUBMIT_LOCATOR).click();
        WebElement headerUserName = driver.findElement(YandexMail.HEADER_USER_NAME_LOCATOR);
        Assert.assertEquals(headerUserName.getText(), YandexMail.FULL_USER_NAME);
    }

}
