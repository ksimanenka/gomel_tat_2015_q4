package tests;

import framework.YandexMail;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendEmptyMailTest extends LoginTest {

    @Test
    public void sendEmptyMessage(){
        waitForElementPresent(YandexMail.WRITE_MESSAGE_BUTTON_LOCATOR);
        driver.findElement(YandexMail.WRITE_MESSAGE_BUTTON_LOCATOR).click();
        driver.findElement(YandexMail.RECIPIENT_LOCATOR).sendKeys(YandexMail.FULL_USER_NAME);
        driver.findElement(YandexMail.SEND_MAIL_BUTTON_LOCATOR).click();

        String mailPattern = "//*[@value='" + getIdMail() + "']";
        driver.findElement(YandexMail.INBOX_LOCATOR).click();
        boolean isMailInInbox = driver.findElement(By.xpath(mailPattern)).isEnabled();
        String sentId = getIdMail();
        sentId.substring(0, sentId.length() - 2);

        mailPattern = "//*[contains(@value,'" + sentId + "')]";
        driver.findElement(YandexMail.SENT_LOCATOR).click();
        boolean isMailInSent = driver.findElement(By.xpath(mailPattern)).isDisplayed();

        Assert.assertTrue(isMailInInbox && isMailInSent);
    }

    private static String getIdMail(){
        String href = driver.findElement(YandexMail.GET_ID_MAIL_LOCATOR).getAttribute("href");
        String[] items = href.split("/");
        return items[4];
    }
}
