package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.LoginPage;
import pages.Page;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected static WebDriver driver;
    public static LoginPage loginPage;

    @BeforeClass(description = "Firefox remote launch")
    public void setUp() throws MalformedURLException{
        if (driver == null) {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
            driver.manage().window().maximize();
            //driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
            loginPage = new LoginPage(driver);
            loginPage.open();
        }

    }

//    @BeforeClass(description = "Chrome remote launch")
//    public void chromeRemoteLaunch() throws MalformedURLException {
//        if (driver == null) {
//            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
//            driver.manage().window().maximize();
//            driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
//            loginPage = new LoginPage(driver);
//            loginPage.open();
//        }
//    }

    @AfterClass
    public void tearDown() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            driver = null;
        }
    }

}
