package tests;

import pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest{

    protected MainPage mainPage;

    @Test
    public void correctLogin(){
        mainPage = loginPage.login();
        Assert.assertEquals(mainPage.getHeaderUserName(), MainPage.FULL_USER_NAME);
    }

}
