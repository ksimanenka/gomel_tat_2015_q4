package tests;

import org.testng.Assert;
import org.testng.annotations.Test;


public class SendLetterWithoutRecipientTest extends LoginTest {

    @Test
    public void sendLetterWithoutRecipient(){
        Assert.assertTrue(mainPage.sendLetterWithoutRecipient());
    }
}
