package pages;


import org.openqa.selenium.By;

public class YandexMail {


    //-----------yandex mail page----------------
    public static final By HEADER_USER_NAME_LOCATOR = By.xpath("//*[@id='nb-1']/span[1]");
    public static final By WRITE_MESSAGE_BUTTON_LOCATOR = By.xpath("//*[contains(@class, 'b-toolbar__item__label js-toolbar-item-title-compose')]");
    public static final By DELETE_MESSAGE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//a[@data-action='delete']");
    //--------------send message page-------------------------
    public static final By RECIPIENT_LOCATOR = By.xpath("//tr[@class='b-compose-head__field b-compose-head__field_to js-compose-field-wrapper__to']//input[1]");
    public static final By SUBJECT_LOCATOR = By.name("subj");
    public static final By BODY_MAIL_LOCATOR = By.xpath("//textarea[@id='compose-send']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    //--------------left menu------------------------------
    public static final By INBOX_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#inbox']");
    public static final By SENT_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#sent']");
    public static final By DRAFT_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#draft']");
    public static final By TRASH_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#trash']");

    public static final By GET_ID_MAIL_LOCATOR = By.className("b-statusline__link");

    public static final By POPUP_LOCATOR = By.xpath("//*[@class='b-popup']//*[@class='b-popup__box']");
    public static final By POPUP_BUTTON_LOCATOR = By.xpath("//button[@data-action='dialog.save']");

}
