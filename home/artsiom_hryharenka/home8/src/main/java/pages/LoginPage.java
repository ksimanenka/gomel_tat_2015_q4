package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {

    public static final String YANDEX_START_PAGE = "http://mail.yandex.by";
    public static final String USER_NAME = "artsiom.hryharenka";
    public static final String PASSWORD = "3985209";

    public static final By INPUT_LOGIN_LOCATOR = By.xpath("//input[@name='login']");
    public static final By INPUT_PASSWORD_LOCATOR = By.xpath("//input[@name='passwd']");
    public static final By BUTTON_SUBMIT_LOCATOR = By.xpath("//span/button[@type='submit']");

    public static final By ERROR_LOGIN_MESSAGE_LOCATOR = By.className("error-msg");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open(){
        driver.get(YANDEX_START_PAGE);
    }

    public MainPage login(){
        waitForElementIsClickable(INPUT_LOGIN_LOCATOR);
        driver.findElement(INPUT_LOGIN_LOCATOR).sendKeys(USER_NAME);
        driver.findElement(INPUT_PASSWORD_LOCATOR).sendKeys(PASSWORD);
        driver.findElement(BUTTON_SUBMIT_LOCATOR).click();
        return new MainPage(driver);
    }

    public boolean isErrorDisplayed(){
        driver.findElement(INPUT_LOGIN_LOCATOR).sendKeys(PASSWORD);
        driver.findElement(INPUT_PASSWORD_LOCATOR).sendKeys(PASSWORD);
        driver.findElement(BUTTON_SUBMIT_LOCATOR).click();
        return driver.findElement(ERROR_LOGIN_MESSAGE_LOCATOR).isDisplayed();
    }
}
