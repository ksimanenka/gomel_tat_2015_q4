package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends Page {

    public static final String FULL_USER_NAME = "artsiom.hryharenka@yandex.ru";
    public static final By HEADER_USER_NAME_LOCATOR = By.xpath("//*[@id='nb-1']/span[1]");

    public static final By INBOX_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#inbox']");
    public static final By SENT_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#sent']");
    public static final By DRAFT_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#draft']");
    public static final By TRASH_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#trash']");

    public static final By GET_ID_LETTER_LOCATOR = By.className("b-statusline__link");

    public static final String MAIL_PATTERN = "//*[@value='%s']";
    public String idLetter;

    public MainPage(WebDriver driver) {
        super(driver);
    }


    public InboxPage openInboxPage() {
        driver.findElement(INBOX_LOCATOR).click();
        return new InboxPage(driver);
    }

    public DraftPage openDraftPage() {
        driver.findElement(DRAFT_LOCATOR).click();
        return new DraftPage(driver);
    }

    public TrashPage openTrashPage() {
        waitForElementIsClickable(TRASH_LOCATOR);
        driver.findElement(TRASH_LOCATOR).click();
        return new TrashPage(driver);
    }

    public String getHeaderUserName() {
        return driver.findElement(HEADER_USER_NAME_LOCATOR).getText();
    }

    public boolean saveAndDeleteDraftLetter() {
        InboxPage inboxPage = openInboxPage();
        WriteLetterPage writeLetterPage = inboxPage.openWriteMailPage();
        String subj = writeLetterPage.fillOnlySubjectLetter();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            throw new RuntimeException();
        }
        DraftPage draftPage = openDraftPage();
        draftPage.markLetter(subj);
        draftPage.clickDeleteButtton();
        driver.findElement(TRASH_LOCATOR).click();
        TrashPage trashPage = openTrashPage();
        if (trashPage.isPresentLetter(subj)) {
            trashPage.markLetter(subj);
            trashPage.clickDeleteButton();
            if (trashPage.isPresentLetter(subj)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public boolean sendValidLetter() {
        InboxPage inboxPage = openInboxPage();
        WriteLetterPage writeLetterPage = inboxPage.openWriteMailPage();
        writeLetterPage.fillMailFields();
        writeLetterPage.sendLetter();

        idLetter = getIdMail();
        openInboxPage();
        waitForElementIsClickable(By.xpath(String.format(MAIL_PATTERN, idLetter)));
        return (driver.findElement(By.xpath(String.format(MAIL_PATTERN, idLetter))).isDisplayed());
    }

    public boolean sendEmptyLetter() {
        InboxPage inboxPage = openInboxPage();
        WriteLetterPage writeLetterPage = inboxPage.openWriteMailPage();
        writeLetterPage.fillOnlyRecipientField();
        writeLetterPage.sendLetter();

        idLetter = getIdMail();
        openInboxPage();
        waitForElementIsClickable(By.xpath(String.format(MAIL_PATTERN, idLetter)));
        return (driver.findElement(By.xpath(String.format(MAIL_PATTERN, idLetter))).isDisplayed());
    }

    public boolean sendLetterWithoutRecipient() {
        InboxPage inboxPage = openInboxPage();
        WriteLetterPage writeLetterPage = inboxPage.openWriteMailPage();
        writeLetterPage.sendLetter();
        return writeLetterPage.getErrorMessage();
    }

    private String getIdMail() {
        waitForElementIsClickable(GET_ID_LETTER_LOCATOR);
        String href = driver.findElement(GET_ID_LETTER_LOCATOR).getAttribute("href");
        String[] items = href.split("/");
        System.out.println(items[4]);
        return items[4];
    }

}
