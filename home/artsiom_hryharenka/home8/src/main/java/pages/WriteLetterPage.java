package pages;

import BusinessObjects.Letter;
import BusinessObjects.LetterFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WriteLetterPage extends MainPage{

    public static final By RECIPIENT_LOCATOR = By.xpath("//tr[@class='b-compose-head__field b-compose-head__field_to js-compose-field-wrapper__to']//input[1]");
    public static final By SUBJECT_LOCATOR = By.name("subj");
    public static final By BODY_MAIL_LOCATOR = By.xpath("//textarea[@id='compose-send']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    public static final By ERROR_MESSAGE_LOCATOR = By.cssSelector("tr.b-compose-head__field.b-compose-head__field_to.js-compose-field-wrapper__to span.b-notification.b-notification_error.b-notification_error_required span.b-notification__i");

    private static Letter letter = LetterFactory.getRandomLetter();

    public WriteLetterPage(WebDriver driver) {
        super(driver);
    }

    public void fillMailFields() {
        waitForElementPresent(RECIPIENT_LOCATOR);
        driver.findElement(RECIPIENT_LOCATOR).sendKeys(letter.getTo());
        driver.findElement(SUBJECT_LOCATOR).sendKeys(letter.getSubject());
        driver.findElement(BODY_MAIL_LOCATOR).sendKeys(letter.getBody());
    }

    public void fillOnlyRecipientField() {
        waitForElementPresent(RECIPIENT_LOCATOR);
        driver.findElement(RECIPIENT_LOCATOR).sendKeys(letter.getTo());
    }

    public String fillOnlySubjectLetter() {
        waitForElementIsClickable(SUBJECT_LOCATOR);
        driver.findElement(SUBJECT_LOCATOR).sendKeys(letter.getSubject());
        return letter.getSubject();
    }

    public MainPage sendLetter() {
        waitForElementIsClickable(SEND_MAIL_BUTTON_LOCATOR);
        driver.findElement(SEND_MAIL_BUTTON_LOCATOR).click();
        return new MainPage(driver);
    }

    public boolean getErrorMessage() {
        return driver.findElement(ERROR_MESSAGE_LOCATOR).isDisplayed();
    }
}
