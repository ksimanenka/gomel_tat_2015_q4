package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DraftPage extends MainPage {

    public static final By DELETE_MESSAGE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//a[@data-action='delete']");

    public DraftPage(WebDriver driver) {
        super(driver);
    }
    public void clickDeleteButtton() {
        driver.findElement(DELETE_MESSAGE_BUTTON_LOCATOR).click();
    }

    public void markLetter(String subj) {
        String mailPattern = "//*[@title='" + subj + "']/../../../../../../label/input[@type='checkbox']";
        waitForElementIsClickable(By.xpath(mailPattern));
        driver.findElement(By.xpath(mailPattern)).click();
    }
}
