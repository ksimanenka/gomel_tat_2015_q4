package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InboxPage extends MainPage {

    public static final By WRITE_MESSAGE_BUTTON_LOCATOR = By.xpath("//*[contains(@class, 'b-toolbar__item__label js-toolbar-item-title-compose')]");
    public static final By DELETE_MESSAGE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//a[@data-action='delete']");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public WriteLetterPage openWriteMailPage() {
        driver.findElement(WRITE_MESSAGE_BUTTON_LOCATOR).click();
        return new WriteLetterPage(driver);
    }

    public void deleteMail() {
        driver.findElement(DELETE_MESSAGE_BUTTON_LOCATOR).click();
    }
}
