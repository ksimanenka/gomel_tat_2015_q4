import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.file.NotDirectoryException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WordList {
    public static List<String> wordList = new LinkedList<String>();

    public void printStatistics() {
        Map<String, Integer> words = counter();
        for (String word : words.keySet()) {
            System.out.println(word + " : " + words.get(word));
        }
    }

    private Map<String, Integer> counter() {
        Map<String, Integer> words = new HashMap<String, Integer>();
        for (String word : wordList) {
            if (!words.containsKey(word)) {
                words.put(word, 1);
            } else {
                int count = words.get(word);
                count++;
                words.put(word, count);
            }
        }
        return words;
    }


    public void getWords(String baseDirectory, String fileNamePattern) {
        if (fileNamePattern.isEmpty()) {
            throw new IllegalArgumentException("File name is empty!");
        }
        File folder = new File(baseDirectory);
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    getWords(file.getPath(), fileNamePattern);
                } else {
                    if (file.getPath().endsWith(fileNamePattern)) {
                        parseWords(file);
                    }
                }
            }
        }


    }

    private static void parseWords(File file) {
        FileInputStream stream;
        try {
            stream = new FileInputStream(file.getPath());
            String words = IOUtils.toString(stream);
            Pattern pattern = Pattern.compile("[a-zA-Zа-яА-Я]+");
            Matcher matcher = pattern.matcher(words);
            while (matcher.find()) {
                wordList.add(words.substring(matcher.start(), matcher.end()).toLowerCase());
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
