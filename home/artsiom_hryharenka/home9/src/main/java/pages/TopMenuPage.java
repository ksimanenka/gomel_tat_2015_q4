package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.GenerateFileHelper;

public class TopMenuPage extends Page {

    public static final By FILES_LOCATOR = By.cssSelector("div[data-id='disk'] a[href$='/disk']");
    public static final By MENU_TRASH_LOCATOR = By.cssSelector("div[data-id='trash'] a[href$='/trash']");
    public static final By UPLOAD_INPUT_LOCATOR = By.className("button__attach");
    public static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//*[@class='button__attach']/..");

    public static final By UPLOAD_NOTIFICATION_LOCATOR = By.cssSelector(".b-item-upload__icon.b-item-upload__icon_done");
    public static final By CLOSE_UPLOAD_NOTIFICATION_LOCATOR = By.cssSelector("._nb-popup-close.ns-action.js-cross");

    public TopMenuPage(WebDriver driver) {
        super(driver);
    }

    public FilesPage openFilesPage() {
        driver.navigate().refresh();
        waitForElementIsClickable(FILES_LOCATOR);
        driver.findElement(FILES_LOCATOR).click();
        return new FilesPage(driver);
    }

    public TrashPage openTrashPage() {
        driver.navigate().refresh();
        waitForElementIsClickable(MENU_TRASH_LOCATOR);
        driver.findElement(MENU_TRASH_LOCATOR).click();
        return new TrashPage(driver);
    }

    public void uploadFile() {
        waitForElementIsClickable(UPLOAD_BUTTON_LOCATOR);
        driver.findElement(UPLOAD_INPUT_LOCATOR).sendKeys(GenerateFileHelper.getUploadDirectoryPath() + GenerateFileHelper.getFirstFileName());
        waitForElementIsClickable(UPLOAD_NOTIFICATION_LOCATOR);
        waitForElementIsClickable(CLOSE_UPLOAD_NOTIFICATION_LOCATOR);
        driver.findElement(CLOSE_UPLOAD_NOTIFICATION_LOCATOR).click();
    }

    public void uploadSecondFile() {
        waitForElementIsClickable(UPLOAD_BUTTON_LOCATOR);
        driver.findElement(UPLOAD_INPUT_LOCATOR).sendKeys(GenerateFileHelper.getUploadDirectoryPath() + GenerateFileHelper.getSecondFileName());
        waitForElementIsClickable(UPLOAD_NOTIFICATION_LOCATOR);
        waitForElementIsClickable(CLOSE_UPLOAD_NOTIFICATION_LOCATOR);
        driver.findElement(CLOSE_UPLOAD_NOTIFICATION_LOCATOR).click();
    }

}
