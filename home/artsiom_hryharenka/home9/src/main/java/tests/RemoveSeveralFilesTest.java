package tests;

import org.testng.annotations.Test;
import pages.FilesPage;
import pages.TrashPage;

import static org.testng.Assert.assertTrue;

public class RemoveSeveralFilesTest extends BaseTest {

    @Test(groups = "data")
    public void removeSeveralFiles() {
        topMenuPage.uploadFile();
        topMenuPage.uploadSecondFile();
        FilesPage filesPage = new FilesPage(driver);
        filesPage.removeFiles();
        TrashPage trashPage = topMenuPage.openTrashPage();
        assertTrue(trashPage.isFilesPresent());
    }
}
