package tests;

import org.testng.annotations.Test;
import pages.FilesPage;

import static org.testng.Assert.*;

public class UploadFileTest extends BaseTest{

    @Test(groups = "data")
    public void uploadFileTest(){
        topMenuPage.uploadFile();
        FilesPage filesPage = new FilesPage(driver);
        assertTrue(filesPage.isTrueFile());
    }

}
