import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static utils.CommonFileUtils.getFileEncodingType;

public class WordList {
    public Map<String, Integer> values = new TreeMap<String, Integer>();

    public Map<String, Integer> getValues() {
        return values;
    }

    public void printStatistics() {
        for (String word : values.keySet()) {
            System.out.println(word + " : " + values.get(word));
        }
        System.out.println("Total words: " + getValueSize());

    }

    public int getValueSize() {
        return values.size();
    }

    public void getWords(String baseDirectory, String fileNamePattern) {
        if (fileNamePattern.isEmpty()) {
            throw new IllegalArgumentException("File name is empty!");
        }
        if (baseDirectory.isEmpty()) {
            System.out.println("The file will be searched in the project's folder");
        }
        String mask = ".*\\\\" + fileNamePattern.replace("*", ".*").replace("\\", "\\\\");

        File folder = new File(baseDirectory);
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    getWords(file.getPath(), fileNamePattern);
                } else {
                    if (file.getAbsolutePath().matches(mask)) {
                        parseWords(file);
                    }
                }
            }
        }
    }

    private void parseWords(File file) {
        try {
            String fileContent = FileUtils.readFileToString(file, getFileEncodingType(file.getAbsolutePath()));
            Pattern pattern = Pattern.compile("[a-zA-Zа-яА-Я]+");
            Matcher matcher = pattern.matcher(fileContent);
            while (matcher.find()) {
                if (!values.containsKey(fileContent.substring(matcher.start(), matcher.end()).toLowerCase())) {
                    values.put(fileContent.substring(matcher.start(), matcher.end()).toLowerCase(), 1);
                } else {
                    int count = values.get(fileContent.substring(matcher.start(), matcher.end()).toLowerCase());
                    count++;
                    values.put(fileContent.substring(matcher.start(), matcher.end()).toLowerCase(), count);
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
