import org.testng.annotations.Factory;
import utils.GenerateDataHelper;

public class FileNameMaskTestFactory {

    @Factory
    public static Object[][] valuesForCheck() {
        return new Object[][]{
                {GenerateDataHelper.getBaseDirectoryPath(), "test folder\\test file.txt"},
                {GenerateDataHelper.getBaseDirectoryPath(), "*.txt"},
                {GenerateDataHelper.getBaseDirectoryPath(), "*folder*\\*.txt"}
        };
    }
}
