import org.testng.annotations.*;
import utils.GenerateDataHelper;


public class BaseTest {

    protected WordList wordList;

    @BeforeClass
    public void setUp() {
        wordList = new WordList();
    }

    @BeforeGroups(groups = "testWithData")
    public void createTestDate(){
        GenerateDataHelper.createBaseDirectory();
    }

    @AfterGroups(groups = "testWithData")
    public void removeTestData() {
        GenerateDataHelper.removeDirectory();
    }

}
