package listeners;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class CustomListener extends TestListenerAdapter {

    private List<String> logList = new LinkedList<String>();

    @Override
    public void onTestFailure(ITestResult result) {
        logList.add(String.format("[Test method %s -- failed] - [%s]", result.getName(), result.getThrowable().fillInStackTrace().toString()));
        setLog();
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logList.add(String.format("[Test method %s -- skipped] - [%s]", result.getName(), result.getThrowable().fillInStackTrace().toString()));
        setLog();
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logList.add(String.format("[Test method %s -- success] - [%s]", result.getName(), result.getThrowable().fillInStackTrace().toString()));
        setLog();
    }

    public void setLog() {
        File folder = new File("log\\");
        folder.mkdirs();
        File file = new File(folder, "logfile.txt");
        FileWriter writer;
        try{
            writer = new FileWriter(file, false);
            for (String line : logList) {
                writer.write(line);
                writer.write("\r\n");
            }
            writer.flush();
        }catch (IOException ex){
            throw new RuntimeException(ex.getMessage());
        }

    }


}
