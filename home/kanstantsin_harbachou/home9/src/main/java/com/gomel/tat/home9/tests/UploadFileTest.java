package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.bo.Yandexdisk.DiskPage;
import com.gomel.tat.home9.bo.Yandexdisk.TrashPage;
import com.gomel.tat.home9.utils.FileService;
import com.gomel.tat.home9.utils.InitialData;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class UploadFileTest extends LoginTest {

    protected String fileName;
    DiskPage diskPage;

    @BeforeClass(description = "Create file")
    public void createFile() {
        fileName = FileService.createFile();
    }

    @Test(description = "Upload Test", priority = 1)
    public void checkUploadFile() throws InterruptedException {
        diskPage = new DiskPage(driver);
        diskPage.open();
        diskPage.uploadFile(InitialData.PATH_FOR_UPLOADING + fileName);
        Assert.assertTrue(diskPage.isFilePresent(fileName));
    }

    @AfterClass(description = "Deleting file")
    public void deleteFile() {
        FileService.deleteFile(InitialData.PATH_FOR_UPLOADING + fileName);
        diskPage.deleteFile(fileName);
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        trashPage.clearTrash();
    }
}
