package com.gomel.tat.home9.bo.Yandexdisk;

import com.gomel.tat.home9.bo.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static com.gomel.tat.home9.utils.WebDriverHelper.waitForElementIsClickable;

public class TrashPage extends AbstractPage {

    public static final By TRASH_PICTURE_LOCATOR = By.cssSelector("[data-id='/trash']");
    public static final By CLEAR_TRASH_BUTTON_LOCATOR = By.cssSelector("div.b-toolset__buttons button[data-click-action=\"trash.clean\"]");
    public static final By CONFIRM_CLEARING_BUTTON_LOCATOR = By.cssSelector("button.nb-button._nb-small-action-button._init.b-confirmation__action.b-confirmation__action_right.js-confirmation-accept");
    public static final By EXISTING_FILES_LOCATOR = By.className("nb-resource__text-name");
    public static final By CONFIRM_RESTORE_NOTIFICATION_LOCATOR = By.cssSelector(".notifications__text.js-message");

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(TRASH_PICTURE_LOCATOR)).doubleClick().perform();
    }

    public void clearTrash() {
        waitForElementIsClickable(CLEAR_TRASH_BUTTON_LOCATOR);
        driver.findElement(CLEAR_TRASH_BUTTON_LOCATOR).click();
        waitForElementIsClickable(CONFIRM_CLEARING_BUTTON_LOCATOR);
        driver.findElement(CONFIRM_CLEARING_BUTTON_LOCATOR).click();
    }

    public boolean isFilePresent(String fileName) throws InterruptedException {
        Thread.sleep(1000);
        boolean result = false;
        List<WebElement> listOfFiles = driver.findElements(EXISTING_FILES_LOCATOR);
        for (WebElement file : listOfFiles) {
            if (file.getText().equals(fileName)) {
                result = true;
            }
        }
        return result;
    }

    public void restoreFile(String fileName) {
        List<WebElement> listOfFiles = driver.findElements(EXISTING_FILES_LOCATOR);
        for (WebElement file : listOfFiles) {
            if (file.getText().equals(fileName)) {
                file.click();
                waitForElementIsClickable(By.cssSelector("button[data-params='{\"id\":\"/trash/" + fileName + "\"}']"));
                List<WebElement> list = driver.findElements(By.cssSelector("button[data-params='{\"id\":\"/trash/" + fileName + "\"}']"));
                list.get(0).click();
            }
        }
        waitForElementIsClickable(CONFIRM_RESTORE_NOTIFICATION_LOCATOR);
    }
}
