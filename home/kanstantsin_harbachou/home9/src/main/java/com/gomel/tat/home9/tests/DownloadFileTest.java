package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.bo.Yandexdisk.DiskPage;
import com.gomel.tat.home9.bo.Yandexdisk.TrashPage;
import com.gomel.tat.home9.utils.FileService;
import com.gomel.tat.home9.utils.InitialData;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

public class DownloadFileTest extends LoginTest {

    protected String fileName;
    DiskPage diskPage;

    @BeforeClass(description = "Create file")
    public void createFile() {
        fileName = FileService.createFile();
    }

    @Test(description = "Download Test", priority = 1)
    public void checkDownloadFile() throws Exception {
        diskPage = new DiskPage(driver);
        diskPage.open();
        diskPage.uploadFile(InitialData.PATH_FOR_UPLOADING + fileName);
        diskPage.downloadFile(fileName);
        File file = new File(InitialData.PATH_FOR_DOWNLOADING + fileName);
        while (!file.canWrite())
            Thread.sleep(1000);
        Assert.assertTrue(FileService.isFilesEquals(InitialData.PATH_FOR_UPLOADING + fileName,
                InitialData.PATH_FOR_DOWNLOADING + fileName));
    }

    @AfterClass(description = "Deleting file")
    public void deleteFile() {
        FileService.deleteFile(InitialData.PATH_FOR_DOWNLOADING + fileName);
        FileService.deleteFile(InitialData.PATH_FOR_UPLOADING + fileName);
        diskPage.deleteFile(fileName);
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        trashPage.clearTrash();
    }
}
