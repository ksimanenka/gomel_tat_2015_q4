package com.gomel.tat.home9.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileService {

    public static String createFile() {
        String fileName = RandomStringUtils.randomAlphabetic(5) + ".txt";
        File file = new File(InitialData.PATH_FOR_UPLOADING, fileName);
        try (FileWriter writer = new FileWriter(file, false)) {
            writer.write(RandomStringUtils.randomAlphabetic(10));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return fileName;
    }

    public static void deleteFile(String file) {
        try {
            Files.delete(Paths.get(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isFilesEquals(String uploadedFile, String downloadedFile) throws Exception {
        FileInputStream uploadedFileStream = new FileInputStream(uploadedFile);
        String md5Uploaded = org.apache.commons.codec.digest.DigestUtils.md5Hex(uploadedFileStream);
        uploadedFileStream.close();
        FileInputStream downloadedFileStream = new FileInputStream(uploadedFile);
        String md5Downloaded = org.apache.commons.codec.digest.DigestUtils.md5Hex(downloadedFileStream);
        downloadedFileStream.close();
        return md5Uploaded.equals(md5Downloaded);

    }
}
