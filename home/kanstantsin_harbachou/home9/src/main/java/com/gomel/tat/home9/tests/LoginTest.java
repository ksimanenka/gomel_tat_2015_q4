package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.bo.Yandexdisk.DiskPage;
import com.gomel.tat.home9.bo.Yandexdisk.LoginPage;
import com.gomel.tat.home9.utils.InitialData;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseYandexDiskTest {

    @Test(description = "Login Test", priority = 0)
    public void checkLogin() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(InitialData.LOGIN_YANDEX_DISK, InitialData.PASSWORD_YANDEX_DISK);
        DiskPage diskPage = new DiskPage(driver);
        Assert.assertEquals(diskPage.getUserName(), InitialData.LOGIN_YANDEX_DISK);
    }
}
