package com.gomel.tat.home9.bo.Yandexdisk;

import com.gomel.tat.home9.bo.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static com.gomel.tat.home9.utils.WebDriverHelper.waitForElementIsClickable;

public class DiskPage extends AbstractPage {

    public DiskPage(WebDriver driver) {
        super(driver);
    }

    public static final By MAIN_PAGE_LOCATOR = By.className("header__title");
    public static final By ATTACH_BUTTON_LOCATOR = By.className("button__attach");
    public static final By EXISTING_FILES_LOCATOR = By.className("nb-resource__text-name");
    public static final By COMPLETED_UPLOAD_NOTIFICATION_LOCATOR = By.cssSelector(".b-item-upload__icon.b-item-upload__icon_done");
    public static final By CLOSE_UPLOAD_POPUP_LOCATOR = By.cssSelector("._nb-popup-close.ns-action.js-cross");
    public static final By USERNAME_LOCATOR = By.className("header__username");
    public static final By CONFIRM_DELETING_NOTIFICATION_LOCATOR = By.cssSelector(".notifications__item.nb-island.notifications__item_moved");

    public void open() {
        waitForElementIsClickable(MAIN_PAGE_LOCATOR).click();
    }

    public String getUserName() {
        WebElement userName = driver.findElement(USERNAME_LOCATOR);
        return userName.getText();
    }

    public void uploadFile(String path) throws InterruptedException {
        driver.findElement(ATTACH_BUTTON_LOCATOR).sendKeys(path);
        waitForElementIsClickable(COMPLETED_UPLOAD_NOTIFICATION_LOCATOR);
        waitForElementIsClickable(CLOSE_UPLOAD_POPUP_LOCATOR).click();
        Thread.sleep(1000);
    }

    public boolean isFilePresent(String fileName) throws InterruptedException {
        Thread.sleep(1000);
        boolean result = false;
        List<WebElement> listOfFiles = driver.findElements(EXISTING_FILES_LOCATOR);
        for (WebElement file : listOfFiles) {
            if (file.getText().equals(fileName)) {
                result = true;
            }
        }
        return result;
    }

    public void downloadFile(String fileName) {
        List<WebElement> listOfFiles = driver.findElements(EXISTING_FILES_LOCATOR);
        for (WebElement file : listOfFiles) {
            if (file.getText().equals(fileName)) {
                file.click();
                waitForElementIsClickable(By.cssSelector("button[data-params='{\"id\":\"/disk/" + fileName + "\"}']"));
                List<WebElement> list = driver.findElements(By.cssSelector("button[data-params='{\"id\":\"/disk/" + fileName + "\"}']"));
                list.get(0).click();
            }
        }
    }

    public void deleteFile(String fileName) {
        List<WebElement> listOfFiles = driver.findElements(EXISTING_FILES_LOCATOR);
        for (WebElement file : listOfFiles) {
            if (file.getText().equals(fileName)) {
                file.click();
                waitForElementIsClickable(By.cssSelector("button[data-params='{\"id\":\"/disk/" + fileName + "\"}']"));
                List<WebElement> list = driver.findElements(By.cssSelector("button[data-params='{\"id\":\"/disk/" + fileName + "\"}']"));
                list.get(1).click();
            }
        }
        waitForElementIsClickable(CONFIRM_DELETING_NOTIFICATION_LOCATOR);
    }

    public void deleteFiles(List<String> fileNames) throws InterruptedException {
        waitForElementIsClickable(EXISTING_FILES_LOCATOR);
        WebElement element = null;
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL);
        List<WebElement> listOfFiles = driver.findElements(EXISTING_FILES_LOCATOR);
        for (WebElement file : listOfFiles) {
            for (String fileName : fileNames)
                if (file.getText().equals(fileName)) {
                    element = file;
                    action.moveToElement(file).click();
                    Thread.sleep(500);
                }
        }
        action.keyUp(Keys.CONTROL).build().perform();
        action.dragAndDrop(element, listOfFiles.get(11));
        action.build().perform();
        waitForElementIsClickable(CONFIRM_DELETING_NOTIFICATION_LOCATOR);
    }
}
