package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.bo.Yandexdisk.DiskPage;
import com.gomel.tat.home9.bo.Yandexdisk.TrashPage;
import com.gomel.tat.home9.utils.FileService;
import com.gomel.tat.home9.utils.InitialData;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RemoveFileTest extends LoginTest {

    protected String fileName;
    DiskPage diskPage;

    @BeforeClass(description = "Create file")
    public void createFile() {
        fileName = FileService.createFile();
    }

    @Test(description = "Remove Test", priority = 1)
    public void checkRemovingFile() throws InterruptedException {
        diskPage = new DiskPage(driver);
        diskPage.open();
        diskPage.uploadFile(InitialData.PATH_FOR_UPLOADING + fileName);
        Assert.assertTrue(diskPage.isFilePresent(fileName));
        diskPage.deleteFile(fileName);
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        Assert.assertTrue(trashPage.isFilePresent(fileName));
        trashPage.clearTrash();
        Assert.assertFalse(trashPage.isFilePresent(fileName));
    }
}
