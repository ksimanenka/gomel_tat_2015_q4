package com.gomel.tat.home10.lib.mail.service;


import com.gomel.tat.home10.lib.mail.LetterBuilder;
import com.gomel.tat.home10.lib.mail.screen.ComposePage;
import com.gomel.tat.home10.lib.mail.screen.ConfirmPage;
import com.gomel.tat.home10.lib.mail.screen.InboxPage;
import com.gomel.tat.home10.lib.mail.screen.OutboxPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class MailService {

    public static void sendFullLetter(WebDriver driver) {
        ComposePage composePage = PageFactory.initElements(driver, ComposePage.class);
        wrightLetter(driver);
        composePage.clickOnSendButton(driver);
    }

    public static void sendEmptyLetter(WebDriver driver) {
        InboxPage inboxPage = PageFactory.initElements(driver, InboxPage.class);
        inboxPage.goToLetterForm(driver);
        ComposePage composePage = PageFactory.initElements(driver, ComposePage.class);
        composePage.clickOnSendButton(driver);
    }

    public static void sendLetterWithAddress(WebDriver driver) {
        InboxPage inboxPage = PageFactory.initElements(driver, InboxPage.class);
        inboxPage.goToLetterForm(driver);
        ComposePage composePage = PageFactory.initElements(driver, ComposePage.class);
        composePage.enterAddress(LetterBuilder.getLetter().getAddress(), driver);
        composePage.clickOnSendButton(driver);
    }

    public static void wrightLetter(WebDriver driver) {
        InboxPage inboxPage = PageFactory.initElements(driver, InboxPage.class);
        inboxPage.goToLetterForm(driver);
        ComposePage composePage = PageFactory.initElements(driver, ComposePage.class);
        composePage.enterAddress(LetterBuilder.getLetter().getAddress(), driver);
        composePage.enterSubject(LetterBuilder.getLetter().getSubject(), driver);
        composePage.switchToPostFrame(driver);
        composePage.enterPost(LetterBuilder.getLetter().getPost(), driver);
        composePage.switchToMainFrame(driver);
    }

    public static boolean isLetterPresentIntoInbox(WebDriver driver) {
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        ConfirmPage confirmPage = PageFactory.initElements(driver, ConfirmPage.class);
        InboxPage inboxPage = PageFactory.initElements(driver, InboxPage.class);
        String mailLink = confirmPage.returnLetterLink(driver);
        return inboxPage.isLetterPresent(driver, mailLink.substring(mailLink.indexOf("#")));
    }

    public static void deleteAllLetters(WebDriver driver) {
        InboxPage inboxPage = PageFactory.initElements(driver, InboxPage.class);
        inboxPage.visitInboxPage();
        if (inboxPage.isInboxFull()) {
            inboxPage.selectAll(driver);
            inboxPage.deleteAll(driver);
        }
        OutboxPage outboxPage = PageFactory.initElements(driver, OutboxPage.class);
        outboxPage.visitOutboxPage(driver);
        if (outboxPage.isOutboxFull(driver)) {
            outboxPage.selectAll(driver);
            outboxPage.deleteAll(driver);
        }


        inboxPage.visitInboxPage();
        inboxPage.clickOnClearTrash(driver);
        inboxPage.confirmClearingTrash();

    }

    public static boolean isNotificationPresent(WebDriver driver) {
        ComposePage composePage = PageFactory.initElements(driver, ComposePage.class);
        return composePage.isNotificationPresent();
    }
}



