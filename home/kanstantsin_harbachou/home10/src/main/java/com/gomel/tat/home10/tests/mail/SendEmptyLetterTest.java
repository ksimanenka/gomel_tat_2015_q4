package com.gomel.tat.home10.tests.mail;


import com.gomel.tat.home10.lib.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.gomel.tat.home10.lib.mail.service.MailService.isNotificationPresent;
import static com.gomel.tat.home10.lib.mail.service.MailService.sendEmptyLetter;
public class SendEmptyLetterTest extends BaseMailTest {

    @BeforeMethod(description = "Preparing letter")
    public void login() {
        LoginService.enterWithRightCredential(driver);
    }

    @Test(description = "Checking sending of empty letter")
    public void checkSendEmptyLetter() throws InterruptedException {
        sendEmptyLetter(driver);
        Assert.assertTrue(isNotificationPresent(driver));
    }
}
