package com.gomel.tat.home10.lib.mail.screen;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.gomel.tat.home10.lib.common.service.WebDriverHelper.waitForElementIsClickable;

public class ComposePage {

    @FindBy(xpath = "//*[@data-params='field=to']//ancestor::tr//input[@type='text']")
    WebElement addressInput;

    @FindBy(name = "subj")
    WebElement subjectInput;

    @FindBy(css = "body#tinymce")
    WebElement postInput;

    @FindBy(id = "compose-send_ifr")
    WebElement postFrame;

    @FindBy(id = "compose-submit")
    WebElement submitButton;

    @FindBy(css = "span.b-notification__i")
    WebElement notification;

    public void enterAddress(String email, WebDriver driver) {
        waitForElementIsClickable(addressInput, driver);
        addressInput.sendKeys(email);
    }

    public void enterSubject(String subject, WebDriver driver) {
        waitForElementIsClickable(subjectInput, driver).sendKeys(subject);
    }

    public void enterPost(String post, WebDriver driver) {
        waitForElementIsClickable(postInput, driver).sendKeys(post);
    }

    public void switchToPostFrame(WebDriver driver) {
        driver.switchTo().frame(postFrame);
    }

    public void switchToMainFrame(WebDriver driver) {
        driver.switchTo().defaultContent();
    }

    public void clickOnSendButton(WebDriver driver) {
        waitForElementIsClickable(submitButton, driver).click();
    }

    public boolean isNotificationPresent() {
       return notification.isEnabled();
    }
}
