package com.gomel.tat.home10.lib.mail.screen;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.gomel.tat.home10.lib.common.service.WebDriverHelper.waitForElementIsClickable;

public class ConfirmPage {

    @FindBy(xpath = "//a[@class='b-statusline__link']")
    WebElement confirmationMessage;

    public String returnLetterLink(WebDriver driver) {
        return waitForElementIsClickable(confirmationMessage, driver).getAttribute("href");
    }
}
