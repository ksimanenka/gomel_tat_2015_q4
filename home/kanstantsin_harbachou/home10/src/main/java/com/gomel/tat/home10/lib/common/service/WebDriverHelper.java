package com.gomel.tat.home10.lib.common.service;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverHelper {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;

    public static WebElement waitForElementIsClickable(WebElement element, WebDriver driver) {
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOf(element));
        return element;
    }
}
