package com.gomel.tat.home10.tests.mail;

import com.gomel.tat.home10.lib.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginNegativeTest extends BaseMailTest {
    @Test(description = "Negative Login Test")
    public void checkNegativeLogin() {
        LoginService.enterWithWrongCredential(driver);
        Assert.assertTrue(LoginService.isErrorPresent(driver));
    }
}
