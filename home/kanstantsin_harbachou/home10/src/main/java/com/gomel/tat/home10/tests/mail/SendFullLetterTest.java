package com.gomel.tat.home10.tests.mail;

import com.gomel.tat.home10.lib.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.gomel.tat.home10.lib.mail.service.MailService.*;

public class SendFullLetterTest extends BaseMailTest {

    @BeforeMethod(description = "Preparing letter")
    public void login() {
        LoginService.enterWithRightCredential(driver);
    }

    @Test(description = "Checking sending of full letter")
    public void checkSendFullLetter() throws InterruptedException {
        sendFullLetter(driver);
        Assert.assertTrue(isLetterPresentIntoInbox(driver));
    }

    @AfterMethod(description = "Deleting of all letters")
    public void clearMailBox() {
        deleteAllLetters(driver);
    }
}
