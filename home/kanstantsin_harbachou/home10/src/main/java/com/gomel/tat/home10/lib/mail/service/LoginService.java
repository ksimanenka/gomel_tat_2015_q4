package com.gomel.tat.home10.lib.mail.service;

import com.gomel.tat.home10.lib.common.AccountBuilder;
import com.gomel.tat.home10.lib.mail.screen.InboxPage;
import com.gomel.tat.home10.lib.mail.screen.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginService {

    public static void enterWithRightCredential(WebDriver driver) {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.enterLogin(AccountBuilder.getDefaultAccount().getLogin());
        loginPage.enterPassword(AccountBuilder.getDefaultAccount().getPassword());
        loginPage.submitButtonClick();
    }

    public static void enterWithWrongCredential(WebDriver driver) {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.enterLogin(AccountBuilder.getAccountWithWrongPass().getLogin());
        loginPage.enterPassword(AccountBuilder.getAccountWithWrongPass().getPassword());
        loginPage.submitButtonClick();
    }

    public static String getUserEmail(WebDriver driver) {
        InboxPage inboxPage = PageFactory.initElements(driver, InboxPage.class);
        return inboxPage.getUserEmail();
    }

    public static boolean isErrorPresent(WebDriver driver) {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        return loginPage.isErrorPresent();
    }
}
