package com.gomel.tat.home10.lib.mail.screen;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.gomel.tat.home10.lib.common.service.WebDriverHelper.waitForElementIsClickable;

public class InboxPage {

    @FindBy(xpath = "//a[@href='#inbox']")
    WebElement inboxPageLink;

    @FindBy(css = ".header-user-name.js-header-user-name")
    WebElement userEmailLocator;

    @FindBy(xpath = "//a[@href='#compose']")
    WebElement writeNewLetterLink;

    @FindBy(css = "input.b-messages-head__checkbox")
    WebElement selectAllLetter;

    @FindBy(css = "[data-action=delete]")
    WebElement deleteSelected;

    @FindBy(css = ".b-folders__folder__info img")
    WebElement clearTrash;

    @FindBy(css = "div.b-popup__confirm button")
    WebElement confirmClearTrash;

    public java.lang.String getUserEmail() {
        return userEmailLocator.getText();
    }

    public void goToLetterForm(WebDriver driver) {
        waitForElementIsClickable(writeNewLetterLink, driver).click();
    }

    public boolean isLetterPresent(WebDriver driver, java.lang.String emailId) {
        return driver.findElement(By.xpath(java.lang.String.format("//a[@href='%s']", emailId))).isEnabled();
    }

    public void visitInboxPage() {
        inboxPageLink.click();
    }

    public boolean isInboxFull() {
        return selectAllLetter.isEnabled();
    }

    public void selectAll(WebDriver driver) {
        waitForElementIsClickable(selectAllLetter, driver).click();
    }

    public void deleteAll(WebDriver driver) {
        waitForElementIsClickable(deleteSelected, driver).click();
    }

    public void clickOnClearTrash(WebDriver driver) { waitForElementIsClickable(clearTrash, driver).click(); }

    public void confirmClearingTrash() {
        confirmClearTrash.click();
    }
}
