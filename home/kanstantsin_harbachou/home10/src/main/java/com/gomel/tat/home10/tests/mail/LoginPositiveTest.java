package com.gomel.tat.home10.tests.mail;

import com.gomel.tat.home10.lib.common.AccountBuilder;
import com.gomel.tat.home10.lib.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPositiveTest extends BaseMailTest{

    @Test(description = "Positive Login Test")
    public void checkPositiveLogin(){
        LoginService.enterWithRightCredential(driver);
        Assert.assertEquals(LoginService.getUserEmail(driver), AccountBuilder.getDefaultAccount().getEmail());
    }
}
