package com.gomel.tat.home10.tests.mail;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.gomel.tat.home10.lib.mail.service.LoginService.enterWithRightCredential;
import static com.gomel.tat.home10.lib.mail.service.MailService.*;

public class SendLetterOnlyWithAddressTest extends BaseMailTest {
    @BeforeMethod(description = "Preparing letter")
    public void login() {
        enterWithRightCredential(driver);
    }

    @Test(description = "Checking sending of letter with address")
    public void checkSendLetterWithAddress() {
        sendLetterWithAddress(driver);
        Assert.assertTrue(isLetterPresentIntoInbox(driver));
    }

    @AfterMethod(description = "Deleting of all letters")
    public void clearMailBox() {
        deleteAllLetters(driver);
    }
}
