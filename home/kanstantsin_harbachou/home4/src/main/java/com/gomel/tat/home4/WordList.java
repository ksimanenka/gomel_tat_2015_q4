package com.gomel.tat.home4;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

public class WordList {
    private Map<String, Integer> values = new HashMap<>();

    public void getWords(String baseDirectory, String fileNamePattern) {
        if (baseDirectory == null || baseDirectory.length() == 0
                || fileNamePattern == null || fileNamePattern.length() == 0) {
            return;
        }

        Path basePath = Paths.get(baseDirectory);
        final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:**/" + fileNamePattern);
        try {
            Files.walkFileTree(basePath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (pathMatcher.matches(file)) {
                        readWords(file);
                    }

                    return super.visitFile(file, attrs);
                }


            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readWords(Path file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file.toFile()))) {
            String line;
            while ((line = br.readLine()) != null) {
                for (String word : line.split("[\\W[а-я][А-Я]0-9]")) {
                    if (word.length() != 0) {
                        String trimmed = word.trim();
                        if (values.containsKey(trimmed)) {
                            Integer i = values.get(trimmed);
                            i++;
                            values.put(trimmed, i);
                        } else {
                            values.put(trimmed, 1);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printStatistics() {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out))) {
            for (Map.Entry entry : values.entrySet()) {
                bw.append(entry.getKey().toString()).append(' ').append(entry.getValue().toString()).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
