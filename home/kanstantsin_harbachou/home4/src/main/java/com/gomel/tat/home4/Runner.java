package com.gomel.tat.home4;

public class Runner {
    private final static String BASE_DIRECTORY = "e:/gomel_tat_2015_q4/home";
    private final static String FILE_NAME_PATTERN = "home3/hello.txt";

    public static void main(String[] args) {
        WordList wordList = new WordList();
        wordList.getWords(BASE_DIRECTORY, FILE_NAME_PATTERN);
        wordList.printStatistics();
    }
}
