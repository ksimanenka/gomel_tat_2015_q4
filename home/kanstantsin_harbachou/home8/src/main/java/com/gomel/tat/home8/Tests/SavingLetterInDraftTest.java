package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.bo.LetterFactory;
import com.gomel.tat.home8.page.Yandexmail.ComposePage;
import com.gomel.tat.home8.page.Yandexmail.DraftPage;
import com.gomel.tat.home8.page.Yandexmail.InboxPage;
import com.gomel.tat.home8.page.Yandexmail.TrashPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SavingLetterInDraftTest extends LoginPositiveTest {

    boolean isLetterInDraft, isLetterInTrash, isLetterInTrashAfterClear = false;
    Letter letter;

    @BeforeMethod(description = "Preparing letter.")
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test(description = "Checking saving letter in draft and deleting")
    public void checkSavingAndDeletingLetter() throws InterruptedException {

        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.wrightLetter(letter);
        Thread.sleep(10000);
        String currentUrl = driver.getCurrentUrl();
        int index = currentUrl.indexOf("#");
        String letterId = currentUrl.substring(index + 9);
        DraftPage draftPage = new DraftPage(driver);
        draftPage.open();
        draftPage.deleteLetter(letterId);
        draftPage.open();
        Thread.sleep(3000);
        isLetterInDraft = draftPage.isEnabled(letterId);
        TrashPage trashPage = new TrashPage(driver);
        isLetterInTrash = trashPage.isEnabled(letterId);
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.clearTrash();
        isLetterInTrashAfterClear = trashPage.isEnabled(letterId);

        Assert.assertFalse(isLetterInDraft && !isLetterInTrash && isLetterInTrashAfterClear);
    }
}
