package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.page.Yandexmail.ComposePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendVoidEmailTest extends LoginPositiveTest {

    @Test(description = "Checking sending of void letter")
    public void checkSendVoidLetter() throws InterruptedException {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendEmptyLetter();

        Assert.assertTrue(composePage.getNotification().isEnabled());
    }
}
