package com.gomel.tat.home8.bo;

import com.gomel.tat.home8.utils.TextGenerator;

public class LetterFactory {

    public static Letter getRandomLetter() {
        String mailTo = "mailfortest3@yandex.ru";
        String mailSubject = "test subject " + TextGenerator.createMessage(5);
        String mailContent = "mail content " + TextGenerator.createMessage(20);
        return new Letter(mailTo, mailSubject, mailContent);
    }
}
