package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.bo.LetterFactory;
import com.gomel.tat.home8.page.Yandexmail.ComposePage;
import com.gomel.tat.home8.page.Yandexmail.ConfirmPage;
import com.gomel.tat.home8.page.Yandexmail.InboxPage;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SendFullEmailTest extends LoginPositiveTest {

    private Letter letter;

    @BeforeMethod(description = "Preparing letter.")
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test(description = "Checking sending of full letter", groups = "full")
    public void checkSendFullLetter() throws InterruptedException {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        ConfirmPage confirmPage = new ConfirmPage(driver);
        String emailId = confirmPage.getEmailId();
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();

        Assert.assertTrue(inboxPage.isLetterPresent(emailId));
    }


    @AfterGroups("full")
    public void deleteAllLetters() {

        driver.findElement(InboxPage.INBOX_LINK_LOCATOR).click();
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        inboxPage.deleteAllLetter();
        inboxPage.open();
        inboxPage.clearTrash();
    }
}