package com.gomel.tat.home8.page.Yandexmail;

import com.gomel.tat.home8.page.Page;
import com.gomel.tat.home8.utils.InitialData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends Page {

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//input[@name='login']");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By ERROR_MESSAGE = By.cssSelector(".error-msg");
    public static WebElement errorMessage;

    public WebElement getErrorMessage() {
        return errorMessage;
    }

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {

        driver.get(InitialData.YANDEX_START_PAGE);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
    }

    public InboxPage login(String userLogin, String userPassword) {

        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        return new InboxPage(driver);
    }

    public LoginPage wrongLogin(String userLogin, String userPassword) {

        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        errorMessage = driver.findElement(ERROR_MESSAGE);
        return new LoginPage(driver);
    }
}
