package com.gomel.tat.home8.page.Yandexmail;

import com.gomel.tat.home8.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home8.utils.WebDriverHelper.waitForElementIsClickable;

public class OutcomingPage extends Page {

    public static final By OUTCOMING_LINK_LOCATOR = By.cssSelector("a[href$='#sent']");
    public static final By DELETE_ACTION_LOCATOR = By.cssSelector("[data-action=delete]");
    public static final By SELECT_ALL_CHECKBOX_LOCATOR = By.cssSelector("label.b-messages-head__title input");

    public OutcomingPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(OUTCOMING_LINK_LOCATOR).click();
    }

    public void deleteAllLetter() {

        waitForElementIsClickable(SELECT_ALL_CHECKBOX_LOCATOR);
        driver.findElement(SELECT_ALL_CHECKBOX_LOCATOR).click();
        driver.findElement(DELETE_ACTION_LOCATOR).click();

    }
}