package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.bo.LetterFactory;
import com.gomel.tat.home8.page.Yandexmail.ComposePage;
import com.gomel.tat.home8.page.Yandexmail.ConfirmPage;
import com.gomel.tat.home8.page.Yandexmail.InboxPage;
import com.gomel.tat.home8.page.Yandexmail.OutcomingPage;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SendLetterOnlyWithAddressTest extends LoginPositiveTest {

    private Letter letter;

    @BeforeMethod(description = "Preparing letter.")
    public void prepareData() {

        letter = LetterFactory.getRandomLetter();
        letter.setBody("");
        letter.setSubject("");
    }

    @Test(description = "Checking sending of full letter", groups = "only address")
    public void checkSendLetterWithAdress() throws InterruptedException {

        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        ConfirmPage confirmPage = new ConfirmPage(driver);
        String emailId = confirmPage.getEmailId();
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();

        Assert.assertTrue(inboxPage.isLetterPresent(emailId), "Letter should be present in inbox folder");
    }

    @AfterGroups("only address")
    public void deleteAllLetters() throws InterruptedException {

        driver.findElement(InboxPage.INBOX_LINK_LOCATOR).click();
        OutcomingPage outcomingPage = new OutcomingPage(driver);
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        inboxPage.deleteAllLetter();
        Thread.sleep(1000);
        outcomingPage.open();
        outcomingPage.deleteAllLetter();
        inboxPage.open();
        inboxPage.clearTrash();
    }
}
