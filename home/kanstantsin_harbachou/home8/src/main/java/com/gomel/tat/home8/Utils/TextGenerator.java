package com.gomel.tat.home8.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class TextGenerator {

    public static String createMessage(Integer lengthOfString) {
        return RandomStringUtils.randomAlphabetic(lengthOfString);
    }
}