package com.gomel.tat.home8.page.Yandexmail;

import com.gomel.tat.home8.page.Page;
import com.gomel.tat.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.gomel.tat.home8.utils.WebDriverHelper.waitForElementIsClickable;

public class InboxPage extends Page {

    public static final By USER_EMAIL_LOCATOR = By.cssSelector(".header-user-name.js-header-user-name");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By SELECT_ALL_CHECKBOX_LOCATOR = By.cssSelector("input.b-messages-head__checkbox");
    public static final By DELETE_ACTION_LOCATOR = By.cssSelector("[data-action=delete]");
    public static final By CLEAR_TRASH_LOCATOR = By.cssSelector(".b-folders__folder__info img");
    public static final By CONFIRM_CLEAR_TRASH_LOCATOR = By.cssSelector("div.b-popup__confirm button");

    List<WebElement> popupButton;

    public WebElement getUserEmail() {
        return userEmail;
    }

    WebElement userEmail = driver.findElement(USER_EMAIL_LOCATOR);

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(INBOX_LINK_LOCATOR).click();
    }

    public boolean isLetterPresent(String emailId) {

        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        WebElement findMailInbox = driver.findElement(By.xpath("//a[@href='" + emailId + "']"));
        return findMailInbox.isEnabled();
    }

    public void deleteAllLetter() {

        WebDriverHelper.waitForElementIsClickable(SELECT_ALL_CHECKBOX_LOCATOR);
        waitForElementIsClickable(SELECT_ALL_CHECKBOX_LOCATOR);
        driver.findElement(SELECT_ALL_CHECKBOX_LOCATOR).click();
        driver.findElement(DELETE_ACTION_LOCATOR).click();
    }

    public void clearTrash() {

        WebDriverHelper.waitForElementIsClickable(CLEAR_TRASH_LOCATOR);
        driver.findElement(CLEAR_TRASH_LOCATOR).click();
        popupButton = driver.findElements(CONFIRM_CLEAR_TRASH_LOCATOR);
        popupButton.get(0).click();
    }
}
