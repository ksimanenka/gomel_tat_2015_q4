package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.page.Yandexmail.LoginPage;
import com.gomel.tat.home8.utils.TextGenerator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginNegativeTest extends BaseYandexMailTest {

    @Test(description = "Negative Login Test")
    public void checkNegativeLogin() throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.wrongLogin(TextGenerator.createMessage(7), TextGenerator.createMessage(7));

        Assert.assertTrue(loginPage.getErrorMessage().isDisplayed());
    }
}
