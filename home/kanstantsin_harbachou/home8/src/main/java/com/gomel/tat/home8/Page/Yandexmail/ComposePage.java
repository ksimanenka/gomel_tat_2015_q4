package com.gomel.tat.home8.page.Yandexmail;

import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.gomel.tat.home8.utils.WebDriverHelper.waitForDisappear;
import static com.gomel.tat.home8.utils.WebDriverHelper.waitForElementIsClickable;

public class ComposePage extends Page {

    public static final By WRITE_NEW_MAIL_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By MAIL_ADDRESS_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By MAIL_SUBJECT_LOCATOR = By.name("subj");
    public static final By MAIL_FRAME_POST_LOCATOR = By.id("compose-send_ifr");
    public static final By MAIL_POST_LOCATOR = By.cssSelector("body#tinymce");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By NOTIFICATION_LOCATOR = By.cssSelector("span.b-notification__i");

    public WebElement getNotification() {
        return driver.findElement(NOTIFICATION_LOCATOR);
    }

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public void open() {

        waitForElementIsClickable(WRITE_NEW_MAIL_BUTTON_LOCATOR);
        WebElement composeButton = driver.findElement(WRITE_NEW_MAIL_BUTTON_LOCATOR);
        composeButton.click();
    }

    public ConfirmPage sendLetter(Letter letter) {

        waitForElementIsClickable(MAIL_ADDRESS_LOCATOR);
        WebElement toInput = driver.findElement(MAIL_ADDRESS_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(MAIL_SUBJECT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        driver.switchTo().frame(driver.findElement(MAIL_FRAME_POST_LOCATOR));
        driver.findElement(MAIL_POST_LOCATOR).sendKeys(letter.getBody());
        driver.switchTo().defaultContent();
        waitForElementIsClickable(SEND_MAIL_BUTTON_LOCATOR);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
        return new ConfirmPage(driver);
    }

    public ConfirmPage wrightLetter(Letter letter) {

        waitForElementIsClickable(MAIL_ADDRESS_LOCATOR);
        WebElement toInput = driver.findElement(MAIL_ADDRESS_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(MAIL_SUBJECT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        driver.switchTo().frame(driver.findElement(MAIL_FRAME_POST_LOCATOR));
        driver.findElement(MAIL_POST_LOCATOR).sendKeys(letter.getBody());
        driver.switchTo().defaultContent();
        return new ConfirmPage(driver);
    }

    public ComposePage sendEmptyLetter() {

        waitForElementIsClickable(SEND_MAIL_BUTTON_LOCATOR);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        return new ComposePage(driver);
    }
}
