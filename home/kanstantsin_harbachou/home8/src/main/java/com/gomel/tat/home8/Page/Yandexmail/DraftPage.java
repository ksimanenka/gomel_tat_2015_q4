package com.gomel.tat.home8.page.Yandexmail;

import com.gomel.tat.home8.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.gomel.tat.home8.utils.WebDriverHelper.waitForElementIsClickable;


public class DraftPage extends Page {

    public static final By DRAFT_LINK_LOCATOR = By.cssSelector("a[href$='#draft']");
    public static final By DELETE_BUTTON_LOCATOR = By.cssSelector("[data-action='delete']");


    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(DRAFT_LINK_LOCATOR).click();
    }

    public DraftPage deleteLetter(String emailId) {

        waitForElementIsClickable(By.xpath("//a[contains(@href,'" + emailId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        WebElement findMailDraft = driver.findElement(By.xpath("//a[contains(@href,'" + emailId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        findMailDraft.click();
        waitForElementIsClickable(DELETE_BUTTON_LOCATOR);
        WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR);
        deleteButton.click();
        return new DraftPage(driver);
    }

    public boolean isEnabled(String emailId) {

        try {
            WebElement findMailDraft = driver.findElement(By.xpath("//a[contains(@href,'" + emailId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
