package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.utils.InitialData;
import com.gomel.tat.home8.utils.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseYandexMailTest {

    protected static WebDriver driver;

    @BeforeClass(description = "Preparing browser")
    @Parameters({"browserName"})
    public void preparingBrowser(String browserName) throws MalformedURLException {

        if (driver == null) {
            if (browserName == "firefox") {
                driver = new RemoteWebDriver(new URL(InitialData.REMOTE_DRIVER_URL), DesiredCapabilities.firefox());
            } else {
                driver = new RemoteWebDriver(new URL(InitialData.REMOTE_DRIVER_URL), DesiredCapabilities.chrome());
            }
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(WebDriverHelper.DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
    }

    @AfterClass(description = "Finishing browser")
    public void finishingBrowser() {

        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            driver = null;
        }
    }
}
