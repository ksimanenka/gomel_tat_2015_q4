package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.page.Yandexmail.InboxPage;
import com.gomel.tat.home8.page.Yandexmail.LoginPage;
import com.gomel.tat.home8.utils.InitialData;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPositiveTest extends BaseYandexMailTest {

    @Test(description = "Positive Login Test")
    public void checkPositiveLogin() throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(InitialData.LOGIN_YANDEX_MAIL, InitialData.PASSWORD_YANDEX_MAIL);
        InboxPage inboxPage = new InboxPage(driver);
        WebElement userEmail = inboxPage.getUserEmail();

        Assert.assertEquals(userEmail.getText(), InitialData.LOGIN_YANDEX_MAIL + "@yandex.ru");
    }
}
