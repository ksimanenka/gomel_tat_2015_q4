package com.gomel.tat.home5.tests.listeners;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.xml.XmlSuite;

public class SuitListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(XmlSuite.ParallelMode.FALSE);
        suite.getXmlSuite().setThreadCount(0);
    }

    @Override
    public void onFinish(ISuite suite) {
    }
}
