package com.gomel.tat.home5.tests.runner;

import com.gomel.tat.home5.tests.listeners.FailedTestListener;
import com.gomel.tat.home5.tests.listeners.SuitListener;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) {

        TestListenerAdapter tla = new TestListenerAdapter();
        TestNG tng = new TestNG();
        tng.addListener(tla);
        tng.addListener(new FailedTestListener());
        tng.addListener(new FailedTestListener());
        tng.addListener(new SuitListener());

        XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");
        List<String> files = new ArrayList<>();
        files.addAll(new ArrayList<String>() {{
            add("./src/test/resources/testmanipulation.xml");
            add("./src/test/resources/parallel.xml");
            add("./src/test/resources/alltests.xml");
        }});
        suite.setSuiteFiles(files);
        suite.setParallel(XmlSuite.ParallelMode.METHODS);
        suite.setThreadCount(3);
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        tng.setXmlSuites(suites);

        tng.run();
    }
}
