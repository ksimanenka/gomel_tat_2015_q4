package com.gomel.tat.home5.tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class FileNameMaskTest extends BaseWordListTests {

    private String directoryPath;
    private String filePath;

    @Factory(dataProvider = "valuesPath")
    public FileNameMaskTest(String directoryPath, String filePath) {
        this.filePath = filePath;
        this.directoryPath = directoryPath;
    }

    @Test(description = "Checking all masks for file name")
    public void checkFileNameMask() {
        System.out.println("checkFileNameMask");
        wordList.getWords(directoryPath, filePath);
        Assert.assertEquals(false, wordList.getValues().isEmpty());
    }

    @DataProvider(name = "valuesPath")
    public static Object[][] valuesForCheck() {
        return new Object[][]{
                {"e:/gomel_tat_2015_q4/home", "home3/hello.txt"},
                {"e:/gomel_tat_2015_q4/home", "*.txt"},
                {"e:/gomel_tat_2015_q4/home", "*home*/*.txt"}
        };
    }
}
