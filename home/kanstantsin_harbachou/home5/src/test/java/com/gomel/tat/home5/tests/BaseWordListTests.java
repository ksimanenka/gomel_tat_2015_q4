package com.gomel.tat.home5.tests;

import com.gomel.tat.home5.code.WordList;
import com.gomel.tat.home5.tests.utils.PrepareData;
import com.gomel.tat.home5.tests.utils.Timeout;
import org.testng.annotations.*;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;

public class BaseWordListTests {
    protected WordList wordList;

    @BeforeSuite
    public void beforeSuit() {
        System.out.println("Before suit");
    }

    @BeforeClass
    public void beforeClass() {
        wordList = new WordList();
        System.out.println("Before class tests");
    }

    @BeforeMethod
    public void beforeMethod() {
        long id = Thread.currentThread().getId();
        System.out.println("Before method. Thread id is: " + id);
    }

    @BeforeGroups(groups = "Checking file's entry")
    public void beforeGroups() {
        System.out.println("Before groups \"Checking file's entry\"");
        PrepareData.createFolderAndFile();
        PrepareData.createFolderAndFillingFile();
    }

    @AfterMethod
    public void afterMethod() {
        long id = Thread.currentThread().getId();
        System.out.println("After method. Thread id is: " + id);
    }

    @AfterSuite
    public void afterSuit() {
        System.out.println("After suit");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("After class tests");
    }

    @AfterGroups(groups = "Checking file's entry")
    public void afterGroups() throws IOException {
        Timeout.sleep(1);
        Files.deleteIfExists(PrepareData.pathToEmptyFile);
        Files.deleteIfExists(PrepareData.pathToFillFile);
        System.out.println("After groups \"Checking file's entry\"");
    }

    protected void checkTime() {
        System.out.println("Current time: " + new Date(System.currentTimeMillis()));
    }
}
