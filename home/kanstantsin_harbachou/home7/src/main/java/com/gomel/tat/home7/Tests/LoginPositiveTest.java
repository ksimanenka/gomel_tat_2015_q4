package com.gomel.tat.home7.Tests;

import com.gomel.tat.home7.Locators.YandexMailLocator;
import com.gomel.tat.home7.Utils.InitialData;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPositiveTest extends BaseYandexMailTest {

    @Test(description = "Positive Login Test")
    public void checkPositiveLogin() throws InterruptedException {
        driver.findElement(YandexMailLocator.LOGIN_INPUT_LOCATOR).sendKeys(InitialData.LOGIN_YANDEX_MAIL);
        driver.findElement(YandexMailLocator.PASSWORD_INPUT_LOCATOR).sendKeys(InitialData.PASSWORD_YANDEX_MAIL);
        driver.findElement(YandexMailLocator.ENTER_BUTTON_LOCATOR).click();
        WebElement userEmail = driver.findElement(YandexMailLocator.USER_EMAIL_LOCATOR);

        Assert.assertEquals(userEmail.getText(), InitialData.LOGIN_YANDEX_MAIL + "@yandex.ru");
    }
}
