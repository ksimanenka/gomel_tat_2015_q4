package com.gomel.tat.home7.Tests;

import com.gomel.tat.home7.Locators.YandexMailLocator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendVoidEmailTest extends LoginPositiveTest {


    @Test(description = "Checking sending of void letter", groups = "letter group")
    public void checkSendVoidLetter() throws InterruptedException {
        waitForElementPresent(YandexMailLocator.WRITE_NEW_MAIL_LOCATOR);
        driver.findElement(YandexMailLocator.WRITE_NEW_MAIL_LOCATOR).click();
        waitForElementPresent(YandexMailLocator.MAIL_SUBMIT_LOCATOR);
        driver.findElement(YandexMailLocator.MAIL_SUBMIT_LOCATOR).click();

        Assert.assertTrue(driver.findElement(YandexMailLocator.NOTIFICATION_LOCATOR).isEnabled());
    }
}
