package com.gomel.tat.home7.Tests;

import com.gomel.tat.home7.Locators.YandexMailLocator;
import com.gomel.tat.home7.Utils.InitialData;
import com.gomel.tat.home7.Utils.TextGenerator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SavingLetterInDraftTest extends LoginPositiveTest {

    boolean isLetterInDraft, isLetterInTrash, isLetterInTrashAfterClear = false;

    @Test(description = "Checking saving letter in draft and deleting", groups = "letter group")
    public void checkSavingAndDeletingLetter() throws InterruptedException {
        waitForElementPresent(YandexMailLocator.WRITE_NEW_MAIL_LOCATOR);
        driver.findElement(YandexMailLocator.WRITE_NEW_MAIL_LOCATOR).click();
        waitForElementPresent(YandexMailLocator.MAIL_ADDRESS_LOCATOR);
        driver.findElement(YandexMailLocator.MAIL_ADDRESS_LOCATOR).sendKeys(InitialData.LOGIN_YANDEX_MAIL + "@yandex.ru");
        String subject = TextGenerator.createMessage(9);
        driver.findElement(YandexMailLocator.MAIL_SUBJECT_LOCATOR).sendKeys(subject);
        driver.switchTo().frame(driver.findElement(By.cssSelector("#compose-send_ifr")));
        driver.findElement(By.cssSelector("body#tinymce")).sendKeys(TextGenerator.createMessage(30));
        driver.switchTo().defaultContent();
        driver.findElement(YandexMailLocator.DRAFT_LINK_LOCATOR).click();
        List<WebElement> listForDelete = driver.findElements(By.cssSelector("div.b-popup__confirm button"));
        listForDelete.get(0).click();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        String letterLocatorPattern = "span[title='" + subject + "']";
        isLetterInDraft = driver.findElement(By.cssSelector(letterLocatorPattern)).isEnabled();
        driver.findElement(YandexMailLocator.DRAFT_MAIL_CHECKBOX).click();
        driver.findElement(YandexMailLocator.DELETE_ACTION_LOCATOR).click();
        driver.findElement(YandexMailLocator.DELETED_LINK_LOCATOR).click();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        isLetterInTrash = driver.findElement(By.cssSelector(letterLocatorPattern)).isEnabled();
        driver.findElement(YandexMailLocator.CLEAR_TRASH_LOCATOR).click();
        List<WebElement> listForClear = driver.findElements(By.cssSelector("div.b-popup__confirm button"));
        listForClear.get(0).click();
        driver.findElement(YandexMailLocator.DELETED_LINK_LOCATOR).click();
        driver.manage().timeouts().pageLoadTimeout(200, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
        isLetterInTrash = driver.findElement(By.cssSelector(letterLocatorPattern)).isDisplayed();
        isLetterInTrashAfterClear = !isLetterInTrash;


        Assert.assertTrue(isLetterInDraft && isLetterInTrash && isLetterInTrashAfterClear);
    }
}
