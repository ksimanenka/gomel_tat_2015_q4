package com.gomel.tat.home7.Tests;

import com.gomel.tat.home7.Locators.YandexMailLocator;
import com.gomel.tat.home7.Utils.TextGenerator;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginNegativeTest extends BaseYandexMailTest {

    @Test(description = "Negative Login Test")
    public void checkNegativeLogin() throws InterruptedException {
        driver.findElement(YandexMailLocator.LOGIN_INPUT_LOCATOR).sendKeys(TextGenerator.createMessage(5));
        driver.findElement(YandexMailLocator.PASSWORD_INPUT_LOCATOR).sendKeys(TextGenerator.createMessage(8));
        driver.findElement(YandexMailLocator.ENTER_BUTTON_LOCATOR).click();
        WebElement errorMessage = driver.findElement(YandexMailLocator.ERROR_MESSAGE);

        Assert.assertTrue(errorMessage.isDisplayed());
    }
}
