package com.gomel.tat.home7.Utils;

public class InitialData {

    public static final String LOGIN_YANDEX_MAIL = "mailfortest3";
    public static final String PASSWORD_YANDEX_MAIL = "passwordfortest3";
    public static final String GOOGLE_DRIVER_PATH = "./src/main/resources/chromedriver.exe";
    public static final String REMOTE_DRIVER_URL = "http://127.0.0.1:4444/wd/hub";
    public static final String YANDEX_START_PAGE = "http://yandex.ru";
    public static final String USING_BROWSER = "googlechrome"; // use "firefox" or "googlechrome"
}
