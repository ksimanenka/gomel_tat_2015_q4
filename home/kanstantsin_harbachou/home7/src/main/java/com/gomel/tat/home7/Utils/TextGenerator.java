package com.gomel.tat.home7.Utils;

import org.apache.commons.lang3.RandomStringUtils;

public class TextGenerator {

    public static String createMessage(Integer lengthOfString) {
        return RandomStringUtils.randomAlphabetic(lengthOfString);
    }
}