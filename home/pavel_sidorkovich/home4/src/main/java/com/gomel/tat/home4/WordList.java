package com.gomel.tat.home4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordList {

    private ArrayList<String> mList;
    private final static String FOLDER_ERROR = "No directory exists";
    private final static String FOLDER_EMPTY = "No files in directory";
    private final static String FILE_NAME_FORMAT = "Wrong filename format";
    private final static String FILENAME_SEPARATOR = "/";

    public void getWords(String baseDirectory, String fileNamePattern) {
        if (baseDirectory != null && baseDirectory.trim().length() > 0) {
            Pattern pattern;
            if (fileNamePattern != null && fileNamePattern.trim().length() > 0) {
                if (fileNamePattern.contains(FILENAME_SEPARATOR)) {
                    String[] folders = fileNamePattern.split(FILENAME_SEPARATOR);
                    if (folders.length > 2) {
                        System.out.println(FILE_NAME_FORMAT);
                        return;
                    }
                    baseDirectory += FILENAME_SEPARATOR + folders[0];
                    fileNamePattern = folders[1];
                }
                if (fileNamePattern.contains("*")) {
                    fileNamePattern = fileNamePattern.replace("*", ".*");
                }
                pattern = Pattern.compile(fileNamePattern);
            } else {
                return;
            }

            File folder = new File(baseDirectory);
            if (folder.exists() && folder.isDirectory()) {
                File[] listOfFiles = folder.listFiles();

                if (listOfFiles != null) {
                    for (File listOfFile : listOfFiles) {
                        if (listOfFile.isFile()) {
                            String temp = listOfFile.getName();
                            Matcher matcher = pattern.matcher(temp);
                            if (matcher.find()) {
                                readFromFile(listOfFile.getAbsolutePath());
                            }
                        }
                    }
                } else {
                    System.out.println(FOLDER_EMPTY);
                }
            } else {
                System.out.println(FOLDER_ERROR);
            }
        } else {
            System.out.println(FOLDER_ERROR);
        }
    }

    public void printStatistics() {
        if (mList == null || mList.size() < 1) {
            System.err.println("Empty list");
            return;
        }
        for (int i = 0; i < mList.size(); i++) {
            int numberOfEnters = 0;
            String item = mList.get(i);
            List<String> temp = mList.subList(i + 1, mList.size());
            for (String aTemp : temp) {

                if (item.equals(aTemp)) {
                    numberOfEnters++;
                }
            }
            if (numberOfEnters > 0) {
                System.out.println(item + " " + String.valueOf(numberOfEnters));
            }
        }
    }

    private void readFromFile(String fileName) {
        String result;

        try {
            if (fileName != null) {
                System.out.println(fileName);
                BufferedReader br = new BufferedReader(new FileReader(fileName));
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append(" ");
                    line = br.readLine();
                }
                result = sb.toString();

                if (mList == null) {
                    mList = new ArrayList<>();
                }
                String[] array = result.split(" ");
                Collections.addAll(mList, array);
            }
        } catch (IOException ex) {
            System.err.println(ex.toString());
        }
    }
}
