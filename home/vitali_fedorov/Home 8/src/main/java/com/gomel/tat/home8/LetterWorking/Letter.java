package com.gomel.tat.home8.LetterWorking;

/**
 * Created by user on 27.12.2015.
 */
public class Letter {
    private String to;

    private String subject;

    private String body;


    public Letter(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public Letter(String to) {
        this.to = to;

    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }
}
