package com.gomel.tat.home7.FirefoxTests;


import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by user on 24.12.2015.
 */
public class SendMailEmptyFieldsTest extends LoginPreparation {

    @Test
    public void EmptyFieldsTest() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(wrongMailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement attachFileButton = driver.findElement(ATTACH_LOCATOR);
        attachFileButton.sendKeys("D:\\test.jpg");
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.submit();
        Assert.assertEquals(driver.findElement(ERROR_NOTIFICATION).isDisplayed(), true, "This is the right email");

    }
}
