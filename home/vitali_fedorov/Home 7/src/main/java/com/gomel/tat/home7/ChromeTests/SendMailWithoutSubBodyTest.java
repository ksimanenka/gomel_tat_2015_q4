package com.gomel.tat.home7.ChromeTests;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by user on 24.12.2015.
 */
public class SendMailWithoutSubBodyTest extends LoginPreparation {

    @Test
    public void WithoutSubjectBodyTest(){
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        toInput.submit();
      //  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.submit();
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
        waitForElementIsClickable(INBOX_LINK_LOCATOR);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        Assert.assertEquals(driver.findElement(SEND_TEST_LETTER),true,"There is no send letter");

        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();

        Assert.assertEquals(driver.findElement(INBOX_TEST_LETTER),true,"There is no send letter");



    }

}
