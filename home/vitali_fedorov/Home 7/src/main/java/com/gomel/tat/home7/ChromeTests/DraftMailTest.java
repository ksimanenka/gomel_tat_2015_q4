package com.gomel.tat.home7.ChromeTests;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by user on 24.12.2015.
 */
public class DraftMailTest extends LoginPreparation {

    @Test
    public void DraftMailTest() {
        waitForElementIsClickable(DRAFT_LINK_LOCATOR);
        WebElement draftLink = driver.findElement(DRAFT_LINK_LOCATOR);
        draftLink.click();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        draftLink.click();
        Assert.assertEquals(driver.findElement(DRAFT_TEST_LETTER), true, "There is no draft letter");
        driver.findElement(CHECK_BOX_LOCATOR_DRAFT).click();
        WebElement delete_draft = driver.findElement(DRAFT_LINK_LOCATOR);
        delete_draft.click();
        Assert.assertEquals(driver.findElement(DRAFT_TEST_LETTER), false, "Your letter was not deleted");

    }

}

