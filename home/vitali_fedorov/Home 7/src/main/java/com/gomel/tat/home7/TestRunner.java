package com.gomel.tat.home7;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17.12.2015.
 */
public class TestRunner {
    public static void main(String[] args) {

        TestNG tng = new TestNG();

        XmlSuite suite = new XmlSuite();
        suite.setName("TMPSuite");


        List<String> files = new ArrayList<String>();
        files.addAll(new ArrayList<String>() {{
            add("./src/main/resources/suites/test.xml");
            add("./src/main/resources/suites/testFire.xml");
        }});
        suite.setSuiteFiles(files);


        suite.setParallel(XmlSuite.ParallelMode.METHODS);
        suite.setThreadCount(4);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);

        tng.setXmlSuites(suites);
        tng.run();
    }
}