package com.gomel.tat.home7.FirefoxTests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;

/**
 * Created by user on 24.12.2015.
 */
public class LoginPreparation extends BrowserPreparation {
    @BeforeMethod
    public void LoginRun() {
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(PositiveUserLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(PositiveUserPassword);
        passInput.submit();

    }
}
