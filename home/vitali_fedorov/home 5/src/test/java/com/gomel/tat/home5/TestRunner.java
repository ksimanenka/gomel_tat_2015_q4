package com.gomel.tat.home5;

import com.gomel.tat.home5.TestListeners.AlartTestListener;
import com.gomel.tat.home5.TestListeners.MyTestListener;
import org.testng.TestNG;
import org.testng.TestListenerAdapter;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17.12.2015.
 */
public class TestRunner {
    public static void main(String[] args) {
        TestListenerAdapter test= new TestListenerAdapter();
        TestNG tng = new TestNG();

        XmlSuite suite = new XmlSuite();
        suite.setName("TMPSuite");
        tng.addListener(test);

        tng.addListener(new AlartTestListener());
        tng.addListener(new MyTestListener());

        List<String> files = new ArrayList<String>();
        files.addAll(new ArrayList<String>() {{
            add("./src/test/resources/suites/test.xml");
        }});
        suite.setSuiteFiles(files);

        suite.setSuiteFiles(files);
        suite.setParallel(XmlSuite.ParallelMode.METHODS);
        suite.setThreadCount(4);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);

        tng.setXmlSuites(suites);
        tng.run();
    }
}