package com.gomel.tat.home5;

import junit.framework.Assert;

import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;

/**
 * Created by Vitali on 19.12.2015.
 */

public class ReadFileTest extends BaseWordListTest {
    @Test(groups = "FirstPriorityGroup")
    public void TestSplitReadWords() throws IOException {
        wordList.readWords(testReadFile);
        String result = "";
        for (Map.Entry<String, Integer> pair : wordList.getValues().entrySet()) {
            result += pair.getKey() + " ";
        }
        assertEquals(result, "HELLO hello ", "Wrong reading\\split of the file");
    }

    @Test(dependsOnGroups = "FirstPriorityGroup")
    public void TestInputMapReadWords() throws IOException {
        Map<String, Integer> mapExpected = new HashMap();
        mapExpected.put("HELLO", 1);
        mapExpected.put("hello", 1);
        wordList.readWords(testReadFile);
        assertEquals(wordList.getValues(), mapExpected, "Map has been created invalid");


    }

}
