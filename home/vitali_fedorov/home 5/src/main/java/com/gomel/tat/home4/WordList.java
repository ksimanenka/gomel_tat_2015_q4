package com.gomel.tat.home4;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordList {
    private Map<String, Integer> values = new HashMap<String, Integer>();

    public void getWords(String baseDirectory, String fileNamePattern) throws IOException {
        File folder = new File(baseDirectory);
        if (!folder.exists())
            throw new NullPointerException("Start folder is not found");
        File[] folderEntries = folder.listFiles();
        Pattern pattern = Pattern.compile(fileNamePattern);
        Matcher matcher;
        for (File entry : folderEntries) {
            matcher = pattern.matcher(entry.getPath());
            if (entry.isDirectory()) {
                getWords(entry.getPath(), fileNamePattern);

            } else if (matcher.find()) {
                readWords(entry);

            }
        }
    }


    public void readWords(File file) throws IOException {

        String oneLine = FileUtils.readFileToString(file, CommonFileUtils.getFileEncodingType(file.getPath()));
        String[] wordsArray = oneLine.split("[^a-zA-Z�-��-�]+");
        for (String word : wordsArray) {
            if (values.containsKey(word)) {
                values.put(word, values.get(word) + 1);
            } else {
                values.put(word, 1);
            }
        }
    }

    public Map<String, Integer> getValues() {
        return values;
    }

    public void printStatistics(Map<String, Integer> map) {
        for (Map.Entry<String, Integer> pair : map.entrySet()) {
            System.out.println(pair.getKey() + " " + pair.getValue());
        }
    }
}

class MostCommonValueFirst implements Comparator<String> {
    Map<String, Integer> base;

    public MostCommonValueFirst(Map<String, Integer> base) {
        this.base = base;
    }

    public int compare(String a, String b) {
        if (base.get(a) <= base.get(b)) {
            return 1;
        } else {
            return -1;
        }
    }
}


