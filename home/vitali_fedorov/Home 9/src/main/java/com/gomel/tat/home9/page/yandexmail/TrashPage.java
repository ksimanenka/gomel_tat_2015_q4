package com.gomel.tat.home9.page.yandexmail;

import com.gomel.tat.home9.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home9.BrowserPreparation.StartBrowserPreparation.waitForElementIsClickable;
import static com.gomel.tat.home9.BrowserPreparation.StartBrowserPreparation.waitForVisibilityLocated;

/**
 * Created by user on 09.01.2016.
 */
public class TrashPage extends Page {

    public static final By TRASH_ICON_LOCATOR = By.xpath("//div[@data-id='/trash']//div[contains(@data-params,'trash')]");
    public static final By TRASH_ICON_BUTTON_LOCATOR = By.xpath("//button[contains(@data-params,'/trash')]");
    public static final By UPLOADED_FILE_LOCATOR = By.xpath("//div[contains(@title,'test.txt')]");
    public static final By CLEAN_TRASH_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolset__buttons']//button[@data-click-action='trash.clean']");
    public static final By CLEAN_TRASH_ACCEPT_BUTTON_LOCATOR = By.xpath("//button[contains(@class,'accept')]");
    public static final By CLEAN_TRASH_NOTIFICATION_LOCATOR = By.xpath("//div[@data-key='view=notifications']");
    public static final By RESTORE_FILE_BUTTON_LOCATOR = By.xpath("//div[@class='nb-panel__actions b-aside__actions']//button[contains(@data-metrika-params,'restore')]");
    public static final By FILE_RESTORED_NOTIFICATION_LOCATOR = By.xpath("//div[@class='notifications__text js-message']");
    public static final By UPLOADED1_FILE_LOCATOR = By.xpath("//div[contains(@title,'test1.txt')]");
    public static final By UPLOADED2_FILE_LOCATOR = By.xpath("//div[contains(@title,'test2.txt')]");

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(TRASH_ICON_LOCATOR).click();
        waitForElementIsClickable(TRASH_ICON_BUTTON_LOCATOR).click();
    }

    public boolean assertRemove() {
        try {
            waitForElementIsClickable(UPLOADED_FILE_LOCATOR);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void finalRemove() {
        waitForElementIsClickable(CLEAN_TRASH_BUTTON_LOCATOR).click();
        waitForElementIsClickable(CLEAN_TRASH_ACCEPT_BUTTON_LOCATOR).click();
        waitForVisibilityLocated(CLEAN_TRASH_NOTIFICATION_LOCATOR);
    }

    public boolean assertFinalRemove() {

        return driver.findElement(UPLOADED_FILE_LOCATOR).isDisplayed();
    }

    public void restore() {
        waitForElementIsClickable(UPLOADED_FILE_LOCATOR).click();
        waitForElementIsClickable(RESTORE_FILE_BUTTON_LOCATOR).click();
        waitForVisibilityLocated(FILE_RESTORED_NOTIFICATION_LOCATOR);
    }

    public boolean assertMultiplyRemove() {
        try {
            waitForElementIsClickable(UPLOADED_FILE_LOCATOR);
            waitForElementIsClickable(UPLOADED1_FILE_LOCATOR);
            waitForElementIsClickable(UPLOADED2_FILE_LOCATOR);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}


