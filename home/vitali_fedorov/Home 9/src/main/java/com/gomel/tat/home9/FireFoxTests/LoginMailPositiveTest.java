package com.gomel.tat.home9.FireFoxTests;

import com.gomel.tat.home9.BrowserPreparation.StartBrowserPreparation;
import com.gomel.tat.home9.BrowserPreparation.StartingStandaloneServer;
import com.gomel.tat.home9.page.yandexmail.LoginPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by user on 27.12.2015.
 */
public class LoginMailPositiveTest extends StartingStandaloneServer {
    private String userLogin = "samagonvvv@yandex.ru";
    private String userPassword = "newzar1234";
    protected WebDriver driver;

    @BeforeClass
    public void prepareBrowser() throws IOException {
        driver = StartBrowserPreparation.getWebDriver(false);
    }


    @Test
    public void LoginMail() throws IOException {

        LoginPage loginPage = new LoginPage(driver);

        loginPage.open();
        loginPage.login(userLogin, userPassword);

        Assert.assertTrue(loginPage.loginConfirmation(), "You are not logged in");


    }




    @AfterClass
    public void shutdown() {
        StartBrowserPreparation.shutdownWebDriver();
    }


}
