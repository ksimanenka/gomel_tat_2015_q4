package com.gomel.tat.home9.page.yandexmail;

import com.gomel.tat.home9.page.Page;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.IOException;

import static com.gomel.tat.home9.BrowserPreparation.StartBrowserPreparation.*;

/**
 * Created by user on 07.01.2016.
 */
public class DiskPage extends Page {
    public static final By DISK_BUTTON_LOCATOR = By.xpath("//*[@href='https://disk.yandex.by?source=tab-mail']");
    public static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//input[@class='button__attach']");
    public static final By UPLOADED_FILE_LOCATOR = By.xpath("//div[contains(@title,'test.txt')]");
    public static final By UPLOADED1_FILE_LOCATOR = By.xpath("//div[contains(@title,'test1.txt')]");
    public static final By UPLOADED2_FILE_LOCATOR = By.xpath("//div[contains(@title,'test2.txt')]");
    public static final By UPLOADED_DONE_LOCATOR = By.xpath("//div[@class='b-item-upload__icon b-item-upload__icon_done']");
    public static final By CLOSE_UPLOAD_WINDOW = By.xpath("//a[@data-click-action='dialog.close']");
    public static final By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//button[contains(@data-metrika-params,'download')]");
    public static final By DELETE_NOTIFICATION_LOCATOR = By.xpath("//*[@data-key='view=notifications']");
    public static final By FILES_BUTTON_LOCATOR = By.xpath("//*[contains(@class,'b-crumbs__disk')]");
    public static final By TRASH_ICON_LOCATOR = By.xpath("//div[@data-id='/trash']//div[contains(@data-params,'trash')]");
    public static final By ALREADY_EXISTS_NOTIFICATION = By.xpath("//div[contains(@class,'upload_exists')]");
    public static final By REPLACE_BUTTON_LOCATOR = By.xpath("//div[contains(@class,'upload_exists')]");


    public static final String FILE_CONTENT = "Support";

    public static final File FILE_READY = new File("C:\\test.txt");
    public static final File FILE_DOWNLOADING = new File("C:\\text.txt.part");


    public DiskPage(WebDriver driver) {
        super(driver);
    }

    public void openFromMail() {
        waitForElementIsClickable(DISK_BUTTON_LOCATOR).click();
    }

    public void openFromDisk() {
        waitForElementIsClickable(FILES_BUTTON_LOCATOR).click();
    }

    public void uploadFile() {

        WebElement upload = waitForLocated(UPLOAD_BUTTON_LOCATOR);
        upload.sendKeys("c:\\Test\\test.txt");
        if (!driver.findElements(ALREADY_EXISTS_NOTIFICATION).isEmpty()) {
            waitForElementIsClickable(REPLACE_BUTTON_LOCATOR).click();
        }
        waitForLocated(UPLOADED_DONE_LOCATOR);
        driver.findElement(CLOSE_UPLOAD_WINDOW).click();
        driver.navigate().refresh();
    }

    public void multiplyUploadFile() {

        WebElement upload = waitForLocated(UPLOAD_BUTTON_LOCATOR);
        upload.sendKeys("c:\\Test\\test.txt");
        upload.sendKeys("c:\\Test\\test1.txt");
        upload.sendKeys("c:\\Test\\test2.txt");
        if (!driver.findElements(ALREADY_EXISTS_NOTIFICATION).isEmpty()) {
            waitForElementIsClickable(REPLACE_BUTTON_LOCATOR).click();
            if (!driver.findElements(ALREADY_EXISTS_NOTIFICATION).isEmpty()) {
                waitForElementIsClickable(REPLACE_BUTTON_LOCATOR).click();
                if (!driver.findElements(ALREADY_EXISTS_NOTIFICATION).isEmpty()) {
                    waitForElementIsClickable(REPLACE_BUTTON_LOCATOR).click();
                }
            }
        }
        waitForLocated(UPLOADED_DONE_LOCATOR);
        driver.findElement(CLOSE_UPLOAD_WINDOW).click();
        driver.navigate().refresh();
    }


    public boolean assert_Upload() {
        try {
            waitForLocated(UPLOADED_FILE_LOCATOR);

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean multiplyAssert_Upload() {
        try {
            waitForLocated(UPLOADED_FILE_LOCATOR);
            waitForLocated(UPLOADED1_FILE_LOCATOR);
            waitForLocated(UPLOADED2_FILE_LOCATOR);

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void downloadFile() {
        waitForElementIsClickable(UPLOADED_FILE_LOCATOR).click();
        driver.findElement(DOWNLOAD_BUTTON_LOCATOR).click();
    }

    public boolean checkDownloadFile() {
        try {
            Thread.sleep(1000);
            while (FILE_READY.exists()) {
                if (!FILE_DOWNLOADING.exists())
                    return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkContent(boolean fileExists) throws IOException {
        if (fileExists) {
            return (FileUtils.readLines(FILE_READY).get(0).equals(FILE_CONTENT));
        } else
            return false;
    }

    public void removeFile() {
        WebElement draggable = driver.findElement(UPLOADED_FILE_LOCATOR);
        WebElement dragTarget = driver.findElement(TRASH_ICON_LOCATOR);
        waitForElementIsClickable(UPLOADED_FILE_LOCATOR).click();
        Action dragAction = new Actions(driver).dragAndDrop(draggable, dragTarget).build();
        dragAction.perform();
        waitForVisibilityLocated(DELETE_NOTIFICATION_LOCATOR);
        waitForDisappear(DELETE_NOTIFICATION_LOCATOR);
    }

    public boolean assertRestoration() {
        return driver.findElement(UPLOADED_FILE_LOCATOR).isDisplayed();
    }

    public void multiplyRemovement() {
        WebElement testElement = driver.findElement(UPLOADED_FILE_LOCATOR);
        WebElement testElement1 = driver.findElement(UPLOADED1_FILE_LOCATOR);
        WebElement testElement2 = driver.findElement(UPLOADED2_FILE_LOCATOR);
        WebElement draggable = testElement;
        WebElement dragTarget = driver.findElement(TRASH_ICON_LOCATOR);

        waitForElementIsClickable(UPLOADED_FILE_LOCATOR);
        Action ctrlAndDragAction = new Actions(driver)
                .keyDown(Keys.CONTROL)
                .click(testElement)
                .click(testElement1)
                .click(testElement2)
                .keyUp(Keys.CONTROL)
                .dragAndDrop(draggable,dragTarget)
                .build();
        ctrlAndDragAction.perform();

       // Action dragAction = new Actions(driver).dragAndDrop(draggable, dragTarget).build();
       // dragAction.perform();

        waitForVisibilityLocated(DELETE_NOTIFICATION_LOCATOR);
        waitForDisappear(DELETE_NOTIFICATION_LOCATOR);
    }

}

