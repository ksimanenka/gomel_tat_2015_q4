package com.gomel.tat.home9.FireFoxTests;

import com.gomel.tat.home9.page.yandexmail.DiskPage;
import com.gomel.tat.home9.page.yandexmail.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by user on 10.01.2016.
 */
public class MultiplyRemovingTests extends LoginMailPositiveTest {
    @Test(priority = 0)
    public void multiplyUploadTest() {
        DiskPage diskPage = new DiskPage(driver);

        diskPage.openFromMail();
        diskPage.multiplyUploadFile();

        Assert.assertTrue(diskPage.multiplyAssert_Upload(), "Uploading failed.");
    }

    @Test(priority = 1)
    public void multiplyRemoveTest(){
        DiskPage diskPage = new DiskPage(driver);
        TrashPage trashPage = new TrashPage(driver);

        diskPage.multiplyRemovement();
        trashPage.open();

        Assert.assertTrue(trashPage.assertMultiplyRemove(), "This test failed.");

    }

}
