Regex
telephone number (mobile)
\+[0-9]{1,3}-?\([0-9]{1,3}\)-?[0-9]{1,3}-?[0-9]{3,4}
Checks:
+1-(800)-555-2468
+375(44)1234567
+375(44)1635894

email address
[a-zA-Z0-9]+.?([a-zA-Z0-9]+)?@[a-z]{2,7}.?[a-z]{2,}.?[a-z]{2,}
Checks:
foo@demo.net
bar.ba@test.co.uk
entes@yandex.by
vives-viti12@gmail.com

URL
http://[a-zA-Z0-9]+.?[a-z]{2,}/?.?[a-z]{2,}/?.?[a-z0-9]+\??_?/?-?([a-z]+)?/?=?-?([a-z]+)?.?([a-z]+)?
Checks:
http://foo.co.uk/
http://regexr.com/foo.html?q=bar
http://citforum.ru/internet/cgi_tut/url.shtml
http://stackoverflow.com/questions/30985866/error-expected-expression

