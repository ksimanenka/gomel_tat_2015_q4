package com.gomel.tat.home10.LetterWorking;

/**
 * Created by user on 27.12.2015.
 */
public class LetterFactory {

    public static Letter getPositiveRandomLetter() {
        String mailTo = "samagonvvv@yandex.ru";
        String mailSubject = "test subject" + Math.random() * 10000;
        String mailContent = "mail content" + Math.random() * 10000;
        return new Letter(mailTo, mailSubject, mailContent);
    }

    public static Letter getNegativeRandomLetter() {
        String mailTo = "JOHN_CENA!!";
        String mailSubject = "test subject" + Math.random() * 10000;
        String mailContent = "mail content" + Math.random() * 10000;
        return new Letter(mailTo, mailSubject, mailContent);
    }

    public static Letter getLetterWithoutSubBody(){
        String mailTo = "samagonvvv@yandex.ru";
        return new Letter(mailTo);
    }


}
