package com.gomel.tat.home10.FirefoxTests;


import com.gomel.tat.home10.LetterWorking.Letter;
import com.gomel.tat.home10.LetterWorking.LetterFactory;
import com.gomel.tat.home10.ui.page.yandexmail.*;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by user on 24.12.2015.
 */
public class SendMailWithProperFieldsTest extends LoginMailPositiveTest {
    private Letter letter;

    @BeforeClass
    public void prepareRandomLetter() {
        letter = LetterFactory.getPositiveRandomLetter();
    }

    @Test(priority = 0)
    public void CheckLetterInSent() {
        ComposePage composePage = new ComposePage(driver);
        SentPage sentPage = new SentPage(driver);

        composePage.open();
        composePage.sendLetter(letter, true);

        sentPage.open();
        Assert.assertTrue(sentPage.isLetterPresent(letter), "There is no such message in Sent");
    }

    @Test(priority = 1)
    public void CheckLetterInInbox() {
        ComposePage composePage = new ComposePage(driver);
        InboxPage inboxPage = new InboxPage(driver);
        DraftPage draftPage = new DraftPage(driver);
        TrashPage trashPage = new TrashPage(driver);

        inboxPage.open();
        Assert.assertTrue(composePage.isLetterHere(), "There is no such message in Inbox");


        


    }
}



