package com.gomel.tat.home10.lib.common.Service;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by user on 14.01.2016.
 */
public class WebDriverHelp {
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;


    public static WebElement waitForElementIsBeClickable(WebElement element, WebDriver driver) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(element));
        return element;
    }


//    public static WebElement waitForDisappear(WebElement element, WebDriver driver) {
//        new WebDriverWait(driver, 5000)
//                .until(ExpectedConditions.invisibilityOfElementLocated(element));
//        return element;
//    }


    public static WebElement waitForLocated(WebElement element, WebDriver driver) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.presenceOfElementLocated((By) element));
        return element;
    }
}
