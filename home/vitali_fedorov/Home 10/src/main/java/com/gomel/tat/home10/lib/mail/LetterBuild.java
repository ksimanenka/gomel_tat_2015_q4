package com.gomel.tat.home10.lib.mail;

import org.apache.commons.lang3.RandomStringUtils;

import static com.gomel.tat.home10.lib.common.CommonConstans.DEFAULT_MAIL_TO_SEND;

/**
 * Created by user on 14.01.2016.
 */
public class LetterBuild {


    public static Letter getLetter() {
        Letter letter = new Letter();
        letter.setRecipient(DEFAULT_MAIL_TO_SEND);
        letter.setSubject(RandomStringUtils.randomAlphabetic(11));
        letter.setBody(RandomStringUtils.randomAlphabetic(111));
        return letter;
    }
}
