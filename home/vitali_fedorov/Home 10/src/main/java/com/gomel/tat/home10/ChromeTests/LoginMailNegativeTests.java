package com.gomel.tat.home10.ChromeTests;

import com.gomel.tat.home10.BrowserPreparation.StartBrowserPreparation;
import com.gomel.tat.home10.BrowserPreparation.StartingStandaloneServer;
import com.gomel.tat.home10.ui.page.yandexmail.LoginPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by user on 27.12.2015.
 */
public class LoginMailNegativeTests extends StartingStandaloneServer {
    protected WebDriver driver;

    private String PositiveUserLogin = "samagonvvv@yandex.ru";
    private String NegativeUserLogin = "admin";
    private String NegativeUserPassword = "sdfsfwelolol";


    @BeforeClass
    public void prepareBrowser() throws IOException {
        driver = StartBrowserPreparation.getWebDriver(true);
    }

    @Test
    public void InvalidAccountTest() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(NegativeUserLogin, NegativeUserPassword);

        Assert.assertTrue(loginPage.errorMessage(), "It is the real login!");

    }

    @Test
    public void InvalidAccountPasswordTest() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(PositiveUserLogin, NegativeUserPassword);

        Assert.assertTrue(loginPage.errorMessage(), "It is the real login and password");
    }


    @AfterClass
    public void shutdown() {
        StartBrowserPreparation.shutdownWebDriver();
    }
}


