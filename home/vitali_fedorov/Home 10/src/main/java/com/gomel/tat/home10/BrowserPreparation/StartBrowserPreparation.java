package com.gomel.tat.home10.BrowserPreparation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 27.12.2015.
 */
public class StartBrowserPreparation {
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;

    private static WebDriver driver;

    public static WebDriver getWebDriver(boolean isChrome) throws IOException {
        if (driver == null) {
            if (isChrome)
                driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
            else
                driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        }
    }

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }


    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }


    public static WebElement waitForLocated(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator);
    }


}


