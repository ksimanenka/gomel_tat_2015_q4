package com.gomel.tat.home10.ChromeTests.Mail;


import com.gomel.tat.home10.ChromeTests.LoginMailPositiveTest;
import com.gomel.tat.home10.LetterWorking.Letter;
import com.gomel.tat.home10.LetterWorking.LetterFactory;
import com.gomel.tat.home10.ui.page.yandexmail.ComposePage;
import com.gomel.tat.home10.ui.page.yandexmail.InboxPage;
import com.gomel.tat.home10.ui.page.yandexmail.SentPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by user on 24.12.2015.
 */
public class SendMailWithoutSubBodyTest extends LoginMailPositiveTest {
    private Letter letter;

    @BeforeClass
    public void prepareRandomLetterWithoutSubBody() {
        letter = LetterFactory.getLetterWithoutSubBody();
    }

    @Test(priority = 0)
    public void checkInSentFolderTest() {
        ComposePage composePage = new ComposePage(driver);
        SentPage sentPage = new SentPage(driver);

        composePage.open();
        composePage.sendLetterWithoutBodySub(letter);

        sentPage.open();

        Assert.assertTrue(sentPage.isLetterWithoutBodySubPresent(), "You did not send the letter without subject");
    }

    @Test(priority = 1)
    public void checkInInboxFolderTest(){
        InboxPage inboxPage = new InboxPage(driver);
        ComposePage composePage = new ComposePage(driver);

        inboxPage.open();
        Assert.assertTrue(composePage.isLetterHere(), "There is no such message in Inbox");
    }
}
