package com.gomel.tat.home10.lib.mail.screen;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by user on 27.12.2015.
 */
public class LoginPage {

    @FindBy(xpath = "//a[contains(@href, 'mail.yandex')]")
    WebElement enterButton;

    @FindBy(name = "login")
    WebElement loginInput;

    @FindBy(name = "passwd")
    WebElement passwdInput;

    @FindBy(xpath = ".//*[@id='nb-1']/body/div/div[1]/div[2]/div/div/div[1]")
    WebElement errorLoginAssert;

    @FindBy(xpath = ".//*[@id='js-page']/div/div[3]/div[1]/a[2]/span[1]")
    WebElement positiveLoginAssert;

    public void enterLogin(String email) {
        loginInput.sendKeys(email);
    }


    public void enterPassword(String password) {
        passwdInput.sendKeys(password);
    }

    public void enterButtonClick() {
        enterButton.click();
    }

    public boolean isItError() {
        return errorLoginAssert.isDisplayed();
    }

    public boolean isItInbox() {
        return positiveLoginAssert.isDisplayed();
    }

}

//
//
//
//    public static final String BASE_URL = "http://www.ya.ru";
//
//
////    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
////    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
////    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
//   // public static final By ERROR_LOGIN_ASSERT_LOCATOR = By.xpath(".//*[@id='nb-1']/body/div/div[1]/div[2]/div/div/div[1]");
//   // public static final By POSITIVE_LOGIN_ASSERT_LOCATOR = By.xpath(".//*[@id='js-page']/div/div[3]/div[1]/a[2]/span[1]");
//
//    public static final int TIME_OUT_ERROR_APPEARS = 5;
//    public static final int TIME_OUT_LOGGING_IN = 10;
//
//
//    public LoginPage(WebDriver driver) {
//
//        super(driver);
//    }
//
//    public void open() {
//        driver.get(BASE_URL);
//        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
//        enterButton.click();
//
//    }
//
//    public InboxPage login(String userLogin, String userPassword) {
//        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
//        loginInput.sendKeys(userLogin);
//        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
//        passInput.sendKeys(userPassword);
//        passInput.submit();
//        return new InboxPage(driver);
//    }
//
//    public boolean errorMessage() {
//
//        try {
//            new WebDriverWait(driver, TIME_OUT_LOGGING_IN)
//                    .until(ExpectedConditions.presenceOfElementLocated(
//                            ERROR_LOGIN_ASSERT_LOCATOR));
//
//        } catch (Exception e) {
//            return false;
//        }
//        return true;
//    }
//
//    public boolean loginConfirmation() {
//
//        try {
//            new WebDriverWait(driver, TIME_OUT_ERROR_APPEARS)
//                    .until(ExpectedConditions.presenceOfElementLocated(
//                            POSITIVE_LOGIN_ASSERT_LOCATOR));
//
//        } catch (Exception e) {
//            return false;
//        }
//        return true;
//    }

