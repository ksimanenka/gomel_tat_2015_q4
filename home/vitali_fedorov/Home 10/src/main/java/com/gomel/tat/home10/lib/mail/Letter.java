package com.gomel.tat.home10.lib.mail;

import java.io.File;

/**
 * Created by user on 14.01.2016.
 */
public class Letter {
    private String recipient;
    private String subject;
    private String body;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
