package com.gomel.tat.home8.tests;


import com.gomel.tat.home8.ui.page.yandexmail.*;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendWithoutAddressTest extends BaseClass {

    private MenueToolsPage menueTools;

    @BeforeClass
    public void loginMail() {
        logOnMail();
        clickButtonEnter();
        menueTools = new MenueToolsPage(myDriver);
    }

    @BeforeMethod
    public void goOnMailPage() {
        menueTools.gotoInbox();
    }

    @Test(description = "Send message without address")
    public void fillNewMailFields() {
        NewMailPage newMail = new NewMailPage(myDriver);

        menueTools.clickButtonCompose();
        newMail.fillMailFields("");
        newMail.clickSendMailButton();
        WebElement notificationEmptyField = newMail.emptyFieldAddressNotification();
        Assert.assertNotNull(notificationEmptyField);
        System.out.println("Success send message without address");
    }
}
