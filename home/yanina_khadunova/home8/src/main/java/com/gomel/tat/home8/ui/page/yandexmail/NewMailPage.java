package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.util.MyWebDriver;
import org.openqa.selenium.*;

public class NewMailPage {

    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.xpath("//tr[@class='mceLast']//iframe");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By NOTIFICATION_FIELD_SUBJECT_NOT_FILL_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//span[contains(@class,'error_required')]");

    private String mailTo = "epamtest2015@yandex.ru";
    private String mailSubject = "test subject" + Math.random() * 100000000;
    private String mailContent = "mail content" + Math.random() * 100000000;

    protected MyWebDriver myDriver;

    public NewMailPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void fillMailFields(String mailTo, String mailSubject, String mailContent) {
        WebElement toInput = myDriver.waitForShow(TO_INPUT_LOCATOR);
        WebElement subjectInput = myDriver.getDriver().findElement(SUBJECT_INPUT_LOCATOR);
        WebElement mailContentText = myDriver.getDriver().findElement(MAIL_TEXT_LOCATOR);

        toInput.sendKeys(mailTo);
        subjectInput.sendKeys(mailSubject);
        mailContentText.sendKeys(mailContent);

    }

    public void fillMailFields(String mailSubject, String mailContent) {
        fillMailFields(mailTo, mailSubject, mailContent);

    }

    public void fillMailFields(String mailTo) {
        fillMailFields(mailTo, mailSubject, mailContent);

    }

    public void fillMailFields() {
        fillMailFields(mailTo, mailSubject, mailContent);

    }

    public void clickSendMailButton() {
        WebElement sendMailButton = myDriver.getDriver().findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

    }

    public String getMailIdfromUrl() {
        String currentUrl = myDriver.getDriver().getCurrentUrl();
        int index = currentUrl.indexOf("#");
        String mailId = currentUrl.substring(index + 9);
        return mailId;
    }

    public String getMailIdfromNotification(By locator) {
        WebElement notificationMailSend = myDriver.getDriver().findElement(locator);
        String mail = notificationMailSend.getAttribute("href");
        int index = mail.indexOf("#");
        String mailId = mail.substring(index + 9);
        return mailId;
    }

    public WebElement emptyFieldAddressNotification() {
        WebElement notificationAddressEmptyField = myDriver.getDriver().findElement(NOTIFICATION_FIELD_SUBJECT_NOT_FILL_LOCATOR);
        return notificationAddressEmptyField;
    }
}
