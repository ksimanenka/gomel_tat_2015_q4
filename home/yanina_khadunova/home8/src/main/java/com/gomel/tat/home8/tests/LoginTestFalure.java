package com.gomel.tat.home8.tests;

import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTestFalure extends BaseClass {
    public static final By ERROR_MESSAGE_LOCATOR = By.className("error-msg");


    @Test(description = "Failure mail login")
    public void loginMailFailure(){
        logOnMail("123");
        clickButtonEnter();
        WebElement errorMessage = myDriver.getDriver().findElement(ERROR_MESSAGE_LOCATOR);
        Assert.assertNotNull(errorMessage);
        System.out.println("Failure mail login");
    }
}
