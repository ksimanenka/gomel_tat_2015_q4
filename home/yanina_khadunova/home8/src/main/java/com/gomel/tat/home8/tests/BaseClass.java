package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.util.MyWebDriver;
import org.openqa.selenium.*;
import org.testng.annotations.*;

import java.net.MalformedURLException;

public class BaseClass {

    public static final String BASE_URL = "https://mail.yandex.by/";
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//form[@method='POST']/div[4]/span/button");
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//label[@id='nb-1']//input");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//label[@id='nb-2']//input");

    protected MyWebDriver myDriver;
    protected String userLogin = "epamtest2015@yandex.ru";
    protected String userPassword = "epam2015";

    @BeforeClass
    @Parameters({"webDriverName"})
    protected void SetupTestClass(String webDriverName) throws MalformedURLException {
        myDriver = new MyWebDriver(webDriverName);
    }

    public void logOnMail(String userPassword) {
        logOnMail(userLogin, userPassword);

    }

    public void logOnMail() {
        logOnMail(userLogin, userPassword);

    }

    public void logOnMail(String userLogin, String userPassword) {
        myDriver.getDriver().get(BASE_URL);

        WebElement loginInput = myDriver.waitForShow(LOGIN_INPUT_LOCATOR);
        WebElement passwordImput = myDriver.waitForShow(PASSWORD_INPUT_LOCATOR);

        loginInput.clear();
        loginInput.sendKeys(userLogin);
        passwordImput.sendKeys(userPassword);
    }

    public void clickButtonEnter() {
        WebElement buttonEnter = myDriver.getDriver().findElement(ENTER_BUTTON_LOCATOR);
        buttonEnter.click();

    }

    @AfterClass
    protected void ClearTestClass() {
        myDriver.getDriver().quit();
    }

}
