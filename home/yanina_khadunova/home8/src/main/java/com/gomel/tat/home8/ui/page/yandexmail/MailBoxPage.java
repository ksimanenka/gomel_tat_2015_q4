package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MailBoxPage {

    protected MyWebDriver myDriver;

    public MailBoxPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void selectMail (String mailId){
        WebElement findMailDraft = myDriver.waitForShow(By.xpath("//a[contains(@href,'" + mailId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        findMailDraft.click();

    }

    public WebElement findMail(String mailId) {
        WebElement findMailInbox =  myDriver.getDriver().findElement(By.xpath("//a[contains(@href,'" + mailId + "')]"));
        return findMailInbox;
    }
}
