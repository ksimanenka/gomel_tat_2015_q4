package com.gomel.tat.home8.tests;


import com.gomel.tat.home8.ui.page.yandexmail.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.*;

public class DraftMailTest extends BaseClass {

    public static final By NOTIFICATION_DRAFT_MAIL_CREATE_LOCATOR = By.xpath("//div[@data-compose-type='letter postcard']//span[contains(@data-action,'save-dropdown')]");
    public static final By NOTIFICATION_MAIL_DELETED_LOCATOR = By.xpath("//div[@class ='b-statusline']");

    private MenueToolsPage menueTools;

    @BeforeClass
    public void loginMail() {
        logOnMail();
        clickButtonEnter();
        menueTools = new MenueToolsPage(myDriver);
    }

    @BeforeMethod
    public void goOnMailPage() {
        menueTools.gotoInbox();
    }

    @Test(description = "Create and delete draft mail", expectedExceptions = NoSuchElementException.class)
    public void draftTest() throws Exception{
        NewMailPage newMail = new NewMailPage(myDriver);
        MailBoxPage mailBox = new MailBoxPage(myDriver);

        menueTools.clickButtonCompose();
        newMail.fillMailFields();
        myDriver.waitForShow(NOTIFICATION_DRAFT_MAIL_CREATE_LOCATOR, 20);
        String mailId = newMail.getMailIdfromUrl();

        menueTools.gotoDraft();
        mailBox.selectMail(mailId);
        menueTools.clickButtonDelete();
        myDriver.waitForShow(NOTIFICATION_MAIL_DELETED_LOCATOR);
        myDriver.waitForDisappear(NOTIFICATION_MAIL_DELETED_LOCATOR, 7);

        menueTools.gotoTrash();
        mailBox.selectMail(mailId);
        menueTools.clickButtonDelete();

        myDriver.waitForShow(NOTIFICATION_MAIL_DELETED_LOCATOR);
        try {
            mailBox.findMail(mailId);
        } catch (NoSuchElementException exc) {
            System.out.println("Success draft mail test");
            throw exc;
        }
    }
}
