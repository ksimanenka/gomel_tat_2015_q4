package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendWithoutAddressTest extends SendMailTestBase {

    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By NOTIFICATION_FIELD_SUBJECT_NOT_FILL_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//span[contains(@class,'error_required')]");

    // Test data
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @Test(description = "Send message without address")
    public void fillNewMailFields() {
        fillNewMailFields("", mailSubject, mailContent);

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        WebElement notificationEmptyField = driver.findElement(NOTIFICATION_FIELD_SUBJECT_NOT_FILL_LOCATOR);
        Assert.assertNotNull(notificationEmptyField);
        System.out.println("Success send message");
    }
}
