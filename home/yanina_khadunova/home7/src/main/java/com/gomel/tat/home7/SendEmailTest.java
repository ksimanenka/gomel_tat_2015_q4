package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SendEmailTest extends SendMailTestBase {
    // UI data
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INCOMING_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#inbox' and contains(@data-params,'folder')]");
    public static final By OUTCOMING_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#sent' and contains(@data-params,'folder')]");
    public static final By NOTIFICATION_MAIL_SEND_LOCATOR = By.xpath("//a[@class='b-statusline__link']");

    @Test(description = "Success send message", dataProvider = "sendMail")
    public void sendMessage(String mailTo, String mailSubject, String mailContent) throws InterruptedException {
        fillNewMailFields(mailTo, mailSubject, mailContent);

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        WebElement notificationMailSend = driver.findElement(NOTIFICATION_MAIL_SEND_LOCATOR);
        String mail = notificationMailSend.getAttribute("href");
        int index = mail.indexOf("#");
        String mailId = mail.substring(index);

        WebElement incomingMail = driver.findElement(INCOMING_MAIL_LINK_LOCATOR);
        incomingMail.click();
        WebElement findMailInbox = driver.findElement(By.xpath("//a[@href='" + mailId + "']"));

        WebElement outcomingMail = driver.findElement(OUTCOMING_MAIL_LINK_LOCATOR);
        outcomingMail.click();
        WebElement findMailSend = driver.findElement(By.xpath("//a[@href='" + mailId + "']"));

        Assert.assertNotNull(findMailInbox);
        Assert.assertNotNull(findMailSend);
        logOffMail();
        System.out.println("Success send message");
    }

    @DataProvider(name = "sendMail")
    public Object[][] valuesForMap() {

        String mailTo = "epamtest2015@yandex.ru";
        String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
        String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

        return new Object[][]{
                {mailTo, mailSubject, mailContent},
                {mailTo, "", ""}
        };
    }

}

