package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SendMailTestBase extends WebDriverTestBase {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.xpath("//tr[@class='mceLast']//iframe");

    private String userLogin = "epamtest2015@yandex.ru"; // ACCOUNT
    private String userPassword = "epam2015"; // ACCOUNT

    public void fillNewMailFields(String mailTo, String mailSubject, String mailContent) {
        logOnMail(userLogin, userPassword);

        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();

        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);

        toInput.sendKeys(mailTo);
        subjectInput.sendKeys(mailSubject);
        mailContentText.sendKeys(mailContent);
    }

}
