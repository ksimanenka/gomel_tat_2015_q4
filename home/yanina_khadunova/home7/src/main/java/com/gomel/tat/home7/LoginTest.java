package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends WebDriverTestBase {

    // UI data
    public static final By USER_NAME_HEADER_LOCATOR = By.xpath("//a[@id='nb-1']");
    public static final By ERROR_MESSAGE_LOCATOR = By.className("error-msg");

    // Test data
    private String userLogin = "epamtest2015@yandex.ru"; // ACCOUNT
    private String userPassword = "epam2015"; // ACCOUNT
    private String wrongUserPassword = "123"; // ACCOUNT

    @Test(description = "Success mail login")
    public void loginSuccess() throws InterruptedException {
        logOnMail(userLogin, userPassword);

        WebElement userNameHeader = driver.findElement(USER_NAME_HEADER_LOCATOR);
        Assert.assertEquals(userNameHeader.getText(), userLogin);
        logOffMail();
        System.out.println("Success mail login");
    }

    @Test(description = "Failure mail login")
    public void loginFailure() {
        logOnMail(userLogin, wrongUserPassword);

        WebElement errorMessage = driver.findElement(ERROR_MESSAGE_LOCATOR);
        Assert.assertNotNull(errorMessage);
        System.out.println("Failure mail login");
    }

}

