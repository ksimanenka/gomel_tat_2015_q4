package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class WebDriverTestBase {
    // AUT data
    public static final String BASE_URL = "https://mail.yandex.by/";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//form[@method='POST']/div[4]/span/button");
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//label[@id='nb-1']//input");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//label[@id='nb-2']//input");
    public static final By USER_PROFILE_LOCATOR = By.xpath("//span[contains(@class,'header-user-name')]");
    public static final By EXIT_LOCATOR = By.xpath("//a[contains(@href,'logout&uid')]");
    public static final By LOGO_LOCATOR = By.className("home-logo");

    // Tools data
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;

    protected WebDriver driver;

    @BeforeClass
    @Parameters({"webDriverName"})
    protected void SetupTestClass(String webDriverName) throws MalformedURLException {
        if (webDriverName.equalsIgnoreCase("firefox")) {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        } else if (webDriverName.equalsIgnoreCase("chrome")) {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        }

        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @AfterClass
    protected void ClearTestClass() {
        driver.close();
    }

    public void logOnMail(String userLogin, String UserPassword) {
        driver.get(BASE_URL);

        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        WebElement passwordImput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        WebElement buttonEnter = driver.findElement(ENTER_BUTTON_LOCATOR);

        loginInput.clear();
        loginInput.sendKeys(userLogin);
        passwordImput.sendKeys(UserPassword);
        buttonEnter.click();
    }

    public void logOffMail() throws InterruptedException {
        WebElement profile = driver.findElement(USER_PROFILE_LOCATOR);
        profile.click();
        WebElement exit = driver.findElement(EXIT_LOCATOR);
        exit.click();
        driver.findElement(LOGO_LOCATOR);
        Thread.sleep(1000);
    }
}
