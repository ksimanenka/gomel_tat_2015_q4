package com.gomel.tat.home5.test.testing;

import com.gomel.tat.home5.WordList;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;

public class ComposeWIMapDataProviderTest {

    @Test(dataProvider = "wCount")
    public void wordCount(List<String> wordList, Map<String, Integer> resultsMap) {
        Map<String, Integer> realMap = WordList.composeWordInclusionMap(wordList);
        Assert.assertEquals(realMap, resultsMap);
        System.out.println("wordCount-DataProvider");
    }

    @DataProvider(name = "wCount")
    public Object[][] valuesForMap() {

        Map<String, Integer> resultMap = new HashMap<String, Integer>();
        resultMap.put("Hello", 1);
        resultMap.put("Hi", 2);
        resultMap.put("No", 3);
        List<String> worldList = Arrays.asList("Hello", "No", "Hi", "No", "No", "Hi");

        Map<String, Integer> resultMap1 = new HashMap<String, Integer>();
        List<String> worldList1 = Arrays.asList();

        Map<String, Integer> resultMap2 = new HashMap<String, Integer>();
        resultMap2.put("Hello", 1);
        resultMap2.put("Hi", 1);
        resultMap2.put("No", 1);
        List<String> worldList2 = Arrays.asList("Hello", "No", "Hi");

        Map<String, Integer> resultMap3 = new HashMap<String, Integer>();
        resultMap3.put("No", 3);
        List<String> worldList3 = Arrays.asList("No", "No", "No");

        return new Object[][]{
                {worldList, resultMap},
                {worldList1, resultMap1},
                {worldList2, resultMap2},
                {worldList3, resultMap3}
        };
    }
}
