package com.gomel.tat.home5.test.testing.listeners;

import org.testng.*;
import org.testng.xml.XmlSuite;

import java.io.IOException;
import java.nio.charset.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


public class ListenerFailedTestMethod extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult tr) {

        StringBuilder info = new StringBuilder();
        info.append(tr.getTestClass().getName())
                .append(System.lineSeparator())
                .append(tr.getMethod().getMethodName())
                .append(System.lineSeparator())
                .append(tr.getThrowable().getMessage())
                .append(System.lineSeparator());

        StackTraceElement[] elements = tr.getThrowable().getStackTrace();
        for (StackTraceElement e : elements) {
            info.append(e.toString()).append(System.lineSeparator());
        }

        Path dir = Paths.get("C:", "Report", "Java");
        Path fileName = dir.resolve("FailureMessage.txt");
        List<String> lines = new ArrayList<String>();
        lines.add(info.toString());
        try {
            Files.createDirectories(dir);
            if (!Files.exists(fileName)) {
                Files.createFile(fileName);
            }
            Files.write(fileName, lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

