package com.gomel.tat.home5;

import javafx.scene.shape.*;
import org.apache.tools.ant.util.StringUtils;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordList {
    private List<String> values;
    private static final String ALL_ENTRIES_PATTERN = "*";

    public WordList() {
        values = new ArrayList<String>();
    }

    public void getWords(String baseDirectory, String fileNamePattern) throws IOException {
        if ((baseDirectory == null) || (baseDirectory.isEmpty())){
            throw new IllegalArgumentException("Base directory can't be null or empty");
        }

        List<Path> files = findAllFiles(baseDirectory, fileNamePattern);
        values = readAllWords(files);
    }

    public void printStatistics() {
        Map<String, Integer> map = composeWordInclusionMap(values);
        System.out.println(map);
    }

    public static List<Path> findAllFiles(String bDirectory, String fNPattern) throws IOException {
        Path directory = Paths.get(bDirectory);
        PathMatcher pattern = FileSystems.getDefault().getPathMatcher("glob:" + fNPattern);
        return findAllFiles(directory, pattern);
    }

    public static List<String> readAllWords(List<Path> files) throws IOException {
        List<String> allWords = new ArrayList<String>();
        for (Path entry : files) {
            List<String> words = readsWords(entry);
            allWords.addAll(words);
        }
        return allWords;
    }

    private static List<Path> findAllFiles(Path source, PathMatcher pm) throws IOException {
        List<Path> result = new ArrayList<Path>();
        DirectoryStream<Path> stream = Files.newDirectoryStream(source, ALL_ENTRIES_PATTERN);

        for (Path entry : stream) {
            if (Files.isDirectory(entry)) {
                List<Path> subfiles = findAllFiles(entry, pm);
                result.addAll(subfiles);
                continue;
            }
            if (pm.matches(entry.getFileName())) {
                result.add(entry);
            }
        }
        return result;
    }

    private static List<String> readsWords(Path source) throws IOException {
        byte[] buffer = Files.readAllBytes(source);
        StringBuilder builder = new StringBuilder();
        StringBuilder sbufferWord = new StringBuilder();
        ArrayList<String> list = new ArrayList<String>();
        Pattern patternLetter = Pattern.compile("[a-zA-Z]");

        for (int j = 0; j < buffer.length; j++) {
            builder.append((char) buffer[j]);
            char letter = builder.charAt(j);
            Matcher m = patternLetter.matcher(String.valueOf(letter));
            if (m.matches())
                sbufferWord.append(letter);
            else {
                String word = sbufferWord.toString();
                if (word.length() > 0) {
                    list.add(word);
                }
                sbufferWord.delete(0, j);
            }
        }
        String word = sbufferWord.toString();
        if (word.length() > 0) {
            list.add(word);
        }
        return list;
    }

    public static Map<String, Integer> composeWordInclusionMap(List<String> words) {
        if (words == null) {
            throw new IllegalArgumentException("Words can't be null");
        }

        Map<String, Integer> resultsMap = new HashMap<String, Integer>();

        for (String word : words) {
            if (resultsMap.containsKey(word)) {
                int count = resultsMap.get(word);
                count++;
                resultsMap.put(word, count);
            } else {
                resultsMap.put(word, 1);
            }
        }

        return resultsMap;
    }

}


