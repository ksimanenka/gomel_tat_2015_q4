package com.gomel.tat.home5;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class Runner {

    public static void main(String[] args) throws IOException {
        WordList wordList = new WordList();
        wordList.getWords("D://Java/TAT/gomel_tat_2015_q4/home/", "h*.txt");
        wordList.printStatistics();
    }
}
