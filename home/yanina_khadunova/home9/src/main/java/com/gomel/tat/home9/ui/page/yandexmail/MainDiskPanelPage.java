package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.*;

public class MainDiskPanelPage {

    protected MyWebDriver myDriver;
    public static final By DOWNLOAD_ONE_WITHOUT_PREVIEW_LOCATOR = By.xpath("//div[@data-key='box=boxAside']//button[@data-click-action='resource.download']");
    public static final By DELETE_SEVERAL_FILES_BUTTON_LOCATOR = By.xpath("//div[@data-key='box=boxAside']/div[contains(@class,'visible')]//button[contains(@data-metrika-params,'delete')]");
    public static final By DELETE_ONE_WITHOUT_PREVIEW_BUTTON_LOCATOR = By.xpath("//div[@data-key='box=boxAside']/div[contains(@class,'visible')]//button[@data-click-action='resource.delete']");
    public static final By CLEAR_TRASH_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='trash.clean']");

    public MainDiskPanelPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void downloadFile() {
        WebElement download = myDriver.waitForShow(DOWNLOAD_ONE_WITHOUT_PREVIEW_LOCATOR);
        download.click();
    }

    public void deleteSeveralFiles() {
        WebElement delete = myDriver.waitForShow(DELETE_SEVERAL_FILES_BUTTON_LOCATOR);
        delete.click();
    }

    public void deleteFile() {
        WebElement delete = myDriver.waitForShow(DELETE_ONE_WITHOUT_PREVIEW_BUTTON_LOCATOR);
        delete.click();
    }

    public void emptyTrash() throws InterruptedException {
        WebElement empty = myDriver.waitForElementIsClickable(CLEAR_TRASH_BUTTON_LOCATOR);
        empty.click();
    }
}
