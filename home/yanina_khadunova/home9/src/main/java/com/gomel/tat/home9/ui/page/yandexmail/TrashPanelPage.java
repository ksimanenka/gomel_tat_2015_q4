package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TrashPanelPage {
    protected MyWebDriver myDriver;
    public static final By DELETE_ONE_WITHOUT_PREVIEW_BUTTON_LOCATOR = By.xpath("//div[@data-key='box=boxAside']//div[contains(@class,'visible')]//button[@data-click-action='resource.delete' and contains(@data-params,'trash')]");
    public static final By RESTORE_BUTTON_LOCATOR = By.xpath("//div[@data-key='box=boxAside']//button[@data-click-action='resource.restore' and contains(@data-params,'trash')]");

    public TrashPanelPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void deleteFile() {
        WebElement delete = myDriver.waitForShow(DELETE_ONE_WITHOUT_PREVIEW_BUTTON_LOCATOR,20);
        delete.click();
    }

    public void restoreFile() {
        WebElement delete = myDriver.waitForShow(RESTORE_BUTTON_LOCATOR);
        delete.click();
    }
}
