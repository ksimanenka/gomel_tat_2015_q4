package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.*;
import org.testng.annotations.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BaseClass {

    public static final String BASE_URL = "https://mail.yandex.by/";
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//form[@method='POST']/div[4]/span/button");
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//label[@id='nb-1']//input");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//label[@id='nb-2']//input");
    public static final By DISK_LINK_LOCATOR = By.xpath("//a[contains(@class,'item_disk')]");
    public static final By CLOSE_NOTIFICATION_LOCATOR = By.xpath("//a[contains(@data-metrika-params,'x-closed')]");
    protected MyWebDriver myDriver;
    protected String userLogin = "epamtest2015@yandex.ru";
    protected String userPassword = "epam2015";

    private Path testDirectoryUpload;
    private Path testDirectoryDownload;
    private Path testDirectory;
    private Path testFile1;
    private Path testFile2;


    @BeforeSuite
    public void beforeSuite() throws IOException {
        System.out.println("Before suite");
        testDirectory = Paths.get("C:", "home9");
        testDirectoryUpload = testDirectory.resolve("upload");
        testDirectoryDownload = testDirectory.resolve("download");
        Files.createDirectories(testDirectoryUpload);
        Files.createDirectories(testDirectoryDownload);
        testFile1 = testDirectoryUpload.resolve("home9.txt");
        testFile2 = testDirectoryUpload.resolve("home9copy.txt");
        Files.createFile(testFile1);
        Files.createFile(testFile2);
        PrintWriter home9 = new PrintWriter("C:\\home9\\upload\\home9.txt", "UTF-8");
        home9.println("Hello World!");
        home9.close();
        PrintWriter home9copy = new PrintWriter("C:\\home9\\upload\\home9copy.txt", "UTF-8");
        home9copy.println("Hello World!");
        home9copy.close();
    }

    @BeforeClass
    @Parameters({"webDriverName"})
    protected void SetupTestClass(String webDriverName) throws MalformedURLException {
        myDriver = new MyWebDriver(webDriverName);

    }

    public void logOnMail() {
        myDriver.getDriver().get(BASE_URL);

        WebElement loginInput = myDriver.waitForShow(LOGIN_INPUT_LOCATOR);
        WebElement passwordImput = myDriver.waitForShow(PASSWORD_INPUT_LOCATOR);
        WebElement buttonEnter = myDriver.getDriver().findElement(ENTER_BUTTON_LOCATOR);

        loginInput.clear();
        loginInput.sendKeys(userLogin);
        passwordImput.sendKeys(userPassword);
        buttonEnter.click();

    }

    public void goToDisk(){
        WebElement diskLink = myDriver.waitForShow(DISK_LINK_LOCATOR);
        diskLink.click();

    }

    public void closeNotification(){
        WebElement diskLink = myDriver.getDriver().findElement(CLOSE_NOTIFICATION_LOCATOR);
        diskLink.click();

    }

    @AfterClass
    protected void ClearTestClass() {
        myDriver.getDriver().quit();

    }

    @AfterSuite
    public void afterSuite() throws IOException {
        Files.deleteIfExists(testFile1);
        Files.deleteIfExists(testFile2);
        Files.deleteIfExists(testDirectoryUpload);
        Files.deleteIfExists(testDirectoryDownload);
        Files.deleteIfExists(testDirectory);
        System.out.println("After suite");
    }
}
