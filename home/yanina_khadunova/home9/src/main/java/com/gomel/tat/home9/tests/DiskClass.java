package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.*;
import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.*;

public class DiskClass {
    private MainDiskPage mainDiskPage;
    private UploadModalWindowPage uploadModalWindowPage;
    private TrashPage trashPage;
    private TrashPanelPage trashPanelPage;

    public WebElement upload(By bypath, String path, MyWebDriver webDriver) {
        mainDiskPage = new MainDiskPage(webDriver);
        uploadModalWindowPage = new UploadModalWindowPage(webDriver);

        mainDiskPage.uploadFiles().sendKeys(path);
        uploadModalWindowPage.uploadCheckComplete();
        uploadModalWindowPage.closeUploadModalWindow();
        WebElement uploadFile = mainDiskPage.findUploadFile(bypath);
        return uploadFile;
    }

    public void goToTrash(MyWebDriver webDriver) {
        mainDiskPage = new MainDiskPage(webDriver);
        WebElement trash = mainDiskPage.trash();
        Action goToTrash = new Actions(webDriver.getDriver()).doubleClick(trash).build();
        goToTrash.perform();

    }

    public void removeToTrash(WebElement uploadFile, MyWebDriver webDriver) {
        mainDiskPage = new MainDiskPage(webDriver);
        WebElement trash = mainDiskPage.trash();
        Action actionDelete = new Actions(webDriver.getDriver()).dragAndDrop(uploadFile, trash).build();
        actionDelete.perform();

    }

    public WebElement removePermanently(WebElement uploadFile, MyWebDriver webDriver, By bypath) throws InterruptedException {
        trashPage = new TrashPage(webDriver);
        trashPanelPage = new TrashPanelPage(webDriver);

        removeToTrash(uploadFile, webDriver);
        goToTrash(webDriver);

        WebElement uploadFileInTrash = trashPage.findUploadFile(bypath);
        uploadFileInTrash.click();

        trashPanelPage.deleteFile();
        WebElement notification = trashPage.fileDeletedNotification();
        return notification;

    }

    public void removeFromTrashPermanently(MyWebDriver webDriver, By bypath) throws InterruptedException {
        trashPage = new TrashPage(webDriver);
        trashPanelPage = new TrashPanelPage(webDriver);

        WebElement uploadFileInTrash = trashPage.findUploadFile(bypath);
        uploadFileInTrash.click();

        trashPanelPage.deleteFile();
        trashPage.fileDeletedNotification();

    }

}
