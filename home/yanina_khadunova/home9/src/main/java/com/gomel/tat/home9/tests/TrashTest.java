package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TrashTest extends BaseClass {
    public static final By UPLOAD_FILE_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/home9.txt')]");
    public static final By UPLOAD_FILE_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/home9.txt')]");
    public static String uploadFrom = "C:\\home9\\upload\\home9.txt";

    private MainDiskPage mainDiskPage;
    private UploadModalWindowPage uploadModalWindow;
    private TrashPage trashPage;
    private TrashPanelPage trashPanelPage;
    private DiskClass commonActions;

    @Test(description = "Trash test")
    public void trash() throws InterruptedException, IOException {
        mainDiskPage = new MainDiskPage(myDriver);
        uploadModalWindow = new UploadModalWindowPage(myDriver);
        trashPage = new TrashPage(myDriver);
        trashPanelPage = new TrashPanelPage(myDriver);
        commonActions = new DiskClass();

        logOnMail();
        goToDisk();
        mainDiskPage.diskLinkShow();
        WebElement uploadFile = commonActions.upload(UPLOAD_FILE_DISK_LOCATOR, uploadFrom, myDriver);

        WebElement trash = mainDiskPage.trash();
        Action actionDelete = new Actions(myDriver.getDriver()).dragAndDrop(uploadFile, trash).build();
        actionDelete.perform();
        Action goToTrash = new Actions(myDriver.getDriver()).doubleClick(trash).build();
        goToTrash.perform();

        WebElement uploadFileInTrash = trashPage.findUploadFile(UPLOAD_FILE_TRASH_LOCATOR);
        uploadFileInTrash.click();
        trashPanelPage.deleteFile();
        WebElement notification = trashPage.fileDeletedNotification();

        Assert.assertNotNull(uploadFile);
        Assert.assertNotNull(uploadFileInTrash);
        Assert.assertNotNull(notification);

        System.out.println("Trash test success");

    }
}
