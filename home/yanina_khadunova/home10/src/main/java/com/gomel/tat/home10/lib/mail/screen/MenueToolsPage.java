package com.gomel.tat.home10.lib.mail.screen;

import com.gomel.tat.home10.lib.ui.MyWebDriver;
import org.openqa.selenium.*;

public class MenueToolsPage {
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//div[2]/a[2]");
    public static final By DRAFT_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#draft' and contains(@data-params,'folder')]");
    public static final By TRASH_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#trash' and contains(@data-params,'folder')]");
    public static final By INCOMING_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");
    public static final By OUTCOMING_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#sent' and @data-action='move']");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//div[2]/a[8]");
    public static final By USER_NAME_HEADER_LOCATOR = By.xpath("//a[@id='nb-1']");

    private MyWebDriver myDriver;

    public MenueToolsPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void clickButtonCompose() {
        WebElement sendMailButton = myDriver.waitForShow(SEND_MAIL_BUTTON_LOCATOR, 20);
        sendMailButton.click();
    }

    public void clickButtonDelete() {
        WebElement deletePermanentlyButton = myDriver.waitForShow(DELETE_BUTTON_LOCATOR);
        deletePermanentlyButton.click();
    }

    public void gotoDraft() {
        WebElement draftMail = myDriver.waitForShow(DRAFT_MAIL_LINK_LOCATOR);
        myDriver.clickOn(draftMail);
    }

    public void gotoTrash() {
        WebElement trashMail = myDriver.waitForShow(TRASH_MAIL_LINK_LOCATOR);
        myDriver.clickOn(trashMail);
    }

    public void gotoInbox() {
        WebElement inboxMail = myDriver.waitForShow(INCOMING_MAIL_LINK_LOCATOR);
        myDriver.clickOn(inboxMail);
    }

    public void gotoOutcoming() {
        WebElement outcomingMail = myDriver.getDriver().findElement(OUTCOMING_MAIL_LINK_LOCATOR);
        myDriver.clickOn(outcomingMail);
    }

    public String getUserIdentification() {
        WebElement identification = myDriver.waitForShow(USER_NAME_HEADER_LOCATOR);
        return identification.getText();
    }

}