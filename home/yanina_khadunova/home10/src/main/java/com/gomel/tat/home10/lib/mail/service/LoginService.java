package com.gomel.tat.home10.lib.mail.service;

import com.gomel.tat.home10.lib.common.service.Account;
import com.gomel.tat.home10.lib.mail.screen.LoginPage;
import com.gomel.tat.home10.lib.ui.MyWebDriver;

public class LoginService {
    protected MyWebDriver myDriver;

    public LoginService(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void loginToMail(Account account) {
        LoginPage loginPage = new LoginPage(myDriver);

        loginPage.typeUsername(account.getLogin());
        loginPage.typePassword(account.getPassword());
        loginPage.submitLogin();
    }

}
