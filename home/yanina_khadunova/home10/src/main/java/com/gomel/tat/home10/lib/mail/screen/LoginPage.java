package com.gomel.tat.home10.lib.mail.screen;

import com.gomel.tat.home10.lib.ui.MyWebDriver;
import org.openqa.selenium.By;

public class LoginPage {

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//form[@method='POST']/div[4]/span/button");
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//label[@id='nb-1']//input");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//label[@id='nb-2']//input");

    private MyWebDriver myDriver;

    public LoginPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void typeUsername(String username) {
        myDriver.getDriver().findElement(LOGIN_INPUT_LOCATOR).sendKeys(username);
    }

    public void typePassword(String password) {
        myDriver.getDriver().findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
    }

    public void submitLogin() {
        myDriver.getDriver().findElement(ENTER_BUTTON_LOCATOR).click();
    }

}
