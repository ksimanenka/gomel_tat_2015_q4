package com.gomel.tat.home10.test.mail;

import com.gomel.tat.home10.lib.mail.screen.MenueToolsPage;
import com.gomel.tat.home10.lib.mail.service.LoginService;
import org.testng.annotations.BeforeMethod;

import static com.gomel.tat.home10.lib.common.service.AccountBuilder.getDefaultAccount;

public class BaseLogin extends Base {

    @BeforeMethod
    public void goOnMailPage() {
        LoginService loginService = new LoginService(myDriver);
        MenueToolsPage menueToolsPage = new MenueToolsPage(myDriver);
        loginService.loginToMail(getDefaultAccount());
        menueToolsPage.gotoInbox();
    }

}
