package com.gomel.tat.home10.lib.mail.screen;

import com.gomel.tat.home10.lib.ui.MyWebDriver;
import org.openqa.selenium.By;

public class PassportPage {
    public static final By ERROR_MESSAGE_LOCATOR = By.className("error-msg");

    private MyWebDriver myDriver;

    public PassportPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public boolean errorMessage() {
        boolean locator;

        if (myDriver.getDriver().findElement(ERROR_MESSAGE_LOCATOR) != null) {
            locator = true;
        } else {
            locator = false;
        }
        return locator;
    }

}
