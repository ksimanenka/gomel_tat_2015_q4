package com.gomel.tat.home10.lib.mail.service;

import com.gomel.tat.home10.lib.mail.modelsObjects.Letter;
import com.gomel.tat.home10.lib.mail.screen.*;
import com.gomel.tat.home10.lib.ui.MyWebDriver;

public class MailService {
    private MyWebDriver myDriver;
    private NewMailPage newMailPage;

    public MailService(MyWebDriver myDriver) {
        this.myDriver = myDriver;
        newMailPage = new NewMailPage(myDriver);
    }

    public void fillMailFields(Letter letter) {
        newMailPage.typeMailTo(letter.getMailTo());
        newMailPage.typeSubject(letter.getMailSubject());
        newMailPage.typeContent(letter.getMailContent());
    }

    public void sendLetter(Letter letter) {
        MenueToolsPage menueToolsPage = new MenueToolsPage(myDriver);

        menueToolsPage.clickButtonCompose();
        fillMailFields(letter);
        newMailPage.clickSendMailButton();
    }

}
