package com.gomel.tat.home10.lib.mail.screen;

import com.gomel.tat.home10.lib.ui.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class NewMailPage {
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_CONTENT_LOCATOR = By.xpath("//tr[@class='mceLast']//iframe");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By NOTIFICATION_FIELD_SUBJECT_NOT_FILL_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//span[contains(@class,'error_required')]");
    public static final By NOTIFICATION_MAIL_SEND_LOCATOR = By.xpath("//a[@class='b-statusline__link']");

    private MyWebDriver myDriver;

    public NewMailPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void typeMailTo(String mailTo) {
        myDriver.waitForShow(TO_INPUT_LOCATOR).sendKeys(mailTo);
    }

    public void typeSubject(String subject) {
        myDriver.waitForShow(SUBJECT_INPUT_LOCATOR).sendKeys(subject);
    }

    public void typeContent(String content) {
        myDriver.waitForShow(MAIL_CONTENT_LOCATOR).sendKeys(content);
    }

    public void clickSendMailButton() {
        WebElement sendMailButton = myDriver.getDriver().findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }

    public String getMailIdfromUrl() {
        String currentUrl = myDriver.getDriver().getCurrentUrl();
        int index = currentUrl.indexOf("#");
        String mailId = currentUrl.substring(index + 9);
        return mailId;
    }

    public String getMailIdFromNotification() {
        WebElement notificationMailSend = myDriver.waitForShow(NOTIFICATION_MAIL_SEND_LOCATOR, 20);
        String mail = notificationMailSend.getAttribute("href");
        int index = mail.indexOf("#");
        String mailId = mail.substring(index + 9);
        return mailId;
    }

    public WebElement emptyFieldAddressNotificationShown() {
        WebElement notificationAddressEmptyField = myDriver.getDriver().findElement(NOTIFICATION_FIELD_SUBJECT_NOT_FILL_LOCATOR);
        return notificationAddressEmptyField;
    }

    public By notificationMailSend() {
        myDriver.waitForShow(NOTIFICATION_MAIL_SEND_LOCATOR, 40);
        return NOTIFICATION_MAIL_SEND_LOCATOR;
    }

}