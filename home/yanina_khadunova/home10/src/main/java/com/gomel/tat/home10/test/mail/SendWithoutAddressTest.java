package com.gomel.tat.home10.test.mail;

import com.gomel.tat.home10.lib.mail.screen.NewMailPage;
import com.gomel.tat.home10.lib.mail.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home10.lib.mail.modelsObjects.LetterBuilder.*;

public class SendWithoutAddressTest extends BaseLogin {

    @Test(description = "Send message without address")
    public void mailTest() {
        NewMailPage newMailPage = new NewMailPage(myDriver);
        MailService mailService = new MailService(myDriver);

        mailService.sendLetter(getLetterWithoutToSendField());

        Assert.assertNotNull(newMailPage.emptyFieldAddressNotificationShown());
        System.out.println("Success send message without address");
    }

}
