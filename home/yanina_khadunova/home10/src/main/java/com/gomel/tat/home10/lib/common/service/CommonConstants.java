package com.gomel.tat.home10.lib.common.service;

import org.apache.commons.lang3.RandomStringUtils;

public interface CommonConstants {

    String MAILBOX_URL = "https://mail.yandex.by/";
    String DEFAULT_MAIL_USER_LOGIN = "epamtest2015@yandex.ru";
    String DEFAULT_MAIL_USER_PASSWORD = "epam2015";

    String DEFAULT_MAIL_TO_SEND = "epamtest2015@yandex.ru";
    String DEFAULT_MAIL_SUBJECT = "test subject" + RandomStringUtils.randomAlphanumeric(10);
    String DEFAULT_MAIL_CONTENET = "mail content" + RandomStringUtils.randomAlphanumeric(100);

    String WITHOUT_MAIL_TO_SEND = "";
    String WITHOUT_MAIL_SUBJECT = "";
    String WITHOUT_MAIL_CONTENET = "";

}
