package com.gomel.tat.home4;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordList {
    private List<String> values;

    WordList() {
        values = new ArrayList<String>();
    }

    public void getWords(String baseDirectory, String fileNamePattern) throws IOException {
        Path directory = Paths.get(baseDirectory);
        PathMatcher pattern = FileSystems.getDefault().getPathMatcher("glob:" + fileNamePattern);
        List<Path> allfiles = findAllFiles(directory, pattern);
        values.clear();

        for (Path entry : allfiles) {
            List<String> words = readsWords(entry);
            values.addAll(words);
        }
    }

    private static List<Path> findAllFiles(Path source, PathMatcher pm) throws IOException {
        List<Path> result = new ArrayList<Path>();
        DirectoryStream<Path> stream = Files.newDirectoryStream(source, "*");

        for (Path entry : stream) {
            if (Files.isDirectory(entry)) {
                List<Path> subfiles = findAllFiles(entry, pm);
                result.addAll(subfiles);
                continue;
            }
            if (pm.matches(entry.getFileName())) {
                result.add(entry);
            }
        }
        return result;
    }

    private static List<String> readsWords(Path source) throws IOException {
        byte[] buffer = Files.readAllBytes(source);
        StringBuilder builder = new StringBuilder();
        StringBuilder sbufferWord = new StringBuilder();
        ArrayList<String> list = new ArrayList<String>();

        for (int j = 0; j < buffer.length; j++) {
            builder.append((char) buffer[j]);
            if ((builder.charAt(j) != '\r')
                    && (builder.charAt(j) != '\"')
                    && (builder.charAt(j) != '.')
                    && (builder.charAt(j) != ',')
                    && (builder.charAt(j) != '!')
                    && (builder.charAt(j) != '-')
                    && (builder.charAt(j) != '\'')
                    && (builder.charAt(j) != '1')
                    && (builder.charAt(j) != '2')
                    && (builder.charAt(j) != '3')
                    && (builder.charAt(j) != '4')
                    && (builder.charAt(j) != '5')
                    && (builder.charAt(j) != '6')
                    && (builder.charAt(j) != '7')
                    && (builder.charAt(j) != '8')
                    && (builder.charAt(j) != '9')
                    && (builder.charAt(j) != '0')
                    && (builder.charAt(j) != '\t')
                    && (builder.charAt(j) != '\n')
                    && (builder.charAt(j) != ' '))
                sbufferWord.append(builder.charAt(j));
            else {
                String str1 = sbufferWord.toString();
                if (str1.length() > 0) {
                    list.add(str1);
                }
                sbufferWord.delete(0, j);
            }
        }
        return list;
    }

    public void printStatistics() {
        Map<String, Integer> resultsMap = new HashMap<String, Integer>();

        for (String word : values) {
            if (resultsMap.containsKey(word)) {
                int count = resultsMap.get(word);
                count++;
                resultsMap.put(word, count);
            } else {
                resultsMap.put(word, 1);
            }
        }
        System.out.println(resultsMap);
    }

}


