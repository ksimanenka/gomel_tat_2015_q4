package com.gomel.tat.home7;


import org.testng.annotations.*;
import java.util.Date;



public class BaseTest {


    @BeforeSuite
    public void bs() {
        System.out.println("bs");
    }

    @BeforeClass
    public void bc() {
        System.out.println("bc");
    }

    @BeforeMethod
    public void bm() { System.out.println("bm"); }

    @BeforeGroups(value = "a")
    public void bg() {
        System.out.println("bg");
    }

    @BeforeClass(groups = "a")
    public void setUp() { System.out.println("Config1"); }

    @AfterMethod
    public void am() {
        System.out.println("am");
    }

    @AfterSuite
    public void as() {
        System.out.println("as");
    }

    @AfterClass
    public void ac() {
        System.out.println("ac");
    }

    protected void checkTime() {
        System.out.println("Current time: " + new Date(System.currentTimeMillis()));
    }

}
