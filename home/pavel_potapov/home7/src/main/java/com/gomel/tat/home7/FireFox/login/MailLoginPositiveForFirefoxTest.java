package com.gomel.tat.home7.FireFox.login;


import com.gomel.tat.home7.BeforePrepareBrowserFirefox;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;



public class MailLoginPositiveForFirefoxTest extends BeforePrepareBrowserFirefox {

    @Test(groups = "login", description = "Success mail login")
    public void MailLoginPositiveTest() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

        WebElement searchError = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        Assert.assertTrue(searchError.isDisplayed());

        System.out.println(" -- Success mail login");

    }

}