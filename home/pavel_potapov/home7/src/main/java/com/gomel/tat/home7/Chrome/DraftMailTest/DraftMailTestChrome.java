package com.gomel.tat.home7.Chrome.DraftMailTest;


import com.gomel.tat.home7.BeforePrepareBrowserChrome;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DraftMailTestChrome extends BeforePrepareBrowserChrome {

    @Test()
    public void TestCreateDraftMail() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);

        waitForShow(NOTIFICATION_DRAFT_MAIL_CREATE_LOCATOR);

        String currentUrl = driver.getCurrentUrl();
        int index = currentUrl.indexOf("#");
        String draftId = currentUrl.substring(index + 9);

        WebElement draftMail = driver.findElement(DRAFT_MAIL_LINK_LOCATOR);
        draftMail.click();
        WebElement findMailDraft = driver.findElement(By.xpath("//a[contains(@href,'" + draftId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        findMailDraft.click();
        WebElement deleteButton = driver.findElement(DELETE_MESSAGE_BUTTON_LOCATOR);
        deleteButton.click();

        waitForShow(NOTIFICATION_MAIL_DELETED_LOCATOR);

        WebElement deletedMail = driver.findElement(DELETED_MAIL_LINK_LOCATOR);
        deletedMail.click();
        WebElement findMailDeleted = driver.findElement(By.xpath("//a[contains(@href,'" + draftId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        findMailDeleted.click();
        WebElement deletePermanentlyButton = driver.findElement(DELETE_MESSAGE_BUTTON_LOCATOR);
        deletePermanentlyButton.click();

        Assert.assertTrue(driver.findElement(By.xpath("//a[contains(@href,'" + draftId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input")).isEnabled());
    }


    private WebElement waitForShow(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}