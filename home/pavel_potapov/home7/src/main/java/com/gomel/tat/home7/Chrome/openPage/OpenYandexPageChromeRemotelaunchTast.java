package com.gomel.tat.home7.Chrome.openPage;

import com.gomel.tat.home7.BeforePrepare;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Pavel on 23.12.2015.
 */
public class OpenYandexPageChromeRemotelaunchTast extends BeforePrepare {

    @Test(groups = "OpenPage", description = "Chrome remote launch")
    public void chromeRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444 -Dwebdriver.chrome.driver="d:\path\to\chromedriver.exe"
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.get(YANDEX_START_PAGE);
        System.out.println(" -- Chrome remote launch");
    }
}
