package com.gomel.tat.home7.FireFox.openPage;


import com.gomel.tat.home7.BeforePrepare;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class OpenYandexPageFirefoxRemotelaunchTast extends BeforePrepare {

    @Test(groups = "OpenPage", description = "Firefox remote launch")
    public void firefoxRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.get(YANDEX_START_PAGE);

        System.out.println(" -- Firefox remote launch");
    }

}
