package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;





public class BeforePrepare extends BaseTest {
    // AUT data
    public static final String BASE_URL = "http://www.ya.ru";
    public static final String YANDEX_START_PAGE = "http://ya.ru";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By ERROR_WINDOWS = By.xpath("//*[@class='error-isle js-error-isle']");
    public static final By RESTORE_WINDOWS = By.xpath("//*[@class='pseudo-button_small pseudo-button_remember']");
    public static final By ERROR_NO_ADDRESSES_LETTER = By.xpath("//*[@class='b-notification b-notification_error b-notification_error_required']");
    public static final By ERROR_WRONG_ADDRESSES_LETTER = By.xpath("//*[@class='b-notification b-notification_error b-notification_error_invalid']");
    public static final By STATUSLINE_LINK = By.xpath("//*[@class='b-statusline__link']");
    public static final By DRAFT_FOLDER_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final By POPUP_LOCATOR = By.xpath("//*[@class='b-popup']//*[@class='b-popup__box']");
    public static final By SAVE_BUTTON_LOCATOR = By.xpath("//button[@data-action='dialog.save']");
    public static final By DELETE_MESSAGE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//a[@data-action='delete']");
    public static final By TRASH_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#trash'");
    public static final By TRASH_LOCATOR1 = By.cssSelector("a[href$='#trash']");

    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INPOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");
    public static final By OUTPOX_LOCATOR = By.cssSelector("a[href$='#sent']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final String MAIL_DATA_LOCATOR_PATTERN = "//*[@class='b-messages__message__right']//span[contains(., '%s')]";
    public static final String MAIL_DATA_ID = "//*[@data-id='%s']";
    public static final String CHECKBOX_DRAFT = "//*[@class='b-messages__message__checkbox__input'and@value='%s']";
    public static final String MAIL_LINK_LOCATOR = "//*[@class='b-messages']//[contains(., '%s')]";

    public static final By DRAFT_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#draft' and contains(@data-params,'folder')]");
    public static final By DELETED_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#trash' and contains(@data-params,'folder')]");
    public static final By NOTIFICATION_DRAFT_MAIL_CREATE_LOCATOR = By.xpath("//div[@data-compose-type='letter postcard']//span[contains(@data-action,'save-dropdown')]");
    public static final By NOTIFICATION_MAIL_DELETED_LOCATOR = By.xpath("//div[@class ='b-statusline']");

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    protected WebDriver driver;

    // Test data
    protected String userLogin = "tat-test-user@yandex.ru"; // ACCOUNT
    protected String userLoginNegative = "negativeLogin"; // NEGATIVE ACCOUNT
    protected String userPassword = "tat-123qwe"; // ACCOUNT
    protected String mailTo = "tat-test-user@yandex.ru"; // ENUM
    protected String wrongUserMail = "wrongMail@123"; // WRONG ENUM
    protected String mailSubject = "subject" + Math.random() * 100000000; // RANDOM
    protected String mailContent = "content" + Math.random() * 100000000;// RANDOM

    public static final By SELECT_ALL_MAIL_LOCATOR = By.xpath("//*[@class='block-messages' and (not(@style) or @style='')]//*[contains(@class,'head')]//*[contains(@type, 'checkbox')]");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    public static final By STATUSLINE_LOCATOR = By.xpath("//div[@class='b-statusline']");
    public static final By CLEAR_TRASH_BUTTON_LOCATOR = By.xpath("//*[@data-action='folder.clear']");
    public static final By CONFIRM_CLEAR_BUTTON_LOCATOR = By.xpath("//*[@data-action='dialog.submit']");
    public static final By INBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");
    public static final By TRASH_FOLDER_LOCATOR = By.xpath("//a[@href='#trash']");


    protected WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    protected WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public void clearMailFolder(By locator) {
        waitForElementIsClickable(locator).click();
        waitForElementIsClickable(SELECT_ALL_MAIL_LOCATOR).click();
        WebElement deleteButtonOut = driver.findElement(DELETE_BUTTON_LOCATOR);
        deleteButtonOut.click();
        waitForDisappear(STATUSLINE_LOCATOR);
    }

    public void clearTrashbox() {
        waitForElementIsClickable(CLEAR_TRASH_BUTTON_LOCATOR).click();
        waitForElementIsClickable(CONFIRM_CLEAR_BUTTON_LOCATOR).click();
        waitForElementIsClickable(INBOX_FOLDER_LOCATOR).click();
    }

}

