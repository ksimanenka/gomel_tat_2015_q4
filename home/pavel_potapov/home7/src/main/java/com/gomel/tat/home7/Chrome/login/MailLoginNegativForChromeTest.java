package com.gomel.tat.home7.Chrome.login;

import com.gomel.tat.home7.BeforePrepareBrowserChrome;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;


public class MailLoginNegativForChromeTest extends BeforePrepareBrowserChrome {

    @Test(groups = "login", description = "Success mail login Negative")
    public void MailLoginNegativTest() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLoginNegative);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

        WebElement searchError = driver.findElement(ERROR_WINDOWS);
        Assert.assertTrue(searchError.isDisplayed());

        System.out.println(" -- Success mail login Negative");

    }


    @Test(groups = "login", description = "Success mail login Negative")
    public void MailLoginNegativTest2() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);

        loginInput.submit();

        WebElement searchError = driver.findElement(RESTORE_WINDOWS);
        Assert.assertTrue(searchError.isDisplayed());

        System.out.println(" -- Success mail login Negative");

    }

}