package com.gomel.tat.home11.lib.common;

import org.apache.commons.lang3.RandomStringUtils;

public class AccountBuilder {

    public static Account getDefaultAccount() {
        Account account = new Account();
        account.setLogin(CommonConstants.DEFAULT_MAIL_USER_LOGIN);
        account.setPassword(CommonConstants.DEFAULT_MAIL_USER_PASSWORD);
        account.setEmail(CommonConstants.DEFAULT_MAIL_TO_SEND);
        return account;
    }

    public static Account getAccountWithInvalidPassword() {
        Account account = getDefaultAccount();
        account.setPassword(account.getPassword() + RandomStringUtils.randomAlphabetic(4));
        return account;
    }

}