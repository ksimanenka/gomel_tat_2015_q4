package com.gomel.tat.home11.tests.mail;

import com.gomel.tat.home11.lib.mail.Letter;
import com.gomel.tat.home11.lib.mail.LetterBuilder;
import com.gomel.tat.home11.lib.mail.service.LetterService;
import com.gomel.tat.home11.lib.mail.service.LoginService;
import com.gomel.tat.home11.lib.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * Created by Pavel on 18.01.2016.
 */
@Listeners(TestListener.class)
public class testSendMailNegative {

    @BeforeClass
    public void prepeareForTests() {
        Browser.rise();
        LoginService loginService = new LoginService();
        loginService.loginToMailBox();
    }

    @Test(description = "Sent a letter with invalid address and check that Error Message appears")
    public void testSendMailNegative() {

        Letter letter = LetterBuilder.getLetterWithWrongAddress();
        LetterService letterService = new LetterService();
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isErrorWhileSend());
        letterService.cancelCompose();
    }

    @AfterClass
    public void closeWebdriver() {
        Browser.current().quit();
    }

}
