package com.gomel.tat.home11.tests.mail;

import com.gomel.tat.home11.lib.mail.Letter;
import com.gomel.tat.home11.lib.mail.LetterBuilder;
import com.gomel.tat.home11.lib.mail.screen.DraftPage;
import com.gomel.tat.home11.lib.mail.screen.TrashPage;
import com.gomel.tat.home11.lib.mail.service.LetterService;
import com.gomel.tat.home11.lib.mail.service.LoginService;
import com.gomel.tat.home11.lib.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

/**
 * Created by Pavel on 18.01.2016.
 */
@Listeners(TestListener.class)
public class testPermanentDeleteDraft {

    @BeforeClass
    public void prepeareForTests() {
        Browser.rise();
        LoginService loginService = new LoginService();
        loginService.loginToMailBox();
    }

    @Test(description = "Create a draft, delete it from Draft folder, delete it from Trash folder and check that it disappears in Trash folder")
    public void testPermanentDeleteDraft() {
        Letter letter = LetterBuilder.getTestLetter();
        LetterService letterService = new LetterService();
        letterService.createDraft(letter);
        letterService.deleteLetter(new DraftPage(), letter);
        letterService.deleteLetter(new TrashPage(), letter);
        Assert.assertTrue(!letterService.isLetterInFolder(new TrashPage(), letter));
    }

    @AfterClass
    public void closeWebdriver() {
        Browser.current().quit();
    }

}
