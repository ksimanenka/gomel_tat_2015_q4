package com.gomel.tat.home11.lib.config;

import com.gomel.tat.home11.lib.ui.BrowserType;

public class GlobalConfig {

    private static BrowserType selectedBrowser = BrowserType.FIREFOX; //default browser

    public static BrowserType getSelectedBrowser() {
        return selectedBrowser;
    }

    public static void setSelectedBrowser(String browserArgument) {
        if (browserArgument.toUpperCase().equals("CHROME")) {
            selectedBrowser = BrowserType.CHROME;
        } else {
            selectedBrowser = BrowserType.FIREFOX;
        }

    }
}