package com.gomel.tat.home11.lib.mail.service;

import com.gomel.tat.home11.lib.mail.Letter;
import com.gomel.tat.home11.lib.mail.screen.ComposePage;
import com.gomel.tat.home11.lib.mail.screen.Page;
import com.gomel.tat.home11.lib.ui.Browser;
import com.gomel.tat.home11.lib.util.Logger;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LetterService {
    private ComposePage composePage;
    private static Logger logger = new Logger();

    public void sendLetter(Letter letter) {
        composePage = new ComposePage();
        logger.log(" sending letter '" + letter.getSubject() + "'");
        composePage.sendLetter(letter);
        new WebDriverWait(Browser.current().getWrappedDriver(), 5, 1000);
    }

    public String sendLetterReturnId(Letter letter) {
        composePage = new ComposePage();
        logger.log(" return sending letter ID");
        return composePage.sendLetterReturnId(letter);
    }

    public void createDraft(Letter letter) {
        composePage = new ComposePage();
        logger.log(" creating draft '" + letter.getSubject() + "'");
        composePage.createDraft(letter);
        //Browser.current().makeScreenshot();
    }

    public void deleteLetter(Page page, Letter letter) {
        logger.log(" deleting letter '" + letter.getSubject() + "'");
        page.deleteLetterBySubj(letter);
        //Browser.current().makeScreenshot();
        new WebDriverWait(Browser.current().getWrappedDriver(), 6, 1000);
    }

    public void cancelCompose() {
        composePage.cancelCompose();
    }

    public boolean isLetterInFolder(Page page, Letter letter) {
        return page.isLetterPresent(letter);
    }

    public boolean isLetterInFolderWithId(Page page, Letter letter) {
        return page.isLetterIDPresent(letter);
    }

    public boolean isErrorWhileSend() {
        return composePage.isErrorSendingLetter();
    }

}