package com.gomel.tat.home11.lib.ui;

import com.gomel.tat.home11.lib.config.GlobalConfig;

import com.gomel.tat.home11.lib.util.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Browser implements WrapsDriver {

    public static final String DOWNLOAD_DIR = ".";
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 10;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 6;

    private WebDriver driver;
    private static Browser instance = null;
    private static Logger logger = new Logger();

    private Browser() {
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    public static Browser rise() {
        if (instance != null) {
            instance.quit();
        }
        Browser browser = new Browser();
        browser.createDriver();
        instance = browser;
        logger.log(" rising browser " + GlobalConfig.getSelectedBrowser().toString());
        return browser;
    }

    public static Browser current() {
        if (instance == null) {
            rise();
        }
        return instance;
    }

    public void quit() {
        logger.log(" closing browser \n");
        try {
            WebDriver wrappedDriver = getWrappedDriver();
            if (wrappedDriver != null) {
                wrappedDriver.quit();
            }
        } catch (Exception e) {
        } finally {
            instance = null;
        }
    }

    public void open(String url) {
        logger.log(" opening " + url);
        getWrappedDriver().get(url);
    }

    private static WebDriver firefoxLocal() {
        return new FirefoxDriver();
    }

    private static WebDriver chromeLocal() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        return new ChromeDriver();
    }

    private static WebDriver firefoxRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444

        standAloneServerLaunch(BrowserType.FIREFOX);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.useDownloadDir", true);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        profile.setPreference("browser.privatebrowsing.dont_prompt_on_enter", true);
        profile.setPreference("startup.homepage_welcome_url", "");
        profile.setPreference("startup.homepage_welcome_url.additional", "");
        profile.setEnableNativeEvents(true);
        DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
        firefoxCapabilities.setCapability("firefox_profile", profile);

        WebDriver driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), firefoxCapabilities);
        return driver;
    }

    private static WebDriver chromeRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444 -Dwebdriver.chrome.driver="d:\path\to\chromedriver.exe"

        standAloneServerLaunch(BrowserType.CHROME);
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome");
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.prompt_for_download", "false");
        prefs.put("download.default_directory", DOWNLOAD_DIR);
        options.setExperimentalOption("prefs", prefs);
        capability.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capability);
        return driver;
    }

    private static void standAloneServerLaunch(BrowserType browser) {
        String[] cmd;
        switch (browser) {
            default:
            case FIREFOX:
                cmd = new String[]{"java", "-jar", "./src/main/resources/selenium-standalone-server/selenium-server-standalone-2.48.2.jar", "-port4444"};
                break;
            case CHROME:
                cmd = new String[]{"java", "-jar", "./src/main/resources/selenium-standalone-server/selenium-server-standalone-2.48.2.jar", "-port4444", "-Dwebdriver.chrome.driver=./src/main/resources/chromedriver/chromedriver"};
                break;
        }
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {
            System.err.println("Problem with launching standalone-server: " + e.getMessage());
        }
    }

    private void createDriver() {
        this.driver = null;
        standAloneServerLaunch(GlobalConfig.getSelectedBrowser());
        try {
            switch (GlobalConfig.getSelectedBrowser()) {
                case CHROME:
                    driver = chromeRemoteLaunch();
                    break;
                case FIREFOX:
                    driver = firefoxRemoteLaunch();
                    break;
            }
        } catch (MalformedURLException e) {
            System.err.println("Problem with launching driver: " + e.getMessage());
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    public WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(getWrappedDriver(), 7).until(ExpectedConditions.elementToBeClickable(locator));
        return getWrappedDriver().findElement(locator);
    }

    public WebElement waitForElementIsDisplayed(By locator) {
        new WebDriverWait(getWrappedDriver(), 7).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return getWrappedDriver().findElement(locator);
    }

    public WebElement waitForDisappear(By locator) {
        new WebDriverWait(getWrappedDriver(), 7)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return getWrappedDriver().findElement(locator);
    }

    public void makeScreenshot(String name) {
        File scrFile = ((TakesScreenshot) getWrappedDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("screenshot " + name + ".png"));
        } catch (IOException e) {
        }
    }

}