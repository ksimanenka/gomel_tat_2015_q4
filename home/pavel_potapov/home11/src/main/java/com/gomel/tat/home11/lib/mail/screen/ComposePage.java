package com.gomel.tat.home11.lib.mail.screen;

import com.gomel.tat.home11.lib.common.CommonConstants;
import com.gomel.tat.home11.lib.mail.Letter;
import com.gomel.tat.home11.lib.ui.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ComposePage extends Page {
    private ComposeForm composeForm;

    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final By SAVE_DIALOG_POPUP_LOCATOR = By.xpath(".//p[text()='Сохранить сделанные изменения?']");
    public static final By SAVE_DIALOG_SAVE_BUTTON_LOCATOR = By.xpath(".//*[@data-action='dialog.save']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");


    public ComposePage() {

        super();
        this.compose();
    }

    public void sendLetter(Letter letter) {

        composeForm.send(letter);
        if (!isErrorSendingLetter()) Browser.current().waitForDisappear(CommonConstants.SEND_MAIL_BUTTON_LOCATOR);
    }

    public String sendLetterReturnId(Letter letter) {

        String messageID[], href;
        composeForm.send(letter);
        Browser.current().waitForElementIsDisplayed(CommonConstants.YOU_HAVE_GOT_MAIL_MESSAGE);
        WebElement recievedMessage = Browser.current().getWrappedDriver().findElement(By.xpath("//a[contains(text(),'от Sim Sim')]"));
        href = recievedMessage.getAttribute("href");
        messageID = href.split("#message/");
        return messageID[1];
    }


    public void cancelCompose() {

        composeForm.cancelCompose();
        Browser.current().waitForElementIsDisplayed(CommonConstants.SAVE_DIALOG_POPUP_LOCATOR);
        WebElement cancelButton = Browser.current().getWrappedDriver().findElement(CommonConstants.SAVE_DIALOG_CANCEL_BUTTON_LOCATOR);
        cancelButton.click();
    }

    public void createDraft(Letter letter) {

        composeForm.createDraft(letter);
        WebElement draftLink = Browser.current().getWrappedDriver().findElement(DRAFT_LINK_LOCATOR);
        draftLink.click();
        Browser.current().waitForElementIsDisplayed(SAVE_DIALOG_POPUP_LOCATOR);
        WebElement saveButton = Browser.current().getWrappedDriver().findElement(SAVE_DIALOG_SAVE_BUTTON_LOCATOR);
        saveButton.click();
        Browser.current().waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

    public boolean isErrorSendingLetter() {

        List<WebElement> search = Browser.current().getWrappedDriver().findElements(By.xpath("//*[contains(text(),'Некорректный адрес электронной почты')]"));
        int i = search.size();
        if (0 == i) return false;
        for (WebElement elementN : search) if (elementN.isDisplayed()) return true;
        return false;
    }


}