package com.gomel.tat.home11.tests.mail;

import com.gomel.tat.home11.lib.ui.Browser;
import com.gomel.tat.home11.lib.util.Logger;
import org.testng.*;

public class TestListener extends TestListenerAdapter {
    private static Integer screenShotNumber = 0;

    @Override
    public void onTestFailure(ITestResult result) {
        Logger logger = new Logger();
        logger.err("----------------------------------------------------");
        logger.err("Test failed: " +result.getName());
        logger.err("Message is: " +result.getThrowable().getMessage());
        logger.err("");
        //   logger.err("Stack trace is: " + result.getThrowable().getStackTrace());
        String screenShotName = (screenShotNumber++).toString();
        Browser.current().makeScreenshot(screenShotName);
        logger.err("Saved screenshot #'" + screenShotName + "'");
        logger.err("----------------------------------------------------");
    }
}