package com.gomel.tat.home11.lib.ui;

public enum BrowserType {
    FIREFOX("FIREFOX"),
    CHROME("CHROME");

    String name;

    BrowserType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}