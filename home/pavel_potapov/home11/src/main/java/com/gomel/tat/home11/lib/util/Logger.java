package com.gomel.tat.home11.lib.util;


public class Logger {

    public void log(String message) {
        String caller = "unknown";
        Throwable thr = new Throwable();
        StackTraceElement[] ste = thr.getStackTrace();
        int i = 1;
        while (i < ste.length && ste[i].getMethodName().startsWith("access$")) {
            ++i;
        }
        if (i < ste.length) {
            caller = ste[i].getMethodName();
        }
        System.out.println(caller + ": " + message);
    }

    public void err(String message) {
        System.out.println(message);
    }

}