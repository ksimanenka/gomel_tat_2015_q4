package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.LoginPage;
import org.testng.annotations.Test;

/**
 * Created by Pavel on 13.01.2016.
 */
public class LoginPositiveTest extends BaseTest {

    private LoginPage myLoginPage;


    @Test
    public void LoginPositiveTest() {
        myLoginPage = new LoginPage(myDriver);

        myLoginPage.open().login(userLogin,userPassword).diskLinkShow();
        myLoginPage.isSucceffullyLogin();
        System.out.println("Login positive test successfully completed");
    }
}
