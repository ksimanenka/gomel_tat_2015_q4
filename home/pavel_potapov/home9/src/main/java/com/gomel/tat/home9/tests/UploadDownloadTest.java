package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.LoginPage;
import com.gomel.tat.home9.ui.page.yandexmail.MainPage;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Pavel on 13.01.2016.
 */
public class UploadDownloadTest extends BaseTest {
    private LoginPage myLoginPage;
    private MainPage myMainPage;
    private static final String uploadFrom = "D:\\TAThome9\\upload\\HomeTask9.txt";
    private static final String nameUploadFile = "HomeTask9.txt";
    private static final String expecteContentFile = "Hello Home Task 9 !";

    private static final Path pathDownload = Paths.get("D:\\TAThome9\\download\\HomeTask9.txt");
    private static final Path pathDownloadCopy = Paths.get("D:\\TAThome9\\download\\HomeTask9.txt");
    private static final Path pathUpload = Paths.get("D:\\TAThome9\\upload\\HomeTask9.txt");



    @BeforeMethod(description = "Creating directories and files to upload")
    public void createDirectoriesAndFiles() throws IOException {
        if (!Files.exists(pathUpload)){
            myMainPage = new MainPage(myDriver);
            myMainPage.createDirectoriesAndFiles(expecteContentFile);
        }
    }


    @Test
    public void UploadDownloadTest() throws InterruptedException, IOException {
        myLoginPage = new LoginPage(myDriver);
        myMainPage = new MainPage(myDriver);

        myLoginPage.open()
                .login(userLogin,userPassword).diskLinkShow()
                .load(uploadFrom,nameUploadFile);

        myMainPage.download(nameUploadFile,pathDownload);
        myMainPage.contentInspectionFile(pathDownload,expecteContentFile);

        System.out.println("Upload and download file test successfully completed");
    }

    @AfterMethod(description = "Delete the test data")
    public void deletedTestData() throws IOException, InterruptedException {
        myMainPage = new MainPage(myDriver);
        myMainPage.deletedFiles(pathUpload,pathDownload,pathDownloadCopy,nameUploadFile);
    }
}
