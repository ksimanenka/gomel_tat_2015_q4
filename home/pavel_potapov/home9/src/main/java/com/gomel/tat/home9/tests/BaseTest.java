package com.gomel.tat.home9.tests;



import com.gomel.tat.home9.util.WebDriverHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;


import java.net.MalformedURLException;

/**
 * Created by Pavel on 12.01.2016.
 */
public class BaseTest {

    protected WebDriverHelper myDriver;

    public static final String userLogin = "tat-test-user@yandex.ru";
    public static final String userPassword = "tat-123qwe";

    @BeforeClass
    @Parameters({"webDriverName"})
    protected void SetupDriverNameClass(String webDriverName) throws MalformedURLException {
        myDriver = new WebDriverHelper(webDriverName);
    }



    @AfterClass
    protected void ClearTestClass() {
        myDriver.getDriver().quit();

    }
}
