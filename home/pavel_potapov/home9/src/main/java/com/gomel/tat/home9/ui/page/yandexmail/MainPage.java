package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;


import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Pavel on 12.01.2016.
 */
public class MainPage {

    public static WebDriverHelper myDriver;

    public static final By DISK_LINK_LOCATOR = By.xpath("//div[@class='header__side-left']/a[2]");
    public static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//div[@class='header']//input[@type='file']");
    public static final By CHECK_COMPLETE_LOCATOR = By.xpath("//div[contains(@class,'icon_done')]");
    public static final By CLOSE_BUTTON_LOCATOR = By.xpath("//div[@class='b-dialog-upload__actions']/button[4]");
    public static final By DOWNLOAD_ONE_WITHOUT_PREVIEW_LOCATOR = By.xpath("//div[@data-key='box=boxAside']//button[@data-click-action='resource.download']");


    public static final String UPLOAD_FILE_DISK_LOCATOR = "//div[contains(@data-id,'disk/%s')]";
    public static final String UPLOAD_FILE_TRASH_LOCATOR_STRING = "//div[contains(@data-id,'trash/%s')]";


    public MainPage(WebDriverHelper myDriver) {
        this.myDriver = myDriver;
    }


    public void load(String uploadFrom,String nameUploadFile) {
        uploadFiles().sendKeys(uploadFrom);
        uploadCheckComplete();
        closeWindowDownload();
        WebElement uploadFile = findUploadFile(By.xpath(String.format(UPLOAD_FILE_DISK_LOCATOR,nameUploadFile)));
        Assert.assertNotNull(uploadFile);
    }

    public MainPage download(String nameUploadFile, Path path_download) throws InterruptedException {
        WebElement uploadFile = findUploadFile(By.xpath(String.format(UPLOAD_FILE_DISK_LOCATOR,nameUploadFile)));
        uploadFile.click();
        downloadFile();

        while (!Files.exists(path_download)) {
            Thread.sleep(1000);
        }

        return this;
    }

    public MainPage diskLinkShow() {
        myDriver.waitForShow(DISK_LINK_LOCATOR, 20);
        return this;
    }

    public WebElement uploadFiles() {
        WebElement upload = myDriver.getDriver().findElement(UPLOAD_BUTTON_LOCATOR);
        return upload;
    }

    public void uploadCheckComplete() {
        myDriver.waitForShow(CHECK_COMPLETE_LOCATOR, 90);
    }

    public void closeWindowDownload() {
        WebElement closeUploadModalWindow = myDriver.waitForShow(CLOSE_BUTTON_LOCATOR);
        closeUploadModalWindow.click();
    }

    public static WebElement findUploadFile(By path) {
        WebElement uploadFile = myDriver.waitForShow(path, 20);
        return uploadFile;

    }

    public void downloadFile() {
        WebElement download = myDriver.waitForShow(DOWNLOAD_ONE_WITHOUT_PREVIEW_LOCATOR);
        download.click();
    }

    public void contentInspectionFile(Path pathDownload,String expecteContentFile) throws IOException {
        String realCcontentFile = fileContent(pathDownload);
        Assert.assertEquals(realCcontentFile, expecteContentFile,"The contents of files are not the same");

    }



        public String fileContent(Path path) throws IOException {
            Charset charset = Charset.forName("UTF-8");
            List<String> content = Files.readAllLines(path, charset);
            StringBuilder sbuilder = new StringBuilder();
            for (String s : content) {
                sbuilder.append(s);
            }
            return sbuilder.toString();

        }

    public void deletedFiles(Path pathUpload,Path pathDownload,Path pathDownloadCopy,String nameUploadFile) throws IOException, InterruptedException {
        Files.deleteIfExists(pathUpload);
        Files.deleteIfExists(pathDownload);
        Files.deleteIfExists(pathDownloadCopy);

        TrashPage myTrashPage = new TrashPage(myDriver);

        WebElement uploadedFile = findUploadFile(By.xpath(String.format(UPLOAD_FILE_DISK_LOCATOR,nameUploadFile)));
        By UPLOAD_FILE_TRASH_LOCATOR = By.xpath(String.format(UPLOAD_FILE_TRASH_LOCATOR_STRING,nameUploadFile));
        myTrashPage.removePermanently(uploadedFile, myDriver, UPLOAD_FILE_TRASH_LOCATOR);
    }


    public void createDirectoriesAndFiles(String expecteContentFile) throws IOException {
        Path FolderUpload;
        Path FolderDownload;
        Path Folder;
        Path File1;
        Path File2;

        Folder = Paths.get("D:", "TAThome9");
        FolderUpload = Folder.resolve("upload");
        FolderDownload = Folder.resolve("download");
        Files.createDirectories(FolderUpload);
        Files.createDirectories(FolderDownload);
        File1 = FolderUpload.resolve("HomeTask9.txt");
        File2 = FolderUpload.resolve("HomeTask9copy.txt");

        Files.createFile(File1);
        PrintWriter home9 = new PrintWriter("D:\\TAThome9\\upload\\HomeTask9.txt", "UTF-8");
        home9.println(expecteContentFile);
        home9.close();

        Files.createFile(File2);
        PrintWriter home9copy = new PrintWriter("D:\\TAThome9\\upload\\HomeTask9copy.txt", "UTF-8");
        home9copy.println("Hello Home Task 9 !");
        home9copy.close();
    }
}
