package com.gomel.tat.home9.ui.page.yandexmail;


import com.gomel.tat.home9.util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;


import static com.gomel.tat.home9.tests.BaseTest.userLogin;


/**
 * Created by Pavel on 12.01.2016.
 */
public class LoginPage {

    public static final String BASE_URL = "https://disk.yandex.ru/";

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//*[@type='submit']");
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='password']");

    public static final By USER_NAME_HEADER_LOCATOR = By.xpath("//span[@class='header__username']");
    public static final By ERROR_MESSAGE_LOCATOR = By.xpath("//*[@id='nb-input_error1']");

    public static WebDriverHelper myDriver;


    public LoginPage(WebDriverHelper myDriver) {
        this.myDriver = myDriver;
    }


    public LoginPage open() {
        LoginPage.myDriver.getDriver().get(BASE_URL);
        return this;
    }

    public MainPage login(String userLogin, String userPassword) {

        WebElement loginInput = myDriver.waitForShow(LOGIN_INPUT_LOCATOR);
        WebElement passwordImput = myDriver.waitForShow(PASSWORD_INPUT_LOCATOR);
        WebElement buttonEnter = myDriver.getDriver().findElement(ENTER_BUTTON_LOCATOR);

        loginInput.clear();
        loginInput.sendKeys(userLogin);
        passwordImput.sendKeys(userPassword);
        buttonEnter.click();
        return new MainPage(myDriver);
    }

    public void isSucceffullyLogin() {
        Assert.assertEquals(getIdentifUser(), userLogin.substring(0,userLogin.length()-10),"Wrong user logged");
    }

    public String getIdentifUser() {
        WebElement identification = myDriver.waitForShow(USER_NAME_HEADER_LOCATOR);
        return identification.getText();
    }

    public void lookforErrorMessage(){
        myDriver.waitForShow(ERROR_MESSAGE_LOCATOR);
        WebElement errorMessage = myDriver.getDriver().findElement(ERROR_MESSAGE_LOCATOR);
        Assert.assertNotNull(errorMessage);
    }



}
