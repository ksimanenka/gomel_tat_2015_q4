package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.LoginPage;
import org.testng.annotations.Test;

/**
 * Created by Pavel on 13.01.2016.
 */
public class LoginNegativeTest extends BaseTest {

    private LoginPage myLoginPage;
    public static final String wrongUserPassword = "wrong_password_123";

    @Test
    public void LoginNegativeTest() {
        myLoginPage = new LoginPage(myDriver);

        myLoginPage.open().login(userLogin,wrongUserPassword);
        myLoginPage.lookforErrorMessage();
        System.out.println("Login negative test successfully completed");
    }
}
