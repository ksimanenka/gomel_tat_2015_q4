package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by Pavel on 13.01.2016.
 */
public class TrashPage {

    public static WebDriverHelper myDriver;
    public static final By DELETE_ONE_WITHOUT_PREVIEW_BUTTON_TRASH_LOCATOR = By.xpath("//div[@data-key='box=boxAside']//div[contains(@class,'visible')]//button[@data-click-action='resource.delete' and contains(@data-params,'trash')]");
    public static final By TRASH_ICON_LOCATOR = By.xpath("//div[@data-id='/trash']/div");
    public static final By FILE_DELETED_NOTIFICATION_LOCATOR = By.xpath("//div[@data-key='box=layerWdg']//div[contains(@class,'item_moved')]");


    public TrashPage(WebDriverHelper myDriver) {
        this.myDriver = myDriver;
    }

    public static WebElement removePermanently(WebElement uploadFile, WebDriverHelper webDriver, By bypath) throws InterruptedException {


        removeToTrash(uploadFile, webDriver);
        goToTrash(webDriver);

        WebElement uploadFileInTrash = findUploadFile(bypath);
        uploadFileInTrash.click();

        deleteFile();
        WebElement notification = fileDeletedNotification();
        return notification;

    }

    public static void removeToTrash(WebElement uploadFile, WebDriverHelper webDriver) {
        WebElement trash = trash();
        Action actionDelete = new Actions(webDriver.getDriver()).dragAndDrop(uploadFile, trash).build();
        actionDelete.perform();

    }

    public static void goToTrash(WebDriverHelper webDriver) {
        WebElement trash = trash();
        Action goToTrash = new Actions(webDriver.getDriver()).doubleClick(trash).build();
        goToTrash.perform();

    }

    public static void deleteFile() {
        WebElement delete = myDriver.waitForShow(DELETE_ONE_WITHOUT_PREVIEW_BUTTON_TRASH_LOCATOR, 20);
        delete.click();
    }
    public static WebElement findUploadFile(By path) {
        WebElement uploadFile = myDriver.waitForShow(path, 30);
        return uploadFile;
    }

    public static WebElement fileDeletedNotification() {
        WebElement deletedNotofication = myDriver.waitForShow(FILE_DELETED_NOTIFICATION_LOCATOR, 20);
        return deletedNotofication;

    }

    public static WebElement trash() {
        WebElement trash = myDriver.getDriver().findElement(TRASH_ICON_LOCATOR);
        return trash;
    }

}
