package com.epam.gomel.tat.home10.tests.mail;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.epam.gomel.tat.home10.lib.ui.Browser.*;

public class BaseMailTest {
    protected WebDriver driver;

    @BeforeMethod
    public void prepareBrowser() {
        driver = getWebDriver();
    }

    @AfterMethod
    public void shutdown() {
        shutdownWebDriver();
    }
}
