package com.epam.gomel.tat.home10.lib.mail.screen;

import org.openqa.selenium.WebDriver;

public abstract class Page {

    public WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }
}
