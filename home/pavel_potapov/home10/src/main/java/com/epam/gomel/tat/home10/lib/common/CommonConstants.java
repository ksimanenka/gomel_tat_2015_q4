package com.epam.gomel.tat.home10.lib.common;


import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;

public interface CommonConstants {
    String MAILBOX_URL = "http://mail.yandex.by";
    String DEFAULT_MAIL_USER_LOGIN = "tat-test-user@yandex.ru";
    String DEFAULT_MAIL_USER_PASSWORD = "tat-123qwe";
    String DEFAULT_MAIL_TO_SEND = "tat-test-user@yandex.ru";
    int COMMON_TIME_OUT_SECONDS = 20;
    By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    By ERROR_MASSAGE_LOCATOR = By.xpath("//div[@class='error-isle js-error-isle']");
    String emailSubject = RandomStringUtils.randomAlphanumeric(40);
    String emailBodyText = RandomStringUtils.randomAlphanumeric(1000);
    String LATTER_LOCATOR_PATTERN = "//*[text()='%s']";
}
