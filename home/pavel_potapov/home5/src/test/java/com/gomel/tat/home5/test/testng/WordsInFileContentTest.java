package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

public class WordsInFileContentTest extends BaseWordListTest {
    @Test(groups = "get words")
    public void wordsInFileContent() {
        String[] fileContentArr = wordList.wordsInFileContent("список слов 1");
        String[] expectedArray = {"список", "слов"};
        Assert.assertEquals(fileContentArr, expectedArray, "Invalid result array");
    }
}