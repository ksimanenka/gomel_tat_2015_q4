package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.ui.page.yandexmail.ActionPage;
import org.testng.Assert;
import org.testng.annotations.Test;
/**
 * Created by Pavel on 27.12.2015.
 */
public class LoginTestPositive extends BaseClass {

    @Test(description = "Positive test mail login")
    public void loginMailPositive() {
        ActionPage menueTools = new ActionPage(myDriver);
        correctLogOnMail();
        clickButtonEnter();
        Assert.assertEquals(menueTools.getIdentifUser(), userLogin);
        System.out.println("Success test positive mail login");
    }

}
