package com.gomel.tat.home8.tests;


import com.gomel.tat.home8.ui.page.yandexmail.LatterPage;
import com.gomel.tat.home8.ui.page.yandexmail.ActionPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;
/**
 * Created by Pavel on 27.12.2015.
 */
public class SendWithoutAddressTest extends BaseClass {

    @BeforeClass
    public void loginMail() {
        correctLogOnMail();
        clickButtonEnter();
        myAction = new ActionPage(myDriver);
    }

    @BeforeMethod
    public void gotoInboxMailPage() {
        myAction.gotoInbox();
    }

    @Test(description = "Send message without address")
    public void fillNewLetterFields() {
        LatterPage newMail = new LatterPage(myDriver);

        myAction.clickButtonCompose();
        newMail.fillFieldsLetter("");
        newMail.clickSendMailButton();
        WebElement announcementEmptyField = newMail.withoutAddressAnnouncement();
        Assert.assertNotNull(announcementEmptyField);
        System.out.println("Success test send message without address");
    }
}
