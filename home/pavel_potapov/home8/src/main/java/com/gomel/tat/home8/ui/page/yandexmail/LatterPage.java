package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.ui.page.BeforePreparePage;
import com.gomel.tat.home8.util.MyWebDriver;
import org.openqa.selenium.*;

/**
 * Created by Pavel on 27.12.2015.
 */
public class LatterPage extends BeforePreparePage {

    public LatterPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void clickSendMailButton() {
        WebElement sendMailButton = myDriver.getDriver().findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

    }

    public String getLetterIdfromUrl() {
        String currentUrl = myDriver.getDriver().getCurrentUrl();
        int index = currentUrl.indexOf("#");
        String mailId = currentUrl.substring(index + 9);
        return mailId;
    }

    public String getLetterIdfromAnnouncement(By locator) {
        WebElement announcementMailSend = myDriver.getDriver().findElement(locator);
        String mailHref = announcementMailSend.getAttribute("href");
        int index = mailHref.indexOf("#");
        String mailId = mailHref.substring(index + 9);
//        System.out.println(mailId);
        return mailId;
    }

    public WebElement withoutAddressAnnouncement() {
        WebElement announcementAddressEmptyField = myDriver.getDriver().findElement(ANNOUNCEMENT_FIELD_SUBJECT_NOT_FILL_LOCATOR);
        return announcementAddressEmptyField;
    }

    public void fillFieldsLetter(String mailTo) {
        fillallFieldsLetters(mailTo, mailSubject, mailContent);

    }

    public void fillFieldsLetter() {
        fillallFieldsLetters(mailTo, mailSubject, mailContent);

    }

    public void fillallFieldsLetters(String mailTo, String mailSubject, String mailContent) {
        WebElement toInput = myDriver.waitForShow(TO_INPUT_LOCATOR);
        WebElement subjectInput = myDriver.getDriver().findElement(SUBJECT_INPUT_LOCATOR);
        WebElement mailContentText = myDriver.getDriver().findElement(MAIL_TEXT_LOCATOR);

        toInput.sendKeys(mailTo);
        subjectInput.sendKeys(mailSubject);
        mailContentText.sendKeys(mailContent);

    }


}
