package com.gomel.tat.home8.tests;


import com.gomel.tat.home8.ui.page.yandexmail.FindSelectLetterPage;
import com.gomel.tat.home8.ui.page.yandexmail.LatterPage;
import com.gomel.tat.home8.ui.page.yandexmail.ActionPage;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.*;
/**
 * Created by Pavel on 27.12.2015.
 */
public class DraftLetterTest extends BaseClass {

    private ActionPage myAction;

    @BeforeClass
    public void loginMail() {
        correctLogOnMail();
        clickButtonEnter();
        myAction = new ActionPage(myDriver);
    }

    @BeforeMethod
    public void gotoInboxMailPage() {
        myAction.gotoInbox();
    }

    @Test(description = "Create and delete draft mail", expectedExceptions = NoSuchElementException.class)
    public void draftTest() throws Exception{
        LatterPage newMail = new LatterPage(myDriver);
        FindSelectLetterPage mailBox = new FindSelectLetterPage(myDriver);

        myAction.clickButtonCompose();
        newMail.fillFieldsLetter();
        myDriver.waitForShow(ANNOUNCEMENT_DRAFT_MAIL_CREATE_LOCATOR, 20);
        String mailId = newMail.getLetterIdfromUrl();

        myAction.gotoDraft();
        mailBox.selectMail(mailId);
        myAction.clickButtonDelete();
        myDriver.waitForShow(ANNOUNCEMENT_MAIL_DELETED_LOCATOR);
        myDriver.waitForDisappear(ANNOUNCEMENT_MAIL_DELETED_LOCATOR, 7);

        myAction.gotoTrash();
        mailBox.selectMail(mailId);
        myAction.clickButtonDelete();

        myDriver.waitForShow(ANNOUNCEMENT_MAIL_DELETED_LOCATOR);
        try {
            mailBox.findMail(mailId);
        } catch (NoSuchElementException exc) {
            System.out.println("Success draft mail test");
            throw exc;
        }
    }
}
