package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.ui.page.yandexmail.ActionPage;
import com.gomel.tat.home8.util.MyWebDriver;
import org.openqa.selenium.*;
import org.testng.annotations.*;

import java.net.MalformedURLException;
/**
 * Created by Pavel on 27.12.2015.
 */
public class BaseClass extends BeforePrepare {



    @BeforeClass
    @Parameters({"webDriverName"})
    protected void SetupDriverNameClass(String webDriverName) throws MalformedURLException {
        myDriver = new MyWebDriver(webDriverName);
    }

    public void clickButtonEnter() {
        WebElement buttonEnter = myDriver.getDriver().findElement(ENTER_BUTTON_LOCATOR);
        buttonEnter.click();

    }

    public void logOnMail(String userLogin, String userPassword) {
        myDriver.getDriver().get(BASE_URL);

        WebElement loginSet = myDriver.waitForShow(LOGIN_INPUT_LOCATOR);
        WebElement passwordSet = myDriver.waitForShow(PASSWORD_INPUT_LOCATOR);

        loginSet.clear();
        loginSet.sendKeys(userLogin);
        passwordSet.sendKeys(userPassword);
    }

    public void wrongLogOnMail(String userPassword) {
        logOnMail(userLogin, userPassword);

    }

    public void correctLogOnMail() {
        logOnMail(userLogin, userPassword);

    }

    public void goToMailBox(){
        correctLogOnMail();
        clickButtonEnter();
        myAction = new ActionPage(myDriver);
    }

    @AfterClass
    protected void CloseBrowser() {
        myDriver.getDriver().quit();
    }

}
