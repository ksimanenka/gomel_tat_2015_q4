package com.gomel.tat.home8.ui.page;

import com.gomel.tat.home8.util.MyWebDriver;
import org.openqa.selenium.By;

/**
 * Created by Pavel on 27.12.2015.
 */
public class BeforePreparePage {
    protected MyWebDriver myDriver;

    /* FindSelectLetterPage */
    protected String FIND_LETTER_ID = "//a[contains(@href,'%s')]";
    protected String FIND_LETTER_ID_SELECT = "//a[contains(@href,'%s')]//ancestor::div[@data-action='mail.message.show-or-select']//input";


    /* ActionPage */
    protected By SEND_LETTER_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//div[2]/a[2]");
    protected By DRAFT_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#draft' and contains(@data-params,'folder')]");
    protected By TRASH_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#trash' and contains(@data-params,'folder')]");
    protected By INCOMING_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");
    protected By OUTCOMING_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#sent' and @data-action='move']");
    protected By DELETE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//div[2]/a[8]");
    protected By USER_NAME_HEADER_LOCATOR = By.xpath("//a[@id='nb-1']");

    /* LatterPage */
    protected By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    protected By SUBJECT_INPUT_LOCATOR = By.name("subj");
    protected By MAIL_TEXT_LOCATOR = By.id("compose-send");
    protected By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    protected By ANNOUNCEMENT_FIELD_SUBJECT_NOT_FILL_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//span[contains(@class,'error_required')]");

    public static String mailTo = "tat-test-user@yandex.ru";
    public static String mailSubject = "subject" + Math.random() * 100000000;
    public static String mailContent = "content" + Math.random() * 100000000;

}
