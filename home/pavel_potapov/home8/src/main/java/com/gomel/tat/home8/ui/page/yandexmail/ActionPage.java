package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.ui.page.BeforePreparePage;
import com.gomel.tat.home8.util.MyWebDriver;
import org.openqa.selenium.*;
/**
 * Created by Pavel on 27.12.2015.
 */
public class ActionPage extends BeforePreparePage {

    public ActionPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void gotoDraft() {
        WebElement draftMail = myDriver.waitForShow(DRAFT_MAIL_LINK_LOCATOR);
        myDriver.clickOn(draftMail);

    }

    public void gotoTrash() {
        WebElement trashMail = myDriver.waitForShow(TRASH_MAIL_LINK_LOCATOR);
        myDriver.clickOn(trashMail);

    }

    public void gotoInbox() {
        WebElement inboxMail = myDriver.waitForShow(INCOMING_MAIL_LINK_LOCATOR);
        myDriver.clickOn(inboxMail);

    }

    public void gotoSent() {
        WebElement outcomingMail = myDriver.getDriver().findElement(OUTCOMING_MAIL_LINK_LOCATOR);
        myDriver.clickOn(outcomingMail);

    }

    public void clickButtonCompose() {
        WebElement sendMailButton = myDriver.waitForShow(SEND_LETTER_BUTTON_LOCATOR);
        sendMailButton.click();

    }

    public void clickButtonDelete() {
        WebElement deletePermanentlyButton = myDriver.waitForShow(DELETE_BUTTON_LOCATOR);
        deletePermanentlyButton.click();

    }

    public String getIdentifUser() {
        WebElement identification = myDriver.waitForShow(USER_NAME_HEADER_LOCATOR);
        return identification.getText();
    }

}
