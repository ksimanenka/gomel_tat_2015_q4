package com.gomel.tat.home5;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.io.*;

public class TestListener1 implements IInvokedMethodListener {
    FileWriter fileWriter;

    TestListener1(FileWriter fileWriter) {
        this.fileWriter = fileWriter;
    }

    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println(iInvokedMethod.getTestMethod().getTestClass().getName() +
                "." + iInvokedMethod.getTestMethod().getMethodName() + "() is started.");
    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println(iInvokedMethod.getTestMethod().getTestClass().getClass());
        try {
            String methodName = "Exception: " + iInvokedMethod.getTestMethod().getMethodName() + "\r\n";
            fileWriter.write(methodName);
        } catch (IOException e) {
        }
        if (iInvokedMethod.getTestResult().getStatus() == 2) {
        }
    }
}

