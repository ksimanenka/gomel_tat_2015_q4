package com.gomel.tat.home5;

import org.testng.annotations.*;
import com.gomel.tat.home4.*;

import java.io.*;

public class BaseTest {
    WordList wordList;
    String baseDirectory;
    Exception exception;
    File tempDir;

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("bs");
        File file = new File("C:/Users/HozWin/Desktop/TestInformation.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            System.out.println("Exception message: " + e);
            this.exception = e;
        }
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("bc");
        wordList = new WordList();
        System.out.println("WordList object was created.");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("bm");
        baseDirectory = new File("").getAbsolutePath();
        String directoryName = "TempDir";
        tempDir = new File(baseDirectory + "/" + directoryName);
        tempDir.mkdir();
    }

    @BeforeGroups
    public void beforeGroups() {
        System.out.println("bg");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("as");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("ac");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("am");
        System.out.println("DirName is: " + tempDir.getAbsolutePath());
        try {
            Thread.sleep(100);
        } catch (Exception e) {
        }
        WorkUtilClass.removeDir(tempDir);
        tempDir.delete();
    }

    @AfterGroups
    public void afterGroups() {
        System.out.println("ag");
    }
}
