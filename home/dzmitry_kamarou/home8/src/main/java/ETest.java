import org.testng.Assert;
import org.testng.annotations.Test;

public class ETest extends BaseTest {

    @Test
    public void testInFirefox() {
        driverFirefox = helper.createDriver("firefox");
        pageMail = new PageMail(driverFirefox);
        pageMail.open();
        pageMail.createLetter("valid");
        pageMail.send();
        pageMail.deleteMail("Inbox", pageMail.letter.mailSubject);
        Assert.assertNotNull(pageMail.checkInTrash());
        System.out.println("Letter in the trash.");
        pageMail.deletePermanently();
        System.out.println("Checking is over, letter deleted permanently.");
    }

    @Test
    public void testInChrome() {
        driverChrome = helper.createDriver("chrome");
        pageMail = new PageMail(driverChrome);
        pageMail.open();
        pageMail.createLetter("valid");
        pageMail.send();
        pageMail.deleteMail("Inbox", pageMail.letter.mailSubject);
        Assert.assertNotNull(pageMail.checkInTrash());
        System.out.println("Letter in the trash.");
        pageMail.deletePermanently();
        System.out.println("Checking is over, letter deleted permanently.");
    }
}
