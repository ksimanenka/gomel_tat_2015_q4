import org.testng.Assert;
import org.testng.annotations.Test;

public class BTest extends BaseTest {

    @Test
    public void firefoxSendMailTest() {
        driverFirefox = helper.createDriver("firefox");
        pageMail = new PageMail(driverFirefox);
        pageMail.open();
        pageMail.createLetter("valid");
        pageMail.send();
        Assert.assertNotNull(pageMail.checkMailInFolder("Sent"));
        Assert.assertNotNull(pageMail.checkMailInFolder("Inbox"));
        System.out.println("Mail exists in Sent and Inbox.");
    }

    @Test
    public void chromeSendMailTest() {
        driverChrome = helper.createDriver("chrome");
        pageMail = new PageMail(driverChrome);
        pageMail.open();
        pageMail.createLetter("valid");
        pageMail.send();
        Assert.assertNotNull(pageMail.checkMailInFolder("Sent"));
        Assert.assertNotNull(pageMail.checkMailInFolder("Inbox"));
        System.out.println("Mail exists in Sent and Inbox.");
    }
}
