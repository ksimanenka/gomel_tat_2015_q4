import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

public class Helper {
    WebDriver webDriver;

    public WebDriver createDriver(String name) {
        try {
            if (name.equals("firefox")) {
                System.out.println("Creating firefox webdriver.");
                webDriver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
            } else {
                if (name.equals("chrome")) {
                    System.out.println("Creating chrome webdriver.");
                    webDriver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
                } else {
                    System.out.println("Helper.createDriver() say: bad browsers name.");
                }
            }
        } catch (Exception e) {
            System.out.println("Helper.createDriver() say: " + e);
        }
        return webDriver;
    }
}