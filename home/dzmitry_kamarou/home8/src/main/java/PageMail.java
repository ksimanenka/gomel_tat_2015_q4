import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Scanner;

public class PageMail extends Page {
    public static final By INBOX_LINK = By.xpath("//a[contains(text(),'Inbox')]");
    public static final By COMPOSE_BUTTON = By.xpath("//a[@href='#compose']");
    public static final By SEND_BUTTON = By.xpath("//button[@id='compose-submit']");
    public static final By TO_INPUT = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT = By.xpath("//input[@id='compose-subj']");
    public static final By TEXT_AREA = By.xpath("//textarea[@id='compose-send']");
    public static final By SENT_LINK = By.xpath("//a[@href='#sent']");
    public static final By SAVE_AND_GO_BUTTON = By.xpath("//span[contains(text(),'Save and go')]");
    public static final By STATUS_LINE = By.xpath("//span[@class='b-statusline__content']/a[@href]");
    public static final By SUCCESS_MESSAGE = By.xpath("//div[contains(text(),'Message sent successfully.')]");
    public static final By TRASH = By.xpath("//a[@href='#trash']");
    public static final By DELETE = By.xpath("//a[@title='Delete (Delete)']");
    public By MAIL_LINK_LOCATOR_PATTERN, MAIL_ROW;

    WebElement composeButton, sendButton, toInput,
            subjectInput, textArea, folder, mail,
            saveAndGoButton, statusLine, mailRow, delete, read;
    Letter letter;
    String id = "";

    PageMail() {
    }

    PageMail(WebDriver driver) {
        super(driver);
    }

    void open() {
        PageLogin pageLogin = new PageLogin(driver);
        pageLogin.open();
        pageLogin.login();
        System.out.println("Mail page is open.");
    }

    void createLetter(String type) {
        composeButton = driver.findElement(COMPOSE_BUTTON);
        composeButton.click();
        System.out.println("Compose letter button is clicked.");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(SEND_BUTTON));
        toInput = driver.findElement(TO_INPUT);
        subjectInput = driver.findElement(SUBJECT_INPUT);
        textArea = driver.findElement(TEXT_AREA);
        System.out.println("Mail fields are found.");
        letter = new Letter();
        MAIL_LINK_LOCATOR_PATTERN = By.xpath("//*[@class='block-messages']//a[contains(., '" + letter.mailSubject + "')]");
        System.out.println("The letter generated.");
        if (type.equals("invalid")) {
            toInput.sendKeys("Mail to " + Math.random() * 100000000);
        } else {
            toInput.sendKeys(letter.mailTo);
        }
        subjectInput.sendKeys(letter.mailSubject);
        textArea.sendKeys(letter.mailContent);
        System.out.println("Fields are filled in.");
    }

    void createLetter(String to, String subject, String text) {
        letter = new Letter(to, subject, text);
        composeButton = driver.findElement(COMPOSE_BUTTON);
        composeButton.click();
        System.out.println("Compose letter button is clicked.");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(SEND_BUTTON));
        toInput = driver.findElement(TO_INPUT);
        subjectInput = driver.findElement(SUBJECT_INPUT);
        textArea = driver.findElement(TEXT_AREA);
        System.out.println("Mail fields are found.");
        MAIL_LINK_LOCATOR_PATTERN = By.xpath("//*[@class='block-messages']//a[contains(., '" + subject + "')]");
        toInput.sendKeys(to);
        subjectInput.sendKeys(subject);
        textArea.sendKeys(text);
        System.out.println("Fields are filled in.");
    }

    void send() {
        sendButton = driver.findElement(SEND_BUTTON);
        sendButton.click();
        System.out.println("Send button is clicked.");
        try {
            Thread.sleep(5000);
            url = driver.getCurrentUrl();
        } catch (InterruptedException e) {
            System.out.println("Interrupted exception.");
        }
        int includeWord = url.indexOf("compose");
        if (includeWord > -1) {
            message = "The letter wasn't sent.";
        } else {
            new WebDriverWait(driver, 15000).until(ExpectedConditions.visibilityOfElementLocated(SUCCESS_MESSAGE));
            statusLine = driver.findElement(STATUS_LINE);
            String href = statusLine.getAttribute("href");
            letter.href = href;
            Scanner scanner = new Scanner(href);
            scanner.useDelimiter("/");
            while (scanner.hasNext()) {
                id = scanner.next();
            }
            letter.letterID = id;
            System.out.println("Letters id=" + id);
            MAIL_ROW = By.xpath("//div[@data-id='" + id + "']");
            System.out.println(MAIL_ROW);
            message = "The letter was sent.";
        }
        System.out.println(message);
    }

    WebElement checkMailInFolder(String name) {
        System.out.println("Checking mail exists in " + name + ".");
        if (name.equals("Sent")) {
            folder = driver.findElement(SENT_LINK);
        }
        if (name.equals("Inbox")) {
            folder = driver.findElement(INBOX_LINK);
        }
        System.out.println(name + " link is found.");
        folder.click();
        System.out.println(name + " clicked.");
        try {
            saveAndGoButton = driver.findElement(SAVE_AND_GO_BUTTON);
            System.out.println("There is 'save and go' button visible.");
            saveAndGoButton.click();
            System.out.println("'Save and go' button clicked.");
        } catch (Exception e) {
            System.out.println("'Save and go' button processed.");
        }
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(COMPOSE_BUTTON));
        if (name.equals("Sent")) {
            return mail = driver.findElement(MAIL_LINK_LOCATOR_PATTERN);
        }
        if (name.equals("Inbox")) {
            if (!letter.mailSubject.equals(""))
                return mail = driver.findElement(MAIL_LINK_LOCATOR_PATTERN);
            else {
                mailRow = driver.findElement(MAIL_ROW);
                System.out.println("mailRow: " + mailRow);
                return mailRow;
            }
        }
        System.out.println("Something wrong.");
        return null;
    }

    void deleteMail(String folderName, String subject) {
        By FOLDER = By.xpath("//a[text()='" + folderName + "']");
        String spanString = letter.mailSubject;
        System.out.println("spanString=" + spanString);
        String s = "//span[@title='" + spanString + "']";
        By SPAN = By.xpath(s);
        System.out.println("SPAN=" + SPAN);
        new WebDriverWait(driver, 15000).until(ExpectedConditions.visibilityOfElementLocated(SPAN));
        WebElement span = driver.findElement(SPAN);
        System.out.println("span=" + span);
        String followingSiblingString = "//span[@title='" + spanString + "']" + "//following-sibling::span";
        System.out.println("followingSibling=" + followingSiblingString);
        By FOLLOWING_SIBLING = By.xpath(followingSiblingString);
        System.out.println("FOLLOWING_SIBLING=" + FOLLOWING_SIBLING);
        WebElement followingSibling = driver.findElement(FOLLOWING_SIBLING);
        String dataParams = followingSibling.getAttribute("data-params");
        System.out.println("dataParams=" + dataParams);
        Scanner scanner = new Scanner(dataParams);
        scanner.useDelimiter("=");
        String id1 = "";
        while (scanner.hasNext()) {
            id1 = scanner.next();
        }
        id1 = "t" + id1;
        String checkboxString = "//input[@value='" + id1 + "']";
        System.out.println("checkboxString=" + checkboxString);
        By CHECKBOX = By.xpath(checkboxString);
        System.out.println("CHECKBOX=" + CHECKBOX);
        WebElement checkbox = driver.findElement(CHECKBOX);
        checkbox.click();
        System.out.println("DELETE=" + DELETE);
        delete = driver.findElement(DELETE);
        System.out.println("delete=" + delete);
        delete.click();
        folder = driver.findElement(FOLDER);
        folder.click();
        System.out.println("//input[@value='t" + id + "']");
    }

    WebElement checkInTrash() {
        System.out.println("Check in trash");
        By TRASH = By.xpath("//a[@href='#trash']");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.visibilityOfElementLocated(TRASH));
        System.out.println("TRASH=" + TRASH);
        WebElement trash = driver.findElement(TRASH);
        System.out.println("id=" + id);
        trash.click();
        By CHECKBOX = By.xpath("//input[@value='" + id + "']");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.visibilityOfElementLocated(CHECKBOX));
        System.out.println("CHECKBOX=" + CHECKBOX);
        WebElement checkbox = driver.findElement(CHECKBOX);
        System.out.println("checkbox=" + checkbox);
        return checkbox;
    }

    void deletePermanently() {
        System.out.println("Delete permanently.");
        WebElement trash = driver.findElement(TRASH);
        trash.click();
        System.out.println("Trash clicked.");
        By CHECKBOX = By.xpath("//input[@value='" + id + "']");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.visibilityOfElementLocated(CHECKBOX));
        System.out.println("Checkbox founded.");
        WebElement checkbox = driver.findElement(CHECKBOX);
        checkbox.click();
        System.out.println("Checkbox checked.");
        By READ = By.xpath("//a[@title='Read (q)']");
        System.out.println("READ=" + READ);
        read = driver.findElement(READ);
        read.click();
        System.out.println("Readed.");
        checkbox.click();
        delete.click();
        System.out.println("The letter was deleted permanently.");
    }
}
