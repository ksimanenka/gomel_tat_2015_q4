
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ATest extends BaseTest {
    @Test
    public void positiveFirefox() {
        driverFirefox = helper.createDriver("firefox");
        pageLogin = new PageLogin(driverFirefox);
        pageLogin.open();
        pageLogin.login();
        Assert.assertNotNull(driverFirefox.findElement(new PageMail().INBOX_LINK));
    }

    @Test
    public void positiveChrome() {
        driverChrome = helper.createDriver("chrome");
        pageLogin = new PageLogin(driverChrome);
        pageLogin.open();
        pageLogin.login();
        Assert.assertNotNull(driverChrome.findElement(new PageMail().INBOX_LINK));
    }

    @Test
    public void negativeFirefox() {
        driverFirefox = helper.createDriver("firefox");
        pageLogin = new PageLogin(driverFirefox);
        pageLogin.open();
        pageLogin.login("", "");
        Assert.assertNotNull(driverFirefox.findElement(new PagePassport().DOMIK_CONTENT));
    }

    @Test
    public void negativeChrome() {
        driverChrome = helper.createDriver("chrome");
        pageLogin = new PageLogin(driverChrome);
        pageLogin.open();
        pageLogin.login("", "");
        Assert.assertNotNull(driverChrome.findElement(new PagePassport().DOMIK_CONTENT));
    }
}
