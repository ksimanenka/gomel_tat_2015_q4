import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class UtilWebDriverClass {
    static WebDriver webDriver;

    static WebDriver createFirefoxWebDriver() {
        try {
            webDriver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        } catch (MalformedURLException e) {
            System.out.println("MyException: " + e);
        }
        System.out.println("Firefox WebDriver was created.");
        return webDriver;
    }

    static WebDriver createChromeWebDriver() {
        try {
            webDriver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        } catch (MalformedURLException e) {
            System.out.println("MyException: " + e);
        }
        System.out.println("Chrome WebDriver was created.");
        return webDriver;
    }
}


