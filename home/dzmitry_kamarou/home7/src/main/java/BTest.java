import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.TestNG;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

public class BTest extends BaseTest {
    @Test
    public void firefoxLogin() {
        webDriver = UtilWebDriverClass.createFirefoxWebDriver();
        webDriver.get(YANDEX_START_PAGE);
        WebElement loginButton = webDriver.findElement(LOGIN_BUTTON);
        WebElement loginInput = webDriver.findElement(LOGIN_INPUT);
        WebElement passwordInput = webDriver.findElement(PASSWORD_INPUT);
        loginInput.sendKeys("dzmitry.kamarou");
        passwordInput.sendKeys("q1w1e1r1");
        loginButton.click();
        WebElement inboxLink = webDriver.findElement(INBOX_LINK);
        System.out.println(inboxLink);
        Assert.assertNotNull(inboxLink);
        //webDriver.close();
        //webDriver.quit();
    }

    @Test(dependsOnMethods="firefoxLogin")
    public void firefoxCreateLetter() {
        WebElement composeButton = webDriver.findElement(COMPOSE_BUTTON);
        composeButton.click();
        new WebDriverWait(webDriver, 5000).until(ExpectedConditions.elementToBeClickable(TO_INPUT));
        WebElement toInput = webDriver.findElement(TO_INPUT);
        toInput.sendKeys("dzmitry.kamarou@yandex.ru");
        WebElement subjectInput = webDriver.findElement(SUBJECT_INPUT);
        subjectInput.sendKeys("Test Letter");
        WebElement letterTextArea = webDriver.findElement(LETTER_TEXT_AREA);
        letterTextArea.sendKeys("This is a test letter.");
        WebElement sendButton = webDriver.findElement(SEND_BUTTON);
        sendButton.click();
        WebElement sentResult = webDriver.findElement(SENT_RESULT);
        Assert.assertNotNull(sentResult);
    }

    @Test
    public void chromeLogin() {
        webDriver = UtilWebDriverClass.createChromeWebDriver();
        webDriver.get(YANDEX_START_PAGE);
        WebElement loginButton = webDriver.findElement(LOGIN_BUTTON);
        WebElement loginInput = webDriver.findElement(LOGIN_INPUT);
        WebElement passwordInput = webDriver.findElement(PASSWORD_INPUT);
        loginInput.sendKeys("dzmitry.kamarou");
        passwordInput.sendKeys("q1w1e1r1");
        loginButton.click();
        WebElement inboxLink = webDriver.findElement(INBOX_LINK);
        System.out.println(inboxLink);
        Assert.assertNotNull(inboxLink);
        //webDriver.close();
        //webDriver.quit();
    }

    @Test(dependsOnMethods="chromeLogin")
    public void chromeCreateLetter() {
        WebElement composeButton = webDriver.findElement(COMPOSE_BUTTON);
        composeButton.click();
        new WebDriverWait(webDriver, 5000).until(ExpectedConditions.elementToBeClickable(TO_INPUT));
        WebElement toInput = webDriver.findElement(TO_INPUT);
        toInput.sendKeys("dzmitry.kamarou@yandex.ru");
        WebElement subjectInput = webDriver.findElement(SUBJECT_INPUT);
        subjectInput.sendKeys("Test Letter");
        WebElement letterTextArea = webDriver.findElement(LETTER_TEXT_AREA);
        letterTextArea.sendKeys("This is a test letter.");
        WebElement sendButton = webDriver.findElement(SEND_BUTTON);
        sendButton.click();
        WebElement sentResult = webDriver.findElement(SENT_RESULT);
        Assert.assertNotNull(sentResult);
    }
}
