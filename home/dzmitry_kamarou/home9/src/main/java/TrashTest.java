import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class TrashTest extends BaseTest {
    PageDisk pageDisk;
    File fileFromPC;
    String fileFromPCContent, title;
    WebElement uploadedFile;

    @Test
    public void uploadFileTest() {
        System.out.println("Upload file test started.");
        pageDisk = new PageDisk(driver, driverName);
        pageDisk.open();
        fileFromPC = pageDisk.uploadFile();
        System.out.println("File is uploaded.");
        fileFromPCContent = pageDisk.fileFromPCContent;
        By UPLOADED_FILE = By.xpath("//div[@title='" + fileFromPC.getName() + "']");
        uploadedFile = driver.findElement(UPLOADED_FILE);
        title = uploadedFile.getAttribute("title");
        Assert.assertEquals(fileFromPC.getName(), title, "Test failed: 'file correct uploaded checking'.");
        System.out.println("File is uploaded correctly.");
    }

    @Test(dependsOnMethods = "uploadFileTest")
    public void removeFileAndCheckInTrashTest() {
        System.out.println("Remove file and check in trash test started.");
        pageDisk.removeToTrash(uploadedFile);
        boolean isDeleted = pageDisk.checkFileInTrash(title);
        Assert.assertEquals(isDeleted, true, "Test failed: 'remove file and check in trash'.");
    }

    @Test(dependsOnMethods = {"uploadFileTest", "removeFileAndCheckInTrashTest"})
    public void deletePermanentlyTest() {
        System.out.println("Delete permanently test started.");
        pageDisk.deleteSelectedFileInTrash(title);
        boolean isInTrash = pageDisk.checkFileInTrash(title);
        Assert.assertEquals(isInTrash, false, "Test failed: 'delete file permanently'.");
        System.out.println(title + " was deleted permanently.");
    }
}
