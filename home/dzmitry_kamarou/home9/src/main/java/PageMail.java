import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageMail extends Page {
    public static final By INBOX_LINK = By.xpath("//a[contains(text(),'Inbox')]");
    public static final By DISK_TAB = By.xpath("//a[@title='Disk']");
    public static final By SAVE_TIME_CLOSE = By.xpath("//a[contains(@data-metrika-params,'x-closed')]");

    PageLogin pageLogin;
    WebElement diskTab, saveTimeClose;

    PageMail() {
    }

    PageMail(WebDriver driver, String driverName) {
        super(driver, driverName);
    }

    void open() {
        pageLogin = new PageLogin(driver, driverName);
        pageLogin.open();
        pageLogin.login();
        System.out.println("Mail page open.");
    }

    void diskClick() {
        diskTab = driver.findElement(DISK_TAB);
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(DISK_TAB));
        diskTab.click();
        System.out.println("Disk tab clicked.");
        try {
            Thread.sleep(3500);
            saveTimeClose = driver.findElement(SAVE_TIME_CLOSE);
            System.out.println("There is 'Save time!' content frame.");
            saveTimeClose.click();
            System.out.println("'Save time!' content frame processed.");
        } catch (NoSuchElementException e) {
            System.out.println("There is no 'Save time!' content frame.");
        } catch (InterruptedException e) {
            System.out.println("Interrupted exception.");
        }
    }
}
