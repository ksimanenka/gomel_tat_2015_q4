import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class RemoveSeveralElementsAtOnceTest extends BaseTest {
    int filesCount = 5;
    PageDisk pageDisk;

    WebElement uploadedFile;
    File[] uploadedFiles = new File[filesCount];
    String[] titles = new String[filesCount];

    @Test
    public void uploadSeveralFilesTest() {
        System.out.println("Remove several elements at once test started.");
        System.out.println("Uploading " + filesCount + " files.");
        pageDisk = new PageDisk(driver, driverName);
        pageDisk.open();
        String titlesSummary = "", filesNamesSummary = "";
        for (int i = 0; i < filesCount; i++) {
            uploadedFiles[i] = pageDisk.uploadFile();
            titles[i] = uploadedFiles[i].getName();
            filesNamesSummary += uploadedFiles[i].getName() + " ";
            By UPLOADED_FILE = By.xpath("//div[@title='" + uploadedFiles[i].getName() + "']");
            uploadedFile = driver.findElement(UPLOADED_FILE);
            titlesSummary += uploadedFile.getAttribute("title") + " ";
        }
        System.out.println("Files from PC : " + filesNamesSummary);
        System.out.println("Uploaded files: " + filesNamesSummary);
        Assert.assertEquals(filesNamesSummary, titlesSummary, "Test failed: 'files correct uploaded checking'.");
        System.out.println(filesCount + " files are uploaded.");
    }

    @Test(dependsOnMethods = "uploadSeveralFilesTest")
    public void removeUploadedFilesAndCheckTest() {
        pageDisk.removeSeveralElements(uploadedFiles);
        boolean isDeleted = pageDisk.checkFilesInTrash(titles);
        Assert.assertEquals(isDeleted, true, "Test failed: 'files correct uploaded checking'.");
        System.out.println("Files are deleted and exists in trash.");
    }
}
