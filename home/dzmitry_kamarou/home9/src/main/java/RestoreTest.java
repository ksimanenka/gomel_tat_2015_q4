import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class RestoreTest extends BaseTest {
    PageDisk pageDisk;
    File fileFromPC;
    String fileFromPCContent, title, deletedFrom;
    WebElement uploadedFile;

    @Test
    public void uploadFileTest() {
        System.out.println("Upload file test started.");
        pageDisk = new PageDisk(driver, driverName);
        pageDisk.open();
        fileFromPC = pageDisk.uploadFile();
        System.out.println("File is uploaded.");
        fileFromPCContent = pageDisk.fileFromPCContent;
        By UPLOADED_FILE = By.xpath("//div[@title='" + fileFromPC.getName() + "']");
        uploadedFile = driver.findElement(UPLOADED_FILE);
        title = uploadedFile.getAttribute("title");
        Assert.assertEquals(fileFromPC.getName(), title, "Test failed: 'file correct uploaded checking'.");
        System.out.println("File is uploaded correctly.");
    }

    @Test(dependsOnMethods = "uploadFileTest")
    public void removeFileAndCheckInTrashTest() {
        System.out.println("Remove file and check in trash test started.");
        deletedFrom = pageDisk.removeToTrash(uploadedFile);
        boolean isDeleted = pageDisk.checkFileInTrash(title);
        Assert.assertEquals(isDeleted, true, "Test failed: 'remove file and check in trash'.");
    }

    @Test(dependsOnMethods = {"uploadFileTest", "removeFileAndCheckInTrashTest"})
    public void restoreFileAndCheckItTest() {
        pageDisk.restoreFile(title);
        boolean restored = pageDisk.checkRestoredFile(deletedFrom, title);
        Assert.assertEquals(restored, true, "Test failed: 'remove file and check in trash'.");
        System.out.println("File restored and checked well.");
    }
}
