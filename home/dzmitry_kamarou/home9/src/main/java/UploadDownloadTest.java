import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class UploadDownloadTest extends BaseTest {
    PageDisk pageDisk;
    File fileFromPC, downloadedFile;
    WebElement uploadedFile;
    String fileFromPCContent;

    @Test
    public void uploadFileTest() {
        System.out.println("Upload file test started.");
        pageDisk = new PageDisk(driver, driverName);
        pageDisk.open();
        fileFromPC = pageDisk.uploadFile();
        System.out.println("File is uploaded.");
        fileFromPCContent = pageDisk.fileFromPCContent;
        By UPLOADED_FILE = By.xpath("//div[@title='" + fileFromPC.getName() + "']");
        uploadedFile = driver.findElement(UPLOADED_FILE);
        String title = uploadedFile.getAttribute("title");
        Assert.assertEquals(fileFromPC.getName(), title, "Test failed: 'file correct uploaded checking'.");
        System.out.println("File is uploaded correctly.");
    }

    @Test(dependsOnMethods = "uploadFileTest")
    public void downloadFileAndCheckTest() {
        System.out.println("Check downloaded file started.");
        pageDisk.downloadFile(fileFromPC.getName());
        System.out.println("Loaded file content is not blank.");
        System.out.println("File loaded correctly.");
        downloadedFile = new File("c:/Download/" + fileFromPC.getName());
        Assert.assertEquals(downloadedFile.exists(), true, "Test failed: 'download file checking'.");
        System.out.println("Checking of downloaded file is OK.");
    }

    @Test(dependsOnMethods = {"uploadFileTest", "downloadFileAndCheckTest"})
    public void checkExpectedContentTest() {
        System.out.println("Check expected content started.");
        String downloadedFileContent = Helper.getFileContent(downloadedFile);
        System.out.println("File from PC content was: " + fileFromPCContent);
        System.out.println("Download file content is: " + downloadedFileContent);
        Assert.assertEquals(downloadedFileContent, fileFromPCContent, "Test failed: 'expected content checking'.");
        System.out.println("Content matched.");
    }
}
