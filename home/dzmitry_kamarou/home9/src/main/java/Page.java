import org.openqa.selenium.WebDriver;

public class Page {
    WebDriver driver;
    String driverName;

    Page() {
    }

    Page(WebDriver driver, String driverName) {
        this.driver = driver;
        this.driverName = driverName;
    }

    public void open(String URL) {
        System.out.println("Open " + URL);
        driver.get(URL);
    }
}
