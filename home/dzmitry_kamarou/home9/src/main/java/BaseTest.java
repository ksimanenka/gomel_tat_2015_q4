import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;

import java.io.File;

public class BaseTest {
    WebDriver driver;
    File filesDirectory, downloadDirectory;
    /*Use next two names for testing firefox or chrome browsers.
    */
    //String driverName = "firefox";
    String driverName="chrome";

    @BeforeClass
    public void createDriver() {
        driver = Helper.createDriver(driverName);
        driver.manage().window().maximize();
        filesDirectory = new File("C:/Files");
        filesDirectory.mkdir();
        downloadDirectory = new File("C:/Download");
        downloadDirectory.mkdir();
        System.out.println("Directories are created.");
    }
}
