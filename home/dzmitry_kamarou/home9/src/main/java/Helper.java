import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class Helper {

    public static WebDriver createDriver(String browserName) {
        System.out.println("Web driver creating.");
        try {
            if (browserName.equals("firefox")) {
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("browser.download.folderList", 2);
                profile.setPreference("browser.download.manager.showWhenStarting", false);
                profile.setPreference("browser. download. manager. useWindow", true);
                profile.setPreference("plugin.disable_full_page_plugin_for_types", "application/txt");
                profile.setPreference("browser.download.dir", "C:\\Download");
                profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/txt;text/plain;text/csv");
                profile.setPreference("browser.helperApps.alwaysAsk.force", false);
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                capabilities.setCapability(FirefoxDriver.PROFILE, profile);
                return new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
            } else {
                if (browserName.equals("chrome")) {
                    String downloadFilepath = "c:/Download";
                    HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                    chromePrefs.put("download.default_directory", downloadFilepath);
                    ChromeOptions options = new ChromeOptions();
                    options.setExperimentalOption("prefs", chromePrefs);
                    DesiredCapabilities cap = DesiredCapabilities.chrome();
                    cap.setCapability(ChromeOptions.CAPABILITY, options);
                    return new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), cap);
                } else {
                    System.out.println("Use 'firefox' or 'chrome' as parameter.");
                }
            }
        } catch (MalformedURLException e) {
            System.out.println("MyException: " + e);
        }
        return null;
    }

    public static File fileGenerate(PageDisk pageDisk) {
        System.out.println("File generate started.");
        String fileName, content, path;
        fileName = ((new Double(Math.random() * 100000000)).toString()) + ".txt";
        content = (new Double(Math.random() * 100000000)).toString();
        pageDisk.fileName = fileName;
        pageDisk.fileFromPCContent = content;
        path = "C:/Files/" + fileName;
        File file = new File(path);
        try {
            FileWriter fileWriter = new FileWriter(file);
            file.createNewFile();
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("IO exception.");
        }
        System.out.println("Generated file: " + file);
        return file;
    }

    public static String getFileContent(File downloadedFile) {
        String downloadedFileContent = "";
        FileInputStream fis = null;
        byte[] text;
        while (downloadedFileContent.equals("")) {
            try {
                System.out.println("Content is blank. Wait...");
                fis = new FileInputStream(downloadedFile);
                text = new byte[fis.available()];
                fis.read(text);
                downloadedFileContent = new String(text);
            } catch (Exception e) {
                System.out.println("MyException: " + e);
            }
        }
        return downloadedFileContent;
    }
}
