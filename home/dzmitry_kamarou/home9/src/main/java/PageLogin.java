import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageLogin extends Page {
    public static final String START_PAGE = "https://mail.yandex.by/";
    public static final By LOGIN_BUTTON = By.xpath("//div[@class='new-left']//button[@type='submit']");
    public static final By LOGIN_INPUT = By.xpath("//label[@id='nb-1']//input[@name='login']");
    public static final By PASSWORD_INPUT = By.xpath("//label[@id='nb-2']//input[@name='passwd']");

    WebElement loginButton, loginInput, passwordInput;

    PageLogin(WebDriver driver, String driverName) {
        super(driver, driverName);
    }

    void open() {
        super.open(START_PAGE);
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(LOGIN_BUTTON));
        this.loginButton = driver.findElement(LOGIN_BUTTON);
        this.loginInput = driver.findElement(LOGIN_INPUT);
        this.passwordInput = driver.findElement(PASSWORD_INPUT);
        System.out.println("Login page is open.");
    }

    void login() {
        loginInput.sendKeys("dzmitry.kamarou");
        passwordInput.sendKeys("q1w1e1r1");
        System.out.println("Login and password was entered.");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(LOGIN_BUTTON));
        loginButton.click();
        System.out.println("Login button clicked.");
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(new PageMail().INBOX_LINK));
        System.out.println("Login executed.");
    }
}
