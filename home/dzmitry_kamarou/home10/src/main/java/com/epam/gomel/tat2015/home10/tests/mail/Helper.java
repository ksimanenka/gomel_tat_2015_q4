package com.epam.gomel.tat2015.home10.tests.mail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class Helper {

    public static WebDriver createDriver(String browserName) {
        System.out.println("Web driver creating.");
        try {
            if (browserName.equals("firefox")) {
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("browser.download.folderList", 2);
                profile.setPreference("browser.download.manager.showWhenStarting", false);
                profile.setPreference("browser. download. manager. useWindow", true);
                profile.setPreference("plugin.disable_full_page_plugin_for_types", "application/txt");
                profile.setPreference("browser.download.dir", "C:\\Download");
                profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/txt;text/plain;text/csv");
                profile.setPreference("browser.helperApps.alwaysAsk.force", false);
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                capabilities.setCapability(FirefoxDriver.PROFILE, profile);
                return new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
            } else {
                if (browserName.equals("chrome")) {
                    String downloadFilepath = "c:/Download";
                    HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                    chromePrefs.put("download.default_directory", downloadFilepath);
                    ChromeOptions options = new ChromeOptions();
                    options.setExperimentalOption("prefs", chromePrefs);
                    DesiredCapabilities cap = DesiredCapabilities.chrome();
                    cap.setCapability(ChromeOptions.CAPABILITY, options);
                    return new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), cap);
                } else {
                    System.out.println("Use 'firefox' or 'chrome' as parameter.");
                }
            }
        } catch (MalformedURLException e) {
            System.out.println("MyException: " + e);
        }
        return null;
    }
}