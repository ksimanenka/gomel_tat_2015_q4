package com.epam.gomel.tat2015.home10.tests.mail;

import com.epam.gomel.tat2015.home10.lib.common.Account;
import com.epam.gomel.tat2015.home10.lib.common.AccountBuilder;
import com.epam.gomel.tat2015.home10.lib.login.service.LoginService;
import com.epam.gomel.tat2015.home10.lib.mail.business.Letter;
import com.epam.gomel.tat2015.home10.lib.mail.business.LetterBuilder;
import com.epam.gomel.tat2015.home10.lib.mail.screen.MailPage;
import com.epam.gomel.tat2015.home10.lib.mail.service.MailService;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendMailTest extends BaseTest {
    Account account;
    Letter letter;
    LoginService loginService;
    MailService mailService;

    @Test
    public void loginIntoMail() {
        loginService = new LoginService(driver);
        account = AccountBuilder.getValidAccount();
        loginService.loginToMailbox(account);
        System.out.println("Waiting for mail page inbox link.");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(MailPage.INBOX_LINK));
    }

    @Test(dependsOnMethods = "loginIntoMail")
    public void sendLetter() {
        System.out.println("Create letter started.");
        mailService = new MailService(driver);
        System.out.println("mailService object created.");
        letter = LetterBuilder.buildLetter();
        System.out.println("Letter was created.");
        System.out.println("letter.subject=" + letter.getSubject());
        System.out.println("letter.content=" + letter.getBody());
        mailService.sendLetter(letter);
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(MailPage.SUCCESS_MESSAGE));
    }

    @Test(dependsOnMethods = {"loginIntoMail", "sendLetter"})
    public void isMailInSentTest() {
        System.out.println("Is mail in sent test started.");
        mailService.goToSent();
        Assert.assertEquals(mailService.isLetterExistInSent(), true, "Mail isn't in Sent.");
    }

    @Test(dependsOnMethods = {"loginIntoMail", "sendLetter", "isMailInSentTest"})
    public void isMailInInboxTest() {
        System.out.println("Is mail in inbox test started.");
        mailService.goToInbox();
        Assert.assertEquals(mailService.isLetterExistInInbox(), true, "Mail isn't in Inbox.");
    }
}
