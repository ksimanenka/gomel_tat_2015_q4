package com.epam.gomel.tat2015.home10.lib.mail.business;

import com.epam.gomel.tat2015.home10.lib.common.CommonConstants;

public class LetterBuilder {

    public static Letter buildLetter() {
        Letter letter = new Letter();
        letter.setSubject("test subject" + Math.random() * 100000000);
        letter.setRecipient(CommonConstants.VALID_USER_LOGIN + "@yandex.ru");
        letter.setBody("mail content " + Math.random() * 100000000);
        return letter;
    }

    public static Letter buildInvalidAddressLetter() {
        Letter letter = new Letter();
        letter.setSubject("test subject" + Math.random() * 100000000);
        letter.setRecipient((new Double(Math.random() * 100000000)).toString());
        letter.setBody("mail content " + Math.random() * 100000000);
        return letter;
    }

    public static Letter buildBlankLetter() {
        Letter letter = new Letter();
        letter.setSubject(CommonConstants.DEFAULT_MAIL_SUBJECT);
        letter.setRecipient(CommonConstants.VALID_USER_LOGIN + "@yandex.ru");
        letter.setBody(CommonConstants.DEFAULT_MAIL_BODY);
        return letter;
    }
}
