package com.epam.gomel.tat2015.home10.tests.mail;

import com.epam.gomel.tat2015.home10.lib.common.Account;
import com.epam.gomel.tat2015.home10.lib.common.AccountBuilder;
import com.epam.gomel.tat2015.home10.lib.login.service.LoginService;
import com.epam.gomel.tat2015.home10.lib.mail.screen.MailPage;
import com.epam.gomel.tat2015.home10.lib.passport.screen.PassportPage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginIntoMailTest extends BaseTest {
    Account account;
    LoginService loginService;

    @Test
    public void positiveTest() {
        loginService = new LoginService(driver);
        account = AccountBuilder.getValidAccount();
        loginService.checkSuccessLogin(account);
        System.out.println("Waiting for mail page inbox link.");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(MailPage.INBOX_LINK));
        Assert.assertNotNull(driver.findElement(new MailPage().INBOX_LINK));
        System.out.println("Positive test passed.");
    }

    @Test
    public void negativeTest() {
        loginService = new LoginService(driver);
        account = AccountBuilder.getBlankAccount();
        loginService.checkErrorOnFailedLogin(account);
        System.out.println("Waiting for passport page domik content.");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(PassportPage.DOMIK_CONTENT));
        Assert.assertNotNull(driver.findElement(new PassportPage().DOMIK_CONTENT));
        System.out.println("Negative test passed.");
    }
}
