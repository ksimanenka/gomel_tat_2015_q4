package com.epam.gomel.tat2015.home10.lib.login.service;

import com.epam.gomel.tat2015.home10.lib.common.Account;
import com.epam.gomel.tat2015.home10.lib.login.screen.LoginPage;
import org.openqa.selenium.WebDriver;

public class LoginService {

    LoginPage loginPage;

    public LoginService(WebDriver driver) {
        loginPage = new LoginPage(driver);
    }

    public void loginToMailbox(Account account) {
        loginPage.fillLoginInput(account);
        loginPage.fillPasswordInput(account);
        loginPage.clickEnterButton();
    }

    public void checkSuccessLogin(Account account) {
        loginPage.fillLoginInput(account);
        loginPage.fillPasswordInput(account);
        loginPage.clickEnterButton();

    }

    public void checkErrorOnFailedLogin(Account account) {
        loginPage.fillLoginInput(account);
        loginPage.fillPasswordInput(account);
        loginPage.clickEnterButton();
    }
}
