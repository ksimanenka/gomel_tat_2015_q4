package com.epam.gomel.tat2015.home10.tests.mail;

import com.epam.gomel.tat2015.home10.lib.common.CommonConstants;
import com.epam.gomel.tat2015.home10.lib.login.screen.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class BaseTest {

    WebDriver driver;

    /*Use next two names for testing firefox or chrome browsers.
    */
    //String driverName = "firefox";
    String driverName = "chrome";

    @BeforeClass
    public void prepare() {
        driver = Helper.createDriver(driverName);
        driver.manage().window().maximize();
        driver.get(CommonConstants.LOGIN_PAGE_URL);
        System.out.println(CommonConstants.LOGIN_PAGE_URL + " is loaded.");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(LoginPage.LOGIN_BUTTON));
    }
}
