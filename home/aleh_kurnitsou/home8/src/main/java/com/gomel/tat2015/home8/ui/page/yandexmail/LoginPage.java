package com.gomel.tat2015.home8.ui.page.yandexmail;

import com.gomel.tat2015.home8.ui.page.Page;

import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends Page {

    public static final String BASE_URL = "http://www.mail.yandex.ru";
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(BASE_URL);
    }

    public InboxPage login(String userLogin, String userPassword) {
        WebElement loginInput = WebDriverHelper.waitForElementIsClickable(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        return new InboxPage(driver);
    }

}
