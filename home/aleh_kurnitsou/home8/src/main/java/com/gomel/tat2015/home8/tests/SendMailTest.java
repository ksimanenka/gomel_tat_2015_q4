package com.gomel.tat2015.home8.tests;

import com.gomel.tat2015.home8.ui.page.yandexmail.ComposePage;
import com.gomel.tat2015.home8.ui.page.yandexmail.InboxPage;
import com.gomel.tat2015.home8.utils.Letter;
import com.gomel.tat2015.home8.utils.LetterFactory;
import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.net.MalformedURLException;


public class SendMailTest extends BaseLoginTest {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");

   @BeforeClass
    public void prepareBrowser() throws InterruptedException, MalformedURLException {
        driver = WebDriverHelper.getWebDriver();
        login();
   }

    @Test
    public void sendMail() {
        WebElement composeButton = WebDriverHelper.waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        ComposePage composePage = new ComposePage(driver);
        Letter letter = LetterFactory.getRandomLetter();
        composePage.sendLetter(letter);
        InboxPage inboxPage = new InboxPage(driver);
        Assert.assertTrue(inboxPage.isLetterPresent(letter));
    }

//    @AfterClass(description = "Close browser")
//    public void clearBrowserChrome() {
//        driver.quit();
//    }
}



