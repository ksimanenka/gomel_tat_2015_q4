package com.gomel.tat2015.home8.ui.page.yandexmail;

import com.gomel.tat2015.home8.ui.page.Page;
import com.gomel.tat2015.home8.utils.Letter;
import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static com.gomel.tat2015.home8.utils.WebDriverHelper.waitForElementIsClickable;


public class InboxPage extends Page {

    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(INBOX_LINK_LOCATOR).click();
    }


}
