package com.gomel.tat2015.home8.tests;

import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class SendEmptyMailTest extends BaseLoginTest {

    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By ERROR_EMPTY_MAIL = By.xpath("//span[@class='b-notification b-notification_error b-notification_error_required']");

    @BeforeClass
    public void prepareBrowser() throws InterruptedException, MalformedURLException {
        driver = WebDriverHelper.getWebDriver();
        login();
    }

    @Test
    public void sendEmptyMail() {
        WebElement composeButton = WebDriverHelper.waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement sendMailButton = WebDriverHelper.waitForElementIsClickable(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        WebElement sendError = WebDriverHelper.waitForElementIsClickable(ERROR_EMPTY_MAIL);

        Assert.assertEquals(sendError.getText(), "Поле не заполнено. Необходимо ввести адрес.", "FailTest");

    }


    @AfterClass(description = "Close browser")
    public void clearBrowserChrome() {
        driver.quit();
    }
}
