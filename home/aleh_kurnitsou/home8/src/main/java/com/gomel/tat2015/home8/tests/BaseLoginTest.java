package com.gomel.tat2015.home8.tests;

import com.gomel.tat2015.home8.ui.page.yandexmail.LoginPage;
import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class BaseLoginTest {

    public static final String USER_LOGIN = "tat-test-tat@yandex.ru";
    public static final String USER_PASSWORD = "12345678Tat";

    protected WebDriver driver;

    @BeforeClass
    public void prepareBrowser() throws MalformedURLException, InterruptedException {
        driver = WebDriverHelper.getWebDriver();
    }

    @Test
    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(USER_LOGIN, USER_PASSWORD);
    }
}
