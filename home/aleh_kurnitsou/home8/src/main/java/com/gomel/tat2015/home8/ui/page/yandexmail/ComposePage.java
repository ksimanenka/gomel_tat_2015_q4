package com.gomel.tat2015.home8.ui.page.yandexmail;

import com.gomel.tat2015.home8.ui.page.Page;
import com.gomel.tat2015.home8.utils.Letter;
import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.gomel.tat2015.home8.utils.WebDriverHelper.waitForDisappear;

public class ComposePage extends Page {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By DRAFT_DIALOG_BUTTON_LOCATOR = By.xpath("//*[@data-action='dialog.save']");
    public static final By DRAFT_FOLDER_LOCATOR = By.xpath("//*[@href='#draft']");
    public static final String USER_LOGIN = "tat-test-tat@yandex.ru";
    public static final By GET_ID_MAIL_LOCATOR = By.className("b-statusline__link");

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebElement composeButton = WebDriverHelper.waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();

    }

    public void sendLetter(Letter letter) {

        WebElement toInput = WebDriverHelper.waitForElementIsClickable(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        letter.setId(getIdMail());

        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

    public void saveDraft(Letter letter) {
        WebElement toInput = WebDriverHelper.waitForElementIsClickable(TO_INPUT_LOCATOR);
        toInput.sendKeys(USER_LOGIN);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

        WebElement draftButton = WebDriverHelper.waitForElementIsClickable(DRAFT_FOLDER_LOCATOR);
        draftButton.click();

        WebElement draftDialogButton = driver.findElement(DRAFT_DIALOG_BUTTON_LOCATOR);
        draftDialogButton.click();

        WebElement YesDraftButton = WebDriverHelper.waitForElementIsClickable(DRAFT_FOLDER_LOCATOR);
        YesDraftButton.click();
    }

    private String getIdMail(){
        WebElement mailLocator = WebDriverHelper.waitForElementIsClickable(GET_ID_MAIL_LOCATOR);
        String href = mailLocator.getAttribute("href");
        String[] items = href.split("/");
        return items[4];
    }

}
