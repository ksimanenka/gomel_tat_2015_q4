package com.gomel.tat2015.home8.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class WebDriverHelper {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 25;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;

    private static WebDriver driver;

    public static WebDriver getWebDriver() throws MalformedURLException {
        if (driver == null) {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            //driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        }
    }

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 1000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
    public static WebElement waitForAppearence(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
