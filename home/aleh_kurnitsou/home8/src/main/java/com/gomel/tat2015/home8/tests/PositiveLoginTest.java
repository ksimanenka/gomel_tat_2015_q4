package com.gomel.tat2015.home8.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PositiveLoginTest extends BaseLoginTest {

    public static final String BASE_URL = "http://www.mail.yandex.ru";

    @Test
    public void loginPositiveTest() {
        login();
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue((currentUrl.contains(BASE_URL)));
    }

}