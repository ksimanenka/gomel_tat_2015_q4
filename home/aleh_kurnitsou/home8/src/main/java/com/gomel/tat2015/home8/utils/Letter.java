package com.gomel.tat2015.home8.utils;


public class Letter {
    private String id;
    private String to;
    private String subject;
    private String body;


    public Letter(String to, String subject, String body) {
        this.setTo(to);
        this.setSubject(subject);
        this.setBody(body);
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
