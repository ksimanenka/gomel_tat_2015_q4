package com.gomel.tat2015.home8.ui.page.yandexmail;

import com.gomel.tat2015.home8.ui.page.Page;
import com.gomel.tat2015.home8.utils.Letter;
import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DraftPage extends Page {

    public static final By DRAFT_FOLDER_LOCATOR = By.xpath("//*[@href='#draft']");
    public static final String LETTER_LOCATOR = ("//*[@class='block-messages']//a[contains(., '%s')]");
    public static final String BUTTON_LOCATOR = ("//*[@value='%s']");
    public static final By REMOVE_BUTTON_LOCATOR = By.xpath("//*[@data-action='delete']");
    public static final String HREF_ATRIBUTE = "href";
    public static final int ID_LENGTH = 19;

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebDriverHelper.waitForElementIsClickable(DRAFT_FOLDER_LOCATOR)
                .click();
    }

    public void removeDraftMail(Letter letter) {

        WebElement letterLocator = WebDriverHelper.waitForElementIsClickable(By.xpath(String.format(LETTER_LOCATOR, letter.getSubject())));
        String hrefAttr = letterLocator.getAttribute(HREF_ATRIBUTE);
        String id = hrefAttr.substring(hrefAttr.length() - ID_LENGTH);
        letter.setId(id);
        WebElement buttonLocator = WebDriverHelper.waitForElementIsClickable(By.xpath(String.format(BUTTON_LOCATOR, letter.getId())));
        buttonLocator.click();
        WebElement removeButton = driver.findElement(REMOVE_BUTTON_LOCATOR);
        removeButton.click();
    }
}
