package com.gomel.tat2015.home8.ui.page;

import com.gomel.tat2015.home8.utils.Letter;
import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class Page {

    public static final String MAIL_LINK_LOCATOR_PATTERN = ("//*[@class='block-messages']//a[@href='#message/%s']");

    protected WebDriver driver;

    public abstract void open();

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isLetterPresent(Letter letter) {
        try {
            open();
            WebDriverHelper.waitForAppearence(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getId())));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
