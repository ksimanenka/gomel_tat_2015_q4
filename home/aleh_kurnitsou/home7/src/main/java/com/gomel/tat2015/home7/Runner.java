package com.gomel.tat2015.home7;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Runner {

    public static void main(String[] args) {

        TestNG tng = new TestNG();

        XmlSuite suite = new XmlSuite();
        suite.setName("RunnerTests");
        List<String> files = new ArrayList();
        files.add("./src/main/resources/suites/runner.xml");
        suite.setSuiteFiles(files);
        List<XmlSuite> suites = Collections.singletonList(suite);
        tng.setXmlSuites(suites);
        tng.run();
    }
}
