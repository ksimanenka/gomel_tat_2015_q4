package com.gomel.tat2015.home7;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class LoginTest {

    public WebDriver driver;

    @Test(description = "Success mail login chrome")
    public void loginChrome() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().window().maximize();
        driver.get(DateClass.BASE_URL);
        driver.manage().timeouts().pageLoadTimeout(DateClass.DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DateClass.DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        WebElement loginInput = driver.findElement(DateClass.LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(DateClass.userLogin);
        WebElement passInput = driver.findElement(DateClass.PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(DateClass.userPassword);
        passInput.submit();
        WebElement enterButton = driver.findElement(DateClass.ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement logintext = driver.findElement(DateClass.LOGIN_LOCATOR);

        Assert.assertEquals(logintext.getText(), "tat-test-tat2015@yandex.by", "LOgin not succesful");
    }


    @Test(description = "Success sendmail chrome")
    public void sendmailChrome() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().window().maximize();
        driver.get(DateClass.BASE_URL);
        driver.manage().timeouts().pageLoadTimeout(DateClass.DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DateClass.DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        WebElement loginInput = driver.findElement(DateClass.LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(DateClass.userLogin);
        WebElement passInput = driver.findElement(DateClass.PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(DateClass.userPassword);
        passInput.submit();
        WebElement enterButton = driver.findElement(DateClass.ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement composeButton = driver.findElement(DateClass.COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(DateClass.TO_INPUT_LOCATOR);
        toInput.sendKeys(DateClass.mailTo);
//        WebElement subjectInput = driver.findElement(DateClass.SUBJECT_INPUT_LOCATOR);
//        subjectInput.sendKeys(DateClass.mailSubject);
//        WebElement mailContentText = driver.findElement(DateClass.MAIL_TEXT_LOCATOR);
//        mailContentText.sendKeys(DateClass.mailContent);
        WebElement sendMailButton = driver.findElement(DateClass.SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

    }


    @AfterClass(description = "Close browser")
    public void clearBrowserChrome() {
        driver.quit();
    }

    @Test(description = "Success mail login firefox")
    public void loginFirefox() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.manage().window().maximize();
        driver.get(DateClass.BASE_URL);
        driver.manage().timeouts().pageLoadTimeout(DateClass.DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DateClass.DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        WebElement loginInput = driver.findElement(DateClass.LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(DateClass.userLogin);
        WebElement passInput = driver.findElement(DateClass.PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(DateClass.userPassword);
        passInput.submit();
        WebElement enterButton = driver.findElement(DateClass.ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement logintext = driver.findElement(DateClass.LOGIN_LOCATOR);

        Assert.assertEquals(logintext.getText(), "tat-test-tat2015@yandex.by", "Fail");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowserFirefox() {
        driver.quit();
    }

    @Test(description = "Success sendmail chrome")
    public void sendmailFirefox() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.manage().window().maximize();
        driver.get(DateClass.BASE_URL);
        driver.manage().timeouts().pageLoadTimeout(DateClass.DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DateClass.DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        WebElement loginInput = driver.findElement(DateClass.LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(DateClass.userLogin);
        WebElement passInput = driver.findElement(DateClass.PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(DateClass.userPassword);
        passInput.submit();
        WebElement enterButton = driver.findElement(DateClass.ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement composeButton = driver.findElement(DateClass.COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(DateClass.TO_INPUT_LOCATOR);
        toInput.sendKeys(DateClass.mailTo);
//        WebElement subjectInput = driver.findElement(DateClass.SUBJECT_INPUT_LOCATOR);
//        subjectInput.sendKeys(DateClass.mailSubject);
//        WebElement mailContentText = driver.findElement(DateClass.MAIL_TEXT_LOCATOR);
//        mailContentText.sendKeys(DateClass.mailContent);
        WebElement sendMailButton = driver.findElement(DateClass.SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }
}
