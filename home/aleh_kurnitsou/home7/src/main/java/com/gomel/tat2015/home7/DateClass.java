package com.gomel.tat2015.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DateClass {

    
    public static final String BASE_URL = "http://mail.yandex.by";

    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By LOGIN_LOCATOR = By.xpath("//span[@class='header-user-name js-header-user-name']");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INPOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 30;
    private WebDriver driver;


    public static String userLogin = "tat-test-tat2015"; // ACCOUNT
    public static String userPassword = "12345678Tat"; // ACCOUNT
    public static String mailTo = "tat-test-tat2015@yandex.by"; // ENUM
    public static String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    public static String mailContent = "mail content" + Math.random() * 100000000;// RANDOM
}

