package com.gomel.tat.home5;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class WordList {

    private static final String WORDS_DELIMITERS = " !\"\'?.,*\t\n\r():-1234567890";

    private static PrintStream stream;

    public Map<String, Integer> values = new HashMap<>();

    public static PrintStream getStream() {
        return stream;
    }

    public static void setStream(PrintStream stream) {
        WordList.stream = stream;
    }

    public void getWords(String baseDirectory, String fileNamePattern) {
        File file = new File(baseDirectory);
        //TODO: add converter from filisystem mask to RegExp mask here
        searchFiles(file, fileNamePattern);
    }

    public void searchFiles (File file, String fileNamePattern) {

        if (!file.isDirectory()) {
            if (isMaskSuitable(fileNamePattern, file)) {
                parseFile(file);
            }
        }
        if (file.isDirectory()) {
            File[] children = file.listFiles();
            for (File child : children) {
                searchFiles(child, fileNamePattern);
            }
        }
    }

    public void parseFile(File file){
        try {
            String fileContent = FileUtils.readFileToString(file, CommonFileUtils.getFileEncodingType(file));
            StringTokenizer tokenizer = new StringTokenizer(fileContent, WORDS_DELIMITERS);
            while (tokenizer.hasMoreTokens()) {
                String token = tokenizer.nextToken();
                if (values.containsKey(token)) {
                    values.put(token, values.get(token) + 1);
                } else {
                    values.put(token, 1);
                }
            }
        } catch (IOException e) {
           e.printStackTrace();
        }
    }

    public void printStatistics() {
        for (Map.Entry<String, Integer> value : values.entrySet()) {
            stream.println(value.getKey() + ": " + value.getValue());
        }
        printWordsAmount();
    }

    public void printWordsAmount(){
        stream.println("Общее кол-во слов: " + values.size());
    }

    public boolean isMaskSuitable (String mask, File file){

        return file.getName().matches(mask);
    }
}
