package com.gomel.tat.home8.page.yandexmail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ClearTrashbox {
    public WebDriver driver;
    public static final By CLEAR_TRASH_BUTTON_LOCATOR = By.xpath("//*[@data-action='folder.clear']");
    public static final By CONFIRM_CLEAR_BUTTON_LOCATOR = By.xpath("//*[@data-action='dialog.submit']");
    public static final By INBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");

    public void clearTrashbox() {
        waitForElementIsClickable(CLEAR_TRASH_BUTTON_LOCATOR).click();
        waitForElementIsClickable(CONFIRM_CLEAR_BUTTON_LOCATOR).click();
        waitForElementIsClickable(INBOX_FOLDER_LOCATOR).click();
    }

    public WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

}
