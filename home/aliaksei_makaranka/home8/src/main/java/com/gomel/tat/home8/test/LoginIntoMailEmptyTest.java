package com.gomel.tat.home8.test;

import com.gomel.tat.home8.page.yandexmail.LoginPage;
import com.gomel.tat.home8.util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class LoginIntoMailEmptyTest {
    protected WebDriver driver;
    private String userLogin = "EpamTestLogin"; // ACCOUNT
    private String userPassword = ""; // ACCOUNT
    private static final By ERROR_ADRESS_REQUIRED_LOCATOR =
            By.xpath("//*[@class='pseudo-button_small pseudo-button_remember']");

    @BeforeClass
    public void prepareBrowser() throws MalformedURLException {
        driver = WebDriverHelper.getWebDriver();
    }

    @Test
    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin, userPassword);
        Assert.assertTrue(driver.findElement(ERROR_ADRESS_REQUIRED_LOCATOR).isDisplayed());
        driver.close();
    }

}
