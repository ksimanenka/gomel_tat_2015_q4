package com.gomel.tat.home8.test;


import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.bo.LetterFactory;
import com.gomel.tat.home8.page.yandexmail.ComposePage;
import com.gomel.tat.home8.page.yandexmail.InboxPage;
import com.gomel.tat.home8.util.WebDriverHelper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;



public class SendMailTest extends LoginIntoMailTest {

    private Letter letter;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test
    public void sendMail() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);

        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresent(letter), "Letter should be present in inbox folder");
    }

    @AfterClass
    public void shutdown() {
        WebDriverHelper.shutdownWebDriver();
    }
}
