package com.gomel.tat.home8.test;

import com.gomel.tat.home8.page.yandexmail.LoginPage;
import com.gomel.tat.home8.util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class LoginIntoMailTest {
    public WebDriver driver;
    private String userLogin = "EpamTestLogin"; // ACCOUNT
    private String userPassword = "EpamTestPassword"; // ACCOUNT

    @BeforeClass
    public void prepareBrowser() throws MalformedURLException {
        driver = WebDriverHelper.getWebDriver();
    }

    @Test
    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin, userPassword);
    }

}
