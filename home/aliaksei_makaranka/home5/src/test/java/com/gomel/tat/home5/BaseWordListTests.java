package com.gomel.tat.home5;

import com.gomel.tat.home4.WordList;
import org.testng.annotations.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BaseWordListTests {
    protected WordList wordList;
    protected static final String tempDirPath = "D:/tempDirectory";
    protected File tempDirectory = new File(tempDirPath);

    protected Map<String, Integer> expectedResult =
            new HashMap<String, Integer>() {{
                put("FirstTest", 3);
                put("SecondTest", 3);
                put("ThirdTest", 3);
            }};

    @BeforeSuite
    public void createTempbaseDirectoryWithTempfileNamePattern() throws IOException {
        createTempBaseDirectoryWithTempfileNamePattern(tempDirectory);
    }

    @BeforeClass
    public void beforeClass() {
        wordList = new WordList();
//        System.out.println("Before class");
    }

    @BeforeMethod
    public void setUp() {
        wordList = new WordList();
//        System.out.println("Before Method");
    }

    @BeforeGroups(value = "a")
    public void BeforeGroups() {
//        System.out.println("Before Groups");
    }

    @BeforeClass(groups = "a")
    public void BeforeClass() {
//        System.out.println("Before Class");
    }

    @AfterMethod
    public void afterMethod() {
//        System.out.println("After method");
    }

    @AfterSuite
    public void deleteTempbaseDirectoryWithTempfileNamePattern() {
        deleteTempBaseDirectoryWithTempfileNamePattern(tempDirectory);
//        System.out.println("After Suite");
    }

    @AfterClass
    public void afterClass() {
//        System.out.println("After class");
    }

    protected void CurrentTime() {
        System.out.println(new Date(System.currentTimeMillis()));
    }

    protected void Sleep() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    private void createTempBaseDirectoryWithTempfileNamePattern(File temp) throws IOException {
        temp.mkdir();
        for (int i = 1; i < 4; i++) {
            File tempSubFolder = new File(temp.getPath() + "\\home" + i);
            tempSubFolder.mkdir();
            File tempTxt = new File(tempSubFolder.getPath() + "\\hello.txt");
            tempTxt.createNewFile();
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(tempTxt));
            writer.write("!!!FirstTest g 23 5 t SecondTest 522  g !  ThirdTest");
            writer.close();
        }
    }

    private void deleteTempBaseDirectoryWithTempfileNamePattern(File tempDir) {
        if (tempDir.isDirectory()) {
            if (tempDir.listFiles() != null) {
                for (File f : tempDir.listFiles())
                    deleteTempBaseDirectoryWithTempfileNamePattern(f);
                tempDir.delete();
            } else {
                tempDir.delete();
            }
        } else {
            tempDir.delete();
        }
    }
}