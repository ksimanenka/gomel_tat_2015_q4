package com.gomel.tat.home4;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class WordList {
    private List<String> values = new ArrayList<String>();
    private Map<String, Integer> words = new TreeMap<String, Integer>();

    public Map<String, Integer> getWords(){
        return words;
    }

    public void getWords(String baseDirectory, String fileNamePattern) {
        try {
            if (!isLinkToFolderCorrect(baseDirectory)) {
                throw new IllegalArgumentException("Некорректный адрес папки");
            }
            Path startDir = Paths.get(baseDirectory);
            final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + fileNamePattern);     //задать начальную папку, glob: позволяет использовать более упрощенный чем регулярки синтаксис на подобии *.txt
            FileVisitor<Path> matcherVisitor = new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attribs) {
                    try {
                        Path name = file.getFileName();
                        if (matcher.matches(name)) {
                            File f1 = new File(file.toString());
                            List<String> lines = Files.readAllLines(Paths.get(f1.getPath()), Charset.defaultCharset());
                            for (String line : lines) {
                                String[] tempWords = line.replaceAll("[-\\\\+\\\\.\\\\^:,\"()'!\\d]", "").replaceAll("\t", " ").split(" ");
                                for (String tempWord : tempWords) {
                                    if (tempWord.length() > 1 && !tempWord.replaceAll(" ", "").equals(""))
                                        values.add(tempWord);
                                }
                            }
                            lines.clear();
                        }
                    } catch (IOException e) {
                        System.out.println(1);
                    }

                    return FileVisitResult.CONTINUE;
                }
            };

            Files.walkFileTree(startDir, matcherVisitor);
            for (String value : values) {
                int i = words.containsKey(value) ? words.get(value) : 0;
                words.put(value, ++i);
            }
            if (words.isEmpty())
                System.out.println("Записей не найдено");
        }
        catch (IOException e) {
            System.out.println("");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    public void printStatistics() {
        for (Map.Entry value : words.entrySet()) {
            System.out.println(value.getValue() + " " + value.getKey());
        }
    }

    private boolean isLinkToFolderCorrect(String baseDirectory) {
        try {
            File f = new File(baseDirectory);
            if (f.isDirectory())
                return true;
            else
               return false;
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Base directory is null");
        }
    }




}
