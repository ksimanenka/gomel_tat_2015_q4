package com.gomel.tat.home7;

import org.openqa.selenium.*;
import org.testng.annotations.Test;

public class SendMailTest extends Methods {

    private static final String mailName = "test";
    private static final By MAIL_SENT_LOCATOR = By.xpath(
            String.format("//*[@class='block-messages' and (not(@style) or @style='')]//a[contains(., '%s')]", mailName));

    @Test()
    public void TestSendLetter() {
        LoginToYandex();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement sendToInput = driver.findElement(SEND_TO_INPUT_LOCATOR);
        sendToInput.sendKeys(login + "@yandex.ru");
        WebElement subjectInput = driver.findElement(SUBJECT_LOCATOR);
        subjectInput.sendKeys(mailName);
        WebElement mailTextArea = driver.findElement(MAIL_TEXT_LOCATOR);
        mailTextArea.sendKeys("Test");
        WebElement sendButton = driver.findElement(SEND_BUTTON_LOCATOR);
        sendButton.click();
        waitForElementIsClickable(BACK_TO_INBOX_LOCATOR).click();
        waitForElementIsClickable(MAIL_SENT_LOCATOR).click();
        WebElement outboxFolder = driver.findElement(OUTBOX_FOLDER_LOCATOR);
        outboxFolder.click();
        waitForElementIsClickable(MAIL_SENT_LOCATOR).click();
        clearMailFolder(INBOX_FOLDER_LOCATOR);
        clearMailFolder(OUTBOX_FOLDER_LOCATOR);
        clearTrashbox();
    }
}
