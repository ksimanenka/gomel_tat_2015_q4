package com.gomel.tat.home7;

import org.openqa.selenium.By;

public class Information {

    public static String login = "EpamTestLogin";
    public static String password = "EpamTestPassword";
    public static String wrongPassword = "WrongPassword";

    //Login
    public static final String YANDEX_START_PAGE = "http://ya.ru";
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href,'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSOWRD_INPUT_LOCATOR = By.name("passwd");

    //Mail
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By INBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");
    public static final By OUTBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#sent' and @data-action='move']");
    public static final By DRAFT_FOLDER_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final By TRASH_FOLDER_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    public static final By STATUSLINE_LOCATOR = By.xpath("//div[@class='b-statusline']");
    public static final By CLEAR_TRASH_BUTTON_LOCATOR = By.xpath("//*[@data-action='folder.clear']");
    public static final By CONFIRM_CLEAR_BUTTON_LOCATOR = By.xpath("//*[@data-action='dialog.submit']");
    public static final By SELECT_ALL_MAIL_LOCATOR =
            By.xpath("//*[@class='block-messages' and (not(@style) or @style='')]//*[contains(@class,'head')]//*[contains(@type, 'checkbox')]");

    //Send mail
    public static final By SEND_TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");
    public static final By SEND_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By BACK_TO_INBOX_LOCATOR = By.xpath("//*[contains(@class,'done') and @href='#inbox']");

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    public static final int SKRIPT_WAIT_TIMEOUT_SECONDS = 10;
}
