package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendWrongLetterTest extends Methods {

    private static final By ERROR_ADRESS_REQUIRED_LOCATOR =
            By.xpath("//span[contains(@class,'error_required') and not(contains(@class,'hidden'))]");

    @Test()
    public void TestSendWrongMail() {
        LoginToYandex();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement sendButton = driver.findElement(SEND_BUTTON_LOCATOR);
        sendButton.click();
        Assert.assertTrue(driver.findElement(ERROR_ADRESS_REQUIRED_LOCATOR).isDisplayed());
    }
}
