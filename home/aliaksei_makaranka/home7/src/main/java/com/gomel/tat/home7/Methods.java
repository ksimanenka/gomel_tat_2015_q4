package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Methods extends Information {
    public WebDriver driver;

    public void LoginToYandex() {
        driver.get(YANDEX_START_PAGE);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(login);
        WebElement passwordInput = driver.findElement(PASSOWRD_INPUT_LOCATOR);
        passwordInput.sendKeys(password);
        passwordInput.submit();
    }

    public void clearMailFolder(By locator) {
        waitForElementIsClickable(locator).click();
        waitForElementIsClickable(SELECT_ALL_MAIL_LOCATOR).click();
        WebElement deleteButtonOut = driver.findElement(DELETE_BUTTON_LOCATOR);
        deleteButtonOut.click();
        waitForDisappear(STATUSLINE_LOCATOR);
    }

    public void clearTrashbox() {
        waitForElementIsClickable(CLEAR_TRASH_BUTTON_LOCATOR).click();
        waitForElementIsClickable(CONFIRM_CLEAR_BUTTON_LOCATOR).click();
        waitForElementIsClickable(INBOX_FOLDER_LOCATOR).click();
    }

    public WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    @BeforeClass(groups = "chrome")
    public void prepareChromeBrowser() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(SKRIPT_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @BeforeClass(groups = "firefox")
    public void prepareFirefoxBrowser() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(SKRIPT_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @AfterClass()
    public void clearBrowser() throws InterruptedException {
        driver.quit();
    }
}
