package com.gomel.tat.home10.lib.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class WebDriverHelper {
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;
    public static final String urlForDriver = "http://localhost:4444/wd/hub";

    protected static WebDriver driver;

    public static WebDriver getWebDriver(String browser) throws MalformedURLException {
        if (driver == null) {
            if (browser.equals("chrome")) {
                driver = new RemoteWebDriver(new URL(urlForDriver), DesiredCapabilities.chrome());
            }
            if (browser.equals("firefox")) {
                driver = new RemoteWebDriver(new URL(urlForDriver), DesiredCapabilities.firefox());
            }
            manageWebDriver();
        }
        return driver;
    }

    public static void manageWebDriver() {
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
            driver = null;
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        }
    }
}
