package com.gomel.tat.home10.tests.mail;

import com.gomel.tat.home10.lib.mail.Letter;
import com.gomel.tat.home10.lib.mail.LetterFactory;
import com.gomel.tat.home10.lib.mail.screen.pages.ComposePage;
import org.testng.Assert;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.gomel.tat.home10.lib.mail.service.MailService.checkIsErrorNotificationDisplayed;

public class SendWrongAddressTest extends TestPreparations {
    private Letter letter;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getLetterWithWrongAddress();
    }

    @Test(description = "Method login to yandexmail, compose mail using wrong mailTo field " +
            "and check the error notification is displayed")
    public void sendWrongMail() {
        loginToMail();
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        Assert.assertTrue(checkIsErrorNotificationDisplayed(), "Did not fill mailto field using bad address");
    }
}
