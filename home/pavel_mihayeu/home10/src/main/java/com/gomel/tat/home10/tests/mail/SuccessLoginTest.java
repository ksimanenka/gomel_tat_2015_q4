package com.gomel.tat.home10.tests.mail;


import com.gomel.tat.home10.lib.mail.screen.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home10.lib.common.AccountBuilder.getDefaultAccount;
import static com.gomel.tat.home10.lib.mail.service.LoginService.checkSuccessLogin;

public class SuccessLoginTest extends TestPreparations {

    @Test(description = "Method login to yandexmail using properly user data " +
            "and check the user name on inbox page is displayed")
    public void successLogin() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.loginDefaultUser(getDefaultAccount());
        Assert.assertTrue(checkSuccessLogin(), "Login is not success");
    }
}
