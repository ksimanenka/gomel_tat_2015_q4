package com.gomel.tat.home10.lib.mail.screen.pages;

import com.gomel.tat.home10.lib.common.Account;
import com.gomel.tat.home10.lib.mail.screen.Page;
import com.gomel.tat.home10.lib.mail.service.LoginService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.gomel.tat.home10.lib.ui.WebDriverWaits.waitForElementIsVisible;
import static com.gomel.tat.home10.lib.common.CommonConstants.*;

public class LoginPage extends Page {
    public static final By WRONG_LOGIN_ERROR_LOCATOR = By.xpath("//div[@class='error-msg']");

    @FindBy(name = "login")
    WebElement loginInput;

    @FindBy(name = "passwd")
    WebElement passInput;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage open() {
        driver.get(MAILBOX_URL);
        return this;
    }

    public LoginPage typeUsername(String username) {
        loginInput.sendKeys(username);
        return this;
    }

    public LoginPage typePassword(String password) {
        passInput.sendKeys(password);
        return this;
    }

    public InboxPage submitLogin() {
        passInput.submit();
        return new InboxPage(driver);
    }

    public LoginPage submitLoginExpectingFailure() {
        submitLogin();
        waitForElementIsVisible(WRONG_LOGIN_ERROR_LOCATOR);
        return this;
    }

    public InboxPage loginDefaultUser(Account account) {
        return LoginService.loginDefaultUser(driver, account);
    }

    public LoginPage wrongPasswordLogin(Account account) {
        return LoginService.wrongPasswordLogin(driver, account);
    }
}
