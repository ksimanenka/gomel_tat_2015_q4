package com.gomel.tat.home10.lib.ui;

import org.openqa.selenium.By;

public class LocatorsHelper {

    public static By getLocatorByPattern(String locatorPattern, String parameter) {
        return By.xpath(String.format(locatorPattern, parameter));
    }
}
