package com.gomel.tat.home10.lib.mail.screen.pages;

import com.gomel.tat.home10.lib.mail.Letter;
import com.gomel.tat.home10.lib.mail.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.gomel.tat.home10.lib.ui.LocatorsHelper.getLocatorByPattern;
import static com.gomel.tat.home10.lib.ui.WebDriverWaits.*;

public class AbstractMailListPage extends Page {
    public static final String MAIL_CHECKBOX_LOCATOR_PATTERN = "//input[@type='checkbox' and @value='%s']";
    public static final String MAIL_CHECKBOX_IS_CHECKED_LOCATOR_PATTERN = "//div[contains(@class, 'b-messages__message_checked') and @data-id='%s']";
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']/div[2]/a[8]");
    public static final By DELETE_NOTIFICATION_LOCATOR = By.xpath("//div[@class ='b-statusline']");

    public AbstractMailListPage(WebDriver driver) {
        super(driver);
    }

    public AbstractMailListPage deleteMail(String mailId) {
        waitForElementIsClickable(getLocatorByPattern(MAIL_CHECKBOX_LOCATOR_PATTERN, mailId)).click();
        waitForElementIsVisible(By.xpath(String.format(MAIL_CHECKBOX_IS_CHECKED_LOCATOR_PATTERN, mailId)));
        waitForElementIsClickable(DELETE_BUTTON_LOCATOR).click();
        waitForElementIsVisible(DELETE_NOTIFICATION_LOCATOR);
        return this;
    }

    public AbstractMailListPage deleteMail(Letter letter) {//TODO Implement this method using subject and body
        letter.getRecipient();
        letter.getSubject();
        letter.getBody();
        return this;
    }

    public Boolean isDeleteNotificationDisplayed() {
        return waitForElementIsVisible(DELETE_NOTIFICATION_LOCATOR).isDisplayed();
    }

    public String getPageUrl(String finalPartOfUrl) {
        String currentUrl = driver.getCurrentUrl();
        return currentUrl.substring(0, currentUrl.indexOf("#")).concat(finalPartOfUrl);
    }
}
