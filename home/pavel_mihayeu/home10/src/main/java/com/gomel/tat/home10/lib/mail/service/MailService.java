package com.gomel.tat.home10.lib.mail.service;

import com.gomel.tat.home10.lib.mail.Letter;
import com.gomel.tat.home10.lib.mail.screen.pages.ComposePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static com.gomel.tat.home10.lib.ui.LocatorsHelper.getLocatorByPattern;
import static com.gomel.tat.home10.lib.ui.WebDriverWaits.*;

public class MailService {
    public static final By ERROR_NOTIFICATION_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//span[contains(@class,'error_invalid')]");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//span[contains(@title, '%s')]";

    public static ComposePage sendLetter(WebDriver driver, Letter letter) {
        ComposePage composePage = PageFactory.initElements(driver, ComposePage.class);
        composePage.fillLetter(letter);
        waitForElementIsClickable(SEND_MAIL_BUTTON_LOCATOR).click();
        waitIsLetterSent();
        return composePage.submitSend();
    }

    public static Boolean checkIsErrorNotificationDisplayed() {
        return waitForElementIsVisible(ERROR_NOTIFICATION_LOCATOR).isDisplayed();
    }

    public static void waitIsLetterSent() {
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

    public static boolean isLetterPresent(Letter letter) {
        String subject = letter.getSubject();//TODO Add check for the letter also for its TEXT_BODY, TO, FROM
        if (subject.equals("")) {
            subject = "(Без темы)";
        }
        try {
            waitForMailArrived(getLocatorByPattern(MAIL_LINK_LOCATOR_PATTERN, subject));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
