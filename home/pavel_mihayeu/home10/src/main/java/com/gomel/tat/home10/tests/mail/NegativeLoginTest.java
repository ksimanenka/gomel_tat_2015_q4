package com.gomel.tat.home10.tests.mail;

import com.gomel.tat.home10.lib.mail.screen.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home10.lib.common.AccountBuilder.getAccountWithWrongPassword;
import static com.gomel.tat.home10.lib.mail.service.LoginService.checkErrorOnFailedLogin;

public class NegativeLoginTest extends TestPreparations {

    @Test(description = "Method try to login into yandexmail using wrong password " +
            "and check the error message is displayed.")
    public void negativeLogin() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.wrongPasswordLogin(getAccountWithWrongPassword());
        Assert.assertTrue(checkErrorOnFailedLogin(), "Error! Login success");
    }
}
