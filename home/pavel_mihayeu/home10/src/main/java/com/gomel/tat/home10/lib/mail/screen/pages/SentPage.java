package com.gomel.tat.home10.lib.mail.screen.pages;

import com.gomel.tat.home10.lib.mail.Letter;
import com.gomel.tat.home10.lib.mail.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home10.lib.ui.WebDriverWaits.waitForElementIsClickable;

public class SentPage extends Page {
    public static final By SENT_LINK_LOCATOR = By.xpath("//div[@class='b-folders__i']//a[@href='#sent']");

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public SentPage open() {
        waitForElementIsClickable(SENT_LINK_LOCATOR).click();
        return this;
    }
}
