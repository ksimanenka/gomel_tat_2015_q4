package com.gomel.tat.home10.lib.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverWaits extends WebDriverHelper{
    public static final int TIME_OUT_ELEMENT_CLICKABLE_SECONDS = 5000;
    public static final int TIME_OUT_ELEMENT_VISIBLE_SECONDS = 5000;
    public static final int TIME_OUT_ELEMENT_DISAPPEAR_SECONDS = 5000;
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_CLICKABLE_SECONDS).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_DISAPPEAR_SECONDS).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForElementIsVisible(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_VISIBLE_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForMailArrived(By locator) {
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
