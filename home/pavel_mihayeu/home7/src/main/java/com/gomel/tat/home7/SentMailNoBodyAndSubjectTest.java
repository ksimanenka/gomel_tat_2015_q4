package com.gomel.tat.home7;

import org.testng.Assert;
import org.openqa.selenium.WebElement;

import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mig on 23.12.2015.
 */
public class SentMailNoBodyAndSubjectTest extends LoginMailTest {

    @Test(description = "Send mail without subject and body")
    public void sendMailNoBodyAndSubjectTest() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement sendMailButton = driver.findElement(SENT_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForDisappear(SENT_MAIL_BUTTON_LOCATOR);
        waitForElementIsClickable(SENT_LINK_LOCATOR);
        WebElement sentLink = driver.findElement(SENT_LINK_LOCATOR);
        sentLink.click();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        String sentMailID = driver.findElement(FIRST_MAIL_LOCATOR).getAttribute("data-id");
        waitForElementIsClickable(INBOX_LINK_LOCATOR);
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        String inboxMailID = driver.findElement(FIRST_MAIL_LOCATOR).getAttribute("data-id");
        Assert.assertEquals(inboxMailID, sentMailID, "Error while mail received");
    }
}
