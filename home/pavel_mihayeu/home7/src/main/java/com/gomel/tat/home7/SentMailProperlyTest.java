package com.gomel.tat.home7;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mig on 22.12.2015.
 */
public class SentMailProperlyTest extends LoginMailTest {

    @Test(description = "Success send mail")
    public void sendMailProperlyTest() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        waitForElementIsVisible(MAIL_TEXT_LOCATOR);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement sentMailButton = driver.findElement(SENT_MAIL_BUTTON_LOCATOR);
        sentMailButton.click();
        waitForDisappear(SENT_MAIL_BUTTON_LOCATOR);
        waitForElementIsClickable(SENT_LINK_LOCATOR);
        WebElement sentLink = driver.findElement(SENT_LINK_LOCATOR);
        sentLink.click();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        String sentMailID = driver.findElement(FIRST_MAIL_LOCATOR).getAttribute("data-id");
        waitForElementIsClickable(INBOX_LINK_LOCATOR);
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        String inboxMailID = driver.findElement(FIRST_MAIL_LOCATOR).getAttribute("data-id");
        Assert.assertEquals(inboxMailID, sentMailID, "Error while mail received");
    }
}
