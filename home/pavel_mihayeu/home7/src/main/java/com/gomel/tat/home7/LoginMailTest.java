package com.gomel.tat.home7;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mig on 23.12.2015.
 */
public class LoginMailTest extends BaseMailTest {

    @Test(description = "Positive mail login")
    public void loginMailTest() {
        driver.get(BASE_URL);
        waitForElementIsVisible(ENTER_BUTTON_LOCATOR);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        Assert.assertEquals(driver.findElement(COMPOSE_BUTTON_LOCATOR).isDisplayed(), true, "Error while try to login");
    }
}
