package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mig on 23.12.2015.
 */
public class CreateDraftMailTest extends LoginMailTest {
    String mailID;

    @Test(description = "Create draft mail", priority = 0)
    public void createDraftMail() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        waitForElementIsVisible(MAIL_TEXT_LOCATOR);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        waitForElementIsClickable(DRAFT_LINK_LOCATOR);
        WebElement draftLink = driver.findElement(DRAFT_LINK_LOCATOR);
        draftLink.click();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        String draftMailParticipants = driver.findElement(DRAFT_MAIL_PARTICIPANTS_LOCATOR).getAttribute("title");
        String draftMailSubject = driver.findElement(DRAFT_MAIL_SUBJECT_LOCATOR).getAttribute("title");
        mailID = driver.findElement(FIRST_DRAFT_LOCATOR).getAttribute("data-id");
        Assert.assertEquals(String.format(draftMailParticipants, draftMailSubject), String.format(mailTo, mailSubject), "Draft do not create");
    }

    @Test(description = "Delete draft mail", priority = 1)
    public void deleteDraftMail() {
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        WebElement toFirstDraftCheckbox = driver.findElement(By.xpath("//input[@type='checkbox' and @value='"+mailID+"']"));
        toFirstDraftCheckbox.click();
        waitForElementIsVisible(DELETE_MAIL_BUTTON_LOCATOR);
        WebElement toDeleteButton = driver.findElement(DELETE_MAIL_BUTTON_LOCATOR);
        toDeleteButton.click();
        Boolean result = driver.findElement(FIRST_DRAFT_LOCATOR).getAttribute("data-id").equals(mailID);
        Assert.assertEquals(result, Boolean.FALSE, "Draft do not delete");
    }
}
