package com.gomel.tat.home9.ui.page.yandexdisk;

import com.gomel.tat.home9.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home9.util.WebDriverHelper.waitForElementIsVisible;

public class LoginPage extends Page{
    public static final By USER_NAME_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("password");
    public static final By USER_NAME_LOCATOR = By.xpath("//*[contains(@class, 'username')]");
    public static final String BASE_URL = "https://disk.yandex.ru/";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage open() {
        driver.get(BASE_URL);
        return this;
    }

    public LoginPage typeUsername(String username) {
        driver.findElement(USER_NAME_INPUT_LOCATOR).sendKeys(username);
        return this;
    }

    public LoginPage typePassword(String password) {
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
        return this;
    }

    public FilesPage submitLogin() {
        driver.findElement(PASSWORD_INPUT_LOCATOR).submit();
        waitForElementIsVisible(USER_NAME_LOCATOR);
        return new FilesPage(driver);
    }

    public FilesPage loginAs(String username, String password) {
        typeUsername(username);
        typePassword(password);
        return submitLogin();
    }
}
