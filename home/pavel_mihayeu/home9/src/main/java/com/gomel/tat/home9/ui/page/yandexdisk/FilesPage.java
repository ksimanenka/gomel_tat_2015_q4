package com.gomel.tat.home9.ui.page.yandexdisk;

import com.gomel.tat.home9.ui.page.Page;
import com.gomel.tat.home9.util.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import static com.gomel.tat.home9.util.DataProvider.getData;
import static com.gomel.tat.home9.util.FileUtils.getFilePath;
import static com.gomel.tat.home9.util.WebDriverHelper.*;
import static org.apache.commons.io.FileUtils.getFile;

public class FilesPage extends Page {
    public static final By FILES_LINK_LOCATOR = By.xpath("//div[@class='header__side-left']/a[2]");
    public static final By DELETE_NOTIFICATION_LOCATOR = By.xpath("//a[@href='/client/trash']/div");
    public static final By TRASH_BOX_LOCATOR = By.xpath("//div[@data-id='/trash']/div");
    public static final String UPLOADED_FILE_LOCATOR_PATTERN = "//div[@data-id='/disk/%s']";
    public static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN = "//button[@data-click-action='resource.download' and contains(@data-params, '%s')]";
    public static final String downloadExtension = ".crdownload";
    public static final String testFilePath = getData("filePath");
    public static final String downloadPath = getData("downloadPath");
    public static final String fileName = getData("fileName");
    public static final String fileName1 = getData("fileName1");
    public static final String fileName2 = getData("fileName2");
    public static final String fileName3 = getData("fileName3");

    public FilesPage(WebDriver driver) {
        super(driver);
    }

    public FilesPage open() {
        waitForElementIsClickable(FILES_LINK_LOCATOR).click();
        return this;
    }

    public FilesPage downloadFile(String fileName) {
        waitForElementIsClickable(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName))).click();
        waitForElementIsClickable(By.xpath(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, fileName))).click();
        FileUtils.waitForDownloadFile(getFilePath(downloadPath, fileName), downloadExtension);
        //TODO implement this for firefox browser using property annotation
        return new FilesPage(driver);
    }

    public TrashPage multipleDeleteFiles() {
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.CONTROL)
                .click(driver.findElement(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName1))))
                .click(driver.findElement(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName2))))
                .click(driver.findElement(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName3))))
                .keyUp(Keys.CONTROL)
                .dragAndDrop(driver.findElement(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName3))), driver.findElement(TRASH_BOX_LOCATOR))
                .build()
                .perform();
        waitForElementIsVisible(DELETE_NOTIFICATION_LOCATOR);
        return new TrashPage(driver);
    }

    public String getActualContent() {
        return FileUtils.readFileContent(FileUtils.getFilePath(testFilePath, fileName));
    }

    public String getExpectedContent() {
        return FileUtils.readFileContent(FileUtils.getFilePath(downloadPath, fileName));
    }

    public Boolean checkIsFileExists() {
        return getFile(FileUtils.getFilePath(testFilePath, fileName)).exists();
    }

    public Boolean checkIsFileDisplayed() {
        waitForElementIsVisible(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName)));
        return driver.findElement(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName))).isDisplayed();
    }
}
