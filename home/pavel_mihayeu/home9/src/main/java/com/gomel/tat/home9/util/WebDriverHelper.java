package com.gomel.tat.home9.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static com.gomel.tat.home9.util.DataProvider.getData;

public class WebDriverHelper {
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;
    //public static final WebDriverWait webDriverWait = new WebDriverWait(driver, 5000);

    private static WebDriver driver;

    public static WebDriver getWebDriver(String browser) throws MalformedURLException {
        if (driver == null) {
            if (browser.equals("chrome")) {
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", getData("downloadPath"));
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", chromePrefs);
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
            }
            if (browser.equals("firefox")) {
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("browser.helperapps.neverAsk.saveToDisk", "txt");
                profile.setPreference("browser.download.dir", getData("downloadPath"));
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                capabilities.setCapability(FirefoxDriver.PROFILE, profile);
                driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
            }
            manageWebDriver();
        }
        return driver;
    }

    public static void manageWebDriver() {
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
            driver = null;
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        }
    }

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    /*public static WebElement waitForDisappear(By locator) {
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }*/

    public static WebElement waitForElementIsVisible(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}