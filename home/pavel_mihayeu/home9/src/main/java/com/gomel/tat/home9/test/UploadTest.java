package com.gomel.tat.home9.test;

import com.gomel.tat.home9.ui.page.yandexdisk.UploadPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UploadTest extends TestPreparations {

    @Test (description = "Method login to yandex disk, upload test file and check the file is displayed on the disk")
    public void uploadTest() {
        loginYandexDisk();
        UploadPage uploadPage = new UploadPage(driver);
        uploadPage.uploadTestFile();
        Assert.assertTrue(uploadPage.checkFileIsDisplayed(), "File does not upload");
    }
}
