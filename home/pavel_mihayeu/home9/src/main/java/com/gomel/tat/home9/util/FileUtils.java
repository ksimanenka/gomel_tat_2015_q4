package com.gomel.tat.home9.util;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;

import static org.apache.commons.io.FileUtils.getFile;

public class FileUtils {

    public static String getFilePath(String path, String fileName) {
        return path.concat(fileName);
    }

    public static String readFileContent(String filePath) {
        String content = null;
        try {
            FileInputStream inputStream = new FileInputStream(filePath);
            try {
                content = IOUtils.toString(inputStream);

            } finally {
                inputStream.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public static void waitForDownloadFile(String filePath, String downloadExtension) {
        while (getFile(filePath.concat(downloadExtension)).exists()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
