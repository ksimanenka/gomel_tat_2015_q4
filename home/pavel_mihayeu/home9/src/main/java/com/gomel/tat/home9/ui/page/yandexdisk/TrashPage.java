package com.gomel.tat.home9.ui.page.yandexdisk;

import com.gomel.tat.home9.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import static com.gomel.tat.home9.util.DataProvider.getData;
import static com.gomel.tat.home9.util.WebDriverHelper.waitForElementIsClickable;
import static com.gomel.tat.home9.util.WebDriverHelper.waitForElementIsVisible;

public class TrashPage extends Page {
    public static final String UPLOADED_FILE_LOCATOR_PATTERN = "//div[@data-id='/disk/%s']";
    public static final String DELETED_FILE_LOCATOR_PATTERN = "//div[@title='%s' and @data-nb='resource']";
    public static final String DELETED_FILE_BUTTON_LOCATOR_PATTERN = "//button[@data-click-action='resource.delete' and contains(@data-params, '%s')]";
    public static final String RESTORE_FILE_BUTTON_LOCATOR_PATTERN = "//button[@data-click-action='resource.restore' and contains(@data-params, '%s')]";
    public static final By TRASH_BOX_LOCATOR = By.xpath("//div[@data-id='/trash']/div");
    public static final By TRASH_BOX_BUTTON_LOCATOR = By.xpath("//button[@data-click-action and contains(@data-params, 'trash')]");
    public static final By RESTORE_NOTIFICATION_LOCATOR = By.xpath("//div[contains(@class, 'item_moved')]");
    public static final By DELETE_NOTIFICATION_LOCATOR = By.xpath("//a[@href='/client/trash']/div");
    public static final By FINAL_DELETE_NOTIFICATION_LOCATOR = By.xpath("//div[contains(@class, 'notifications__text')]");
    public static final String fileName = getData("fileName");
    public static final String fileName1 = getData("fileName1");
    public static final String fileName2 = getData("fileName2");
    public static final String fileName3 = getData("fileName3");


    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public TrashPage open() {
        waitForElementIsClickable(TRASH_BOX_LOCATOR).click();
        waitForElementIsClickable(TRASH_BOX_BUTTON_LOCATOR).click();
        return this;
    }

    public WebElement getDraggableFile() {
        return driver.findElement(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName)));
    }

    public WebElement getTrash() {
        return driver.findElement(TRASH_BOX_LOCATOR);
    }

    public TrashPage deleteFileToTrash() {
        Action action = new Actions(driver).dragAndDrop(getDraggableFile(), getTrash()).build();
        action.perform();
        waitForElementIsVisible(DELETE_NOTIFICATION_LOCATOR);
        return new TrashPage(driver);
    }

    public TrashPage finalDeleteFile() {
        waitForElementIsVisible(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName))).click();
        waitForElementIsVisible(By.xpath(String.format(DELETED_FILE_BUTTON_LOCATOR_PATTERN, fileName))).click();
        waitForElementIsVisible(FINAL_DELETE_NOTIFICATION_LOCATOR);
        return new TrashPage(driver);
    }

    public TrashPage restoreTestFileFromTrash() {
        waitForElementIsVisible(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName))).click();
        waitForElementIsVisible(By.xpath(String.format(RESTORE_FILE_BUTTON_LOCATOR_PATTERN, fileName))).click();
        waitForElementIsVisible(RESTORE_NOTIFICATION_LOCATOR);
        return new TrashPage(driver);
    }

    public Boolean finalDeleteCheck() {
        return driver.findElement(FINAL_DELETE_NOTIFICATION_LOCATOR).isDisplayed();
    }

    public Boolean checkIsDeletedFileDisplayed() {
        waitForElementIsVisible(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName)));
        return driver.findElement(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName))).isDisplayed();
    }

    public Boolean checkIsDeletedFilesDisplayed() {
        waitForElementIsVisible(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName1)));
        waitForElementIsVisible(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName2)));
        waitForElementIsVisible(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName3)));
        if (driver.findElement(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName1))).isDisplayed())
            if (driver.findElement(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName2))).isDisplayed())
                if (driver.findElement(By.xpath(String.format(DELETED_FILE_LOCATOR_PATTERN, fileName2))).isDisplayed());
                else return false;
        return Boolean.TRUE;
    }
}
