package com.gomel.tat.home9.test;

import com.gomel.tat.home9.ui.page.yandexdisk.LoginPage;
import com.gomel.tat.home9.util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;

import static com.gomel.tat.home9.util.DataProvider.getData;

public class TestPreparations {
    String userLogin = getData("userLogin");
    String userPassword = getData("userPassword");
    protected WebDriver driver;

    @BeforeClass
    @Parameters("browser")
    public void prepareBrowser(String browser) throws MalformedURLException {
        driver = WebDriverHelper.getWebDriver(browser);
    }

    public void loginYandexDisk() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.loginAs(userLogin, userPassword);
    }

    @AfterClass
    public void shutdown() {
        WebDriverHelper.shutdownWebDriver();
    }
}
