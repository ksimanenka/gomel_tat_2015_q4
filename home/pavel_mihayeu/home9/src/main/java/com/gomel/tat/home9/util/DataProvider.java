package com.gomel.tat.home9.util;

import java.io.InputStream;
import java.util.Properties;

public class DataProvider {
    private static final Properties userData;
    private static final String propertiesPath = "/data.properties";

    static {
        userData = new Properties();
        InputStream inputStream = DataProvider.class.getResourceAsStream(propertiesPath);
        try {
            try {
                userData.load(inputStream);
            } finally {
                inputStream.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static String getData(String dataName) {
        return userData.getProperty(dataName);
    }
}
