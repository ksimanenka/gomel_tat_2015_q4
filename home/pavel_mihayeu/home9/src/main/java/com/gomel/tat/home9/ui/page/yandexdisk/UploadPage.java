package com.gomel.tat.home9.ui.page.yandexdisk;

import com.gomel.tat.home9.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home9.util.DataProvider.getData;
import static com.gomel.tat.home9.util.WebDriverHelper.waitForElementIsClickable;
import static com.gomel.tat.home9.util.WebDriverHelper.waitForElementIsVisible;

public class UploadPage extends Page {
    public static final By INPUT_BUTTON_LOCATOR = By.xpath("//input[@class='button__attach']");
    public static final By UPLOAD_DONE_LOCATOR = By.xpath("//div[contains(@class, 'upload__icon_done')]");
    public static final By DIALOG_CLOSE_BUTTON_LOCATOR = By.xpath("//button[contains(@class, 'button-close')]");
    public static final String UPLOADED_FILE_LOCATOR_PATTERN = "//div[@data-id='/disk/%s']";
    public static final String testPath = getData("filePath");
    public static final String fileName = getData("fileName");
    public static final String fileName1 = getData("fileName1");
    public static final String fileName2 = getData("fileName2");
    public static final String fileName3 = getData("fileName3");

    public UploadPage(WebDriver driver) {
        super(driver);
    }

    public FilesPage uploadTestFile() {
        return uploadFile(testPath, fileName);
    }

    public FilesPage uploadTestFile1() {
        return uploadFile(testPath, fileName1);
    }
    public FilesPage uploadTestFile2() {
        return uploadFile(testPath, fileName2);
    }
    public FilesPage uploadTestFile3() {
        return uploadFile(testPath, fileName3);
    }


    public FilesPage uploadFile(String testPath, String fileName) {
        driver.findElement(INPUT_BUTTON_LOCATOR).sendKeys(testPath.concat(fileName));
        waitForElementIsVisible(UPLOAD_DONE_LOCATOR);
        waitForElementIsClickable(DIALOG_CLOSE_BUTTON_LOCATOR).click();
        return new FilesPage(driver);
    }

    //TODO Implement multiple upload method
    /*public FilesPage multipleUpload() {
        return finishUpload();
    }*/

    public Boolean checkFileIsDisplayed() {
        return driver.findElement(By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName))).isDisplayed();
    }
}
