package com.gomel.tat.home4;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordList {

    private static List<String> values = new ArrayList<String>();

    public static void main(String[] args) throws IOException {

        // Create URL of Downloaded repository
        URL gitArchiveURL = new URL("https://bitbucket.org/ksimanenka/gomel_tat_2015_q4/get/9b7a63db8498.zip");

        //Download and save repository
        File repo = new File("repo.zip");
        FileUtils.copyURLToFile(gitArchiveURL, repo);

        //UnZip repository
        String source = "repo.zip";
        String destination = "repoTAT2015";


        try {
            ZipFile zipFile = new ZipFile(source);
            zipFile.extractAll(destination);
        } catch (ZipException e) {
            e.printStackTrace();
        }


        getWords("repoTAT2015\\ksimanenka-gomel_tat_2015_q4-9b7a63db8498\\home\\", "home3\\hello.txt");


        //Print words and the number of inclusions
        printStatistics();

        //Delete temporary directories and files
        FileUtils.deleteDirectory(new File(destination));
        FileUtils.forceDelete(repo);


    }

    //Find number of inclusions per word in 'values' and print it
    public static void printStatistics() {

        //Make unique words List
        List<String> uniqWords = new ArrayList<String>();
        for (String word : values) {
            if (!uniqWords.contains(word))
                uniqWords.add(word);

        }
        //Find number of inclusions per word in values
        for (String val : uniqWords) {
            int count = 0;
            for (String word : values) {
                if (val.equals(word)) {
                    count++;
                }
            }
            System.out.println(val + " - " + count);
        }
    }

    //Find recursively all files that match to fileNamePattern argument in baseDirectory and read words from found files
    public static void getWords(String baseDirectory, String fileNamePattern) throws IOException {

        final File folder = new File(baseDirectory);

        for (final File fileEntry : folder.listFiles()) {

            //Find files in base directory
            if (fileEntry.isDirectory()) {
                getWords(fileEntry.getPath(), fileNamePattern);
            } else {
                if (fileEntry.getPath().endsWith(fileNamePattern)) {

                    //Read words from files and add into 'values'
                    FileInputStream inputStream = new FileInputStream(fileEntry.getPath());
                    try {
                        String everything = IOUtils.toString(inputStream);
                        Pattern p = Pattern.compile("[а-яА-Яa-zA-Zí]+");
                        Matcher m = p.matcher(everything);

                        while (m.find()) {
                            values.add(everything.substring(m.start(), m.end()));
                        }

                    } finally {
                        inputStream.close();
                    }
                }
            }

        }

    }


}
