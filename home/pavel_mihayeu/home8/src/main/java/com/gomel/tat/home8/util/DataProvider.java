package com.gomel.tat.home8.util;

import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Mig on 06.01.2016.
 */
public class DataProvider {
    private static final Properties Data;

    static {
        Data = new Properties();
        InputStream is = Locators.class.getResourceAsStream("/data.properties");
        try {
            Data.load(is);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static String getData(String dataName) {
        return Data.getProperty(dataName);
    }

    public static String getRandomMailSubject() {
        return "test subject" + Math.random() * 100000000;
    }

    public static String getRandomMailContent() {
        return "test content" + Math.random() * 100000000;
    }
}
