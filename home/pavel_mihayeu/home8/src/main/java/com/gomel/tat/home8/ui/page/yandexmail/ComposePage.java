package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.util.Locators;
import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.ui.page.Page;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import static com.gomel.tat.home8.util.WebDriverHelper.waitForDisappear;
import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsClickable;

/**
 * Created by Mig on 27.12.2015.
 */
public class ComposePage extends Page {

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public ComposePage open() {
        waitForElementIsClickable(Locators.get("COMPOSE_BUTTON_LOCATOR")).click();
        return new ComposePage(driver);
    }

    public SentPage sendLetter(Letter letter) {
        fillLetter(letter);
        waitForElementIsClickable(Locators.get("SEND_MAIL_BUTTON_LOCATOR")).click();
        waitIsLetterSent();
        return new SentPage(driver);
    }

    public ComposePage pressCtrlEnter() {
        Actions builder = new Actions(driver);
        builder.keyDown(Keys.CONTROL).sendKeys(Keys.ENTER).perform();
        return new ComposePage(driver);
    }

    public void waitIsLetterSent() {
        waitForDisappear(Locators.get("SEND_MAIL_BUTTON_LOCATOR"));
    }
}
