package com.gomel.tat.home8.test;

import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.bo.LetterFactory;
import com.gomel.tat.home8.ui.page.yandexmail.ComposePage;
import com.gomel.tat.home8.ui.page.yandexmail.InboxPage;
import com.gomel.tat.home8.ui.page.yandexmail.SentPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Mig on 27.12.2015.
 */
public class SendEmptyFieldsTest extends TestPreparations {
    private Letter letter;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getEmptyLetter();
    }

    @Test
    public void sendEmptyMail() {
        login();
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresent(letter), "Letter should be present in inbox page");
    }

    @Test(dependsOnMethods = "sendEmptyMail")
    public void checkSentMail() {
        SentPage sentPage = new SentPage(driver);
        sentPage.open();
        Assert.assertTrue(sentPage.isLetterPresent(letter), "Letter should be present in sent page");
    }
}
