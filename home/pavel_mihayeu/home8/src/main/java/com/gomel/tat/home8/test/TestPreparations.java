package com.gomel.tat.home8.test;

import com.gomel.tat.home8.ui.page.yandexmail.LoginPage;
import com.gomel.tat.home8.util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;

import static com.gomel.tat.home8.util.DataProvider.getData;

/**
 * Created by Mig on 27.12.2015.
 */
public class TestPreparations {
    protected WebDriver driver;

    @BeforeClass
    @Parameters("browser")
    public void prepareBrowser(String browser) throws MalformedURLException {
        driver = WebDriverHelper.getWebDriver(browser);
    }

    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.loginAs(getData("userLogin"), getData("userPassword"));
    }

    @AfterClass
    public void shutdown() {
        WebDriverHelper.shutdownWebDriver();
    }
}
