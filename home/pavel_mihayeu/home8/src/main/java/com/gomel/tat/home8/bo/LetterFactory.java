package com.gomel.tat.home8.bo;

import static com.gomel.tat.home8.util.DataProvider.getData;
import static com.gomel.tat.home8.util.DataProvider.getRandomMailContent;
import static com.gomel.tat.home8.util.DataProvider.getRandomMailSubject;

/**
 * Created by Mig on 27.12.2015.
 */
public class LetterFactory {

    public static Letter getRandomLetter() {
        return new Letter(getData("mailTo"), getRandomMailSubject(), getRandomMailContent());
    }

    public static Letter getBadLetter() {
        return new Letter(getData("badMailTo"), getRandomMailSubject(), getRandomMailContent());
    }

    public static Letter getEmptyLetter() {
        return new Letter(getData("mailTo"), getData("emptyMailSubject"), getData("emptyMailContent"));
    }
}
