package com.gomel.tat.home8.ui.page;

import com.gomel.tat.home8.util.Locators;
import com.gomel.tat.home8.bo.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.gomel.tat.home8.util.DataProvider.getData;
import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsClickable;
import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsVisible;

/**
 * Created by Mig on 27.12.2015.
 */
public class Page {
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    protected WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public Page typeTo(String mailTo) {
        driver.findElement(Locators.get("MAILTO_INPUT_LOCATOR")).sendKeys(mailTo);
        return this;
    }

    public Page typeSubject(String subject) {
        driver.findElement(Locators.get("SUBJECT_INPUT_LOCATOR")).sendKeys(subject);
        return this;
    }

    public Page typeMailText(String mailText) {
        driver.findElement(Locators.get("MAIL_TEXT_LOCATOR")).sendKeys(mailText);
        return this;
    }

    public Page fillLetter(Letter letter) {
        typeTo(letter.getTo());
        typeSubject(letter.getSubject());
        typeMailText(letter.getBody());
        return new Page(driver);
    }

    public String getPageUrl(String finalPartOfUrl) {
        String currentUrl = driver.getCurrentUrl();
        String resultUrl = currentUrl.substring(0, currentUrl.indexOf("#")).concat(finalPartOfUrl);
        return resultUrl;
    }

    public Page deleteMail(String mailId) {
        waitForElementIsClickable(By.xpath(String.format(getData("MAIL_CHECKBOX_LOCATOR_PATTERN"), mailId))).click();
        waitForElementIsVisible(By.xpath(String.format(getData("MAIL_CHECKBOX_IS_CHECKED_LOCATOR_PATTERN"), mailId)));
        waitForElementIsClickable(Locators.get("DELETE_BUTTON_LOCATOR")).click();
        waitForElementIsVisible(Locators.get("DELETE_NOTIFICATION_LOCATOR"));
        return new Page(driver);
    }

    public boolean isLetterPresent(Letter letter) {
        String subject = letter.getSubject();
        if (subject.equals(""))
            subject = "(Без темы)";
        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(getData("MAIL_LINK_LOCATOR_PATTERN"), subject))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
