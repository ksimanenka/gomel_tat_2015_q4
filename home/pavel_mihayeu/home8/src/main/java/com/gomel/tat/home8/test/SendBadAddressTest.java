package com.gomel.tat.home8.test;

import com.gomel.tat.home8.util.Locators;
import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.bo.LetterFactory;
import com.gomel.tat.home8.ui.page.yandexmail.ComposePage;
import org.testng.Assert;

import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsVisible;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Mig on 27.12.2015.
 */
public class SendBadAddressTest extends TestPreparations {
    private Letter letter;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getBadLetter();
    }

    @Test
    public void sendBadMail() {
        login();
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.fillLetter(letter);
        composePage.pressCtrlEnter();
        waitForElementIsVisible(Locators.get("NOTIFICATION_ERROR_LOCATOR"));
        Assert.assertTrue(driver.findElement(Locators.get("NOTIFICATION_ERROR_LOCATOR")).isDisplayed(), "Did not fill mailto field using bad address");
    }
}
