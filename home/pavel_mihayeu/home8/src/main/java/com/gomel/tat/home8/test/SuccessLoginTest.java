package com.gomel.tat.home8.test;

import com.gomel.tat.home8.ui.page.yandexmail.LoginPage;
import com.gomel.tat.home8.util.Locators;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home8.util.DataProvider.getData;
import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsVisible;

/**
 * Created by Mig on 27.12.2015.
 */
public class SuccessLoginTest extends TestPreparations {

    @Test
    public void successLogin() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.loginAs(getData("userLogin"), getData("userPassword"));
        waitForElementIsVisible(Locators.get("USER_NAME_LOCATOR"));
        Assert.assertTrue(driver.findElement(Locators.get("USER_NAME_LOCATOR")).isDisplayed(), "Login is not success");
    }
}
