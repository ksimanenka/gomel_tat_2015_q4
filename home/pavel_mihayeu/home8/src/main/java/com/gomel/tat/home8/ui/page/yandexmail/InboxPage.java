package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.util.Locators;
import com.gomel.tat.home8.ui.page.Page;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsClickable;

/**
 * Created by Mig on 27.12.2015.
 */
public class InboxPage extends Page {

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public InboxPage open() {
        waitForElementIsClickable(Locators.get("INBOX_LINK_LOCATOR")).click();
        return new InboxPage(driver);
    }
}
