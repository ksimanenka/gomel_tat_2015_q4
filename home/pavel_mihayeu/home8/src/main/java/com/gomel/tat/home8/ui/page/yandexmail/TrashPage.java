package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.ui.page.Page;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home8.util.DataProvider.getData;

/**
 * Created by Mig on 06.01.2016.
 */
public class TrashPage extends Page {

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public TrashPage open() {
        driver.navigate().to(getPageUrl(getData("partOfTrashUrl")));
        return new TrashPage(driver);
    }

    public TrashPage deleteTrash(String trashId) {
        deleteMail(trashId);
        return new TrashPage(driver);
    }
}
