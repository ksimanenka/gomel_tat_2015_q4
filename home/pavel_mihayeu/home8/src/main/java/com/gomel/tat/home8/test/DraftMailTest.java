package com.gomel.tat.home8.test;

import com.gomel.tat.home8.util.Locators;
import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.bo.LetterFactory;
import com.gomel.tat.home8.ui.page.yandexmail.ComposePage;
import com.gomel.tat.home8.ui.page.yandexmail.DraftPage;
import com.gomel.tat.home8.ui.page.yandexmail.TrashPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Mig on 27.12.2015.
 */
public class DraftMailTest extends TestPreparations {
    private Letter letter;
    private String draftId;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test
    public void createAndCheckDraft() {
        login();
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.fillLetter(letter);
        DraftPage draftPage = new DraftPage(driver);
        draftPage.waitIsDraftSave();
        draftId = draftPage.getDraftId();
        draftPage.open();
        Assert.assertTrue(draftPage.isLetterPresent(letter), "Letter should be present in draft folder");
    }

    @Test(dependsOnMethods = "createAndCheckDraft")
    public void deleteAndCheckDeletedDraft() {
        DraftPage draftPage = new DraftPage(driver);
        draftPage.deleteDraft(draftId);
        Assert.assertTrue(driver.findElement(Locators.get("DELETE_NOTIFICATION_LOCATOR")).isDisplayed(), "Letter does not deleted from draft folder");
    }

    @Test(dependsOnMethods = "deleteAndCheckDeletedDraft")
    public void finalDeleteDraft() {
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        trashPage.deleteTrash(draftId);
        Assert.assertTrue(driver.findElement(Locators.get("DELETE_NOTIFICATION_LOCATOR")).isDisplayed(), "Letter does not deleted from trash folder");
    }
}
