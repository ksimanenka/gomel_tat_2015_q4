package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.util.Locators;
import com.gomel.tat.home8.ui.page.Page;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home8.util.DataProvider.getData;
import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsVisible;

/**
 * Created by Mig on 27.12.2015.
 */
public class LoginPage extends Page {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage open() {
        driver.get(getData("BASE_URL"));
        return new LoginPage(driver);
    }

    public LoginPage typeUsername(String username) {
        driver.findElement(Locators.get("USER_NAME_INPUT_LOCATOR")).sendKeys(username);
        return this;
    }

    public LoginPage typePassword(String password) {
        driver.findElement(Locators.get("PASSWORD_LOCATOR")).sendKeys(password);
        return this;
    }

    public InboxPage submitLogin() {
        driver.findElement(Locators.get("PASSWORD_LOCATOR")).submit();
        return new InboxPage(driver);
    }

    public LoginPage submitLoginExpectingFailure() {
        driver.findElement(Locators.get("PASSWORD_LOCATOR")).submit();
        waitForElementIsVisible(Locators.get("BAD_LOGIN_ERROR_LOCATOR"));
        return new LoginPage(driver);
    }

    public InboxPage loginAs(String username, String password) {
        typeUsername(username);
        typePassword(password);
        return submitLogin();
    }

    public LoginPage badLoginAs(String username, String password) {
        typeUsername(username);
        typePassword(password);
        return submitLoginExpectingFailure();
    }
}
