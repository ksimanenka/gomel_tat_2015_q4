package com.gomel.tat.home4;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * WordList find all files that match to "fileNamePattern" argument in
 * BitBucket.com repository GOMEL_TAT_2015_Q4, read words from found files and
 * print to console number of inclusions per word in these files.
 *
 * @author Pavel Mihayeu
 * @version 0.2 15 Dec 2015
 */

public class WordList {

    private List<String> values = new ArrayList<String>();
    private Map<String, Integer> resultMap = new HashMap<String, Integer>();
    FileUtils fileUtils = new FileUtils();

    public List<String> getValues() {
        return values;
    }

    public Map<String, Integer> getResultMap() {
        return resultMap;
    }

    public void run() {

        String gitArchiveAddress = "https://bitbucket.org/ksimanenka/gomel_tat_2015_q4/get/14a988c221c4.zip";
        String gitArchiveName = "repo.zip";
        String temporaryFolder = "repoTAT2015";
        String fileNamePattern = "*hello*.txt";

        fileUtils.delTempFiles(temporaryFolder, gitArchiveName);
        fileUtils.downloadGitArchive(gitArchiveAddress, gitArchiveName);
        fileUtils.unZipGitArchive(gitArchiveName, temporaryFolder);
        getWords(temporaryFolder + "\\ksimanenka-gomel_tat_2015_q4-14a988c221c4\\home\\", fileNamePattern);
        printStatistics();
        System.out.println(values.size());
        fileUtils.delTempFiles(temporaryFolder, gitArchiveName);
    }

    public void printStatistics() {
        Set<String> uniqueWord = new HashSet<String>(values);
        for (String key : uniqueWord) {
            resultMap.put(key, Collections.frequency(values, key));
            System.out.println(key + " - " + Collections.frequency(values, key));
        }
    }

    public void findWords(String path) {
        FileUtils utils = new FileUtils();
        try {
            FileInputStream inputStream = new FileInputStream(path);
            String words;
            try {
                words = IOUtils.toString(inputStream, utils.getFileEncodingType(path));
                Pattern p = Pattern.compile("[а-яА-Яa-zA-ZÀ-Ⱬ]+");
                Matcher m = p.matcher(words);
                while (m.find()) {
                    values.add(words.substring(m.start(), m.end()));
                }
            } finally {
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getWords(String baseDirectory, String fileNamePattern) {
        if (baseDirectory != null) {
            File folder = new File(baseDirectory);
            File[] files = folder.listFiles();
            if (files == null)
                files = new File[0];
            for (File fileEntry : files) {
                if (fileEntry.isDirectory()) {
                    getWords(fileEntry.getPath(), fileNamePattern);
                } else {
                    if (FilenameUtils.wildcardMatch(fileEntry.getName(), fileNamePattern)) {
                        findWords(fileEntry.getPath());
                    }
                }
            }
        }
    }
}
