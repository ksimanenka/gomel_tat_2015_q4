package com.gomel.tat.home5;

import com.gomel.tat.home4.FileUtils;
import com.gomel.tat.home4.WordList;
import org.testng.annotations.*;

import java.util.Date;

public class BaseTest {
    FileUtils fileUtils = new FileUtils();
    WordList wordList = new WordList();
    String tmpFolder = "temporaryFolder";
    String tmpGitArchive = "testArchive.zip";
    String gitArchiveAddress = "https://bitbucket.org/ksimanenka/gomel_tat_2015_q4/get/14a988c221c4.zip";

    @BeforeSuite
    public void beforeSuite() {
        checkTime();
        fileUtils.delTempFiles(tmpFolder, tmpGitArchive);
        fileUtils.downloadGitArchive(gitArchiveAddress, tmpGitArchive);
        fileUtils.unZipGitArchive(tmpGitArchive, tmpFolder);
        System.out.println("beforeSuite passed");
    }

    @BeforeClass
    public void bc() {
        System.out.println("bc");
    }

    @BeforeMethod
    public void bm() {
        System.out.println("bm");
    }

    @BeforeGroups(value = "a")
    public void bg() {
        System.out.println("bg");
    }

    @AfterMethod
    public void am() {
        System.out.println("am");
    }

    @AfterClass
    public void ac() {
        System.out.println("ac");
    }

    @AfterSuite
    public void as() {
        fileUtils.delTempFiles(tmpFolder, tmpGitArchive);
        System.out.println("After suite passed");
    }

    protected void checkTime() {
        System.out.println("Current time: " + new Date(System.currentTimeMillis()));
    }
}
