
-------------------------------------------------Yandex login page-------------------------------------------------------------------

yandexInputLoginLocator                         //input[@name='login']                                           input[name=login]
yandexInputPassword                             //input[@name='passwd']                                          input[name=passwd]
yandexLoginButton:                              //form[@method='POST']/div[4]/span/button                        form[method=POST] div:nth-child(4) span button

-------------------------------------------------Yandex mail page-------------------------------------------------------------------

yandexIncomingMailLocator
yandexOutcomingMailLocator    		        //a[@href='#sent']					         a[href='#sent']
yandexSpamMailLocator    	                //a[@href='#spam']					         a[href='#spam']
yandexDeletedMailLocator	           	//a[@href='#trash']					         a[href='#trash']
yandexDraftsMailLocator			        //a[@href='#draft']					         a[href='#draft']

yandexWritenewMailButtonLocator 		//a[@href='#compose']					         a[href='#compose']
Refresh button:                                 //div[@class='b-toolbar__i']/div[2]/a[3]                         div.b-toolbar__i div:nth-child(2) a:nth-child(3)
Forward button:                                 //div[@class='b-toolbar__i']/div[2]/a[6]                         div.b-toolbar__i div:nth-child(2) a:nth-child(6)
Delete button:                                  //div[@class='b-toolbar__i']/div[2]/a[8]                         div.b-toolbar__i div:nth-child(2) a:nth-child(8)
"Mark as spam" button

