package com.gomel.tat.home4;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordList {
    private List<String> values = new ArrayList<String>();
    Map <String, Integer> culcWords = new TreeMap <String, Integer>();

    public void getWords(String baseDirectory, final String fileNamePattern) {
        // your code goes here

        File dir = new File(baseDirectory );
        IOFileFilter fileFilter = new IOFileFilter(){

            public boolean accept(File file) {
                return file.getName().equals(fileNamePattern);
            }
            public boolean accept(File file, String s) {
                return file.equals(fileNamePattern);
            }
        };
        List<File> files = (List<File>)FileUtils.listFiles(dir, fileFilter, TrueFileFilter.INSTANCE);
        for (File fileWithRec : files) {
            try {
                values.addAll(FileUtils.readLines(fileWithRec));
            }
            catch (IOException exc) {
                exc.printStackTrace();
            }
        }

        Pattern pattern = Pattern.compile("\\p{L}+");
        for(String string : values){
            Matcher matcher = pattern.matcher(string);
            while (matcher.find()) {
                String line = string.substring(matcher.start(), matcher.end()).toLowerCase();
                if (culcWords.containsKey(line)) {
                    culcWords.put(line, culcWords.get(line) + 1);
                } else {
                    culcWords.put(line, 1);
                }
            }
        }
    }

    public void printStatistics() {
        // your code goes here
        Set<Map.Entry<String, Integer>> set = culcWords.entrySet();

        for (Map.Entry<String, Integer> stat : set) {
            System.out.print(stat.getKey() + ": ");
            System.out.println(stat.getValue());
        }
    }
}
