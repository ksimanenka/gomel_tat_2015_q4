package com.gomel.tat.maxim_shilo.home13.tests.mail;

import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.LoginService;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.MailService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.common.AccountFactory.*;
import static com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo.LetterFactory.*;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DraftMailTest {

    private static final String CREATE_DRAFT_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] isn't present in draft folder.";
    private static final String DELETE_MAIL_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] is not present in trash folder.";
    private static final String DELETE_PERMANENTLY_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] is not deleted permanently and present in trash folder.";

    private Letter letter = getRandomLetter();
    private MailService mailService = new MailService();

    @BeforeClass(description = "Login to mail.")
    public void login(){
        new LoginService().successLogin(getAccount());
    }

    @Test(description = "Save draft mail, check that mail is present in draft folder.")
    public void createDraftMail() {
        mailService.saveInDraft(letter);
        assertThat(format(CREATE_DRAFT_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterPresent(letter));
    }

    @Test(dependsOnMethods = "createDraftMail", description = "Delete draft mail, check that mail is present in trash folder.")
    public void deleteDraftMail() {
        mailService.deleteDraftLetter(letter);
        mailService.openTrashFolder();
        assertThat(format(DELETE_MAIL_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterPresent(letter));
    }

    @Test(dependsOnMethods = "deleteDraftMail", description = "Delete draft mail permanently, check that mail deleted.")
    public void deleteLetterPermanently() {
        mailService.deleteLetter(letter);
        assertThat(format(DELETE_PERMANENTLY_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterNotPresent(letter));
    }
}
