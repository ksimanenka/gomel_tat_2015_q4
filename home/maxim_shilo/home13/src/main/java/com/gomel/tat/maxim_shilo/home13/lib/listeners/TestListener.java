package com.gomel.tat.maxim_shilo.home13.lib.listeners;

import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;
import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class TestListener extends TestListenerAdapter {

    private Map<Thread, String> currentTestNames = new HashMap<>();

    public String getCurrentTestName() {
        return currentTestNames.get(Thread.currentThread());
    }

    @Override
    public void onStart(ITestContext testContext) {
        currentTestNames.put(Thread.currentThread(), testContext.getCurrentXmlTest().getName());
        Logger.current().info("");
        Logger.current().info("==========================================================================");
        Logger.current().info("  TEST STARTED: " + testContext.getCurrentXmlTest().getName());
        Logger.current().info("==========================================================================");
        Browser.rise();
    }

    @Override
    public void onFinish(ITestContext testContext) {
        Logger.current().info("==========================================================================");
        Logger.current().info("  TEST FINISHED: " + testContext.getCurrentXmlTest().getName());
        Logger.current().info("  Test methods run: " + testContext.getAllTestMethods().length +
                ", Failures: " + testContext.getFailedTests().size() + ", Skips: " + testContext.getSkippedTests().size());
        if (testContext.getFailedConfigurations().size() > 0) {
            Logger.current().info("  Configuration failures: " + testContext.getFailedConfigurations().size());
        }
        Logger.current().info("==========================================================================");
        Logger.current().info("");
        Browser.quit();
    }

    @Override
    public void onTestFailure(ITestResult testRes) {
        Logger.current().error("Test method failed: " + testRes.getMethod().getDescription(), testRes.getThrowable());
        try {
            Browser.current().getScreenShot();
        } catch (IOException e) {
            Logger.current().error("Failed to get screenshot.", e);
        }
    }
}
