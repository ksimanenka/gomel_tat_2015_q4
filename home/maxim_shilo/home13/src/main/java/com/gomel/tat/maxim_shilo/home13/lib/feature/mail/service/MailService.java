package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service;

import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen.*;
import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;

public class MailService {

    private BaseMailPage baseMailPage = new BaseMailPage();
    private ComposePage composePage = new ComposePage();

    public void sendMail(Letter letter) {
        Logger.current().info("Send mail.");
        baseMailPage.clickComposeLetterButton()
                .typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody())
                .send();
    }

    public void sendEmptyMail(String recipient) {
        Logger.current().info("Send empty mail.");
        baseMailPage.clickComposeLetterButton()
                .typeRecipient(recipient)
                .send();
    }

    public boolean isLetterSent() {
        Logger.current().info("Check that letter sent.");
        return new LetterSentPage().isOpened();
    }

    public void backToInbox(){
        Logger.current().info("Open inbox folder.");
        new LetterSentPage().backToInbox();
    }

    public void deleteLetter(Letter letter) {
        Logger.current().info("Delete letter [" + letter.getSubject() + "]");
        baseMailPage.openLetter(letter)
                .clickDeleteLetterButton()
                .closeMessageLetterDeleted();
    }

    public void deleteDraftLetter(Letter letter) {
        Logger.current().info("Delete draft letter [" + letter.getSubject() + "]");
        baseMailPage.openDraftLetter(letter)
                .clickDeleteComposeButton()
                .closeMessageLetterDeleted();
    }

    public void openInboxFolder() {
        Logger.current().info("Open inbox folder.");
        baseMailPage.openFolder(new InboxFolderPage());
    }

    public void openOutboxFolder() {
        Logger.current().info("Open outbox folder.");
        baseMailPage.openFolder(new OutboxFolderPage());
    }

    public void openTrashFolder() {
        Logger.current().info("Open trash folder.");
        baseMailPage.openFolder(new TrashFolderPage());
    }

    public boolean isLetterPresent(Letter letter) {
        Logger.current().info("Check that letter [" + letter.getSubject() + "] is present.");
        return baseMailPage.isLetterPresent(letter);
    }

    public boolean isLetterNotPresent(Letter letter) {
        Logger.current().info("Check that letter [" + letter.getSubject() + "] not present.");
        return baseMailPage.isLetterNotPresent(letter);
    }

    public String retrieveErrorOnFailedSendMail() {
        Logger.current().info("Retrieving error message.");
        return composePage.getErrorMessage();
    }

    public void saveInDraft(Letter letter) {
        Logger.current().info("Save letter in drafts.");
        baseMailPage.clickComposeLetterButton()
                .typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody());
        baseMailPage.openFolder(new DraftFolderPage());
        composePage.saveLetter();
    }
}
