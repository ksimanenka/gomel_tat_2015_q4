package com.gomel.tat.maxim_shilo.home13.tests.disk;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.LoginService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.common.AccountFactory.*;
import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileFactory.*;

import static org.hamcrest.MatcherAssert.assertThat;

public class UploadFileTest {

    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "File [%s] is not uploaded to disk.";

    private YandexFile yandexFile = getYandexFile();
    public DiskService diskService = new DiskService();

    @BeforeClass(description = "Login to yandex disk.")
    public void login() {
        new LoginService().loginToDisk(getAccount());
    }

    @Test(description = "Upload file, check that file correctly uploaded.")
    public void uploadFile() {
        diskService.uploadFile(yandexFile);
        assertThat(String.format(ASSERT_FAIL_MESSAGE_PATTERN, yandexFile), diskService.isFilePresent(yandexFile));
    }
}
