package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.Button;

public class FileDetailsWindow {

    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.download' and contains(@data-params,'%s')]";

    private Button download;

    public FileDetailsWindow(YandexFile yandexFile) {
        download = new Button(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, yandexFile));
    }

    public FileDetailsWindow clickDownloadButton() {
        download.click();
        return this;
    }
}
