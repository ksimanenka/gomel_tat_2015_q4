package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.*;

public class ComposePage {

    private Input recipient = new Input("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private Input subject = new Input("//*[@name='subj']");
    private Input mailBody = new Input("//*[@id='compose-send_ifr']");
    private Button send = new Button("//*[@id='compose-submit']");
    private Button deleteCompose = new Button("//a[@data-action='compose.delete']");
    private Button save = new Button("//button[@data-action='dialog.save']");
    private Element adressRequiredMessage = new Element("//span[contains(@class,'error_required') and not(contains(@class,'hidden'))]");
    private Element dialogWindow = new Element("//*[@class='b-popup']");

    private static final int SCROLL_OFFSET = -250;

    public ComposePage typeRecipient(String sendTo) {
        recipient.sendKeys(sendTo);
        return this;
    }

    public ComposePage typeSubject(String subj) {
        subject.sendKeys(subj);
        return this;
    }

    public ComposePage typeBody(String bodyText) {
        mailBody.sendKeys(bodyText);
        return this;
    }

    public LetterSentPage send() {
        send.click();
        return new LetterSentPage();
    }

    public String getErrorMessage() {
        if (adressRequiredMessage.isPresent()) {
            return adressRequiredMessage.getText();
        } else {
            throw new RuntimeException("Error message is not present.");
        }
    }

    public BaseMailPage clickDeleteComposeButton() {
        Browser.current().scrollWindow(SCROLL_OFFSET);
        deleteCompose.click();
        return new BaseMailPage();
    }

    public BaseMailPage saveLetter() {
        if (dialogWindow.isPresent()) {
            save.click();
        }
        return new BaseMailPage();
    }
}
