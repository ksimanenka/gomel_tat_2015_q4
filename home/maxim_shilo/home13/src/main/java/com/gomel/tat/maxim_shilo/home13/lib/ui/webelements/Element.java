package com.gomel.tat.maxim_shilo.home13.lib.ui.webelements;

import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;
import org.openqa.selenium.By;

public class Element {

    private By element;

    public Element(String xpath) {
        element = By.xpath(xpath);
    }

    public boolean isPresent() {
        return Browser.current().isElementPresent(element);
    }

    public boolean isNotPresent() {
        return Browser.current().checkThatElementNotPresent(element);
    }

    public String getText() {
        return Browser.current().getText(element);
    }

    public Element waitForVisibility() {
        Browser.current().waitForElementIsVisible(element);
        return this;
    }

    public Element waitForDisappear() {
        Browser.current().waitForDisappear(element);
        return this;
    }

    public void dragAndDrop(Element to){
        Browser.current().dragAndDrop(element, to.element);
    }

    public By getBy(){
        return element;
    }

    @Override
    public String toString() {
        return element.toString();
    }
}
