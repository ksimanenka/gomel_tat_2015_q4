package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.*;

public class BaseMailPage {

    private static final String MAIL_LINK_LOCATOR_PATTERN =
            "//*[@class='block-messages' and (not(@style) or @style=\"\")]//*[@title='%s']";

    private Button compose = new Button("//a[@href='#compose']");
    private Link userDropdownMenu = new Link("//*[@id='nb-1']");
    private Element statusline = new Element("//span[@class='b-statusline__content' and contains(text(),'message deleted')]");
    private Button closeStatusLine = new Button("//div[@class='b-statusline']//span[@title='Hide message']");

    private Link uniqueLetter(Letter letter) {
        return new Link(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()));
    }

    public boolean isOpened() {
        return userDropdownMenu.isPresent();
    }

    public FoldersMenu openFolder(FoldersMenu folderPage) {
        folderPage.getFolder().click();
        return folderPage;
    }

    public ComposePage clickComposeLetterButton() {
        compose.click();
        return new ComposePage();
    }

    public void closeMessageLetterDeleted() {
        statusline.waitForVisibility();
        closeStatusLine.click();
    }

    public LetterPage openLetter(Letter letter) {
        uniqueLetter(letter).click();
        return new LetterPage();
    }

    public ComposePage openDraftLetter(Letter letter) {
        uniqueLetter(letter).click();
        return new ComposePage();
    }

    public boolean isLetterPresent(Letter letter) {
        return uniqueLetter(letter).isPresent();
    }

    public boolean isLetterNotPresent(Letter letter) {
        return uniqueLetter(letter).isNotPresent();
    }

}
