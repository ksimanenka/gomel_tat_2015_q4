package com.gomel.tat.maxim_shilo.home13.tests.disk;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskServiceScripts;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileFactory.getYandexFile;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class RestoreFileTest {

    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "File [%s] is not restored from trash folder.";

    private YandexFile yandexFile = getYandexFile();
    private DiskService diskService = new DiskService();

    @BeforeClass(description = "Prepare file in trash for test [restoreFile].")
    public void prepareFile() {
        new DiskServiceScripts().prepareFileInTrash(yandexFile);
    }

    @Test(description = "Restore deleted file from trash, check that file restored")
    public void restoreFile() {
        diskService.restoreFile(yandexFile);
        diskService.openDisk();
        assertThat(format(ASSERT_FAIL_MESSAGE_PATTERN, yandexFile), diskService.isFilePresent(yandexFile));
    }
}
