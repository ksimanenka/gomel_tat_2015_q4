package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.Element;

public class LoginFailedPage {

    private Element errorMessage = new Element("//*[@class='error-msg']");

    public String getErrorMessage() {
        if (errorMessage.isPresent()) {
            return errorMessage.getText();
        } else {
            throw new RuntimeException("Error message is not present!");
        }
    }
}
