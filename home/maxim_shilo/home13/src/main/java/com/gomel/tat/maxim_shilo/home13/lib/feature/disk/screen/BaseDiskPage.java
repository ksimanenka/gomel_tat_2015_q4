package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.Element;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.Link;

import java.util.ArrayList;

public class BaseDiskPage {

    private static final String UPLOADED_FILE_LOCATOR_PATTERN = "//div[contains(@data-id,'%s')]";

    private Element fileDeletedMessage = new Element("//div[@class='notifications__text js-message']");

    public Link file(YandexFile yandexFile) {
        return new Link(String.format(UPLOADED_FILE_LOCATOR_PATTERN, yandexFile));
    }

    public boolean isFilePresent(YandexFile yandexFile) {
        return file(yandexFile).isPresent();
    }

    public boolean isFileNotPresent(YandexFile yandexFile) {
        return file(yandexFile).isNotPresent();
    }

    public boolean isFilesPresents(ArrayList<YandexFile> yandexFiles) {
        for (YandexFile yandexFile : yandexFiles) {
            if (!file(yandexFile).isPresent()) {
                return false;
            }
        }
        return true;
    }

    public void waitUntilFileDeleted() {
        fileDeletedMessage.waitForVisibility().waitForDisappear();
    }
}
