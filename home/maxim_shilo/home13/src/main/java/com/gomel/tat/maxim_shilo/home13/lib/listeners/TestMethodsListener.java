package com.gomel.tat.maxim_shilo.home13.lib.listeners;

import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestMethodsListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            Logger.current().debug("TEST METHOD STARTED [" + method.getTestMethod().getDescription() + "]");
        } else {
            Logger.current().debug("METHOD STARTED [" + method.getTestMethod().getDescription() + "]");
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            Logger.current().debug("TEST METHOD FINISHED [" + method.getTestMethod().getDescription() + "]");
        } else {
            Logger.current().debug("METHOD FINISHED [" + method.getTestMethod().getDescription() + "]");
        }
    }
}
