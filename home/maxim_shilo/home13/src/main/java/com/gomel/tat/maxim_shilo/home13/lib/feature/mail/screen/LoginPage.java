package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.Button;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.Input;

public class LoginPage {

    private static final String MAILBOX_URL = "https://mail.yandex.com";

    private Input login = new Input("//input[@name ='login']");
    private Input password = new Input("//input[@name='passwd']");
    private Button logIn = new Button("//button[contains(@class,'start')]");

    public LoginPage open() {
        Browser.current().open(MAILBOX_URL);
        return this;
    }

    public LoginPage typeLogin(String userName) {
        login.sendKeys(userName);
        return this;
    }

    public LoginPage typePassword(String userPassword) {
        password.sendKeys(userPassword);
        return this;
    }

    public void submitLoginForm() {
        logIn.click();
    }
}
