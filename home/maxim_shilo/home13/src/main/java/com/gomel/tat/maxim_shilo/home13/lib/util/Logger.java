package com.gomel.tat.maxim_shilo.home13.lib.util;

import com.gomel.tat.maxim_shilo.home13.config.GlobalConfig;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home13.lib.listeners.TestListener;
import org.apache.commons.io.FileUtils;
import org.testng.xml.XmlSuite;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Logger {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);
    private static final String LOG_DIR = "./test-output/log/";
    public TestListener testListener = new TestListener();

    private static boolean ignore = false;
    private static Logger instance;

    public static Logger current() {
        if (instance == null) {
            instance = new Logger();
            instance.startLog();
        }
        return instance;
    }

    public void debug(String message) {
        if (GlobalConfig.getInstance().getParallelMode() == XmlSuite.ParallelMode.FALSE) {
            logger.debug("[DEBUG] " + message);
        } else {
            logger.debug("[DEBUG][" + testListener.getCurrentTestName() + "] " + message);
        }
    }

    public void info(String message) {
        if (GlobalConfig.getInstance().getParallelMode() == XmlSuite.ParallelMode.FALSE) {
            logger.info("[INFO] " + message);
        } else {
            logger.info("[INFO][" + testListener.getCurrentTestName() + "] " + message);
        }
    }

    public void error(String message, Throwable exception) {
        if (GlobalConfig.getInstance().getParallelMode() == XmlSuite.ParallelMode.FALSE) {
            logger.error("[ERROR] " + message, exception);
        } else {
            logger.error("[ERROR][" + testListener.getCurrentTestName() + "] " + message, exception);
        }
    }


    public void startLog() {
        if (!ignore) {
            ignore = true;
            try {
                File log = new File(LOG_DIR + "lastLog.log");
                if (log.exists()) {
                    String content = FileUtils.readFileToString(log);
                    Pattern pattern = Pattern.compile("([A-Za-z]{3} ){2}([0-9]{2}[ \\.]){4}EET 20[0-9]{2}");
                    Matcher matcher = pattern.matcher(FileUtils.readFileToString(log));
                    if (matcher.find()) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
                        String logDir = LOG_DIR + "/" + dateFormat.format(new Date());
                        new File(logDir).mkdir();
                        File oldLog = new File(logDir + "/" + matcher.group() + ".log");
                        oldLog.createNewFile();
                        FileUtils.write(oldLog, content);
                        FileUtils.write(log, "");
                    }
                }
                String date = java.util.Calendar.getInstance().getTime().toString().replaceAll(":", ".");
                logger.fatal("Starting new session: " + date);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }
}
