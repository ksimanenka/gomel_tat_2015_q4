package com.gomel.tat.maxim_shilo.home13.tests.mail;

import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.LoginService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.common.AccountFactory.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MailMissedCredentialLoginTest {

    private static final String ASSERT_FAIL_MESSAGE = "Error present but actual and expected message not equals.";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS = "Wrong username or password.";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST = "No such account with this username.";

    private LoginService loginService = new LoginService();

    @Test(description = "Check expected error message in case of wrong password")
    public void checkErrorMessageWrongPassword() {
        loginService.loginToMailBox(getAccountWithWrongPassword());
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertThat(ASSERT_FAIL_MESSAGE, actualErrorMessage, equalTo(EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS));
    }

    @Test(description = "Check expected error in case of non existed account")
    public void checkErrorMessageNonExistedAccount() {
        loginService.loginToMailBox(getNonExistedAccount());
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertThat(ASSERT_FAIL_MESSAGE, actualErrorMessage, equalTo(EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST));
    }
}
