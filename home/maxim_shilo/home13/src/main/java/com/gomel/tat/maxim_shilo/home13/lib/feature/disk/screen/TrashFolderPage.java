package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;

public class TrashFolderPage extends BaseDiskPage {

    private static final String TRASH_PAGE = "https://disk.yandex.com/client/trash";

    public TrashFolderPage open() {
        Browser.current().open(TRASH_PAGE);
        return this;
    }

    public DeletedFileDetailsWindow selectFile(YandexFile yandexFile) {
        file(yandexFile).click();
        return new DeletedFileDetailsWindow(yandexFile);
    }
}
