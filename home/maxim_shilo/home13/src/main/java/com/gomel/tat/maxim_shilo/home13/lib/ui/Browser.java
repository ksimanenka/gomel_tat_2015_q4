package com.gomel.tat.maxim_shilo.home13.lib.ui;

import com.gomel.tat.maxim_shilo.home13.config.GlobalConfig;
import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Browser extends BrowserSetup {

    private WebDriver driver;
    private static Map<Thread, Browser> instances = new HashMap<>();

    private static final String DEFAULT_SCREENSHOTS_DIRECTORY = "./test-output/screenshots/";

    private static final int DEFAULT_TIMEOUT_SECONDS = 30;
    private static final int TIMEOUT_FOR_ELEMENT_NOT_PRESENT = 3;

    public static void rise() {
        Logger.current().debug("Rise browser.");
        try {
            if (instances.get(Thread.currentThread()) != null) {
                instances.get(Thread.currentThread()).quit();
            }
            Browser browser = new Browser();
            browser.createDriver();
            instances.put(Thread.currentThread(), browser);
        } catch (MalformedURLException e) {
            Logger.current().error(e.getMessage(), e);
        }
    }

    public static synchronized Browser current() {
        if (instances.get(Thread.currentThread()) == null) {
            throw new SessionNotCreatedException("");
        }
        return instances.get(Thread.currentThread());
    }

    public static void quit() {
        Logger.current().debug("Close browser.");
        try {
            WebDriver driver = instances.get(Thread.currentThread()).getWrappedDriver();
            if (driver != null) {
                driver.quit();
            }
        } catch (Exception e) {
            Logger.current().error("Problem with shutting down driver: " + e.getMessage(), e);
        } finally {
            instances.put(Thread.currentThread(), null);
        }
    }

    private WebDriver createDriver() throws MalformedURLException {
        Logger.current().debug("Create driver." + GlobalConfig.getInstance().getBrowserType());
        switch (GlobalConfig.getInstance().getBrowserType()) {
            case CHROME:
                driver = getChromeRemoteWebDriver();
                break;
            case FIREFOX:
                driver = getFirefoxRemoteWebDriver();
                break;
        }
        return driver;
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void open(String URL) {
        Logger.current().debug("Open page: " + URL);
        driver.get(URL);
    }

    public WebElement findElement(By locator) {
        Logger.current().debug("Find element: '" + locator + "'");
        waitForElementIsPresent(locator);
        return getWrappedDriver().findElement(locator);
    }

    public void click(By locator) {
        waitForElementIsClickable(locator);
        Logger.current().debug("Click: '" + locator + "'");
        getWrappedDriver().findElement(locator).click();
    }

    public void writeText(By locator, String text) {
        waitForElementIsPresent(locator);
        Logger.current().debug("Write text: '" + text + "' to: '" + locator + "'");
        getWrappedDriver().findElement(locator).sendKeys(text);
    }

    public void submitForm(By locator) {
        waitForElementIsPresent(locator);
        Logger.current().debug("Submit: '" + locator + "'");
        getWrappedDriver().findElement(locator).submit();
    }

    public String getText(By locator) {
        waitForElementIsPresent(locator);
        Logger.current().debug("Get text from element located: '" + locator + "'");
        return getWrappedDriver().findElement(locator).getText();
    }

    public void dragAndDrop(By elementLocator, By toLocator) {
        try {
            Logger.current().debug("Drag and drop: [" + elementLocator + "] to: [" + toLocator + "]");
            waitForElementIsPresent(elementLocator);
            waitForElementIsPresent(toLocator);
            Action action = new Actions(getWrappedDriver())
                    .dragAndDrop(findElement(elementLocator), findElement(toLocator))
                    .build();
            action.perform();
        } catch (StaleElementReferenceException e) {
            dragAndDrop(elementLocator, toLocator);
        }
    }

    public void dragAndDrop(ArrayList<By> elements, By to) {
        try {
            Logger.current().debug("Drag and drop: [" + elements + "] to: [" + to + "]");
            Actions builder = new Actions(getWrappedDriver());
            builder.keyDown(Keys.CONTROL);
            for (By element : elements) {
                builder.click(findElement(element));
            }
            builder.keyUp(Keys.CONTROL);
            builder.clickAndHold(findElement(elements.get(0)))
                    .moveToElement(findElement(to))
                    .release();
            builder.build().perform();
        } catch (StaleElementReferenceException e) {
            refresh();
            dragAndDrop(elements, to);
        }
    }

    public boolean isElementPresent(By locator) {
        Logger.current().debug("Check that element present: " + locator);
        try {
            waitForElementIsPresent(locator);
        } catch (RuntimeException e) {
            return false;
        }
        return true;
    }

    public boolean checkThatElementNotPresent(By locator) {
        Logger.current().debug("Check that element not present: " + locator);
        try {
            new WebDriverWait(getWrappedDriver(), TIMEOUT_FOR_ELEMENT_NOT_PRESENT)
                    .until(ExpectedConditions.presenceOfElementLocated(locator));
        } catch (RuntimeException e) {
            return true;
        }
        return false;
    }

    public void scrollWindow(int offset) {
        Logger.current().debug("Scrolling window.");
        ((JavascriptExecutor) getWrappedDriver())
                .executeScript(String.format("window.scrollBy(0,%s)", offset), "");
    }

    public void waitForElementIsClickable(By locator) {
        Logger.current().debug("Wait for element is clickable: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitForElementIsPresent(By locator) {
        Logger.current().debug("Wait for element is present: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForElementIsVisible(By locator) {
        Logger.current().debug("Wait for element is visible: '" + locator + "'");
        try {
            new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (StaleElementReferenceException e) {
            waitForElementIsVisible(locator);
        }
    }

    public void waitForDisappear(By locator) {
        Logger.current().debug("Wait for element is disappear: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void getScreenShot() throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        String screenshotDir = DEFAULT_SCREENSHOTS_DIRECTORY + "/" + dateFormat.format(new Date());
        File screenDir = new File(screenshotDir);
        if (!screenDir.exists()) {
            screenDir.mkdirs();
        }
        String date = java.util.Calendar.getInstance().getTime().toString().replaceAll(":", ".");
        String sreenshotName = screenshotDir + "/" + date + ".png";
        Logger.current().debug("Take screenshot: " + sreenshotName);
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(sreenshotName));
    }

    public void delay(int milliseconds) {
        Logger.current().debug("Delay for " + milliseconds + " milliseconds.");
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            Logger.current().error(e.getMessage(), e);
        }
    }

    public void refresh() {
        Logger.current().debug("  => Refresh page.");
        getWrappedDriver().navigate().refresh();
    }
}