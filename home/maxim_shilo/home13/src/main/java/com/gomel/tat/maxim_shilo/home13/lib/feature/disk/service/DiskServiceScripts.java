package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.LoginService;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.common.AccountFactory.getAccount;

public class DiskServiceScripts {

    private DiskService diskService = new DiskService();

    public void prepareFileInTrash(YandexFile yandexFile){
        new LoginService().loginToDisk(getAccount());
        diskService.uploadFile(yandexFile);
        diskService.deleteFile(yandexFile);
        diskService.openTrash();
    }

    public void prepareFileOnDisk(YandexFile yandexFile){
        new LoginService().loginToDisk(getAccount());
        diskService.uploadFile(yandexFile);
    }
}
