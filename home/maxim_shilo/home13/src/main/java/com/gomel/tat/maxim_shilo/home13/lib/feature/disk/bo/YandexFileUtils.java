package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo;

import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;
import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class YandexFileUtils {

    private static final String YANDEX_FILES_DIR = "test-output\\yandex_files\\";
    private static final int TIME_WAIT_FOR_FILE_DOWNLOADED_SECONDS = 10;

    public static String getDirForFiles() {
        File fileDir = new File(YANDEX_FILES_DIR);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        return YANDEX_FILES_DIR;
    }

    private static String getDownloadedFileContent(YandexFile yaFile) throws IOException {
        return FileUtils.readFileToString(yaFile.getDownloadedFile());
    }

    private static String getUploadedFileContent(YandexFile yaFile) throws IOException {
        return FileUtils.readFileToString(yaFile.getUploadedFile());
    }

    private static boolean uploadedAndDownloadedFilesHasSameContext(YandexFile yaFile) throws IOException {
        return getDownloadedFileContent(yaFile).equals(getUploadedFileContent(yaFile));
    }

    public static boolean isFileDownloaded(YandexFile yandexFile) throws IOException {
        Logger.current().info("Check that file [" + yandexFile + "] downloaded to " + yandexFile.getDownloadedFile().getAbsolutePath() + ".");
        for (int i = 0; i < TIME_WAIT_FOR_FILE_DOWNLOADED_SECONDS * 2; i++) {
            if (yandexFile.getDownloadedFile().exists()) {
                if (uploadedAndDownloadedFilesHasSameContext(yandexFile)) {
                    return true;
                }
            }
            Browser.current().delay(500);
        }
        return false;
    }
}
