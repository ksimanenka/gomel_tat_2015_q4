package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo;

import java.io.File;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.BrowserSetup.*;

public class YandexFile {

    private File uploaded;
    private File downloaded;
    private String name;

    public YandexFile(File file) {
        uploaded = file;
        downloaded = new File(getDownloadDirectory() + "\\" + file.getName());
        name = file.getName();
    }

    public File getDownloadedFile() {
        return downloaded;
    }

    public File getUploadedFile() {
        return uploaded;
    }

    @Override
    public String toString() {
        return name;
    }
}
