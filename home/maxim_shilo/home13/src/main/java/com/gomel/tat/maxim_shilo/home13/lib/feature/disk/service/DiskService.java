package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen.BaseDiskPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen.TrashFolderPage;
import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;

import java.util.ArrayList;

public class DiskService {

    private DiskPage diskPage = new DiskPage();
    private TrashFolderPage trashFolderPage = new TrashFolderPage();

    public DiskPage openDisk() {
        Logger.current().info("Open disk.");
        return diskPage.open();
    }

    public TrashFolderPage openTrash() {
        Logger.current().info("Open trash folder.");
        return trashFolderPage.open();
    }

    public void uploadFile(YandexFile yandexFile) {
        Logger.current().info("Upload file: " + yandexFile.getUploadedFile().getPath());
        diskPage.uploadFile(yandexFile)
                .waitUntilFileUploaded()
                .closeUploadWindow();
    }

    public void uploadFiles(ArrayList<YandexFile> yandexFiles) {
        Logger.current().info("Upload several files.");
        for (YandexFile yandexFile : yandexFiles) {
            diskPage.uploadFile(yandexFile);
        }
        diskPage.waitUntilFileUploaded().closeUploadWindow();
    }

    public void downloadFile(YandexFile yandexFile) {
        Logger.current().info("Download file[" + yandexFile + "].");
        diskPage.selectFile(yandexFile)
                .clickDownloadButton();
    }

    public void deleteFile(YandexFile yandexFile) {
        Logger.current().info("Remove file [" + yandexFile + "] to trash.");
        diskPage.deleteFile(yandexFile).waitUntilFileDeleted();
    }

    public void deleteFilePermanently(YandexFile yandexFile) {
        Logger.current().info("Remove file [" + yandexFile + "] from trash permanently.");
        trashFolderPage.selectFile(yandexFile)
                .clickDeletePermanentlyButton()
                .waitUntilFileDeleted();
    }

    public boolean isFilePresent(YandexFile yandexFile) {
        Logger.current().info("Check that file [" + yandexFile + "] is present in opened folder.");
        BaseDiskPage baseDiskPage = new BaseDiskPage();
        return baseDiskPage.isFilePresent(yandexFile);
    }

    public boolean isFileNotPresent(YandexFile yandexFile) {
        Logger.current().info("Check that file [" + yandexFile + "] is not present in opened folder.");
        BaseDiskPage baseDiskPage = new BaseDiskPage();
        return baseDiskPage.isFileNotPresent(yandexFile);
    }

    public boolean isFilesPresents(ArrayList<YandexFile> yandexFiles) {
        Logger.current().info("Check that files [" + yandexFiles + "] is presents in opened folder.");
        BaseDiskPage baseDiskPage = new BaseDiskPage();
        return baseDiskPage.isFilesPresents(yandexFiles);
    }

    public void restoreFile(YandexFile yandexFile) {
        Logger.current().info("Restore file [" + yandexFile + "] from trash.");
        trashFolderPage.selectFile(yandexFile)
                .clickRestoreFileButton();
    }

    public void deleteSeveralFiles(ArrayList<YandexFile> yandexFiles) {
        Logger.current().info("Delete files [" + yandexFiles + "]");
        diskPage.deleteSeveralFiles(yandexFiles)
                .waitUntilFileDeleted();
    }
}