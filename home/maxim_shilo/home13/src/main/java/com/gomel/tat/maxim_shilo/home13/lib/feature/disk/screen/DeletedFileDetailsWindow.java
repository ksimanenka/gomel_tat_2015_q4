package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.Button;

public class DeletedFileDetailsWindow {

    private static final String RESTORE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.restore' and contains(@data-params,'%s')]";
    private static final String DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.delete' and contains(@data-params,'%s')]";

    private Button deleteFilePermanently;
    private Button restore;

    public DeletedFileDetailsWindow(YandexFile yandexFile) {
        deleteFilePermanently = new Button(String.format(DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN, yandexFile));
        restore = new Button(String.format(RESTORE_BUTTON_LOCATOR_PATTERN, yandexFile));
    }

    public TrashFolderPage clickDeletePermanentlyButton() {
        deleteFilePermanently.click();
        return new TrashFolderPage();
    }

    public TrashFolderPage clickRestoreFileButton() {
        restore.click();
        return new TrashFolderPage();
    }
}
