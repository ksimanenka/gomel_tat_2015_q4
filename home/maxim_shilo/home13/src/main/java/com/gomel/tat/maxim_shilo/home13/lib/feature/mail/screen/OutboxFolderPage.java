package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.Link;

public class OutboxFolderPage extends FoldersMenu {

    private Link outboxFolder = new Link("//a[@href='#sent' and @data-action='move']");

    public Link getFolder() {
        return outboxFolder;
    }
}
