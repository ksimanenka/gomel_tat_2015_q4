package com.gomel.tat.maxim_shilo.home13.lib.ui.webelements;

import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;

public class Link extends Element {

    public Link(String xpath) {
        super(xpath);
    }

    public void click() {
        Browser.current().waitForElementIsPresent(getBy());
        Browser.current().click(getBy());
    }
}
