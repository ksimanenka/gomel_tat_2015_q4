package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;
import com.gomel.tat.maxim_shilo.home13.lib.ui.webelements.*;
import org.openqa.selenium.*;

import java.util.ArrayList;

public class DiskPage extends BaseDiskPage {

    private static final String YANDEX_DISK_PAGE = "https://disk.yandex.com/client/disk";

    private Input upload = new Input("//input[@type='file']");
    private Button closeUploadWindow = new Button("//button[contains(@class,'button-close')]");
    private Link trash = new Link("//div[@data-id='/trash']");

    public DiskPage open() {
        Browser.current().open(YANDEX_DISK_PAGE);
        return this;
    }

    public DiskPage uploadFile(YandexFile yandexFile) {
        upload.sendKeys(yandexFile.getUploadedFile().getAbsolutePath());
        return this;
    }

    public DiskPage waitUntilFileUploaded() {
        closeUploadWindow.waitForVisibility();
        return this;
    }

    public DiskPage closeUploadWindow() {
        closeUploadWindow.click();
        return new DiskPage();
    }

    public DiskPage deleteFile(YandexFile yandexFile) {
        file(yandexFile).dragAndDrop(trash);
        return this;
    }

    public FileDetailsWindow selectFile(YandexFile yandexFile) {
        file(yandexFile).click();
        return new FileDetailsWindow(yandexFile);
    }

    public DiskPage deleteSeveralFiles(ArrayList<YandexFile> yandexFiles) {
        ArrayList<By> elements = new ArrayList<>();
        for (YandexFile yandexFile : yandexFiles) {
            elements.add(file(yandexFile).getBy());
        }
        Browser.current().dragAndDrop(elements, trash.getBy());
        return this;
    }
}
