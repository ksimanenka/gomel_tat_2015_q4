package com.gomel.tat.maxim_shilo.home13.tests.mail;

import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.LoginService;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.MailService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.common.AccountFactory.*;
import static com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo.LetterFactory.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SendMailWithEmptyToFieldTest {

    private Letter letter = getLetterWithoutRecipient();
    private MailService mailService = new MailService();

    private static final String ASSERT_FAIL_MESSAGE = "Error present but actual and expected message not equals.";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_EMPTY_RECIPIENT_FIELD = "Please enter at least one email address.";

    @BeforeClass(description = "Login to mail.")
    public void login() {
        new LoginService().successLogin(getAccount());
    }

    @Test(description = "Check expected error in case sending mail with empty recipient field.")
    public void sendMailWithoutRecipient() {
        mailService.sendMail(letter);
        String actualErrorMessage = mailService.retrieveErrorOnFailedSendMail();
        assertThat(ASSERT_FAIL_MESSAGE, actualErrorMessage, equalTo(EXPECTED_ERROR_MESSAGE_IN_CASE_EMPTY_RECIPIENT_FIELD));
    }
}
