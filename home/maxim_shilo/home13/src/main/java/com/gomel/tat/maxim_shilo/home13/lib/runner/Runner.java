package com.gomel.tat.maxim_shilo.home13.lib.runner;

import com.gomel.tat.maxim_shilo.home13.config.GlobalConfig;
import com.gomel.tat.maxim_shilo.home13.lib.listeners.ParallelSuiteListener;
import com.gomel.tat.maxim_shilo.home13.lib.listeners.TestListener;
import com.gomel.tat.maxim_shilo.home13.lib.listeners.TestMethodsListener;
import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        parseCli(args);

        TestNG tng = new TestNG();
        tng.addListener(new TestMethodsListener());
        tng.addListener(Logger.current().testListener);
        tng.addListener(new ParallelSuiteListener());

        XmlSuite suite = new XmlSuite();
        suite.setSuiteFiles(GlobalConfig.getInstance().getSuites());
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        tng.setXmlSuites(suites);

        tng.run();
    }

    public static void parseCli(String[] args) {
        CmdLineParser parser = new CmdLineParser(GlobalConfig.getInstance());
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            Logger.current().error("Failed to parse cli params: " + e.getMessage(), e);
            parser.printUsage(System.out);
            System.exit(1);
        }
    }
}