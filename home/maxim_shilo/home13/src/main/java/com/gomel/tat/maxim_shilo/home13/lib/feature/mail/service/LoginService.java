package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service;

import com.gomel.tat.maxim_shilo.home13.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen.BaseMailPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen.LoginFailedPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen.LoginPage;
import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;

public class LoginService {

    private LoginPage loginPage = new LoginPage();

    public void loginToMailBox(Account account) {
        Logger.current().info("Try login to mail box using credentials [" + account.getLogin() + "::" + account.getPassword() + "]");
        loginPage.open()
                .typeLogin(account.getLogin())
                .typePassword(account.getPassword())
                .submitLoginForm();
    }

    public boolean isLoginSuccess(Account account) {
        Logger.current().info("Check that login success using credentials [" + account.getLogin() + "::" + account.getPassword() + "]");
        return new BaseMailPage().isOpened();
    }

    public void successLogin(Account account) {
        loginToMailBox(account);
        isLoginSuccess(account);
    }

    public String retrieveErrorOnFailedLogin() {
        Logger.current().info("Retrieving error message.");
        return new LoginFailedPage().getErrorMessage();
    }

    public void loginToDisk(Account account) {
        successLogin(account);
        Logger.current().info("Open yandex disk.");
        new DiskPage().open();
    }
}
