package com.gomel.tat.maxim_shilo.home13.tests.disk;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskServiceScripts;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileFactory.*;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DeleteFilePermanentlyTest {

    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "File [%s] is not deleted permanently and present in trash folder.";

    private YandexFile yandexFile = getYandexFile();
    private DiskService diskService = new DiskService();

    @BeforeClass(description = "Prepare file in trash for test [deleteFilePermanently].")
    public void prepareFile() {
        new DiskServiceScripts().prepareFileInTrash(yandexFile);
    }

    @Test(description = "Delete file permanently, check that file deleted.")
    public void deleteFilePermanently() {
        diskService.deleteFilePermanently(yandexFile);
        assertThat(format(ASSERT_FAIL_MESSAGE_PATTERN, yandexFile), diskService.isFileNotPresent(yandexFile));
    }
}
