package com.gomel.tat.maxim_shilo.home13.lib.ui.webelements;

import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;

public class Input extends Element {

    public Input(String xpath) {
        super(xpath);
    }

    public Input sendKeys(String text) {
        Browser.current().writeText(getBy(), text);
        return this;
    }

    public void submit() {
        Browser.current().submitForm(getBy());
    }
}
