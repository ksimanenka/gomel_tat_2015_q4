package com.gomel.tat.maxim_shilo.home13.tests.disk;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskServiceScripts;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileFactory.*;
import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileUtils.*;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DownloadFileTest {

    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "File [%s] is not downloaded or hasn't expected content.";

    private YandexFile yandexFile = getYandexFile();
    private DiskService diskService = new DiskService();

    @BeforeClass(description = "Prepare file on disk for test [downloadFile].")
    public void prepareFile() {
        new DiskServiceScripts().prepareFileOnDisk(yandexFile);
    }

    @Test(description = "Download file, check that file present on PC.")
    public void downloadFile() throws IOException {
        diskService.downloadFile(yandexFile);
        assertThat(format(ASSERT_FAIL_MESSAGE_PATTERN, yandexFile), isFileDownloaded(yandexFile));
    }
}
