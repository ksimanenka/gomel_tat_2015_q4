package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo;

import com.gomel.tat.maxim_shilo.home13.lib.util.Logger;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileUtils.*;
import static com.gomel.tat.maxim_shilo.home13.lib.util.Randomizer.*;

public class YandexFileFactory {

    private static final int DEFAULT_COUNT_OF_FILES = 3;

    public static YandexFile getYandexFile() {
        Logger.current().debug("Create file for upload.");
        File yaFile = new File(getDirForFiles() + randomTxtFileName());
        try {
            if (yaFile.createNewFile()) {
                FileUtils.write(yaFile, randomFileContent());
            }
        } catch (IOException e) {
            Logger.current().error("Failed to create or write file.", e);
        }
        return new YandexFile(yaFile);
    }

    public static ArrayList<YandexFile> getSeveralYandexFiles() {
        ArrayList<YandexFile> listOfFiles = new ArrayList<>();
        for (int i = 0; i < DEFAULT_COUNT_OF_FILES; i++) {
            listOfFiles.add(getYandexFile());
        }
        return listOfFiles;
    }
}
