package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class ComposePage {

    private static final By sendToInput = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By subjectInput = By.name("subj");
    private static final By mailTextInput = By.id("compose-send_ifr");
    private static final By sendButton = By.id("compose-submit");
    private static final By saveButton = By.xpath("//button[@data-action='dialog.save']");
    private static final By errorMessageAdressRequired =
            By.xpath("//span[contains(@class,'error_required') and not(contains(@class,'hidden'))]");
    private static final By deleteComposeButton = By.xpath("//a[@data-action='compose.delete']");

    private static final int SCROLL_OFFSET = -250;

    public ComposePage waitForLoad() {
        info("=> Wait for compose mail page loaded.");
        Browser.current().waitForElementIsPresent(mailTextInput);
        return this;
    }

    public ComposePage typeRecipient(String recipient) {
        info("=> Type to recipient mail field: " + recipient);
        Browser.current().writeText(sendToInput, recipient);
        return this;
    }

    public ComposePage typeSubject(String subject) {
        info("=> Type to subject mail field: " + subject);
        Browser.current().writeText(subjectInput, subject);
        return this;
    }

    public ComposePage typeBody(String bodyText) {
        info("=> Type to body mail field: " + bodyText);
        Browser.current().writeText(mailTextInput, bodyText);
        return this;
    }

    public LetterSentPage send() {
        info("=> Click send mail button.");
        Browser.current().click(sendButton);
        return new LetterSentPage();
    }

    public String getErrorMessage() {
        info("=> Inspect error message with expected text: " + errorMessageAdressRequired);
        Browser browser = Browser.current();
        if (browser.isElementPresent(errorMessageAdressRequired)) {
            return browser.getText(errorMessageAdressRequired);
        } else {
            throw new RuntimeException("Error message is not present.");
        }
    }

    public boolean dialogWindowIsPresent() {
        info("=> Check if dialog window present.");
        return Browser.current().isElementPresent(saveButton);
    }

    public BaseMailPage clickSaveButton() {
        info("=> Click save letter button.");
        Browser.current().click(saveButton);
        return new BaseMailPage();
    }

    public BaseMailPage clickDeleteComposeLetterButton() {
        info("=> Click delete letter permanently button.");
        Browser browser = Browser.current();
        browser.waitForElementIsPresent(deleteComposeButton);
        browser.scrollWindow(SCROLL_OFFSET);
        browser.click(deleteComposeButton);
        return new BaseMailPage();
    }
}
