package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.service;

import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen.*;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class MailService {

    private BaseMailPage baseMailPage = new BaseMailPage();
    private ComposePage composePage;

    public void sendMail(Letter letter) {
        info("Send mail.");
        composePage = baseMailPage.clickComposeLetterButton().waitForLoad();
        composePage.typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody())
                .send();
    }

    public void sendEmptyMail(String recipient) {
        info("Send empty mail.");
        composePage = baseMailPage.clickComposeLetterButton().waitForLoad();
        composePage.typeRecipient(recipient).send();
    }

    public boolean isLetterSent() {
        info("Check that letter sent.");
        LetterSentPage letterSentPage = new LetterSentPage();
        return letterSentPage.isSentPageLoaded();
    }

    public void deleteLetter(Letter letter) {
        info("Delete letter [" + letter.getSubject() + "]");
        baseMailPage.openLetter(letter)
                .waitForLoad()
                .clickDeleteLetterButton();
    }

    public void deleteDraftLetter(Letter letter) {
        info("Delete draft letter [" + letter.getSubject() + "]");
        baseMailPage.openDraftLetter(letter)
                .waitForLoad()
                .clickDeleteComposeLetterButton();
    }

    public void waitForLetterDeletedMessageDisappear(){
        info("Waiting for [letter deleted] message disappear");
        baseMailPage.waitForLetterDeletedMessageDisappear();
    }

    public void openInboxFolder() {
        info("Open inbox folder.");
        baseMailPage.openFolder(new InboxFolderPage());
    }

    public void openOutboxFolder() {
        info("Open outbox folder.");
        baseMailPage.openFolder(new OutboxFolderPage());
    }

    public void openTrashFolder() {
        info("Open trash folder.");
        baseMailPage.openFolder(new TrashFolderPage());
    }

    public boolean isLetterPresent(Letter letter) {
        info("Check that letter [" + letter.getSubject() + "] is present.");
        return baseMailPage.isLetterPresent(letter);
    }

    public String retrieveErrorOnFailedSendMail() {
        info("Retrieving error message.");
        return composePage.getErrorMessage();
    }

    public void saveInDraft(Letter letter) {
        info("Save letter in drafts.");
        composePage = baseMailPage.clickComposeLetterButton().waitForLoad();
        composePage.typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody());
        baseMailPage.openFolder(new DraftFolderPage());
        if (composePage.dialogWindowIsPresent()) {
            composePage.clickSaveButton();
        }
    }
}
