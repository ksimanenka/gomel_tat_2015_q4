package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.info;

public class LetterSentPage {

    private static final By letterSentMessage = By.xpath("//div[text()='Message sent successfully.']");

    public boolean isSentPageLoaded() {
        info("=> Check if mail sent page opened.");
        try {
            Browser.current().waitForElementIsVisible(letterSentMessage);
        }catch(RuntimeException e){
            return false;
        }
        return true;
    }
}
