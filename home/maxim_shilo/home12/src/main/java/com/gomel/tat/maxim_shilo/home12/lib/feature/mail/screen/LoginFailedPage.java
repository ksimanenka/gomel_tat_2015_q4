package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.info;

public class LoginFailedPage {

    private static final By errorMessage = By.className("error-msg");

    public String getErrorMessage() {
        info("=> Get text of present error message.");
        return Browser.current().getText(errorMessage);
    }
}
