package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import org.openqa.selenium.By;

public class OutboxFolderPage extends FoldersMenu {

    private static final By outboxFolderLink = By.xpath("//a[@href='#sent' and @data-action='move']");

    public By getFolderLink() {
        return outboxFolderLink;
    }
}
