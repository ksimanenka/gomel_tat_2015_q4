package com.gomel.tat.maxim_shilo.home12.tests.disk;

import com.gomel.tat.maxim_shilo.home12.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home12.tests.BaseTestLogin;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home12.lib.util.FileHelper.createTempFile;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;

public class UploadFileTest extends BaseTestLogin {

    private File fileToUpload;
    public String fileName;
    public DiskService diskService = new DiskService();

    @BeforeClass()
    public void prepareFile() throws IOException {
        fileToUpload = createTempFile();
        fileName = fileToUpload.getName();
    }

    @Test(dependsOnMethods = "login", description = "Upload file, check that file correctly uploaded.")
    public void uploadFile() {
        diskService.openDisk();
        diskService.uploadFile(fileToUpload);
        assertThat("File '" + fileName + "' is not present on disk.", diskService.isFilePresent(fileName));
    }
}
