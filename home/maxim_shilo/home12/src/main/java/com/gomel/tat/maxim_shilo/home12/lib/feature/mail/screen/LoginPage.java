package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.info;

public class LoginPage {

    private static final String MAILBOX_URL = "https://mail.yandex.com";

    private static final By loginInput = By.name("login");
    private static final By passwordInput = By.name("passwd");


    public LoginPage open() {
        info("=> Open login page.");
        Browser.current().open(MAILBOX_URL);
        return this;
    }

    public LoginPage waitForLoad(){
        info("=> Wait for login page loaded.");
        Browser.current().waitForElementIsPresent(loginInput);
        return this;
    }

    public LoginPage typeLogin(String login) {
        info("=> Type to login field: " + login);
        Browser.current().writeText(loginInput, login);
        return this;
    }

    public LoginPage typePassword(String password) {
        info("=> Type to password field: " + password);
        Browser.current().writeText(passwordInput, password);
        return this;
    }

    public void submitLoginForm() {
        info("=> Submit login form.");
        Browser.current().submitForm(passwordInput);
    }

    public boolean isLoginSuccess() {
        info("=> Check that login success.");
        BaseMailPage baseMailPage = new BaseMailPage();
        try {
            baseMailPage.waitForLoad();
            return true;
        } catch (RuntimeException e) {
            return false;
        }
    }
}
