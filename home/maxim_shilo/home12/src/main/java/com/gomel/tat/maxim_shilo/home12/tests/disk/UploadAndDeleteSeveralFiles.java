package com.gomel.tat.maxim_shilo.home12.tests.disk;

import com.gomel.tat.maxim_shilo.home12.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home12.tests.BaseTestLogin;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home12.lib.util.FileHelper.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class UploadAndDeleteSeveralFiles extends BaseTestLogin {

    private ArrayList<File> listOfFiles;
    private ArrayList<String> fileNames;
    private DiskService diskService = new DiskService();

    @BeforeClass()
    public void prepareFiles() throws IOException {
        listOfFiles = createTempFiles(3);
        fileNames = getFileNames(listOfFiles);
    }

    @Test(description = "Upload several files, check that files correctly uploaded.")
    public void UploadSeveralFiles() {
        diskService.openDisk();
        diskService.uploadFiles(listOfFiles);
        assertThat("Files: " + fileNames + "is not presents on disk.", diskService.isFilesPresent(fileNames));
    }

    @Test(dependsOnMethods = "UploadSeveralFiles", description = "Delete several files to trash, check that files presents in trash.")
    public void DeleteSeveralFiles() {
        diskService.deleteSeveralFiles(fileNames);
        diskService.openTrash();
        assertThat("Files: " + fileNames + "is not presents in trash folder.", diskService.isFilesPresent(fileNames));
    }
}
