package com.gomel.tat.maxim_shilo.home12.lib.ui;

import com.gomel.tat.maxim_shilo.home12.config.GlobalConfig;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;
import static com.gomel.tat.maxim_shilo.home12.lib.util.FileHelper.cleanTempDirectory;
import static com.gomel.tat.maxim_shilo.home12.config.GlobalConfig.*;

public class Browser extends BrowserSetup {

    private WebDriver driver;
    private static Map<Thread, Browser> instances = new HashMap<>();

    private static final String DEFAULT_SCREENSHOTS_DIRECTORY = "./test-output/screenshots/";

    private static final int DEFAULT_TIMEOUT_SECONDS = 20;

    public static void rise() {
        debug("Rise browser.");
        try {
            if (instances.get(Thread.currentThread()) != null) {
                instances.get(Thread.currentThread()).quit();
            }
            Browser browser = new Browser();
            browser.createDriver();
            instances.put(Thread.currentThread(), browser);
        } catch (MalformedURLException e) {
            error(e.getMessage());
        }
    }

    public static synchronized Browser current() {
        if (instances.get(Thread.currentThread()) == null) {
            rise();
        }
        return instances.get(Thread.currentThread());
    }

    public static void quit() {
        debug("Close browser.");
        try {
            WebDriver driver = instances.get(Thread.currentThread()).getWrappedDriver();
            if (driver != null) {
                driver.quit();
            }
        } catch (Exception e) {
            error("Problem with shutting down driver: " + e.getMessage());
        } finally {
            instances.put(Thread.currentThread(), null);
            cleanTempDirectory();
        }
    }

    private WebDriver createDriver() throws MalformedURLException {
        debug("Create driver." + GlobalConfig.getInstance().getBrowserType());
        switch (GlobalConfig.getInstance().getBrowserType()) {
            case CHROME:
                driver = getChromeRemoteWebDriver();
                break;
            case FIREFOX:
                driver = getFirefoxRemoteWebDriver();
                break;
        }
        return driver;
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void open(String URL) {
        debug("  => Open page: " + URL);
        driver.get(URL);
    }

    public WebElement findElement(By locator) {
        debug("  => Find element: '" + locator + "'");
        try {
            return getWrappedDriver().findElement(locator);
        } catch (StaleElementReferenceException e) {
            waitForElementIsPresent(locator);
            return findElement(locator);
        }
    }

    public void click(By locator) {
        debug("  => Click: '" + locator + "'");
        getWrappedDriver().findElement(locator).click();
    }

    public void writeText(By locator, String text) {
        debug("  => Write text: '" + text + "' to: '" + locator + "'");
        getWrappedDriver().findElement(locator).sendKeys(text);
    }

    public void submitForm(By locator) {
        debug("  => Submit: '" + locator + "'");
        getWrappedDriver().findElement(locator).submit();
    }

    public String getText(By locator) {
        debug("  => Get text from element located: '" + locator + "'");
        return getWrappedDriver().findElement(locator).getText();
    }

    public void dragAndDrop(By fromLocator, By toLocator) {
        debug("  => Drag and drop from: '" + fromLocator + "' to: '" + toLocator + "'");
        Action action = new Actions(getWrappedDriver())
                .dragAndDrop(findElement(fromLocator), findElement(toLocator))
                .build();
        action.perform();
    }

    public boolean isElementPresent(By locator) {
        try {
            waitForElementIsPresent(locator);
        } catch (RuntimeException e) {
            return false;
        }
        return true;
    }

    public void scrollWindow(int offset) {
        debug("  => Scrolling window.");
        ((JavascriptExecutor) getWrappedDriver())
                .executeScript(String.format("window.scrollBy(0,%s)", offset), "");
    }

    public Actions getActionsBuilder() {
        return new Actions(getWrappedDriver());
    }

    public void waitForElementIsClickable(By locator) {
        debug("  => Wait for element is clickable: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitForElementIsPresent(By locator) {
        debug("  => Wait for element is present: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForElementIsVisible(By locator) {
        debug("  => Wait for element is visible: '" + locator + "'");
        try {
            new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (StaleElementReferenceException e) {
            waitForElementIsVisible(locator);
        }
    }

    public void waitForDisappear(By locator) {
        debug("  => Wait for element is disappear: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void getScreenShot() throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        String screenshotDir = DEFAULT_SCREENSHOTS_DIRECTORY + "/" + dateFormat.format(new Date());
        File screenDir = new File(screenshotDir);
        if (!screenDir.exists()) {
            screenDir.mkdirs();
        }
        String date = java.util.Calendar.getInstance().getTime().toString().replaceAll(":", ".");
        String sreenshotName = screenshotDir + "/" + date + ".png";
        debug("  => Take screenshot: " + sreenshotName);
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(sreenshotName));
    }

    public void delay(int seconds) {
        debug("  => Delay for " + seconds + " seconds.");
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.getMessage();
        }
    }

    public void refresh() {
        debug("  => Refresh page.");
        getWrappedDriver().navigate().refresh();
    }
}