package com.gomel.tat.maxim_shilo.home12.tests.disk;

import org.testng.annotations.Test;
import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home12.lib.util.FileHelper.*;
import static org.hamcrest.MatcherAssert.assertThat;


public class DownloadFileTest extends UploadFileTest{

    @Test(dependsOnMethods = "uploadFile", description = "Download file, check that file present on PC.")
    public void downloadFile() throws InterruptedException, IOException {
        diskService.downloadFile(fileName);
        assertThat("File '" + fileName + "' is not downloaded or hasn't expected content.", isFilePresentOnPC(fileName));
    }
}
