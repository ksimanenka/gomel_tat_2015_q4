package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import org.openqa.selenium.By;

public class TrashFolderPage extends FoldersMenu {

    private static final By trashFolderLink = By.xpath("//a[@href='#trash']");

    public By getFolderLink() {
        return trashFolderLink;
    }
}