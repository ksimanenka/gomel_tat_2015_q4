package com.gomel.tat.maxim_shilo.home12.config;

import com.gomel.tat.maxim_shilo.home12.lib.ui.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import org.testng.xml.XmlSuite;

import java.util.List;

public class GlobalConfig {

    @Option(name = "-bt", usage = "Browser type(chrome/firefox)")
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-st", usage = "List of paths to suites", handler = StringArrayOptionHandler.class, required = true)
    private List<String> testSuites;

    @Option(name = "-hub", usage = "Selenium hub. If null - running local.")
    private String hub;

    @Option(name = "-pm", usage = "Parallel mode(false/tests)")
    private XmlSuite.ParallelMode parallelMode = XmlSuite.ParallelMode.FALSE;

    @Option(name = "-tc", usage = "Amount of threads for parallel execution, equal to 0 by default")
    private int threadCount = 0;

    private static GlobalConfig instance;

    public static GlobalConfig getInstance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public List<String> getSuites() {
        return testSuites;
    }

    public String getHub() {
        return hub;
    }

    public int getThreadCount(){
        return threadCount;
    }

    public XmlSuite.ParallelMode getParallelMode(){
        return parallelMode;
    }
}
