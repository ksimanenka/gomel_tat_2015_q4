package com.gomel.tat.maxim_shilo.home12.lib.ui;

import com.gomel.tat.maxim_shilo.home12.config.GlobalConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.remote.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.gomel.tat.maxim_shilo.home12.lib.util.FileHelper.DEFAULT_DOWNLOAD_DIRECTORY;

public class BrowserSetup {

    private static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    private static final String PATH_TO_CHROME_DRIWER = "./src/main/resources/chromedriver_win.exe";

    private static String getDownloadDirectory() {
        new File(DEFAULT_DOWNLOAD_DIRECTORY).mkdirs();
        return DEFAULT_DOWNLOAD_DIRECTORY;
    }

    protected static WebDriver getChromeRemoteWebDriver() throws MalformedURLException {
        WebDriver driver;
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("download.default_directory", getDownloadDirectory());
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("start-maximized");
        options.addArguments("--lang=en-us");
        if (GlobalConfig.getInstance().getHub() == null) {
            System.setProperty("webdriver.chrome.driver", PATH_TO_CHROME_DRIWER);
            driver = new ChromeDriver(options);
        } else {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            driver = new RemoteWebDriver(new URL(GlobalConfig.getInstance().getHub()), capabilities);
        }
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        return driver;
    }

    protected static WebDriver getFirefoxRemoteWebDriver() throws MalformedURLException {
        WebDriver driver;
        FirefoxProfile ffProfile = new FirefoxProfile();
        ffProfile.setPreference("browser.download.folderList", 2);
        ffProfile.setPreference("browser.download.dir", getDownloadDirectory());
        ffProfile.setPreference("browser.download.manager.showWhenStarting", false);
        ffProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        if (GlobalConfig.getInstance().getHub() == null) {
            driver = new FirefoxDriver(ffProfile);
        } else {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability(FirefoxDriver.PROFILE, ffProfile);
            driver = new RemoteWebDriver(new URL(GlobalConfig.getInstance().getHub()), capabilities);
        }
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }
}
