package com.gomel.tat.maxim_shilo.home12.lib.feature.disk.service;

import com.gomel.tat.maxim_shilo.home12.lib.feature.disk.screen.BaseDiskPage;
import com.gomel.tat.maxim_shilo.home12.lib.feature.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home12.lib.feature.disk.screen.TrashFolderPage;
import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;

import java.io.File;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class DiskService {

    private DiskPage diskPage = new DiskPage();
    private TrashFolderPage trashFolderPage = new TrashFolderPage();

    public DiskPage openDisk() {
        info("Open disk.");
        diskPage.open().waitForLoad();
        return diskPage;
    }

    public TrashFolderPage openTrash() {
        info("Open trash.");
        return trashFolderPage.open();
    }

    public void uploadFile(File fileToUpload) {
        info("Upload file.");
        diskPage.uploadFile(fileToUpload)
                .waitUntilFileUploaded()
                .closeUploadWindow();
    }

    public void uploadFiles(ArrayList<File> filesToUpload) {
        info("Upload several files.");
        for (File fileToUpload : filesToUpload) {
            diskPage.uploadFile(fileToUpload);
        }
        diskPage.waitUntilFileUploaded().closeUploadWindow();
    }

    public void downloadFile(String fileName) throws InterruptedException {
        info("Download file.");
        diskPage.selectFile(fileName)
                .waitForLoad()
                .clickDownloadButton();
        diskPage.waitUntilFileDownloaded();
    }

    public void deleteFile(String fileName) {
        info("Remove file to trash.");
        diskPage.deleteFile(fileName).waitUntilFileDeleted();
    }

    public void deleteFilePermanently(String fileName) {
        info("Remove file from trash permanently.");
        trashFolderPage.selectFile(fileName)
                .waitForLoad()
                .clickDeletePermanentlyButton()
                .waitUntilFileDeleted();
    }

    public boolean isFilePresent(String fileName) {
        info("Check than file is present.");
        BaseDiskPage baseDiskPage = new BaseDiskPage();
        return baseDiskPage.isFilePresent(fileName);
    }

    public boolean isFilesPresent(ArrayList<String> fileNames){
        info("Check than several files is present.");
        BaseDiskPage baseDiskPage = new BaseDiskPage();
        return baseDiskPage.isFilesPresent(fileNames);
    }

    public void restoreFile(String fileName) {
        info("Restore file from trash.");
        trashFolderPage.selectFile(fileName)
                .waitForLoad()
                .clickRestoreFileButton();
    }

    public void deleteSeveralFiles(ArrayList<String> fileNames) {
        info("Delete several files.");
        diskPage.deleteSeveralFiles(fileNames)
                .waitUntilFileDeleted();
    }
}