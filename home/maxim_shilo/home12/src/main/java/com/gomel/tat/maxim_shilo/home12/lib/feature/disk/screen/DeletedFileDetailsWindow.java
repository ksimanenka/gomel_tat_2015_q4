package com.gomel.tat.maxim_shilo.home12.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.info;

public class DeletedFileDetailsWindow {

    private static final String RESTORE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.restore' and contains(@data-params,'%s')]";
    private static final String DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.delete' and contains(@data-params,'%s')]";

    private final String fileName;
    private final By deleteFilePermanentlyButton;
    private final By restoreButton;

    public DeletedFileDetailsWindow(String fileName) {
        this.fileName = fileName;
        deleteFilePermanentlyButton = By.xpath(String.format(DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN, fileName));
        restoreButton = By.xpath(String.format(RESTORE_BUTTON_LOCATOR_PATTERN, fileName));
    }

    public DeletedFileDetailsWindow waitForLoad() {
        info("=> Wait for file details window opened for file: " + fileName);
        Browser.current().waitForElementIsPresent(restoreButton);
        return this;
    }

    public TrashFolderPage clickDeletePermanentlyButton() {
        info("=> Click delete file permanently button.");
        Browser.current().click(deleteFilePermanentlyButton);
        return new TrashFolderPage();
    }

    public TrashFolderPage clickRestoreFileButton() {
        info("=> Click restore file button.");
        Browser.current().click(restoreButton);
        return new TrashFolderPage();
    }
}
