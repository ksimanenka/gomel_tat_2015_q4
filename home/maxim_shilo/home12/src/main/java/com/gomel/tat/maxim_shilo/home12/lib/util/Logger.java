package com.gomel.tat.maxim_shilo.home12.lib.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Logger {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);
    private static final String LOG_DIR = "./test-output/log/";

    private static boolean ignore = false;

    public static void debug(String message) {
        startLog();
        logger.debug("[DEBUG] " + message);
    }

    public static void info(String message) {
        startLog();
        logger.info("[INFO] " + message);
    }

    public static void error(String message) {
        startLog();
        logger.error("[ERROR] " + message);
    }

    public static void error(String message, Throwable exception) {
        startLog();
        logger.error("[ERROR] " + message, exception);
    }


    public static void startLog() {
        if (!ignore) {
            ignore = true;
            try {
                File log = new File(LOG_DIR + "lastLog.log");
                if (log.exists()) {
                    String content = FileUtils.readFileToString(log);
                    Pattern pattern = Pattern.compile("([A-Za-z]{3} ){2}([0-9]{2}[ \\.]){4}EET 20[0-9]{2}");
                    Matcher matcher = pattern.matcher(FileUtils.readFileToString(log));
                    if (matcher.find()) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
                        String logDir = LOG_DIR + "/" + dateFormat.format(new Date());
                        new File(logDir).mkdir();
                        File oldLog = new File(logDir + "/" + matcher.group() + ".log");
                        oldLog.createNewFile();
                        FileUtils.write(oldLog, content);
                        FileUtils.write(log, "");
                    }
                }
                String date = java.util.Calendar.getInstance().getTime().toString().replaceAll(":", ".");
                logger.fatal("Starting new session: " + date);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }
}
