package com.gomel.tat.maxim_shilo.home12.lib.runner;

import com.gomel.tat.maxim_shilo.home12.config.GlobalConfig;
import com.gomel.tat.maxim_shilo.home12.lib.listeners.ParallelSuiteListener;
import com.gomel.tat.maxim_shilo.home12.lib.listeners.TestFailListener;
import com.gomel.tat.maxim_shilo.home12.lib.listeners.TestMethodsListener;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class Runner {

    public static void main(String[] args) {
        parseCli(args);

        TestNG tng = new TestNG();
        tng.addListener(new TestMethodsListener());
        tng.addListener(new TestFailListener());
        tng.addListener(new ParallelSuiteListener());

        XmlSuite suite = new XmlSuite();
        suite.setSuiteFiles(GlobalConfig.getInstance().getSuites());
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        tng.setXmlSuites(suites);

        tng.run();
    }

    public static void parseCli(String[] args) {
        CmdLineParser parser = new CmdLineParser(GlobalConfig.getInstance());
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            error("Failed to parse cli params: " + e.getMessage(), e);
            parser.printUsage(System.out);
            System.exit(1);
        }
    }
}