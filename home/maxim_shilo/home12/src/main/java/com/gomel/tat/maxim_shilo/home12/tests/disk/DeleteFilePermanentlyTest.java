package com.gomel.tat.maxim_shilo.home12.tests.disk;

import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DeleteFilePermanentlyTest extends DeleteFileTest {

    @Test(dependsOnMethods = "removeFile", description = "Delete file permanently, check that file deleted.")
    public void DeleteFilePermanently() {
        diskService.deleteFilePermanently(fileName);
        assertThat("File '" + fileName + "' is not deleted permanently and present in trash folder." ,
                diskService.isFilePresent(fileName), is(false));
    }
}
