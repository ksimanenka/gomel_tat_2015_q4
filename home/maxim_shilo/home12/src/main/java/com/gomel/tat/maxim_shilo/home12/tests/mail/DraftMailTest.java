package com.gomel.tat.maxim_shilo.home12.tests.mail;

import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.LetterFactory;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.service.MailService;
import com.gomel.tat.maxim_shilo.home12.tests.BaseTestLogin;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DraftMailTest extends BaseTestLogin {

    private Letter letter;
    private MailService mailService = new MailService();

    @BeforeClass
    public void createLetter() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test(description = "Save draft mail, check that mail is present in draft folder.")
    public void createDraftMail() {
        mailService.saveInDraft(letter);
        assertThat("Letter with subject '" + letter.getSubject() + "' isn't present in draft folder.",
                mailService.isLetterPresent(letter));
    }

    @Test(dependsOnMethods = "createDraftMail", description = "Delete draft mail, check that mail is present in trash folder.")
    public void deleteDraftMail() {
        mailService.deleteDraftLetter(letter);
        mailService.waitForLetterDeletedMessageDisappear();
        mailService.openTrashFolder();
        assertThat("Letter with subject '" + letter.getSubject() + "' isn't present in trash folder.",
                mailService.isLetterPresent(letter));
    }

    @Test(dependsOnMethods = "deleteDraftMail", description = "Delete draft mail permanently, check that mail deleted.")
    public void deleteLetterPermanently() {
        mailService.deleteLetter(letter);
        assertThat("Letter with subject '" + letter.getSubject() + "'is not deleted permanently and present in trash folder.",
                mailService.isLetterPresent(letter), is(false));
    }
}
