package com.gomel.tat.maxim_shilo.home12.tests.mail;

import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.LetterFactory;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.service.MailService;
import com.gomel.tat.maxim_shilo.home12.tests.BaseTestLogin;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SendMailWithEmptyToFieldTest extends BaseTestLogin {

    private Letter letter;
    private MailService mailService = new MailService();

    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_EMPTY_RECIPIENT_FIELD = "Please enter at least one email address.";

    @BeforeClass
    public void createLetter() {
        letter = LetterFactory.getLetterWithoutRecipient();
    }

    @Test(description = "Check expected error in case sending mail with empty recipient field.")
    public void sendMailWithoutRecipient() {
        mailService.sendMail(letter);
        String actualErrorMessage = mailService.retrieveErrorOnFailedSendMail();
        assertThat("Error present but actual and expected message not equals.",
                actualErrorMessage, equalTo(EXPECTED_ERROR_MESSAGE_IN_CASE_EMPTY_RECIPIENT_FIELD));
    }
}
