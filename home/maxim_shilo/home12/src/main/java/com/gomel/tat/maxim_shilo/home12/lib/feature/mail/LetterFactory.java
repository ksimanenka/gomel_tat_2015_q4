package com.gomel.tat.maxim_shilo.home12.lib.feature.mail;

import static com.gomel.tat.maxim_shilo.home12.lib.feature.common.AccountFactory.*;
import static com.gomel.tat.maxim_shilo.home12.lib.util.Randomizer.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;

public class LetterFactory {

    private static final String RECIPIENT_EMAIL = getUserMail();

    public static Letter getRandomLetter() {
        return new Letter(RECIPIENT_EMAIL, randomLetterSubject(), randomLetterBody());
    }

    public static Letter getLetterWithoutRecipient(){
        return new Letter(EMPTY, randomLetterSubject(), randomLetterBody());
    }

    public static Letter getLetterWithoutBodyAndSubject(){
        return new Letter(RECIPIENT_EMAIL, EMPTY, EMPTY);
    }
}
