package com.gomel.tat.maxim_shilo.home12.tests;

import com.gomel.tat.maxim_shilo.home12.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home12.lib.feature.common.AccountFactory;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.service.LoginService;
import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BaseTestLogin {

    private Account account;
    private LoginService loginService = new LoginService();

    @BeforeClass()
    public void setUp() {
        account = AccountFactory.getAccount();
    }

    @Test(description = "Login into mail as valid user.")
    public void login() {
        loginService.loginToMailBox(account);
        assertThat("Login failed whith credentials:\nlogin: " + account.getLogin() + "\npassword: " + account.getPassword(),
                loginService.isLoginSuccess(account));
    }

    @AfterClass()
    public void clearBrowser() {
        Browser.quit();
    }
}
