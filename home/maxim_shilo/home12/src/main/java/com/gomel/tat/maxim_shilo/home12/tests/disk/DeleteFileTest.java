package com.gomel.tat.maxim_shilo.home12.tests.disk;

import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class DeleteFileTest extends UploadFileTest {

    @Test(dependsOnMethods = "uploadFile", description = "Delete file to trash, check that file present in trash.")
    public void removeFile() {
        diskService.deleteFile(fileName);
        diskService.openTrash();
        assertThat("File '" + fileName + "' is not deleted and not present in trash folder.", diskService.isFilePresent(fileName));
    }
}
