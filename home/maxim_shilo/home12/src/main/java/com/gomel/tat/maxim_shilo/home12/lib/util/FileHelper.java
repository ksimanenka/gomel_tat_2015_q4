package com.gomel.tat.maxim_shilo.home12.lib.util;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Randomizer.randomFileContent;
import static com.gomel.tat.maxim_shilo.home12.lib.util.Randomizer.randomTxtFileName;

public class FileHelper {

    private static final String TEMP_FILE_DIR = FileUtils.getTempDirectoryPath() + "yandex_tests\\";
    public static final String DEFAULT_DOWNLOAD_DIRECTORY = TEMP_FILE_DIR + "downloads\\";

    public static File createTempFile() throws IOException {
        new File(TEMP_FILE_DIR).mkdirs();
        File tempFile = new File(TEMP_FILE_DIR + randomTxtFileName());
        tempFile.createNewFile();
        FileUtils.write(tempFile, randomFileContent());
        return tempFile;
    }

    public static ArrayList<File> createTempFiles(int count) throws IOException {
        ArrayList<File> listOfFiles = new ArrayList<File>();
        new File(TEMP_FILE_DIR).mkdirs();
        for (int i = 0; i < count; i++) {
            File tempFile = new File(TEMP_FILE_DIR + randomTxtFileName());
            tempFile.createNewFile();
            FileUtils.write(tempFile, randomFileContent());
            listOfFiles.add(tempFile);
        }
        return listOfFiles;
    }

    public static ArrayList<String> getFileNames(ArrayList<File> listOfFiles){
        ArrayList<String> listOfNames = new ArrayList<>();
        for (File file:listOfFiles) {
            listOfNames.add(file.getName());
        }
        return listOfNames;
    }

    public static boolean isFilePresentOnPC(String fileName) throws IOException {
        File downloadedFile = new File(DEFAULT_DOWNLOAD_DIRECTORY + fileName);
        if (downloadedFile.exists()) {
            String content = FileUtils.readFileToString(new File(TEMP_FILE_DIR + fileName));
            if (content.equals(FileUtils.readFileToString(downloadedFile))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static void cleanTempDirectory() {
        try {
            FileUtils.cleanDirectory(new File(TEMP_FILE_DIR));
        } catch (IOException e) {
            e.getMessage();
        }
    }
}
