package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.info;

public class LetterPage {

    private static final By deleteButton = By.xpath("//a[@data-action='delete']");

    public LetterPage waitForLoad() {
        info("=> Wait for letter opened.");
        Browser.current().waitForElementIsPresent(deleteButton);
        return this;
    }

    public BaseMailPage clickDeleteLetterButton() {
        info("=> Click delete letter button");
        Browser.current().click(deleteButton);
        return new BaseMailPage();
    }
}
