package com.gomel.tat.maxim_shilo.home12.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.info;

public class FileDetailsWindow {

    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.download' and contains(@data-params,'%s')]";

    private final String fileName;
    private final By downloadButton;

    public FileDetailsWindow(String fileName) {
        this.fileName = fileName;
        downloadButton = By.xpath(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, fileName));
    }

    public FileDetailsWindow waitForLoad() {
        info("=> Wait for file details window opened for file: " + fileName);
        Browser.current().waitForElementIsVisible(downloadButton);
        return this;
    }

    public FileDetailsWindow clickDownloadButton() {
        info("=> Click download button.");
        Browser.current().click(downloadButton);
        return this;
    }
}
