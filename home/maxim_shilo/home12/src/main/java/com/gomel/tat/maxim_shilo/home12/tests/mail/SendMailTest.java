package com.gomel.tat.maxim_shilo.home12.tests.mail;

import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.LetterFactory;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.service.MailService;
import com.gomel.tat.maxim_shilo.home12.tests.BaseTestLogin;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SendMailTest extends BaseTestLogin {

    private Letter letter;
    private MailService mailService = new MailService();

    @BeforeClass
    public void createLetter() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test(description = "Send mail with correctly filled fields.")
    public void sendMail() {
        mailService.sendMail(letter);
        assertThat("Mail with subject '" + letter.getSubject() + "' not sended.",
                mailService.isLetterSent(), is(true));
    }

    @Test(dependsOnMethods = "sendMail", description = "Check that mail is present in inbox folder.")
    public void checkInbox() {
        mailService.openInboxFolder();
        assertThat("Mail with subject '" + letter.getSubject() + "' not present in inbox folder.",
                mailService.isLetterPresent(letter));
    }

    @Test(dependsOnMethods = "sendMail", description = "Check that mail is present in outbox folder.")
    public void checkOutbox() {
        mailService.openOutboxFolder();
        assertThat("Mail with subject '" + letter.getSubject() + "' not present in outbox folder.",
                mailService.isLetterPresent(letter));
    }
}
