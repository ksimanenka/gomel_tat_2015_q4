package com.gomel.tat.maxim_shilo.home12.tests.disk;

import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;


public class RestoreFileTest extends DeleteFileTest {

    @Test(dependsOnMethods = "removeFile", description = "Restore deleted file from trash, check that file restored")
    public void RestoreFile() {
        diskService.restoreFile(fileName);
        diskService.openDisk();
        assertThat("File '" + fileName + "is not restored from trash folder.", diskService.isFilePresent(fileName));
    }
}
