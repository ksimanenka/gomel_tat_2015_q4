package com.gomel.tat.maxim_shilo.home12.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.info;

public class TrashFolderPage extends BaseDiskPage {

    private static final String TRASH_PAGE = "https://disk.yandex.com/client/trash";

    public TrashFolderPage open() {
        info("=> Open trash page.");
        Browser.current().open(TRASH_PAGE);
        return this;
    }

    public DeletedFileDetailsWindow selectFile(String fileName) {
        info("=> Select file: " + fileName);
        Browser.current().click(fileIcon(fileName));
        return new DeletedFileDetailsWindow(fileName);
    }
}
