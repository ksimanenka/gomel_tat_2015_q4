package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.service;

import com.gomel.tat.maxim_shilo.home12.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen.LoginFailedPage;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen.LoginPage;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class LoginService {

    private LoginPage loginPage = new LoginPage();

    public void loginToMailBox(Account account) {
        info("Login to mail box using credentials [" + account.getLogin() + "::" + account.getPassword() + "]");
        loginPage.open()
                .waitForLoad()
                .typeLogin(account.getLogin())
                .typePassword(account.getPassword())
                .submitLoginForm();
    }

    public boolean isLoginSuccess(Account account) {
        info("Check that login success using credentials [" + account.getLogin() + "::" + account.getPassword() + "]");
        return loginPage.isLoginSuccess();
    }

    public String retrieveErrorOnFailedLogin() {
        info("Retrieving error message.");
        LoginFailedPage loginFailedPage = new LoginFailedPage();
        return loginFailedPage.getErrorMessage();
    }
}
