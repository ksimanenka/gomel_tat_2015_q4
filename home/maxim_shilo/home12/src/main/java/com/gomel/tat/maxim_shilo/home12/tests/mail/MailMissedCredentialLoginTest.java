package com.gomel.tat.maxim_shilo.home12.tests.mail;

import com.gomel.tat.maxim_shilo.home12.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.service.LoginService;
import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home12.lib.feature.common.AccountFactory.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MailMissedCredentialLoginTest {

    private LoginService loginService = new LoginService();
    private Account accountWithWrongPassword;
    private Account accountThatNotExisted;

    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS = "Wrong username or password.";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST = "No such account with this username.";

    @BeforeClass()
    public void prepareData() {
        accountWithWrongPassword = getAccountWithWrongPassword();
        accountThatNotExisted = getNonExistedAccount();
    }

    @Test(description = "Check expected error message in case of wrong password")
    public void checkErrorMessageWrongPassword() {
        loginService.loginToMailBox(accountWithWrongPassword);
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertThat("Error present but actual and expected message not equals.",
                actualErrorMessage, equalTo(EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS));
    }

    @Test(description = "Check expected error in case of non existed account")
    public void checkErrorMessageNonExistedAccount() {
        loginService.loginToMailBox(accountThatNotExisted);
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertThat("Error present but actual and expected message not equals.",
                actualErrorMessage, equalTo(EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST));
    }

    @AfterClass()
    public void clearBrowser() {
        Browser.quit();
    }
}
