package com.gomel.tat.maxim_shilo.home12.lib.listeners;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import com.gomel.tat.maxim_shilo.home12.lib.util.Logger;
import org.openqa.selenium.lift.TestContext;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import java.io.*;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class TestFailListener extends TestListenerAdapter {

    @Override
    public void onStart(ITestContext testContext){
        info("");
        info("==========================================================================");
        info("  TEST STARTED: " + testContext.getCurrentXmlTest().getName());
        info("==========================================================================");
    }

    @Override
    public void onFinish(ITestContext testContext){
        info("==========================================================================");
        info("  TEST FINISHED: " + testContext.getCurrentXmlTest().getName());
        info("  Test methods run: " + testContext.getAllTestMethods().length +
                ", Failures: " + testContext.getFailedTests().size() + ", Skips: " + testContext.getSkippedTests().size());
        info("==========================================================================");
        info("");
    }

    @Override
    public void onTestFailure(ITestResult testRes) {
        error("Test method failed: " + testRes.getMethod().getDescription(), testRes.getThrowable());
        try {
            Browser.current().getScreenShot();
        } catch (IOException e) {
            error("Failed to get screenshot.", e);
        }
    }
}
