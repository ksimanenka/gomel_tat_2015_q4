package com.gomel.tat.maxim_shilo.home12.lib.listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class TestMethodsListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            info("TEST METHOD STARTED [" + method.getTestMethod().getDescription() + "]");
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            info("TEST METHOD FINISHED [" + method.getTestMethod().getDescription() + "]");
        }
    }
}
