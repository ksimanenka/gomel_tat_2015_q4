package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import org.openqa.selenium.By;

public abstract class FoldersMenu extends BaseMailPage {

    public abstract By getFolderLink();
}
