package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import org.openqa.selenium.By;

public class InboxFolderPage extends FoldersMenu {

    private static final By inboxFolderLink = By.xpath("//a[@href='#inbox' and @data-action='move']");

    public By getFolderLink() {
        return inboxFolderLink;
    }
}