package com.gomel.tat.maxim_shilo.home12.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.info;

public class BaseDiskPage {

    private static final String UPLOADED_FILE_LOCATOR_PATTERN = "//div[contains(@data-id,'%s')]";
    private static final By fileDeletedMessage = By.xpath("//div[@class='notifications__text js-message']");

    public By fileIcon(String fileName) {
        return By.xpath(String.format(UPLOADED_FILE_LOCATOR_PATTERN, fileName));
    }

    public boolean isFilePresent(String fileName) {
        info("=> Check that file [" + fileName + "] is (not)present in opened folder.");
        return Browser.current().isElementPresent(fileIcon(fileName));
    }

    public boolean isFilesPresent(ArrayList<String> fileNames) {
        info("=> Check that files [" + fileNames + "] is (not)presents in opened folder.");
        for (String fileName : fileNames) {
            if (!Browser.current().isElementPresent(fileIcon(fileName))) {
                return false;
            }
        }
        return true;
    }

    public void waitUntilFileDeleted() {
        info("=> Wait until file(s) deleted.");
        Browser browser = Browser.current();
        browser.waitForElementIsVisible(fileDeletedMessage);
        browser.waitForDisappear(fileDeletedMessage);
    }
}
