package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class BaseMailPage {

    private static final String MAIL_LINK_LOCATOR_PATTERN =
            "//*[@class='block-messages' and (not(@style) or @style=\"\")]//*[@title='%s']";

    private static final By composeButton = By.xpath("//a[@href='#compose']");
    private static final By userDropdownMenuLink = By.id("nb-1");
    private static final By statusline = By.xpath("//div[@class='b-statusline']");

    private By mailLink(Letter letter) {
        return By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()));
    }

    public FoldersMenu openFolder(FoldersMenu folderPage) {
        info("=> Open mail folder: " + folderPage.getFolderLink());
        Browser.current().click(folderPage.getFolderLink());
        return folderPage;
    }

    public BaseMailPage waitForLoad() {
        info("=> Waiting for load user mail page.");
        Browser.current().waitForElementIsPresent(userDropdownMenuLink);
        return this;
    }

    public ComposePage clickComposeLetterButton() {
        info("=> Click button [compose new letter].");
        Browser.current().click(composeButton);
        return new ComposePage();
    }

    public boolean isLetterPresent(Letter letter) {
        info("=> Check that letter [" + letter.getSubject() + "] (not)present on opened page folder.");
        return Browser.current().isElementPresent(mailLink(letter));
    }

    public void waitForLetterDeletedMessageDisappear() {
        info("=> Wait for disappear message [letter deleted].");
        Browser browser = Browser.current();
        browser.waitForElementIsVisible(statusline);
        browser.waitForDisappear(statusline);
    }

    public LetterPage openLetter(Letter letter) {
        info("=> Open letter with subject: " + letter.getSubject());
        Browser.current().waitForElementIsPresent(mailLink(letter));
        Browser.current().click(mailLink(letter));
        return new LetterPage();
    }

    public ComposePage openDraftLetter(Letter letter) {
        info("=> Open draft letter with subject: " + letter.getSubject());
        Browser.current().click(mailLink(letter));
        return new ComposePage();
    }
}
