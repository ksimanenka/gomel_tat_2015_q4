package com.gomel.tat.maxim_shilo.home12.lib.feature.mail.screen;

import org.openqa.selenium.By;

public class DraftFolderPage extends FoldersMenu {

    private static final By draftFolderLink = By.xpath("//a[@href='#draft']");

    public By getFolderLink() {
        return draftFolderLink;
    }
}