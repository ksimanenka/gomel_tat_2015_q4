package com.gomel.tat.maxim_shilo.home12.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home12.lib.ui.Browser;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home12.lib.util.Logger.*;

public class DiskPage extends BaseDiskPage {

    private static final String YANDEX_DISK_PAGE = "https://disk.yandex.com/client/disk";

    private static final By uploadFileButton = By.xpath("//input[@type='file']");
    private static final By closeUploadWindowButton = By.xpath("//button[contains(@class,'button-close')]");
    private static final By trashIcon = By.xpath("//div[@data-id='/trash']");

    public DiskPage open() {
        info("=> Open disk page.");
        Browser.current().open(YANDEX_DISK_PAGE);
        return this;
    }

    public DiskPage waitForLoad() {
        info("=> Wait for disk page loaded.");
        Browser.current().waitForElementIsPresent(uploadFileButton);
        return this;
    }

    public DiskPage uploadFile(File fileToUpload) {
        info("=> Upload file: " + fileToUpload.getPath());
        Browser.current().writeText(uploadFileButton, fileToUpload.getPath());
        return this;
    }

    public DiskPage waitUntilFileUploaded() {
        info("=> Wait until file(s) uploaded.");
        Browser.current().waitForElementIsClickable(closeUploadWindowButton);
        return this;
    }

    public DiskPage closeUploadWindow() {
        info("=> Close upload window.");
        Browser.current().click(closeUploadWindowButton);
        return new DiskPage();
    }

    public DiskPage deleteFile(String fileName) {
        info("=> Move to trash file named: " + fileName);
        Browser browser = Browser.current();
        browser.refresh();
        browser.waitForElementIsPresent(trashIcon);
        browser.dragAndDrop(fileIcon(fileName), trashIcon);
        return this;
    }

    public FileDetailsWindow selectFile(String fileName) {
        info("=> Select file named: " + fileName);
        Browser.current().click(fileIcon(fileName));
        return new FileDetailsWindow(fileName);
    }

    public void waitUntilFileDownloaded() {
        info("=> Wait until file downloaded.");
        Browser.current().delay(7);
    }

    public DiskPage deleteSeveralFiles(ArrayList<String> fileNames) {
        info("=> Delete files: " + fileNames);
        Browser browser = Browser.current();
        browser.refresh();
        browser.waitForElementIsPresent(trashIcon);
        Actions builder = browser.getActionsBuilder();
        info("==> Select files.");
        debug("   => Click CTRL key.");
        builder.keyDown(Keys.CONTROL);
        for (String fileName : fileNames) {
            debug("   => Select file: " + fileName);
            builder.click(browser.findElement(fileIcon(fileName)));
        }
        debug("   => Release CTRL key.");
        builder.keyUp(Keys.CONTROL);
        builder.dragAndDrop(
                browser.findElement(fileIcon(fileNames.get(0))),
                browser.findElement(trashIcon));
        info("==> Move files to trash.");
        debug("Perform drag'n'drop action.");
        builder.build().perform();
        return this;
    }
}
