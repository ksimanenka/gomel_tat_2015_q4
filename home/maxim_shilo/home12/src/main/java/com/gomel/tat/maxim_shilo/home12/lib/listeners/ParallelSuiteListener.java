package com.gomel.tat.maxim_shilo.home12.lib.listeners;

import com.gomel.tat.maxim_shilo.home12.config.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class ParallelSuiteListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.getInstance().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalConfig.getInstance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {
    }
}
