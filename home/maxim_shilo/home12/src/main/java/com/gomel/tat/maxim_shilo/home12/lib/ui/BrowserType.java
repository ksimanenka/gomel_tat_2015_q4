package com.gomel.tat.maxim_shilo.home12.lib.ui;

public enum BrowserType {
    FIREFOX("firefox"),
    CHROME("chrome");

    String name;

    BrowserType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
