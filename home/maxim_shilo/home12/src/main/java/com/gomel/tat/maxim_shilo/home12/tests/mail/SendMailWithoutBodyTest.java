package com.gomel.tat.maxim_shilo.home12.tests.mail;

import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.LetterFactory;
import com.gomel.tat.maxim_shilo.home12.lib.feature.mail.service.MailService;
import com.gomel.tat.maxim_shilo.home12.tests.BaseTestLogin;
import org.testng.annotations.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SendMailWithoutBodyTest extends BaseTestLogin {

    private Letter letter;
    private MailService mailService = new MailService();
    private boolean mailPresentInInbox = false;
    private boolean mailPresentInOutbox = false;

    @BeforeClass
    public void createLetter() {
        letter = LetterFactory.getLetterWithoutBodyAndSubject();
    }

    @Test(description = "Send mail with empty subject field and body.")
    public void sendMailWithoutBodyAndSubject() {
        mailService.sendEmptyMail(letter.getRecipient());
        assertThat("Mail with subject '" + letter.getSubject() + "' not sended.",
                mailService.isLetterSent(), is(true));
    }

    @Test(dependsOnMethods = "sendMailWithoutBodyAndSubject", description = "Check that mail is present in inbox folder.")
    public void checkInbox() {
        mailService.openInboxFolder();
        assertThat("Mail with subject '" + letter.getSubject() + "' not present in inbox folder.",
                mailService.isLetterPresent(letter));
        mailPresentInInbox = true;
    }

    @Test(dependsOnMethods = "sendMailWithoutBodyAndSubject", description = "Check that mail is present in outbox folder.")
    public void checkOutbox() {
        mailService.openOutboxFolder();
        assertThat("Mail with subject '" + letter.getSubject() + "' not present in outbox folder.",
                mailService.isLetterPresent(letter));
        mailPresentInOutbox = true;
    }

    @AfterClass()
    public void deleteEmptyLetters() {
        if (mailPresentInInbox) {
            mailService.openInboxFolder();
            mailService.deleteLetter(letter);
            mailService.waitForLetterDeletedMessageDisappear();
        }
        if (mailPresentInOutbox) {
            mailService.openOutboxFolder();
            mailService.deleteLetter(letter);
            mailService.waitForLetterDeletedMessageDisappear();
        }
    }
}