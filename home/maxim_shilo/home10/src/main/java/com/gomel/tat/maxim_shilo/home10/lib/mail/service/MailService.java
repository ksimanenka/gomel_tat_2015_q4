package com.gomel.tat.maxim_shilo.home10.lib.mail.service;

import com.gomel.tat.maxim_shilo.home10.lib.mail.Letter;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.BaseMailPage;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.ComposePage;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.DraftPage;

public class MailService {

    private static ComposePage composePage = new ComposePage();

    public static void sendMail(Letter letter) {
        composePage.open()
                .typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody())
                .send();
    }

    public static void saveInDraft(Letter letter) {
        composePage.open()
                .typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody());
        DraftPage draftPage = new DraftPage();
        if (draftPage.open()
                .saveInDraftDialogWindowIsPresent()) {
            draftPage.clickSaveButton();
        }
    }

    public static void waitUntilLetterSent() {
        composePage.waitUntilLetterSent();
    }

    public static boolean isErrorMsgPresent() {
        return composePage.isErrorPresent();
    }

    public static boolean isLetterPresent(Letter letter, BaseMailPage page) {
        return page.open().isLetterPresent(letter);
    }

    public static boolean isEmptyLetterPresent(BaseMailPage page) {
        return page.open().isEmptyLetterPresent();
    }

    public static void deleteLetter(Letter letter, BaseMailPage page) {
        page.open().openLetter(letter).deleteLetter();
    }

    public static void deleteEmptyLetter(BaseMailPage page) {
        page.open().openEmptyLetter().deleteLetter();
    }

    public static void deleteDraftLetter(Letter letter) {
        DraftPage draftPage = new DraftPage();
        draftPage.open().openLetter(letter).deleteDraftLetter();
    }
}
