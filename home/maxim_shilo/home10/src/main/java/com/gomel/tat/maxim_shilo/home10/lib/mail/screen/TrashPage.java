package com.gomel.tat.maxim_shilo.home10.lib.mail.screen;

import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;

public class TrashPage extends BaseMailPage {

    private static final By TRASH_FOLDER_LOCATOR = By.xpath("//a[@href='#trash']");

    @Override
    public TrashPage open(){
        waitForElementIsClickable(TRASH_FOLDER_LOCATOR).click();
        return this;
    }
}