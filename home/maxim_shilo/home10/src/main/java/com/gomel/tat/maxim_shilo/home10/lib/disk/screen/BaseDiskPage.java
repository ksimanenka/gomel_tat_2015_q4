package com.gomel.tat.maxim_shilo.home10.lib.disk.screen;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gomel.tat.maxim_shilo.home10.lib.disk.RandomFile;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;
import static com.gomel.tat.maxim_shilo.home10.lib.common.CommonConstants.*;

public class BaseDiskPage {


    private static final By FILE_DELETED_STATUSLINE_LOCATOR = By.xpath("//div[@class='notifications__text js-message']");
    protected static final String UPLOADED_FILE_PATTERN = "//div[contains(@data-id,'%s')]";


    public BaseDiskPage open() {
        return this;
    }

    public boolean isFilePresent(RandomFile randomFile) {
        try {
            waitForElementIsPresent(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName())));
        } catch (ElementNotFoundException e) {
            return false;
        } catch (StaleElementReferenceException e) {
            return false;
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean isSeveralFilesPresents(List<RandomFile> randomFiles) {
        for (RandomFile file : randomFiles) {
            try {
                waitForElementIsPresent(By.xpath(String.format(UPLOADED_FILE_PATTERN, file.getFileName())));
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    public static boolean isFilePresentOnPC(RandomFile randomFile) throws IOException {
        File fileOnPC = new File(DEFAULT_DOWNLOAD_DIRECTORY + randomFile.getFileName());
        if (fileOnPC.exists()) {
            String content = FileUtils.readFileToString(fileOnPC);
            if (content.equals(randomFile.getContent())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public WebElement findFile(RandomFile randomFile){
        WebElement fileUploaded = waitForElementIsPresent(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName())));
        return fileUploaded;
    }
    public void waitUntilFileDeleted() throws InterruptedException{
        waitForElementIsVisible(FILE_DELETED_STATUSLINE_LOCATOR);
        Thread.sleep(5000);
    }
}
