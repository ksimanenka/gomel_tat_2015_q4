package com.gomel.tat.maxim_shilo.home10.lib.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.gomel.tat.maxim_shilo.home10.lib.common.CommonConstants.*;

public class WebDriverWaits extends WebDriverHelper {

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_IS_CLICKABLE_SECONDS).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForElementIsPresent(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_IS_PRESENT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForElementIsVisible(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_IS_PRESENT_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_IS_DISAPPEAR_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
