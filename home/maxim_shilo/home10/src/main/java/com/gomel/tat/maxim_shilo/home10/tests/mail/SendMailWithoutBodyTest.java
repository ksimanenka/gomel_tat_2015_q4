package com.gomel.tat.maxim_shilo.home10.tests.mail;

import static com.gomel.tat.maxim_shilo.home10.lib.mail.service.MailService.*;

import com.gomel.tat.maxim_shilo.home10.lib.mail.LetterBuilder;
import com.gomel.tat.maxim_shilo.home10.tests.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.InboxPage;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.OutboxPage;
import com.gomel.tat.maxim_shilo.home10.lib.mail.Letter;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendMailWithoutBodyTest extends BaseTestLogin {

    private Letter letter;

    @BeforeClass
    public void createLetter() {
        letter = LetterBuilder.getLetterWithoutBodyAndSubject();
    }

    @Test()
    public void SendMailWithoutBody() {
        sendMail(letter);
        waitUntilLetterSent();
        Assert.assertTrue(isEmptyLetterPresent(new InboxPage()), "Letter is not present in Inbox Folder!");
        Assert.assertTrue(isEmptyLetterPresent(new OutboxPage()), "Letter is not present in Outbox Folder!");
        deleteEmptyLetter(new InboxPage());
        deleteEmptyLetter(new OutboxPage());
    }
}
