package com.gomel.tat.maxim_shilo.home10.tests.mail;

import com.gomel.tat.maxim_shilo.home10.lib.common.AccountBuilder;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home10.lib.mail.service.LoginService.*;
import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverHelper.*;

import java.net.MalformedURLException;

public class LoginIntoMailNegativeTest {

    public WebDriver driver;

    @BeforeClass()
    public void prepareBrowser() throws MalformedURLException {
        driver = getDriver("chrome");
    }

    @Test()
    public void LoginMailWithWrongPassword() {
        loginToMailBox(AccountBuilder.getAccountWithWrongPass());
        Assert.assertTrue(errorMessageIsPresent(), "Error message is not present or successfully logged!");
    }

    @AfterClass()
    public void clearBrowser() {
        shutdownWebDriver();
    }
}
