package com.gomel.tat.maxim_shilo.home10.tests.disk;

import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.DiskPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.maxim_shilo.home10.lib.disk.service.DiskService.*;

public class RestoreFileTest extends DeleteFileTest{

    @Test(priority = 3)
    public void RestoreFile(){
        restoreFile(randomFile);
        Assert.assertTrue(isFilePresent(randomFile, new DiskPage()), "File is not restored!");
    }
}
