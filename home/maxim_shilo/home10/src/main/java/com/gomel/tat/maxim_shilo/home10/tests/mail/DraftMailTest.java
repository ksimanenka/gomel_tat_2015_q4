package com.gomel.tat.maxim_shilo.home10.tests.mail;

import com.gomel.tat.maxim_shilo.home10.lib.mail.LetterBuilder;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.DraftPage;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.TrashPage;
import com.gomel.tat.maxim_shilo.home10.tests.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home10.lib.mail.Letter;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home10.lib.mail.service.MailService.*;

public class DraftMailTest extends BaseTestLogin {

    private Letter letter;

    @BeforeClass
    public void createLetter(){
        letter = LetterBuilder.getRandomLetter();
    }

    @Test()
    public void CreateAndDeleteDraftMail(){
        saveInDraft(letter);
        Assert.assertTrue(isLetterPresent(letter, new DraftPage()), "Letter is not present in Draft Folder!");
        deleteDraftLetter(letter);
        Assert.assertTrue(isLetterPresent(letter, new TrashPage()));
        deleteLetter(letter, new TrashPage());
        Assert.assertFalse(isLetterPresent(letter, new TrashPage()));
    }
}
