package com.gomel.tat.maxim_shilo.home10.lib.mail.screen;

import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;

public class InboxPage extends BaseMailPage {

    private static final By INBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");

    @Override
    public InboxPage open(){
        waitForElementIsClickable(INBOX_FOLDER_LOCATOR).click();
        return this;
    }
}
