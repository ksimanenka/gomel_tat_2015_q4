package com.gomel.tat.maxim_shilo.home10.lib.mail.screen;

import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;
import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverActions.*;

public class DraftPage extends BaseMailPage {

    private static final By DRAFT_FOLDER_LOCATOR = By.xpath("//a[@href='#draft']");
    private static final By SAVE_BUTTON_LOCATOR = By.xpath("//button[@data-action='dialog.save']");

    @Override
    public DraftPage open() {
        waitForElementIsClickable(DRAFT_FOLDER_LOCATOR).click();
        return this;
    }

    public boolean saveInDraftDialogWindowIsPresent() {
        return (isElementPresent(SAVE_BUTTON_LOCATOR));
    }

    public DraftPage clickSaveButton() {
        findElement(SAVE_BUTTON_LOCATOR).click();
        return this;
    }
}
