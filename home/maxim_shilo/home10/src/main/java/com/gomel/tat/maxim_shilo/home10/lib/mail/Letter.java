package com.gomel.tat.maxim_shilo.home10.lib.mail;

public class Letter {

    private String recipient;
    private String subject;
    private String body;

    public Letter setRecipient(String recipient) {
        this.recipient = recipient;
        return this;
    }

    public Letter setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public Letter setBody(String body) {
        this.body = body;
        return this;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }
}
