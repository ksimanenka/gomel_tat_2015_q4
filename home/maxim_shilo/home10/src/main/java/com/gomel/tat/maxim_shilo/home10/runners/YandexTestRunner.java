package com.gomel.tat.maxim_shilo.home10.runners;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class YandexTestRunner {
    public static void main(String[] args) {
        TestNG tng = new TestNG();

        XmlSuite suite = new XmlSuite();
        List<String> files = new ArrayList<String>();
        files.add("./src/main/resources/suites/yandex_test_suite.xml");
        suite.setSuiteFiles(files);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        tng.setXmlSuites(suites);

        tng.run();
    }
}