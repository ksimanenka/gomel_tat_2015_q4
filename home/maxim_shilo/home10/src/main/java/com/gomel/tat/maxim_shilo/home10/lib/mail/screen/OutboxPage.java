package com.gomel.tat.maxim_shilo.home10.lib.mail.screen;

import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;

public class OutboxPage extends BaseMailPage {

    public static final By OUTBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#sent' and @data-action='move']");

    public OutboxPage open() {
        waitForElementIsClickable(OUTBOX_FOLDER_LOCATOR).click();
        return this;
    }
}


