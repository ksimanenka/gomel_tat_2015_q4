package com.gomel.tat.maxim_shilo.home10.lib.disk.screen;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverActions.openPage;

/**
 * Created by Maxim_Shilo on 14.01.2016.
 */
public class TrashPage extends BaseDiskPage {

    private static final String TRASHBOX_PAGE = "https://disk.yandex.ru/client/trash";

    @Override
    public TrashPage open() {
        openPage(TRASHBOX_PAGE);
        return this;
    }
}
