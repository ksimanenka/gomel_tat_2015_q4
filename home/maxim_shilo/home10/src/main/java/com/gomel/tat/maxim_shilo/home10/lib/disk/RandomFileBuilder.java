package com.gomel.tat.maxim_shilo.home10.lib.disk;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.*;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home10.lib.common.CommonConstants.*;

public class RandomFileBuilder {

    public static RandomFile getRandomFile() throws IOException {
        RandomFile randomFile = new RandomFile();
        randomFile.setFileName(RandomStringUtils.randomAlphabetic(10) + ".txt")
                .setFileContent(RandomStringUtils.randomAlphabetic(30))
                .setFileDir(TEMP_FILES_DIRETORY);
        createTempFile(randomFile);
        return randomFile;
    }

    public static ArrayList<RandomFile> getSeveralFiles(int count) throws IOException {
        ArrayList<RandomFile> randomFiles = new ArrayList<RandomFile>();
        for (int i = 0; i < count; i++) {
            RandomFile randomFile = getRandomFile();
            createTempFile(randomFile);
            randomFiles.add(randomFile);
        }
        return randomFiles;
    }

    public static void createTempFile(RandomFile randomFile) throws IOException {
        File tempTxt = new File(randomFile.getPath());
        if (!tempTxt.exists()) {
            tempTxt.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(
                new FileWriter(tempTxt));
        writer.write(randomFile.getContent());
        writer.close();
    }

    public static void deleteTempFile(RandomFile randomFile) {
        File tempFile = new File(randomFile.getPath());
        if (tempFile.exists()) {
            tempFile.delete();
        }
    }
    public static void deleteTempFiles(ArrayList<RandomFile> randomFiles) {
        for (RandomFile file : randomFiles) {
            deleteTempFile(file);
        }
    }
}
