package com.gomel.tat.maxim_shilo.home10.lib.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.gomel.tat.maxim_shilo.home10.lib.common.CommonConstants.*;

public class BrowserSetUp {

    protected static WebDriver getChromeWebDriver() throws MalformedURLException {
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_settings.popups", 0);
        prefs.put("download.directory_upgrade", true);
        prefs.put("prompt_for_download", false);
        prefs.put("download.prompt_for_download", false);
        prefs.put("download.extensions_to_open", "");
        prefs.put("default_directory", DEFAULT_DOWNLOAD_DIRECTORY);
        prefs.put("download.default_directory", DEFAULT_DOWNLOAD_DIRECTORY);
        prefs.put("savefile.default_directory", DEFAULT_DOWNLOAD_DIRECTORY);
        options.setExperimentalOption("prefs", prefs);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    protected static WebDriver getFirefoxWebDriver() throws MalformedURLException {
        FirefoxProfile ffProfile = new FirefoxProfile();
        ffProfile.setPreference("browser.download.dir", DEFAULT_DOWNLOAD_DIRECTORY);
        ffProfile.setPreference("browser.download.folderList", 2);
        ffProfile.setPreference("browser.download.manager.closeWhenDone", true);
        ffProfile.setPreference("browser.helperApps.neverAsk.saveToDisk",
                "application/pdf, image/png, application/vnd.ms-excel, application/octet-stream, application/csv");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, ffProfile);
        WebDriver driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        return driver;
    }
}
