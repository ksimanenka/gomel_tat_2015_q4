package com.gomel.tat.maxim_shilo.home10.lib.disk.service;

import com.gomel.tat.maxim_shilo.home10.lib.disk.RandomFile;
import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.BaseDiskPage;
import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.FilePanel;
import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.TrashPage;

import java.io.IOException;
import java.util.ArrayList;

public class DiskService {

    private static DiskPage diskPage;
    private static FilePanel filePanel;
    private static TrashPage trashPage;

    public static void uploadFile(RandomFile randomFile) {
        diskPage = new DiskPage();
        diskPage.open()
                .uploadFile(randomFile)
                .waitUntilFileUploaded()
                .closeUploadWindow();
    }

    public static void deleteSeveralFiles(ArrayList<RandomFile> randomFiles) throws InterruptedException {
        diskPage = new DiskPage();
        diskPage.open()
                .deleteSeveralFiles(randomFiles)
                .waitUntilFileDeleted();
    }

    public static void uploadSeveralFiles(ArrayList<RandomFile> randomFiles) {
        diskPage = new DiskPage();
        diskPage.open();
        for (RandomFile file : randomFiles) {
            diskPage.uploadFile(file);
        }
        diskPage.waitUntilFileUploaded()
                .closeUploadWindow();
    }

    public static void downloadFile(RandomFile randomFile) throws InterruptedException {
        filePanel = new FilePanel(randomFile);
        filePanel.open()
                .clickDownloadFileButton()
                .waitUntilFileDownloaded();
    }

    public static void restoreFile(RandomFile randomFile) {
        trashPage = new TrashPage();
        trashPage.open();
        filePanel = new FilePanel(randomFile);
        filePanel.open().clickRestoreFileButton();
    }

    public static void removeFileToTrash(RandomFile randomFile) throws InterruptedException {
        diskPage.moveToTrash(diskPage.findFile(randomFile))
                .waitUntilFileDeleted();
    }

    public static void deleteFilePermanently(RandomFile randomFile) throws InterruptedException {
        trashPage = new TrashPage();
        trashPage.open();
        filePanel = new FilePanel(randomFile);
        filePanel.open().clickDeleteFileButton()
                .waitUntilFileDeleted();
    }

    public static boolean isFilePresent(RandomFile randomFile, BaseDiskPage page) {
        return page.open().isFilePresent(randomFile);
    }

    public static boolean isSeveralFilesPresent(ArrayList<RandomFile> randomFiles, BaseDiskPage page) {
        return page.open().isSeveralFilesPresents(randomFiles);
    }

    public static boolean isFilePresentOnDownloads(RandomFile randomFile) throws IOException {
        return BaseDiskPage.isFilePresentOnPC(randomFile);
    }


}
