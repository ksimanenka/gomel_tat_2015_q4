package com.gomel.tat.maxim_shilo.home10.tests.disk;

import com.gomel.tat.maxim_shilo.home10.lib.disk.RandomFileBuilder;
import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.TrashPage;
import com.gomel.tat.maxim_shilo.home10.tests.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home10.lib.disk.RandomFile;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home10.lib.disk.service.DiskService.*;

public class UploadAndDeleteSeveralFiles extends BaseTestLogin {

    public ArrayList<RandomFile> randomFiles;

    @BeforeClass()
    public void prepareFiles() throws IOException {
        randomFiles = RandomFileBuilder.getSeveralFiles(3);
    }

    @Test()
    public void UploadSeveralFiles() {
        uploadSeveralFiles(randomFiles);
        Assert.assertTrue(isSeveralFilesPresent(randomFiles, new DiskPage()), "Files not uploaded!");
    }

    @Test(dependsOnMethods = "UploadSeveralFiles")
    public void DeleteSeveralFiles() throws InterruptedException {
        deleteSeveralFiles(randomFiles);
        Assert.assertTrue(isSeveralFilesPresent(randomFiles, new TrashPage()), "Files not deleted!");
    }

    @AfterClass()
    public void deleteFiles() {
        RandomFileBuilder.deleteTempFiles(randomFiles);
    }
}
