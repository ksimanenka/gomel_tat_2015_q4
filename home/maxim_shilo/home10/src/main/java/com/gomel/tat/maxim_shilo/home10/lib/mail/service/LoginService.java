package com.gomel.tat.maxim_shilo.home10.lib.mail.service;

import com.gomel.tat.maxim_shilo.home10.lib.common.Account;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.LoginPage;

public class LoginService {

    private static LoginPage loginPage = new LoginPage();

    public static void loginToMailBox(Account account){
        loginPage.open()
                .typeLogin(account.getLogin())
                .typePassword(account.getPassword())
                .submitLoginForm();
    }

    public static boolean isLoginSuccess(){
        return loginPage.loginSuccess();
    }

    public static boolean errorMessageIsPresent(){
        return loginPage.errorMsgIsPresent();
    }
}
