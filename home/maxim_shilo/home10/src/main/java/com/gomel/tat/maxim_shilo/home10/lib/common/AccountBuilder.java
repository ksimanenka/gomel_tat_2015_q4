package com.gomel.tat.maxim_shilo.home10.lib.common;

import org.apache.commons.lang3.RandomStringUtils;

import static com.gomel.tat.maxim_shilo.home10.lib.common.CommonConstants.*;

public class AccountBuilder {

    public static Account getAccount() {
        Account account = new Account();
        account.setLogin(DEFAULT_MAIL_USER_LOGIN);
        account.setPassword(DEFAULT_MAIL_USER_PASSWORD);
        return account;
    }

    public static Account getAccountWithWrongPass() {
        Account account = getAccount();
        account.setPassword(account.getPassword() + RandomStringUtils.randomAlphabetic(3));
        return account;
    }
}
