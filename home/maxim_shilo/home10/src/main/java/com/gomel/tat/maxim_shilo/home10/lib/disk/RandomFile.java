package com.gomel.tat.maxim_shilo.home10.lib.disk;

public class RandomFile {

    private String fileDir;
    private String fileContent;
    private String fileName;

    public RandomFile setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public RandomFile setFileDir(String fileDir) {
        this.fileDir = fileDir;
        return this;
    }

    public RandomFile setFileContent(String fileContent) {
        this.fileContent = fileContent;
        return this;
    }

    public String getContent() {
        return fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPath() {
        return fileDir + fileName;
    }
}
