package com.gomel.tat.maxim_shilo.home10.tests.disk;

import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home10.lib.disk.service.DiskService.*;


public class DownloadFileTest extends UploadFileTest{

    @Test(priority = 2)
    public void DownloadFile() throws InterruptedException, IOException {
        downloadFile(randomFile);
        Assert.assertTrue(isFilePresentOnDownloads(randomFile), "Download failed!");
    }
}
