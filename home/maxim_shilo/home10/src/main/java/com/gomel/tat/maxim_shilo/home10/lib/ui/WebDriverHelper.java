package com.gomel.tat.maxim_shilo.home10.lib.ui;

import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;


public class WebDriverHelper extends BrowserSetUp {

    protected static WebDriver driver;

    public static WebDriver getDriver(String browser) throws MalformedURLException {
        if (browser.equals("chrome")) {
            driver = getChromeWebDriver();
        } else {
            if (browser.equals("firefox")) {
                driver = getFirefoxWebDriver();
            } else {
                throw new IllegalArgumentException("Invalid browser name: " + browser);
            }
        }
        return driver;
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            driver = null;
        }
    }
}