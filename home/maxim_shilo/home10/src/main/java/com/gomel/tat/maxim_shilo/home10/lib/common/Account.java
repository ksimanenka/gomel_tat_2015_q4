package com.gomel.tat.maxim_shilo.home10.lib.common;

/**
 * Created by Maxim_Shilo on 13.01.2016.
 */
public class Account {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
