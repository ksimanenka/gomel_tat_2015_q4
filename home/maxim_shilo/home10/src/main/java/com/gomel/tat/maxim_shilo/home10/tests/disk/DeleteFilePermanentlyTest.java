package com.gomel.tat.maxim_shilo.home10.tests.disk;

import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.maxim_shilo.home10.lib.disk.service.DiskService.*;

public class DeleteFilePermanentlyTest extends DeleteFileTest {

    @Test(priority = 3)
    public void DeleteFilePermanently()throws InterruptedException{
        deleteFilePermanently(randomFile);
        Assert.assertFalse(isFilePresent(randomFile, new TrashPage()), "File not deleted permanently!");
    }
}
