Meta:

Narrative:
As an actor
I want try login to mailbox with non existing account
So that I can't get access to mail box and correct error message present

Scenario: scenario description
Given Actor has account that not exists.
When Actor login to mailbox
Then Actor get page with error message in case account not exists.