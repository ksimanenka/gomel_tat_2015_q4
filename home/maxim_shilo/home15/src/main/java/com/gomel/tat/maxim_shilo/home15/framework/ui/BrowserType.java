package com.gomel.tat.maxim_shilo.home15.framework.ui;

public enum BrowserType {
    FIREFOX("firefox"),
    CHROME("chrome");

    String name;

    BrowserType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
