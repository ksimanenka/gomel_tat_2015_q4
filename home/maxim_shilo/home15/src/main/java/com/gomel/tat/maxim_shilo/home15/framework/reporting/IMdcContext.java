package com.gomel.tat.maxim_shilo.home15.framework.reporting;

public interface IMdcContext {
    String REPORT_SUITE = "REPORT_SUITE";
    String REPORT_TEST = "REPORT_TEST";
    String REPORT_METHOD = "REPORT_METHOD";
}
