package com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo;

import java.io.File;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.DriverFactory.*;

public class YaFile {

    private File uploaded;
    private File downloaded;
    private String name;

    public YaFile(File file) {
        uploaded = file;
        downloaded = new File(getDownloadDirectory() + "\\" + file.getName());
        name = file.getName();
    }

    public File downloaded() {
        return downloaded;
    }

    public File uploaded() {
        return uploaded;
    }

    @Override
    public String toString() {
        return name;
    }
}
