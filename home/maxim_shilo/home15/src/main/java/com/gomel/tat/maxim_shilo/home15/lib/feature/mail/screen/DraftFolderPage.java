package com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;

public class DraftFolderPage extends FoldersMenu {

    private Button draftFolder = new Button("//a[@href='#draft']");

    public Button getFolderButton() {
        return draftFolder;
    }
}