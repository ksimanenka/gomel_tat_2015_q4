package com.gomel.tat.maxim_shilo.home15.tests.disk;

import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.service.DiskServiceScripts;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YandexFileFactory.getYandexFile;
import static com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YandexFileUtils.isFileDownloaded;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DownloadFileTest {

    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "File [%s] is not downloaded or hasn't expected content.";

    private YaFile yandexFile;
    private DiskService diskService = new DiskService();

    @BeforeClass(description = "Prepare file on disk for test [downloadFile].")
    public void prepareFile() {
        yandexFile = getYandexFile();
        new DiskServiceScripts().prepareFileOnDisk(yandexFile);
    }

    @Test(description = "Download file, check that file present on PC.")
    public void downloadFile() throws IOException {
        diskService.downloadFile(yandexFile);
        assertThat(format(ASSERT_FAIL_MESSAGE_PATTERN, yandexFile), isFileDownloaded(yandexFile));
    }
}
