package com.gomel.tat.maxim_shilo.home15.tests.mail;

import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home15.lib.feature.login.service.LoginService;
import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.service.MailService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home15.lib.feature.common.AccountFactory.getAccount;
import static com.gomel.tat.maxim_shilo.home15.lib.feature.mail.bo.LetterFactory.getRandomLetter;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class SendMailTest {

    private static final String SEND_MAIL_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] not sended.";
    private static final String CHECK_INBOX_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] is not present in inbox folder.";
    private static final String CHECK_OUTBOX_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] is not present in outbox folder.";

    private Letter letter = getRandomLetter();
    private MailService mailService = new MailService();

    @BeforeClass(description = "Login to mail.")
    public void login() {
        new LoginService().loginToMailbox(getAccount());
    }

    @Test(description = "Send mail with correctly filled fields.")
    public void sendMail() {
        mailService.sendMail(letter);
        assertThat(format(SEND_MAIL_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterSent());
    }

    @Test(dependsOnMethods = "sendMail", description = "Check that mail is present in inbox folder.")
    public void checkInbox() {
        mailService.backToInbox();
        assertThat(format(CHECK_INBOX_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterPresent(letter));
    }

    @Test(dependsOnMethods = "sendMail", description = "Check that mail is present in outbox folder.")
    public void checkOutbox() {
        mailService.openOutboxFolder();
        assertThat(format(CHECK_OUTBOX_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterPresent(letter));
    }
}
