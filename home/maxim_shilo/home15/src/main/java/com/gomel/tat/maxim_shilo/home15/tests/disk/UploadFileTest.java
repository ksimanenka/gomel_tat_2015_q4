package com.gomel.tat.maxim_shilo.home15.tests.disk;

import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home15.lib.feature.login.service.LoginService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home15.lib.feature.common.AccountFactory.getAccount;
import static com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YandexFileFactory.getYandexFile;
import static org.hamcrest.MatcherAssert.assertThat;

public class UploadFileTest {

    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "File [%s] is not uploaded to disk.";

    private DiskService diskService = new DiskService();
    private YaFile yandexFile;

    @BeforeClass(description = "Login to yandex disk.")
    public void loginAndPrepareFile() {
        yandexFile = getYandexFile();
        new LoginService().loginToDisk(getAccount());
    }

    @Test(description = "Upload file, check that file correctly uploaded.")
    public void uploadFileAndCheckThatFilePresent() {
        diskService.uploadFile(yandexFile);
        assertThat(String.format(ASSERT_FAIL_MESSAGE_PATTERN, yandexFile), diskService.isFilePresent(yandexFile));
    }
}
