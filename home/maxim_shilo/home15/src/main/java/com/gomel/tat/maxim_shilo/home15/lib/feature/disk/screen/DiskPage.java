package com.gomel.tat.maxim_shilo.home15.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.ActionsHelper;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Element;
import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Input;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Link;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.Browser.browser;

public class DiskPage extends BaseDiskPage {

    private static final String YANDEX_DISK_PAGE = "https://disk.yandex.com/client/disk";

    private Button closeUploadWindow = new Button("//button[contains(@class,'button-close')]");
    private Input upload = new Input("//input[@type='file']");
    private Link trash = new Link("//div[@data-id='/trash']");

    public DiskPage open() {
        browser().open(YANDEX_DISK_PAGE);
        return this;
    }

    public DiskPage uploadFile(YaFile yaFile) {
        upload.type(yaFile.uploaded().getAbsolutePath());
        return this;
    }

    public DiskPage waitUntilFileUploaded() {
        closeUploadWindow.waitForElementIsClickable();
        return this;
    }

    public DiskPage closeUploadWindow() {
        closeUploadWindow.click();
        return this;
    }

    public DiskPage dragToTrash(YaFile yaFile) {
        new ActionsHelper().dragAndDrop(file(yaFile), trash);
        return this;
    }

    public DiskPage deleteSeveralFiles(ArrayList<YaFile> yandexFiles) {
        ArrayList<Element> elements = new ArrayList<>();
        for (YaFile yandexFile : yandexFiles) {
            elements.add(file(yandexFile));
        }
        new ActionsHelper().selectElementsUsingCTRL(elements)
                .dragAndDrop(elements.get(0), trash);
        return this;
    }
}
