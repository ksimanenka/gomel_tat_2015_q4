package com.gomel.tat.maxim_shilo.home15.lib.feature.steps;

import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.service.MailService;
import org.jbehave.core.annotations.*;

import static com.gomel.tat.maxim_shilo.home15.lib.feature.mail.bo.LetterFactory.*;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;

public class MailSteps {

    private static final String SEND_MAIL_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] not sended.";
    private static final String CHECK_INBOX_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] is not present in inbox folder.";
    private static final String CHECK_OUTBOX_ASSERT_FAIL_MESSAGE_PATTERN = "Mail with subject [%s] is not present in outbox folder.";

    private Letter letter;
    private MailService mailService = new MailService();

    @Given("Letter that will be sent")
    public void prepareLetter() {
        letter = getRandomLetter();
    }

    @When("Actor send mail to himself")
    public void sendMail() {
        mailService.sendMail(letter);
    }

    @Then("Actor get page letter sent")
    public void checkThatMailSentPageOpened() {
        assertThat(format(SEND_MAIL_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterSent());
    }

    @Then("Actor get mail in inbox folder")
    public void checkThatMailPresentInInbox() {
        mailService.backToInbox();
        assertThat(format(CHECK_INBOX_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterPresent(letter));
    }

    @Then("Actor get mail in outbox folder")
    public void checkThatMailPresentInOutbox() {
        mailService.openOutboxFolder();
        assertThat(format(CHECK_OUTBOX_ASSERT_FAIL_MESSAGE_PATTERN, letter), mailService.isLetterPresent(letter));
    }
}
