package com.gomel.tat.maxim_shilo.home15.lib.feature.login.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Form;
import com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen.BaseMailPage;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.Browser.browser;

public class MailLoginPage {

    private static final String MAILBOX_URL = "https://mail.yandex.com";

    private static final String loginInputName = "login";
    private static final String passwordInputName = "passwd";

    private Form loginForm = new Form("//form[contains(@class,'auth-form')]")
            .addInputs(new ArrayList<String>() {{
                add(loginInputName);
                add(passwordInputName);
            }});

    public MailLoginPage open() {
        browser().open(MAILBOX_URL);
        return this;
    }

    public MailLoginPage typeLogin(String userName) {
        loginForm.input(loginInputName).type(userName);
        return this;
    }

    public MailLoginPage typePassword(String userPassword) {
        loginForm.input(passwordInputName).type(userPassword);
        return this;
    }

    public BaseMailPage submitLoginForm() {
        loginForm.submit();
        return new BaseMailPage();
    }
}
