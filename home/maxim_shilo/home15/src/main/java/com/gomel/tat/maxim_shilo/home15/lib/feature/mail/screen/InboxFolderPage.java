package com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;

public class InboxFolderPage extends FoldersMenu {

    private Button inboxFolder = new Button("//a[@href='#inbox' and @data-action='move']");

    public Button getFolderButton() {
        return inboxFolder;
    }
}