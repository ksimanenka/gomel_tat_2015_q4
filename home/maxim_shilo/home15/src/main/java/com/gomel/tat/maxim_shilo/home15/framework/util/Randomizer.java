package com.gomel.tat.maxim_shilo.home15.framework.util;

import org.apache.commons.lang3.RandomStringUtils;

public class Randomizer {

    public static final int LETTER_SUBJECT_LENGTH = 15;
    public static final int LETTER_BODY_LENGTH = 50;
    public static final int FILE_NAME_LENGTH = 10;
    public static final int FILE_CONTENT_LENGTH = 50;


    public static String randomLetterSubject() {
        return RandomStringUtils.randomAlphanumeric(LETTER_SUBJECT_LENGTH);
    }

    public static String randomLetterBody() {
        return RandomStringUtils.randomAlphanumeric(LETTER_BODY_LENGTH);
    }

    public static String randomTxtFileName() {
        return RandomStringUtils.randomAlphanumeric(FILE_NAME_LENGTH) + ".txt";
    }

    public static String randomFileContent() {
        return RandomStringUtils.randomAlphanumeric(FILE_CONTENT_LENGTH);
    }

    public static String randomString(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }
}
