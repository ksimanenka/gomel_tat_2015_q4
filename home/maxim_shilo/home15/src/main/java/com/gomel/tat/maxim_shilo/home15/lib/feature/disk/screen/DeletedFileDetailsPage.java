package com.gomel.tat.maxim_shilo.home15.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;

public class DeletedFileDetailsPage {

    private static final String RESTORE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.restore' and contains(@data-params,'%s')]";
    private static final String DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.delete' and contains(@data-params,'%s')]";

    private Button deletePermanently;
    private Button restore;

    public DeletedFileDetailsPage(YaFile yandexFile) {
        deletePermanently = new Button(String.format(DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN, yandexFile));
        restore = new Button(String.format(RESTORE_BUTTON_LOCATOR_PATTERN, yandexFile));
    }

    public TrashPage clickDeletePermanentlyButton() {
        ((Button)deletePermanently.focus()).click();
        return new TrashPage();
    }

    public TrashPage clickRestoreFileButton() {
        ((Button)restore.focus()).click();
        return new TrashPage();
    }

    public DeletedFileDetailsPage waitForPageLoad() {
        restore.waitForElementIsVisible();
        deletePermanently.waitForElementIsVisible();
        return this;
    }
}
