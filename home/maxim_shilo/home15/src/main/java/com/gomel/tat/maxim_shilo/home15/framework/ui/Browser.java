package com.gomel.tat.maxim_shilo.home15.framework.ui;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;

import static com.gomel.tat.maxim_shilo.home15.framework.config.GlobalConfig.*;
import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.*;
import static com.gomel.tat.maxim_shilo.home15.framework.ui.DriverFactory.*;
import static java.io.File.*;
import static java.lang.System.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class Browser {

    private static final int DEFAULT_TIMEOUT_SECONDS = 30;
    private static final int TIMEOUT_ELEMENT_NOT_PRESENT_SECONDS = 20;

    private WebDriver driver;
    private static Map<Thread, Browser> instances = new HashMap<>();

    public Browser() {
        try {
            driver = createDriver();
            driverFactory().setupDriver(driver);
        } catch (MalformedURLException e) {
            logger().error(e.getMessage(), e);
        }
    }

    public static Browser riseBrowser() {
        logger().debug("Open browser.");
        if (instances.get(Thread.currentThread()) != null) {
            instances.get(Thread.currentThread()).closeBrowser();
        }
        instances.put(Thread.currentThread(), new Browser());
        return instances.get(Thread.currentThread());
    }

    public static synchronized Browser browser() {
        if (instances.get(Thread.currentThread()) == null) {
            throw new SessionNotCreatedException("Browser not opened!");
        }
        return instances.get(Thread.currentThread());
    }

    public static void closeBrowser() {
        logger().debug("Close browser.");
        try {
            WebDriver driver = instances.get(Thread.currentThread()).getWrappedDriver();
            if (driver != null) {
                driver.quit();
            }
        } catch (Exception e) {
            logger().error("Problem with shutting down driver: " + e.getMessage(), e);
        } finally {
            instances.put(Thread.currentThread(), null);
        }
    }

    private WebDriver createDriver() throws MalformedURLException {
        WebDriver driver;
        if (config().getHub() == null) {
            driver = driverFactory().getLocalWebDriver(config().getBrowserType());
        } else {
            driver = driverFactory().getRemoteWebDriver(config().getBrowserType(), config().getHub());
        }
        return driver;
    }

    private WebDriver getWrappedDriver() {
        return driver;
    }

    public Actions getActionBuilder() {
        return new Actions(browser().getWrappedDriver());
    }

    public void open(String URL) {
        logger().debug("Open page: " + URL);
        driver.get(URL);
    }

    public WebElement findElement(By locator) {
        try {
            return getWrappedDriver().findElement(locator);
        } catch (StaleElementReferenceException e) {
            return getWrappedDriver().findElement(locator);
        }
    }

    public void scrollWindow(int offset) {
        logger().debug("Scrolling window.");
        ((JavascriptExecutor) getWrappedDriver())
                .executeScript(String.format("window.scrollBy(0,%s)", offset), "");
    }

    public void waitForElementIsPresent(By locator) {
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(presenceOfElementLocated(locator));
    }

    public void waitForElementIsClickable(By locator) {
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(elementToBeClickable(locator));
    }

    public void waitForAppear(By locator) {
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(visibilityOfElementLocated(locator));
    }

    public void waitForElementIsNotPresent(By locator) {
        for (int i = 0; i < TIMEOUT_ELEMENT_NOT_PRESENT_SECONDS; i++) {
            try {
                findElement(locator);
            } catch (NoSuchElementException e) {
                return;
            }
            delay(500);
        }
        throw new RuntimeException("Element still present.");
    }

    public static void getScreenshot() {
        if (config().isScrenshotEnabled()) {
            try {
                File sreenshotFile = new File(config().getReportsDir() + separator + nanoTime() + ".png");
                File scrFile = ((TakesScreenshot) browser().getWrappedDriver()).getScreenshotAs(OutputType.FILE);
                logger().save(sreenshotFile.getName());
                FileUtils.copyFile(scrFile, sreenshotFile);
            } catch (IOException e) {
                logger().error(e.getMessage(), e);
            }
        }
    }

    public void delay(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            logger().error(e.getMessage(), e);
        }
    }
}