package com.gomel.tat.maxim_shilo.home15.framework.reporting;

import org.testng.ISuite;
import org.testng.ISuiteListener;

import static com.gomel.tat.maxim_shilo.home15.framework.config.GlobalConfig.*;

public class ParallelSuiteListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(config().getParallelMode());
        suite.getXmlSuite().setThreadCount(config().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {
    }
}
