package com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;

public class TrashFolderPage extends FoldersMenu {

    private Button trashFolder = new Button("//a[@href='#trash']");

    public Button getFolderButton() {
        return trashFolder;
    }
}