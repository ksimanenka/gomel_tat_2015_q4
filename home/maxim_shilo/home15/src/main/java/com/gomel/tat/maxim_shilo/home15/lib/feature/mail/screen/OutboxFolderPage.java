package com.gomel.tat.maxim_shilo.home15.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Button;

public class OutboxFolderPage extends FoldersMenu {

    private Button outboxFolder = new Button("//a[@href='#sent' and @data-action='move']");

    public Button getFolderButton() {
        return outboxFolder;
    }
}
