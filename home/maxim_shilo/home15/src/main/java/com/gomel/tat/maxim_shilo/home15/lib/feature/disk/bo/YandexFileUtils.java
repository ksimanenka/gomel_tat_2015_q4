package com.gomel.tat.maxim_shilo.home15.lib.feature.disk.bo;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home15.framework.config.GlobalConfig.config;
import static com.gomel.tat.maxim_shilo.home15.framework.ui.Browser.browser;
import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.*;

public class YandexFileUtils {

    private static final String YANDEX_FILES_DIR = config().getDownloadDirectory() + "yandex_files/";
    private static final int TIME_WAIT_FOR_FILE_DOWNLOADED_SECONDS = 10;

    public static String getDirForFiles() {
        File fileDir = new File(YANDEX_FILES_DIR);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        return YANDEX_FILES_DIR;
    }

    public static boolean isFileDownloaded(YaFile yaFile) throws IOException {
        logger().info("Check that file [" + yaFile + "] downloaded to " + yaFile.downloaded().getAbsolutePath() + ".");
        for (int i = 0; i < TIME_WAIT_FOR_FILE_DOWNLOADED_SECONDS * 2; i++) {
            if (yaFile.downloaded().exists()) {
                if (yaFile.downloaded().getName().equals(yaFile.uploaded().getName())) {
                    if (FileUtils.readFileToString(yaFile.downloaded())
                            .equals(FileUtils.readFileToString(yaFile.uploaded()))) {
                        return true;
                    }
                }
            }
            browser().delay(500);
        }
        return false;
    }
}
