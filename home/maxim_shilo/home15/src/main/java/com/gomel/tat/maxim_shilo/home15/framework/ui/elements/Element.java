package com.gomel.tat.maxim_shilo.home15.framework.ui.elements;

import com.gomel.tat.maxim_shilo.home15.framework.ui.ActionsHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.gomel.tat.maxim_shilo.home15.framework.ui.Browser.*;
import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;

public class Element {

    private By element;
    private String xpath;

    public Element(String xpath) {
        element = By.xpath(xpath);
        this.xpath = xpath;
    }

    public By getBy() {
        return element;
    }

    protected String getXpath() {
        return xpath;
    }

    protected WebElement findWebElement() {
        getScreenshot();
        logger().debug("Find web element: " + this);
        return browser().findElement(getBy());
    }

    public boolean isPresent() {
        logger().debug("Check that element present: " + this);
        try {
            waitForElementIsPresent();
        } catch (RuntimeException e) {
            getScreenshot();
            return false;
        }
        getScreenshot();
        return true;
    }

    public boolean isNotPresent() {
        logger().debug("Check that element not present: " + this);
        try {
            waitForElementIsNotPresent();
        } catch (RuntimeException e) {
            getScreenshot();
            return false;
        }
        getScreenshot();
        return true;
    }

    public String getText() {
        logger().debug("Get text from element located: " + this);
        getScreenshot();
        return findWebElement().getText();
    }

    public Element waitForElementIsPresent() {
        logger().debug("Wait for element is present: " + this);
        browser().waitForElementIsPresent(getBy());
        return this;
    }

    public Element waitForElementIsNotPresent() {
        logger().debug("Wait for element is not present: " + this);
        browser().waitForElementIsNotPresent(getBy());
        return this;
    }

    public Element waitForElementIsClickable() {
        logger().debug("Wait for element is clickable: " + this);
        browser().waitForElementIsClickable(getBy());
        return this;
    }

    public Element waitForElementIsVisible() {
        logger().debug("Wait for element is visible: " + this);
        browser().waitForAppear(getBy());
        return this;
    }

    public Element focus() {
        logger().debug("Focus on element: " + this);
        waitForElementIsPresent();
        if (this instanceof Input){
            this.findWebElement().click();
        } else {
            new ActionsHelper().moveToElement(this);
        }
        return this;
    }

    @Override
    public String toString() {
        return element.toString();
    }
}
