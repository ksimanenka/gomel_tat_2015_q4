package com.gomel.tat.maxim_shilo.home15.framework.runner;

import com.gomel.tat.maxim_shilo.home15.framework.reporting.ParallelSuiteListener;
import com.gomel.tat.maxim_shilo.home15.framework.reporting.TestMethodsListener;
import com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger;
import com.gomel.tat.maxim_shilo.home15.framework.reporting.ExtentReportsListener;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.ITestNGListener;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.gomel.tat.maxim_shilo.home15.framework.config.GlobalConfig.*;

public class Runner {

    public Runner(String[] args) {
        parseCli(args);
    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        new Runner(args).runTests();
    }

    private void runTests() throws ParserConfigurationException, SAXException, IOException {
        TestNG tng = new TestNG();
        addListeners(tng);
        configureSuites(tng);
        tng.run();
    }

    private void configureSuites(TestNG testNG) throws ParserConfigurationException, SAXException, IOException {
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        for (String suitePath : config().getSuites()) {
            InputStream suiteInClassPath = getSuiteInputStream(suitePath);
            if (suiteInClassPath != null) {
                suites.addAll(new Parser(suiteInClassPath).parse());
            } else {
                suites.addAll(new Parser(suitePath).parse());
            }
        }
        for (XmlSuite xmlSuite : suites) {
            testNG.setCommandLineSuite(xmlSuite);
        }
    }

    private InputStream getSuiteInputStream(String suite) {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(suite);
        return resourceAsStream;
    }

    private void addListeners(TestNG tng) {
        tng.setUseDefaultListeners(false);
        List<ITestNGListener> listeners = new ArrayList<ITestNGListener>() {{
            add(new TestMethodsListener());
            add(CustomLogger.logger().testListener);
            add(new ParallelSuiteListener());
            add(new ExtentReportsListener());
        }};
        for (ITestNGListener listener : listeners) {
            tng.addListener(listener);
        }
    }

    private void parseCli(String[] args) {
        CmdLineParser parser = new CmdLineParser(config());
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            CustomLogger.logger().error("Failed to parse cli params: " + e.getMessage(), e);
            parser.printUsage(System.out);
            System.exit(1);
        }
    }
}