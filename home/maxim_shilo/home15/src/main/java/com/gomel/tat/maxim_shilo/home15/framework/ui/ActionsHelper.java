package com.gomel.tat.maxim_shilo.home15.framework.ui;

import com.gomel.tat.maxim_shilo.home15.framework.ui.elements.Element;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home15.framework.reporting.CustomLogger.logger;
import static com.gomel.tat.maxim_shilo.home15.framework.ui.Browser.browser;
import static com.gomel.tat.maxim_shilo.home15.framework.ui.Browser.getScreenshot;

public class ActionsHelper {

    public ActionsHelper selectElementsUsingCTRL(ArrayList<Element> elements) {
        logger().debug("Select elements: [" + elements + "]");
        getScreenshot();
        Actions builder = browser().getActionBuilder();
        builder.keyDown(Keys.CONTROL);
        for (Element element : elements) {
            builder.moveToElement(browser().findElement(element.getBy()))
                    .click(browser().findElement(element.getBy()));
        }
        builder.build().perform();
        return this;
    }

    public ActionsHelper dragAndDrop(Element draggable, Element to) {
        logger().debug("Drag and drop: " + draggable + ", to: " + to);
        getScreenshot();
        browser().getActionBuilder()
                .moveToElement(browser().findElement(draggable.getBy()))
                .clickAndHold(browser().findElement(draggable.getBy()))
                .moveToElement(browser().findElement(to.getBy()))
                .release()
                .build().perform();
        return this;
    }

    public void moveToElement(Element element) {
        browser().getActionBuilder().moveToElement(browser().findElement(element.getBy())).perform();
    }
}
