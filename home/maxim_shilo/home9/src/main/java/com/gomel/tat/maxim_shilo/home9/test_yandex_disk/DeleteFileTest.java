package com.gomel.tat.maxim_shilo.home9.test_yandex_disk;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class DeleteFileTest extends UploadFileTest{

    @Test(priority = 2)
    public void RemoveFile() throws InterruptedException, IOException {
        driver.navigate().refresh();
        diskPage.removeFile(randomFile);
        diskPage.openTrashBox();
        Assert.assertTrue(diskPage.isFilePresent(randomFile));
    }
}
