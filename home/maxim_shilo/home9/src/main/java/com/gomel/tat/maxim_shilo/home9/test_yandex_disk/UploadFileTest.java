package com.gomel.tat.maxim_shilo.home9.test_yandex_disk;

import com.gomel.tat.maxim_shilo.home9.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home9.ui.page.yandex_disk.DiskPage;
import com.gomel.tat.maxim_shilo.home9.utils.*;
import org.testng.Assert;
import org.testng.annotations.*;
import java.io.IOException;

public class UploadFileTest extends BaseTestLogin {

    public RandomFile randomFile;
    public DiskPage diskPage;

    @BeforeClass()
    public void prepareFile() throws IOException {
        randomFile = new RandomFile();
        randomFile.createTempFile();
        diskPage = new DiskPage(driver);
    }

    @Test(priority = 1)
    public void UploadFile() throws InterruptedException {
        diskPage.open();
        diskPage.uploadFile(randomFile);
        Assert.assertTrue(diskPage.isFilePresent(randomFile));
    }

    @AfterClass()
    public void deleteFile() {
        randomFile.deleteTempFile();
    }
}
