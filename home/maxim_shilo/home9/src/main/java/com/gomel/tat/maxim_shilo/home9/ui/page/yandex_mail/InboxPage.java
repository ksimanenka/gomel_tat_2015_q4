package com.gomel.tat.maxim_shilo.home9.ui.page.yandex_mail;

import com.gomel.tat.maxim_shilo.home9.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.maxim_shilo.home9.utils.WebDriverHelper.waitForElementIsClickable;

public class InboxPage extends Page {

    private static final By INBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");

    public InboxPage(WebDriver driver){
        super(driver);
    }

    public void open(){
        waitForElementIsClickable(INBOX_FOLDER_LOCATOR).click();
    }
}
