package com.gomel.tat.maxim_shilo.home9.ui.page;

import com.gomel.tat.maxim_shilo.home9.utils.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.gomel.tat.maxim_shilo.home9.utils.WebDriverHelper.*;

public class Page {

    protected WebDriver driver;

    private static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    private static final By STATUSLINE_LOCATOR = By.xpath("//div[@class='b-statusline']");
    private static final By STATUSLINE_MESSAGE_RECEIVED_LOCATOR =
            By.xpath("//a[contains(@class,'statusline') and contains(@href,'#message')]");

    private static final String MAIL_LINK_LOCATOR_PATTERN =
            "//*[@class='block-messages' and (not(@style) or @style='')]//a[contains(@href, '%s')]";

    private static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 5;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isLetterPresent(Letter letter) {
        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(letter.getLocator()));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isLetterPresent(String ID) {
        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                            String.format(MAIL_LINK_LOCATOR_PATTERN, ID))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static String getLetterIDFromStatusLine(){
        String href = waitForAppear(STATUSLINE_MESSAGE_RECEIVED_LOCATOR).getAttribute("href");
        String ID = href.substring(href.indexOf("#message"), href.length()-3);
        return ID;
    }

    public void deleteLetter(Letter letter){
        driver.findElement(letter.getLocator()).click();
        waitForElementIsClickable(DELETE_BUTTON_LOCATOR).click();
        waitForDisappear(STATUSLINE_LOCATOR);
    }
}
