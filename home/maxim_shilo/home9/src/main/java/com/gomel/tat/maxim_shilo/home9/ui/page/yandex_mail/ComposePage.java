package com.gomel.tat.maxim_shilo.home9.ui.page.yandex_mail;

import com.gomel.tat.maxim_shilo.home9.ui.page.Page;
import com.gomel.tat.maxim_shilo.home9.utils.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.maxim_shilo.home9.utils.WebDriverHelper.*;

public class ComposePage extends Page{

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");

    private static final By SEND_TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By SUBJECT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");
    private static final By SEND_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By SAVE_BUTTON_LOCATOR = By.xpath("//button[@data-action='dialog.save']");

    public ComposePage(WebDriver driver){
        super(driver);
    }

    public void open(){
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR).click();
    }

    public void sendLetter(Letter letter){
        waitForElementIsClickable(SEND_TO_INPUT_LOCATOR).sendKeys(letter.getTo());
        driver.findElement(SUBJECT_LOCATOR).sendKeys(letter.getSubject());
        waitForAppear(MAIL_TEXT_LOCATOR).sendKeys(letter.getBody());
        driver.findElement(SEND_BUTTON_LOCATOR).click();
    }

    public void saveInDraft(Letter letter){
        waitForElementIsClickable(SEND_TO_INPUT_LOCATOR).sendKeys(letter.getTo());
        driver.findElement(SUBJECT_LOCATOR).sendKeys(letter.getSubject());
        waitForAppear(MAIL_TEXT_LOCATOR).sendKeys(letter.getBody());
        DraftPage draftPage = new DraftPage(driver);
        draftPage.open();
        if (isElementPresent(SAVE_BUTTON_LOCATOR)){
            driver.findElement(SAVE_BUTTON_LOCATOR).click();
        }
    }
}
