package com.gomel.tat.maxim_shilo.home9.test_yandex_mail;

import com.gomel.tat.maxim_shilo.home9.ui.page.yandex_mail.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.MalformedURLException;

import static com.gomel.tat.maxim_shilo.home9.utils.WebDriverHelper.*;

public class LoginIntoMailNegativeTest {

    private static final String login = "Maxim.Shilo.tat2015";
    private static final String password = "wrongPassword";
    private static final By ERROR_MSG_WRONG_LOGIN_OR_PASSWORD_LOCATOR = By.xpath("//div[@class='error-msg']");

    public WebDriver driver;

    @BeforeClass(groups = "chrome")
    public void prepareChromeBrowser() throws MalformedURLException {
        driver = getChromeWebDriver();
    }

    @BeforeClass(groups = "firefox")
    public void prepareFirefoxBrowser() throws MalformedURLException {
        driver = getFirefoxWebDriver();
    }

    @Test()
    public void TryLoginIntoMailWithWrongPassword() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(login, password);
        Assert.assertTrue(driver.findElement(ERROR_MSG_WRONG_LOGIN_OR_PASSWORD_LOCATOR).isDisplayed());
    }

    @AfterClass()
    public void clearBrowser() {
        shutdownWebDriver();
    }
}
