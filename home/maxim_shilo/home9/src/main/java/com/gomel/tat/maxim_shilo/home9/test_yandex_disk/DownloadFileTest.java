package com.gomel.tat.maxim_shilo.home9.test_yandex_disk;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class DownloadFileTest extends UploadFileTest{

    @Test(priority = 2)
    public void DownloadFile() throws InterruptedException, IOException {
        driver.navigate().refresh();
        diskPage.downloadFile(randomFile);
        Assert.assertTrue(diskPage.isFilePresentOnPC(randomFile));
    }
}
