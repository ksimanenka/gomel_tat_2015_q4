package com.gomel.tat.maxim_shilo.home9.test_yandex_disk;

import com.gomel.tat.maxim_shilo.home9.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home9.ui.page.yandex_disk.DiskPage;
import com.gomel.tat.maxim_shilo.home9.utils.RandomFile;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.ArrayList;

public class UploadAndDeleteSeveralFiles extends BaseTestLogin {

    public ArrayList<RandomFile> randomFiles;
    public DiskPage diskPage;

    @BeforeClass()
    public void prepareFiles() throws IOException {
        randomFiles = RandomFile.getSeveralFiles();
        diskPage = new DiskPage(driver);
    }

    @Test()
    public void UploadSeveralFiles() throws InterruptedException {
        diskPage.open();
        diskPage.uploadSeveralFiles(randomFiles);
        Assert.assertTrue(diskPage.isSeveralFilesPresents(randomFiles));
    }

    @Test(dependsOnMethods = "UploadSeveralFiles")
    public void DeleteSeveralFiles() throws InterruptedException {
        diskPage.deleteFiles(randomFiles);
        diskPage.openTrashBox();
        Assert.assertTrue(diskPage.isSeveralFilesPresents(randomFiles));
    }

    @AfterClass()
    public void deleteFiles() {
        for (RandomFile file : randomFiles) {
            file.deleteTempFile();
        }
    }
}
