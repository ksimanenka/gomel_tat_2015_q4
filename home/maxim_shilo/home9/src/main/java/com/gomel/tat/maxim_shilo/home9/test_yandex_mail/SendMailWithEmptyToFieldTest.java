package com.gomel.tat.maxim_shilo.home9.test_yandex_mail;

import com.gomel.tat.maxim_shilo.home9.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home9.ui.page.yandex_mail.ComposePage;
import com.gomel.tat.maxim_shilo.home9.utils.Letter;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home9.utils.WebDriverHelper.*;

public class SendMailWithEmptyToFieldTest extends BaseTestLogin {

    private Letter letter;

    private static final By ERROR_ADRESS_REQUIRED_LOCATOR =
            By.xpath("//span[contains(@class,'error_required') and not(contains(@class,'hidden'))]");

    @BeforeClass
    public void createLetter(){
        letter = new Letter("", "test_subject", "test_body");
    }

    @Test()
    public void SendMailWithEmptyToField() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        Assert.assertTrue(waitForAppear(ERROR_ADRESS_REQUIRED_LOCATOR).isDisplayed());
    }
}
