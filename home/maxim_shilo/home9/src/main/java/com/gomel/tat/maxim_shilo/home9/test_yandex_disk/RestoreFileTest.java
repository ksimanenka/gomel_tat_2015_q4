package com.gomel.tat.maxim_shilo.home9.test_yandex_disk;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RestoreFileTest extends DeleteFileTest{

    @Test(priority = 3)
    public void RestoreFile(){
        diskPage.restoreFile(randomFile);
        diskPage.open();
        Assert.assertTrue(diskPage.isFilePresent(randomFile));
    }
}
