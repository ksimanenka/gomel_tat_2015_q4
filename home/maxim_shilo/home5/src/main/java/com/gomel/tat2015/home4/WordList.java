package com.gomel.tat2015.home4;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class WordList {

    protected Map<String, Integer> values = new HashMap<String, Integer>();

    private boolean fileFounded = false;
    private boolean methodGetWordsWasCalled = false;

    public void printStatistics() throws IOException {
        if (!methodGetWordsWasCalled) {
            throw new IOException("Call Method 'WordList.getWords' at first !");
        }
        if (!fileFounded) {
            throw new IOException("No Files Found!");
        } else {
            for (Map.Entry<String, Integer> entry : values.entrySet()) {
                System.out.println("Word '" + entry.getKey() + "' found " + entry.getValue() + " times.");
            }
        }
    }

    public void getWords(String baseDirectory, String fileNamePattern) throws IOException {
        methodGetWordsWasCalled = true;
        String regexpByFileName = convertToRegexp(fileNamePattern);
        File startDirectory = new File(baseDirectory);
        if (!startDirectory.exists()) {
            throw new IOException("Directory Does Not Exist or Wrong Path!");
        }
        File[] filesInStartDir = startDirectory.listFiles();
        if (filesInStartDir == null)
            return;
        for (File fileInDirectory : filesInStartDir) {
            if (fileInDirectory.isDirectory()) {
                getWords(fileInDirectory.getPath(), fileNamePattern);
            } else if (fileInDirectory.getPath().matches(regexpByFileName)) {
                fileFounded = true;
                String fileContent = FileUtils.readFileToString(fileInDirectory,
                        CommonFileUtils.getEncoding(fileInDirectory.getPath()));
                String[] arrayOfWordsFromFile =
                        fileContent.replaceAll("[^а-яА-Я a-zA-Z]", "").split("\\s+");
                for (String word : arrayOfWordsFromFile) {
                    if (word.length() > 1) {
                        addToMap(word);
                    }
                }
            }
        }
    }

    public String convertToRegexp(String fileName) throws IOException {
        String regexpByFileName;
        if (fileName.matches("\\*([\\/|\\\\]?\\w+[\\/|\\\\]?\\*?)*(\\*|\\.txt)")) {
            regexpByFileName = fileName.
                    replace("\\", "\\\\").
                    replace("/", "(\\\\|/)").
                    replace(".", "\\.").
                    replace("*", "(.*)");
        } else throw new IOException("Wrong File Name Pattern!");
        return regexpByFileName;
    }

    private void addToMap(String keyWord) {
        if (values.containsKey(keyWord)) {
            int count = values.get(keyWord) + 1;
            values.put(keyWord, count);
        } else {
            values.put(keyWord, 1);
        }
    }
}
