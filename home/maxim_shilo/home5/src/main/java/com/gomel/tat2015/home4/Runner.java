package com.gomel.tat2015.home4;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) throws IOException{
        String baseDirectory = "D:/gomel_tat_2015_q4/";
        String fileNamePattern = "*hello*";
        WordList wordList = new WordList();
        wordList.getWords(baseDirectory, fileNamePattern);
        wordList.printStatistics();
    }
}
