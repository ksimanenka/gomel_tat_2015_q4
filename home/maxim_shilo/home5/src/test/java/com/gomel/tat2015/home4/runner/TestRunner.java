package com.gomel.tat2015.home4.runner;

import com.gomel.tat2015.home4.listeners.TestListener;
import com.gomel.tat2015.home4.listeners.TestFailReportListener;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class TestRunner {
    public static void main(String[] args) {
        TestNG tng = new TestNG();

        tng.addListener(new TestFailReportListener());
        tng.addListener(new TestListener());

        XmlSuite suite = new XmlSuite();
        List<String> files = new ArrayList<>();
        files.add("./src/test/resources/suites/wordlist.xml");
        suite.setSuiteFiles(files);

        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        tng.setXmlSuites(suites);

        tng.run();
    }
}
