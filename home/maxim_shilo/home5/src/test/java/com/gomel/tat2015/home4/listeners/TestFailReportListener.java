package com.gomel.tat2015.home4.listeners;


import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.*;

public class TestFailReportListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult testRes) {
        File bugReportWordList = new File("D:/bugReportWordList.txt");
        FileWriter fileWriter = null;
        try {
            bugReportWordList.createNewFile();
            fileWriter = new FileWriter(bugReportWordList, true);
            fileWriter.write("\r\n************************ Failure ************************" +
                    "\r\n=> Test: " + testRes.getTestClass().getName() +
                    "\r\n=> Method: " + testRes.getMethod().getMethodName() +
                    "\r\n=> Exception: " + getStackTrace(testRes.getThrowable()) + "\r\n\r\n");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private String getStackTrace(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }
}
