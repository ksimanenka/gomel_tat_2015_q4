package yandex_tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

public class BaseYandexTestClass {

    public String login = "Maxim.Shilo.tat2015";
    public String password = "passwordfortat2015";

    //Login page.
    public static final String YANDEX_START_PAGE = "http://ya.ru";
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href,'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSOWRD_INPUT_LOCATOR = By.name("passwd");

    //User mail page.
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By INBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");
    public static final By OUTBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#sent' and @data-action='move']");
    public static final By DRAFT_FOLDER_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final By TRASH_FOLDER_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    public static final By STATUSLINE_LOCATOR = By.xpath("//div[@class='b-statusline']");
    public static final By CLEAR_TRASH_BUTTON_LOCATOR = By.xpath("//*[@data-action='folder.clear']");
    public static final By CONFIRM_CLEAR_BUTTON_LOCATOR = By.xpath("//*[@data-action='dialog.submit']");
    public static final By SELECT_ALL_MAIL_LOCATOR =
            By.xpath("//*[@class='block-messages' and (not(@style) or @style='')]//*[contains(@class,'head')]//*[contains(@type, 'checkbox')]");

    //Send e-mail page.
    public static final By SEND_TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");
    public static final By SEND_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By BACK_TO_INBOX_LOCATOR = By.xpath("//*[contains(@class,'done') and @href='#inbox']");

    private static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    private static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 10;
    private static final int SKRIPT_WAIT_TIMEOUT_SECONDS = 10;
    public WebDriver driver;

    public void LoginToYandex() {
        driver.get(YANDEX_START_PAGE);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(login);
        WebElement passwordInput = driver.findElement(PASSOWRD_INPUT_LOCATOR);
        passwordInput.sendKeys(password);
        passwordInput.submit();
    }

    public void clearMailFolder(By locator) {
        waitForElementIsClickable(locator).click();
        waitForElementIsClickable(SELECT_ALL_MAIL_LOCATOR).click();
        WebElement deleteButtonOut = driver.findElement(DELETE_BUTTON_LOCATOR);
        deleteButtonOut.click();
        waitForDisappear(STATUSLINE_LOCATOR);
    }

    public void clearTrashbox() {
        waitForElementIsClickable(CLEAR_TRASH_BUTTON_LOCATOR).click();
        waitForElementIsClickable(CONFIRM_CLEAR_BUTTON_LOCATOR).click();
        waitForElementIsClickable(INBOX_FOLDER_LOCATOR).click();
    }

    public WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForAppear(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    @BeforeClass(groups = "chrome")
    public void prepareChromeBrowser() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(SKRIPT_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @BeforeClass(groups = "firefox")
    public void prepareFirefoxBrowser() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(SKRIPT_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @AfterClass()
    public void clearBrowser() throws InterruptedException {
        driver.quit();
    }
}
