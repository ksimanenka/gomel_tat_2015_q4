package yandex_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TestMailWithoutBody extends BaseYandexTestClass{

    private static final By EMPTY_MAIL_SENT_LOCATOR =
            By.xpath("//*[@class='block-messages' and (not(@style) or @style='')]//a[contains(@href, '#message')]");

    @Test()
    public void TestSendMailWithoutBody() {
        LoginToYandex();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement sendToInput = driver.findElement(SEND_TO_INPUT_LOCATOR);
        sendToInput.sendKeys(login + "@yandex.ru");
        WebElement sendButton = driver.findElement(SEND_BUTTON_LOCATOR);
        sendButton.click();
        waitForElementIsClickable(BACK_TO_INBOX_LOCATOR).click();
        waitForElementIsClickable(EMPTY_MAIL_SENT_LOCATOR).click();
        WebElement outboxButton = driver.findElement(OUTBOX_FOLDER_LOCATOR);
        outboxButton.click();
        WebElement mailSentOutbox = driver.findElement(EMPTY_MAIL_SENT_LOCATOR);
        mailSentOutbox.click();
        clearMailFolder(INBOX_FOLDER_LOCATOR);
        clearMailFolder(OUTBOX_FOLDER_LOCATOR);
        clearTrashbox();
    }
}
