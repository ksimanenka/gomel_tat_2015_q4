package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import org.openqa.selenium.By;

public class TrashFolderPage extends FoldersMenu {

    private static final By TRASH_FOLDER_LOCATOR = By.xpath("//a[@href='#trash']");

    public By getFolderLocator() {
        return TRASH_FOLDER_LOCATOR;
    }
}