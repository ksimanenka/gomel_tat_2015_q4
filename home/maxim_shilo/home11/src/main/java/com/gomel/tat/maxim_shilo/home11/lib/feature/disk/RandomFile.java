package com.gomel.tat.maxim_shilo.home11.lib.feature.disk;

public class RandomFile {

    private String fileDir;
    private String fileContent;
    private String fileName;

    public RandomFile(String fileDir, String fileName, String fileContent) {
        this.fileDir = fileDir;
        this.fileContent = fileContent;
        this.fileName = fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileDir(String fileDir) {
        this.fileDir = fileDir;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getContent() {
        return fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPath() {
        return fileDir + fileName;
    }
}
