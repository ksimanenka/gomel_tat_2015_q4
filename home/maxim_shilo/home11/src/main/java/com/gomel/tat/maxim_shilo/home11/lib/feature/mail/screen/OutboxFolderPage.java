package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import org.openqa.selenium.By;

public class OutboxFolderPage extends FoldersMenu {

    private static final By OUTBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#sent' and @data-action='move']");

    public By getFolderLocator() {
        return OUTBOX_FOLDER_LOCATOR;
    }
}
