package com.gomel.tat.maxim_shilo.home11.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class FileDetailsWindow {

    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.download' and contains(@data-params,'%s')]";

    private RandomFile randomFile;

    public FileDetailsWindow(RandomFile randomFile) {
        this.randomFile = randomFile;
    }

    public FileDetailsWindow waitForLoad() {
        Browser.current().waitForElementIsVisible(By.xpath
                (String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, randomFile.getFileName())));
        return this;
    }

    public FileDetailsWindow clickDownloadButton() {
        Browser.current().click(By.xpath
                (String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, randomFile.getFileName())));
        return this;
    }
}
