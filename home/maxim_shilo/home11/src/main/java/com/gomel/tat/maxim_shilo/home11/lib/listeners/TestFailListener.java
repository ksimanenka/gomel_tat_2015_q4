package com.gomel.tat.maxim_shilo.home11.lib.listeners;

import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import java.io.*;

import static com.gomel.tat.maxim_shilo.home11.lib.feature.common.CommonConstants.*;

public class TestFailListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult testRes) {
        String date = java.util.Calendar.getInstance().getTime().toString().replaceAll(":", "-");
        try {
            Browser browser = Browser.current();
            browser.getScreenShot(DEFAULT_SCREENSHOTS_DIRECTORY + date + ".png");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
