package com.gomel.tat.maxim_shilo.home11.tests.disk;

import org.testng.annotations.Test;
import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home11.lib.util.RandomFileHelper.*;
import static org.testng.Assert.assertTrue;


public class DownloadFileTest extends UploadFileTest{

    @Test(priority = 2, description = "Download file, check that file present on PC.")
    public void downloadFile() throws InterruptedException, IOException {
        diskService.downloadFile(randomFile);
        assertTrue(fileIsPresentOnPC(randomFile), "File not downloaded!");
    }
}
