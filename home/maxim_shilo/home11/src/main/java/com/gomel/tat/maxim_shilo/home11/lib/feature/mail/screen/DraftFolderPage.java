package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import org.openqa.selenium.By;

public class DraftFolderPage extends FoldersMenu {

    private static final By DRAFT_FOLDER_LOCATOR = By.xpath("//a[@href='#draft']");

    public By getFolderLocator() {
        return DRAFT_FOLDER_LOCATOR;
    }
}