package com.gomel.tat.maxim_shilo.home11.tests.disk;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;

public class DeleteFilePermanentlyTest extends DeleteFileTest {

    @Test(priority = 3, description = "Delete file permanently, check that file deleted.")
    public void DeleteFilePermanently() {
        diskService.deleteFilePermanently(randomFile);
        assertFalse(diskService.fileIsPresent(randomFile), "File not deleted permanently!");
    }
}
