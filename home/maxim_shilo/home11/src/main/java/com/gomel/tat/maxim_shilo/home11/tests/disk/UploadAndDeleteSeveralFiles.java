package com.gomel.tat.maxim_shilo.home11.tests.disk;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFileFactory;
import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home11.tests.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.ArrayList;

import static org.testng.Assert.assertTrue;
import static com.gomel.tat.maxim_shilo.home11.lib.util.RandomFileHelper.*;

public class UploadAndDeleteSeveralFiles extends BaseTestLogin {

    public ArrayList<RandomFile> randomFiles;
    private DiskService diskService = new DiskService();

    @BeforeClass()
    public void prepareFiles() throws IOException {
        randomFiles = RandomFileFactory.getSeveralFiles();
    }

    @Test(description = "Upload several files, check that files correctly uploaded.")
    public void UploadSeveralFiles() {
        diskService.openDisk();
        diskService.uploadFiles(randomFiles);
        for (RandomFile randomFile : randomFiles) {
            assertTrue(diskService.fileIsPresent(randomFile), "Files not uploaded!");
        }
    }

    @Test(dependsOnMethods = "UploadSeveralFiles", description = "Delete several files to trash, check that files presents in trash.")
    public void DeleteSeveralFiles() {
        diskService.deleteSeveralFiles(randomFiles);
        diskService.openTrash();
        for (RandomFile randomFile : randomFiles) {
            assertTrue(diskService.fileIsPresent(randomFile), "Files not uploaded!");
        }
    }

    @AfterClass()
    public void deleteFiles() {
        deleteTempFiles(randomFiles);
    }
}
