package com.gomel.tat.maxim_shilo.home11.lib.feature.disk;

import java.io.*;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home11.lib.util.Randomizer.*;
import static com.gomel.tat.maxim_shilo.home11.lib.util.RandomFileHelper.*;
import static com.gomel.tat.maxim_shilo.home11.lib.feature.common.CommonConstants.*;

public class RandomFileFactory {

    private static final int DEFAULT_FILES_COUNT = 4;

    public static RandomFile getRandomFile() throws IOException {
        RandomFile randomFile = new RandomFile(TEMP_FILES_DIRETORY, randomTxtFileName(), randomFileContent());
        return createTempFile(randomFile);
    }

    public static ArrayList<RandomFile> getSeveralFiles() throws IOException {
        ArrayList<RandomFile> randomFiles = new ArrayList<>();
        for (int i = 0; i < DEFAULT_FILES_COUNT; i++) {
            randomFiles.add(getRandomFile());
        }
        return randomFiles;
    }
}
