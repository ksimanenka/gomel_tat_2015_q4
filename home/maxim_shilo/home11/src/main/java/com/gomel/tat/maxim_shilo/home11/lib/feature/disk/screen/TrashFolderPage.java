package com.gomel.tat.maxim_shilo.home11.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class TrashFolderPage extends BaseDiskPage {

    private static final String TRASHBOX_PAGE = "https://disk.yandex.ru/client/trash";

    public TrashFolderPage open() {
        Browser.current().open(TRASHBOX_PAGE);
        return this;
    }

    public DeletedFileDetailsWindow selectFile(RandomFile randomFile) {
        Browser.current().click(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName())));
        return new DeletedFileDetailsWindow(randomFile);
    }
}
