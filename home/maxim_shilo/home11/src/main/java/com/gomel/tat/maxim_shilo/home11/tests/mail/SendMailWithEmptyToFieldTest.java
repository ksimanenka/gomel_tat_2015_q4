package com.gomel.tat.maxim_shilo.home11.tests.mail;

import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.LetterFactory;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.service.MailService;
import com.gomel.tat.maxim_shilo.home11.tests.BaseTestLogin;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;

public class SendMailWithEmptyToFieldTest extends BaseTestLogin {

    private Letter letter;
    private MailService mailService = new MailService();

    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_EMPTY_RECIPIENT_FIELD = "Поле не заполнено. Необходимо ввести адрес.";

    @BeforeClass
    public void createLetter() {
        letter = LetterFactory.getLetterWithoutRecipient();
    }

    @Test(description = "Check expected error in case sending mail with empty recipient field.")
    public void sendMailWithoutRecipient() {
        mailService.sendMail(letter);
        String actualErrorMessage = mailService.retrieveErrorOnFailedSendMail();
        assertEquals(actualErrorMessage, EXPECTED_ERROR_MESSAGE_IN_CASE_EMPTY_RECIPIENT_FIELD, "Error message is present not correctly!");
    }
}
