package com.gomel.tat.maxim_shilo.home11.tests.disk;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFileFactory;
import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home11.tests.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home11.lib.util.RandomFileHelper.*;

public class UploadFileTest extends BaseTestLogin {

    public RandomFile randomFile;
    public DiskService diskService = new DiskService();

    @BeforeClass()
    public void prepareFile() throws IOException {
        randomFile = RandomFileFactory.getRandomFile();
    }

    @Test(priority = 1, description = "Upload file, check that file correctly uploaded.")
    public void uploadFile() {
        diskService.openDisk();
        diskService.uploadFile(randomFile);
        Assert.assertTrue(diskService.fileIsPresent(randomFile), "File not uploaded!");
    }

    @AfterClass()
    public void deleteFile() {
        deleteTempFile(randomFile);
    }
}
