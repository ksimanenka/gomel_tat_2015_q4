package com.gomel.tat.maxim_shilo.home11.tests.mail;

import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.LetterFactory;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.service.MailService;
import com.gomel.tat.maxim_shilo.home11.tests.BaseTestLogin;
import org.testng.annotations.*;

import static org.testng.Assert.*;

public class SendMailWithoutBodyTest extends BaseTestLogin {

    private Letter letter;
    private MailService mailService = new MailService();

    @BeforeClass
    public void createLetter() {
        letter = LetterFactory.getLetterWithoutBodyAndSubject();
    }

    @Test(description = "Send mail with empty subject field and body.")
    public void sendMailWithoutBodyAndSubject() {
        mailService.sendEmptyMail(letter.getRecipient());
        mailService.waitUntilLetterSent();
        mailService.openInboxFolder();
        assertTrue(mailService.letterPresent(letter));
        mailService.deleteLetter(letter);
        mailService.openOutboxFolder();
        assertTrue(mailService.letterPresent(letter));
        mailService.deleteLetter(letter);
    }
}