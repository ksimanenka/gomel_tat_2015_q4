package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class BaseMailPage {

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final String MAIL_LINK_LOCATOR_PATTERN =
            "//*[@class='block-messages' and (not(@style) or @style='')]//a[contains(*, '%s')]";
    private static final By USER_DROPDOWN_MENU_LOCATOR = By.id("nb-1");
    private static final By STATUSLINE_LOCATOR = By.xpath("//div[@class='b-statusline']");

    public FoldersMenu openFolder(FoldersMenu folderPage) {
        Browser.current().click(folderPage.getFolderLocator());
        return folderPage;
    }

    public BaseMailPage waitForLoad() {
        Browser.current().waitForElementIsPresent(USER_DROPDOWN_MENU_LOCATOR);
        return this;
    }

    public ComposePage clickComposeLetterButton() {
        Browser.current().click(COMPOSE_BUTTON_LOCATOR);
        return new ComposePage();
    }

    public boolean letterIsPresent(Letter letter) {
        return Browser.current().isElementPresent(By.xpath(String
                .format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
    }

    public void waitUntilLetterDeleted() {
        Browser browser = Browser.current();
        browser.waitForElementIsVisible(STATUSLINE_LOCATOR);
        browser.waitForDisappear(STATUSLINE_LOCATOR);
    }

    public LetterPage openLetter(Letter letter) {
        Browser.current().click(By.xpath(String
                .format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
        return new LetterPage();
    }

    public ComposePage openDraftLetter(Letter letter) {
        Browser.current().click(By.xpath(String
                .format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
        return new ComposePage();
    }
}
