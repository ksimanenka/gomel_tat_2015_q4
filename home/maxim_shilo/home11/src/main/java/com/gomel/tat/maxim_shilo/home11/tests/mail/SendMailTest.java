package com.gomel.tat.maxim_shilo.home11.tests.mail;

import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.LetterFactory;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.service.MailService;
import com.gomel.tat.maxim_shilo.home11.tests.BaseTestLogin;
import org.testng.annotations.*;

import static org.testng.Assert.*;

public class SendMailTest extends BaseTestLogin {

    private Letter letter;
    private MailService mailService = new MailService();

    @BeforeClass
    public void createLetter() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test(description = "Send mail with correctly filled fields.")
    public void sendMail() {
        mailService.sendMail(letter);
        mailService.waitUntilLetterSent();
        mailService.openInboxFolder();
        assertTrue(mailService.letterPresent(letter));
        mailService.openOutboxFolder();
        assertTrue(mailService.letterPresent(letter));
    }
}
