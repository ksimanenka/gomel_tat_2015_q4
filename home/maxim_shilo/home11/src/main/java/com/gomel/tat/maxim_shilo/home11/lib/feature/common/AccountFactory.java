package com.gomel.tat.maxim_shilo.home11.lib.feature.common;

import static com.gomel.tat.maxim_shilo.home11.lib.util.Randomizer.*;

import static com.gomel.tat.maxim_shilo.home11.lib.feature.common.CommonConstants.*;

public class AccountFactory {

    private static final String WRONG_PASSWORD = DEFAULT_MAIL_USER_PASSWORD + randomString(5);
    private static final String NONEXISTENT_LOGIN = DEFAULT_MAIL_USER_LOGIN + randomString(5);

    public static Account getAccount() {
        return new Account(DEFAULT_MAIL_USER_LOGIN, DEFAULT_MAIL_USER_PASSWORD, DEFAULT_USER_EMAIL);
    }

    public static Account getAccountWithWrongPassword() {
        return new Account(DEFAULT_MAIL_USER_LOGIN, WRONG_PASSWORD, DEFAULT_USER_EMAIL);
    }

    public static Account getNonExistedAccount(){
        return new Account(NONEXISTENT_LOGIN, DEFAULT_MAIL_USER_PASSWORD, DEFAULT_USER_EMAIL);
    }
}
