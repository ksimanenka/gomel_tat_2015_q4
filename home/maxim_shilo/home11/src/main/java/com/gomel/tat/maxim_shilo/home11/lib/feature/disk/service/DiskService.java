package com.gomel.tat.maxim_shilo.home11.lib.feature.disk.service;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.screen.BaseDiskPage;
import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.screen.TrashFolderPage;

import java.util.ArrayList;

public class DiskService {

    private DiskPage diskPage = new DiskPage();
    private TrashFolderPage trashFolderPage = new TrashFolderPage();

    public DiskPage openDisk() {
        System.out.println("- Open disk.");
        diskPage.open().waitForLoad();
        return diskPage;
    }

    public TrashFolderPage openTrash() {
        System.out.println("- Open trash.");
        return trashFolderPage.open();
    }

    public void uploadFile(RandomFile randomFile) {
        System.out.println("- Upload file.");
        diskPage.uploadFile(randomFile)
                .waitUntilFileUploaded()
                .closeUploadWindow();
    }

    public void uploadFiles(ArrayList<RandomFile> randomFiles) {
        System.out.println("- Upload several files.");
        for (RandomFile file : randomFiles) {
            diskPage.uploadFile(file);
        }
        diskPage.waitUntilFileUploaded().closeUploadWindow();
    }

    public void downloadFile(RandomFile randomFile) throws InterruptedException {
        System.out.println("- Download file.");
        diskPage.selectFile(randomFile)
                .waitForLoad()
                .clickDownloadButton();
        diskPage.waitUntilFileDownloaded();
    }

    public void removeFileToTrash(RandomFile randomFile) {
        System.out.println("- Remove file to trash.");
        diskPage.moveFileToTrash(randomFile).waitUntilFileDeleted();
    }

    public void deleteFilePermanently(RandomFile randomFile) {
        System.out.println("- Remove file from trash permanently.");
        trashFolderPage.selectFile(randomFile)
                .waitForLoad()
                .clickDeletePermanentlyButton()
                .waitUntilFileDeleted();
    }

    public boolean fileIsPresent(RandomFile randomFile) {
        System.out.println("- Check than file is present.");
        BaseDiskPage baseDiskPage = new BaseDiskPage();
        return baseDiskPage.fileIsPresent(randomFile);
    }

    public void restoreFile(RandomFile randomFile) {
        System.out.println("- Restore file from trash.");
        trashFolderPage.selectFile(randomFile)
                .waitForLoad()
                .clickRestoreFileButton();
    }

    public void deleteSeveralFiles(ArrayList<RandomFile> randomFiles) {
        diskPage.deleteSeveralFiles(randomFiles)
                .waitUntilFileDeleted();
    }
}