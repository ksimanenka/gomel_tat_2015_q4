package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class LoginFailedPage {

    private static final By ERROR_MSG_LOCATOR = By.className("error-msg");

    public String getErrorMessage() {
        return Browser.current().getText(ERROR_MSG_LOCATOR);
    }
}
