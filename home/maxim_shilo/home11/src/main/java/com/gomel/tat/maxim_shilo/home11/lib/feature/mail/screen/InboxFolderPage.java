package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import org.openqa.selenium.By;

public class InboxFolderPage extends FoldersMenu {

    private static final By INBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#inbox' and @data-action='move']");

    public By getFolderLocator() {
        return INBOX_FOLDER_LOCATOR;
    }
}