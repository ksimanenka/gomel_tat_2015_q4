package com.gomel.tat.maxim_shilo.home11.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import java.util.ArrayList;

public class DiskPage extends BaseDiskPage {

    private static final String YANDEX_DISK_PAGE = "https://disk.yandex.ru/client/disk";

    private static final By UPLOAD_FILE_BUTTON_LOCATOR = By.xpath("//input[@class='button__attach']");
    private static final By CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR = By.xpath("//button[contains(@class,'button-close')]");
    private static final By TRASH_ICON_LOCATOR = By.xpath("//div[@data-id='/trash']");

    public DiskPage open() {
        Browser.current().open(YANDEX_DISK_PAGE);
        return this;
    }

    public DiskPage waitForLoad() {
        Browser.current().waitForElementIsPresent(UPLOAD_FILE_BUTTON_LOCATOR);
        return this;
    }

    public DiskPage uploadFile(RandomFile randomFile) {
        Browser.current().writeText(UPLOAD_FILE_BUTTON_LOCATOR, randomFile.getPath());
        return this;
    }

    public DiskPage waitUntilFileUploaded() {
        Browser.current().waitForElementIsClickable(CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR);
        return this;
    }

    public DiskPage closeUploadWindow() {
        Browser.current().click(CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR);
        return new DiskPage();
    }

    public DiskPage moveFileToTrash(RandomFile randomFile) {
        Browser browser = Browser.current();
        browser.dragAndDrop(
                By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName())), TRASH_ICON_LOCATOR);
        return this;
    }

    public FileDetailsWindow selectFile(RandomFile randomFile) {
        Browser.current().click(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName())));
        return new FileDetailsWindow(randomFile);
    }

    public void waitUntilFileDownloaded() throws InterruptedException {
        Browser.current().waitUntilFileDownloaded();
    }

    public DiskPage deleteSeveralFiles(ArrayList<RandomFile> randomFiles) {
        Browser browser = Browser.current();
        Actions builder = browser.getActionsBuilder();
        builder.keyDown(Keys.CONTROL);
        for (RandomFile randomFile : randomFiles) {
            builder.click(browser.findElement(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName()))));
        }
        builder.keyUp(Keys.CONTROL);
        builder.dragAndDrop(
                browser.findElement(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFiles.get(0).getFileName()))),
                browser.findElement(TRASH_ICON_LOCATOR));
        builder.build().perform();
        return this;
    }
}
