package com.gomel.tat.maxim_shilo.home11.lib.feature.mail;

import static com.gomel.tat.maxim_shilo.home11.lib.util.Randomizer.*;
import static com.gomel.tat.maxim_shilo.home11.lib.feature.common.CommonConstants.*;

public class LetterFactory {

    public static Letter getRandomLetter() {
        return new Letter(DEFAULT_RECIPIENT_EMAIL, randomLetterSubject(), randomLetterBody());
    }

    public static Letter getLetterWithoutRecipient(){
        return new Letter(EMPTY, randomLetterSubject(), randomLetterBody());
    }

    public static Letter getLetterWithoutBodyAndSubject(){
        return new Letter(DEFAULT_RECIPIENT_EMAIL, EMPTY, EMPTY);
    }
}
