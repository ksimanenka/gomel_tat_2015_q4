package com.gomel.tat.maxim_shilo.home11.lib.feature.mail;

import static com.gomel.tat.maxim_shilo.home11.lib.feature.common.CommonConstants.*;

public class Letter {

    private String recipient;
    private String subject;
    private String body;

    public Letter(String recipient, String subject, String body){
        this.recipient = recipient;
        this.subject = subject;
        this.body = body;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSubject() {
        if (subject == EMPTY) {
            return EMPTY_LETTER_SUBJECT_NAME;
        } else {
            return subject;
        }
    }

    public String getBody() {
        return body;
    }
}
