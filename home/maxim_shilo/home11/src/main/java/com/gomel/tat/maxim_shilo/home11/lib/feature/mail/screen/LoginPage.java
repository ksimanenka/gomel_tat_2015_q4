package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class LoginPage {

    private static final String MAILBOX_URL = "https://mail.yandex.by";

    private static final By LOGIN_INPUT_LOCATOR = By.name("login");
    private static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");


    public LoginPage open() {
        Browser browser = Browser.current();
        browser.open(MAILBOX_URL);
        browser.waitForElementIsPresent(LOGIN_INPUT_LOCATOR);
        return new LoginPage();
    }

    public LoginPage typeLogin(String login) {
        Browser.current().writeText(LOGIN_INPUT_LOCATOR, login);
        return this;
    }

    public LoginPage typePassword(String password) {
        Browser.current().writeText(PASSWORD_INPUT_LOCATOR, password);
        return this;
    }

    public void submitLoginForm() {
        Browser.current().submitForm(PASSWORD_INPUT_LOCATOR);
    }

    public boolean loginSuccess() {
        BaseMailPage baseMailPage = new BaseMailPage();
        try {
            baseMailPage.waitForLoad();
            return true;
        } catch (RuntimeException e) {
            return false;
        }
    }
}
