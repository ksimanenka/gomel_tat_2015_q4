package com.gomel.tat.maxim_shilo.home11.tests.disk;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class DeleteFileTest extends UploadFileTest {

    @Test(priority = 2, description = "Delete file to trash, check that file present in trash.")
    public void removeFile() {
        diskService.removeFileToTrash(randomFile);
        diskService.openTrash();
        assertTrue(diskService.fileIsPresent(randomFile), "File not present in trash!");
    }
}
