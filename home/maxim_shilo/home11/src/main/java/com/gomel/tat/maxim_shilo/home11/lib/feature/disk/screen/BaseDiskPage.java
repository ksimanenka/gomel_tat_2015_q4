package com.gomel.tat.maxim_shilo.home11.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class BaseDiskPage {

    protected static final String UPLOADED_FILE_PATTERN = "//div[contains(@data-id,'%s')]";
    private static final By FILE_DELETED_STATUSLINE_LOCATOR = By.xpath("//div[@class='notifications__text js-message']");

    public boolean fileIsPresent(RandomFile randomFile) {
        return Browser.current().isElementPresent(By.xpath(
                String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName())));
    }

    public void waitUntilFileDeleted() {
        Browser browser = Browser.current();
        browser.waitForElementIsVisible(FILE_DELETED_STATUSLINE_LOCATOR);
        browser.waitForDisappear(FILE_DELETED_STATUSLINE_LOCATOR);
    }
}
