package com.gomel.tat.maxim_shilo.home11.tests.mail;

import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.LetterFactory;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.service.MailService;
import com.gomel.tat.maxim_shilo.home11.tests.BaseTestLogin;
import org.testng.annotations.*;

import static org.testng.Assert.*;

public class DraftMailTest extends BaseTestLogin {

    private Letter letter;
    private MailService mailService = new MailService();

    @BeforeClass
    public void createLetter() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test(description = "Save letter in drafts, delete letter to trash, delete letter permanently.")
    public void CreateAndDeleteDraftMail() {
        mailService.saveInDraft(letter);
        assertTrue(mailService.letterPresent(letter));
        mailService.deleteDraftLetter(letter);
        mailService.openTrashFolder();
        assertTrue(mailService.letterPresent(letter));
        mailService.deleteLetter(letter);
        assertFalse(mailService.letterPresent(letter));
    }
}
