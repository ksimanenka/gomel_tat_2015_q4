package com.gomel.tat.maxim_shilo.home11.lib.runner;

import com.gomel.tat.maxim_shilo.home11.lib.listeners.TestFailListener;
import com.gomel.tat.maxim_shilo.home11.lib.listeners.TestListener;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.kohsuke.args4j.*;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        Browser webDriver = new Browser();
        CmdLineParser parser = new CmdLineParser(webDriver);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
        }

        TestNG tng = new TestNG();

        tng.addListener(new TestListener());
        tng.addListener(new TestFailListener());

        XmlSuite suite = new XmlSuite();
        List<String> files = new ArrayList<String>();
        files.add("./src/main/resources/suites/yandex_test_suite.xml");
        suite.setSuiteFiles(files);
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        tng.setXmlSuites(suites);

        tng.run();
    }
}