package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.service;

import com.gomel.tat.maxim_shilo.home11.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen.LoginFailedPage;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen.LoginPage;

import java.net.MalformedURLException;

public class LoginService {

    private LoginPage loginPage = new LoginPage();

    public void loginToMailBox(Account account) {
        System.out.println("- Login to mail box.");
        loginPage.open()
                .typeLogin(account.getLogin())
                .typePassword(account.getPassword())
                .submitLoginForm();
    }

    public boolean loginSuccess(Account account) {
        System.out.println("- Check login success.");
        return loginPage.loginSuccess();
    }

    public String retrieveErrorOnFailedLogin() {
        System.out.println("- Retrieving error message.");
        LoginFailedPage loginFailedPage = new LoginFailedPage();
        return loginFailedPage.getErrorMessage();
    }
}
