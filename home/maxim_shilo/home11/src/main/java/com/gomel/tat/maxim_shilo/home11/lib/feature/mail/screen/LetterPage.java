package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class LetterPage {

    private static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");

    public LetterPage waitForLoad() {
        Browser.current().waitForElementIsPresent(DELETE_BUTTON_LOCATOR);
        return this;
    }

    public BaseMailPage clickDeleteLetterButton() {
        Browser.current().click(DELETE_BUTTON_LOCATOR);
        return new BaseMailPage();
    }
}
