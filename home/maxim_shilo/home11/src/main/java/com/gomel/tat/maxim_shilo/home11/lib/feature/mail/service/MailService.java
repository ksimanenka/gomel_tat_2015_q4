package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.service;

import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.Letter;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen.*;

public class MailService {

    private BaseMailPage baseMailPage = new BaseMailPage();
    private ComposePage composePage;

    public void sendMail(Letter letter) {
        System.out.println("- Send mail.");
        composePage = baseMailPage.clickComposeLetterButton().waitForLoad();
        composePage.typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody())
                .send();
    }

    public void sendEmptyMail(String recipient) {
        System.out.println("- Send empty mail.");
        composePage = baseMailPage.clickComposeLetterButton().waitForLoad();
        composePage.typeRecipient(recipient).send();
    }

    public void waitUntilLetterSent() {
        System.out.println("- Waiting until letter sent.");
        LetterSentPage letterSentPage = new LetterSentPage();
        letterSentPage.waitForLoad();
    }

    public void deleteLetter(Letter letter) {
        System.out.println("- Delete letter.");
        baseMailPage.openLetter(letter)
                .waitForLoad()
                .clickDeleteLetterButton();
        baseMailPage.waitUntilLetterDeleted();
    }

    public void deleteDraftLetter(Letter letter) {
        System.out.println("- Delete draft letter.");
        baseMailPage.openDraftLetter(letter)
                .waitForLoad()
                .clickDeleteComposeLetterButton();
        baseMailPage.waitUntilLetterDeleted();
    }

    public void openInboxFolder() {
        System.out.println("- Open inbox folder.");
        baseMailPage.openFolder(new InboxFolderPage());
    }

    public void openOutboxFolder() {
        System.out.println("- Open outbox folder.");
        baseMailPage.openFolder(new OutboxFolderPage());
    }

    public void openTrashFolder() {
        System.out.println("- Open trash folder.");
        baseMailPage.openFolder(new TrashFolderPage());
    }

    public boolean letterPresent(Letter letter) {
        System.out.println("- Check that letter present.");
        return baseMailPage.letterIsPresent(letter);
    }

    public String retrieveErrorOnFailedSendMail() {
        System.out.println("- Retrieving error message.");
        return composePage.getErrorMessage();
    }

    public void saveInDraft(Letter letter) {
        System.out.println("- Save letter in drafts.");
        composePage = baseMailPage.clickComposeLetterButton().waitForLoad();
        composePage.typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody());
        baseMailPage.openFolder(new DraftFolderPage());
        if (composePage.dialogWindowIsPresent()) {
            composePage.clickSaveButton();
        }
    }
}
