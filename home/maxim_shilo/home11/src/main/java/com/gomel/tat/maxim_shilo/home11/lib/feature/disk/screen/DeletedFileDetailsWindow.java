package com.gomel.tat.maxim_shilo.home11.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class DeletedFileDetailsWindow {

    private static final String RESTORE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.restore' and contains(@data-params,'%s')]";
    private static final String DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.delete' and contains(@data-params,'%s')]";

    private RandomFile randomFile;

    public DeletedFileDetailsWindow(RandomFile randomFile) {
        this.randomFile = randomFile;
    }

    public DeletedFileDetailsWindow waitForLoad() {
        Browser.current().waitForElementIsPresent(By.xpath
                (String.format(RESTORE_BUTTON_LOCATOR_PATTERN, randomFile.getFileName())));
        return this;
    }

    public TrashFolderPage clickDeletePermanentlyButton() {
        Browser.current().click(By.xpath
                (String.format(DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN, randomFile.getFileName())));
        return new TrashFolderPage();
    }

    public TrashFolderPage clickRestoreFileButton() {
        Browser.current().click(By.xpath
                (String.format(RESTORE_BUTTON_LOCATOR_PATTERN, randomFile.getFileName())));
        return new TrashFolderPage();
    }
}
