package com.gomel.tat.maxim_shilo.home11.tests;

import com.gomel.tat.maxim_shilo.home11.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home11.lib.feature.common.AccountFactory;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.service.LoginService;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;

public class BaseTestLogin {

    private Account account;
    private LoginService loginService = new LoginService();

    @BeforeClass()
    public void setUp() {
        account = AccountFactory.getAccount();
    }

    @Test(description = "Login into mail as valid user.")
    public void login() {
        loginService.loginToMailBox(account);
        assertTrue(loginService.loginSuccess(account), "Login failed!");
    }

    @AfterClass()
    public void clearBrowser() {
        Browser.quit();
    }
}
