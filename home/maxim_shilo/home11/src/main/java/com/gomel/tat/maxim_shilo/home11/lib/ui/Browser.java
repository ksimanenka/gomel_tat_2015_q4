package com.gomel.tat.maxim_shilo.home11.lib.ui;

import org.apache.commons.io.FileUtils;
import org.kohsuke.args4j.Option;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class Browser extends BrowserSetup {

    @Option(name = "-browser_type")
    protected static BrowserType browserType;

    private WebDriver driver;
    private static Browser instance = null;

    private static final int TIME_OUT_ELEMENT_IS_CLICKABLE_SECONDS = 10;
    private static final int TIME_OUT_ELEMENT_IS_PRESENT_SECONDS = 10;
    private static final int TIME_OUT_ELEMENT_IS_DISAPPEAR_SECONDS = 10;
    private static final int TIME_OUT_FILE_DOWNLOADED_MILISECONDS = 7000;

    public static void rise() {
        System.out.println("//browser// Rise browser.");
        try {
            if (instance != null) {
                quit();
            }
            Browser browser = new Browser();
            browser.createDriver();
            instance = browser;
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static Browser current() {
        if (instance == null) {
            rise();
        }
        return instance;
    }

    public static void quit() {
        System.out.println("//browser// Close browser.");
        try {
            WebDriver driver = instance.getWrappedDriver();
            if (driver != null) {
                driver.quit();
            }
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            instance = null;
        }
    }

    private WebDriver createDriver() throws MalformedURLException {
        System.out.println("//browser// Create driver." + browserType);
        if (browserType == null) {
            driver = getChromeRemoteWebDriver();
            return driver;
        }
        switch (browserType) {
            case CHROME:
                driver = getChromeRemoteWebDriver();
                break;
            case FIREFOX:
                driver = getFirefoxRemoteWebDriver();
                break;
            default:
                throw new IllegalArgumentException("Wrong program argument configuration!");
        }
        return driver;
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    //Browser actions.
    public void open(String URL) {
        System.out.println("  => Open page: " + URL);
        driver.get(URL);
    }

    public WebElement findElement(By locator) {
        System.out.println("  => Find element: '" + locator + "'");
        return getWrappedDriver().findElement(locator);
    }

    public void click(By locator) {
        System.out.println("  => Click: '" + locator + "'");
        getWrappedDriver().findElement(locator).click();
    }

    public void writeText(By locator, String text) {
        System.out.println("  => Write text: '" + text + "' to: '" + locator + "'");
        getWrappedDriver().findElement(locator).sendKeys(text);
    }

    public void submitForm(By locator) {
        System.out.println("  => Submit: '" + locator + "'");
        getWrappedDriver().findElement(locator).submit();
    }

    public String getText(By locator) {
        System.out.println("  => Get text: '" + locator + "'");
        return getWrappedDriver().findElement(locator).getText();
    }

    public void dragAndDrop(By fromLocator, By toLocator) {
        System.out.println("  => Drag and drop from: '" + fromLocator + "' to: '" + toLocator + "'");
        Action action = new Actions(getWrappedDriver())
                .dragAndDrop(findElement(fromLocator), findElement(toLocator))
                .build();
        action.perform();
    }

    public boolean isElementPresent(By locator) {
        try {
            waitForElementIsPresent(locator);
        } catch (RuntimeException e) {
            return false;
        }
        return true;
    }

    public void scrollWindow(int offset) {
        JavascriptExecutor jse = (JavascriptExecutor) getWrappedDriver();
        jse.executeScript(String.format("window.scrollBy(0,%s)", offset), "");
    }

    public Actions getActionsBuilder() {
        return new Actions(getWrappedDriver());
    }

    public void waitForElementIsClickable(By locator) {
        System.out.println("  => Wait for element is clickable: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), TIME_OUT_ELEMENT_IS_CLICKABLE_SECONDS)
                .until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitForElementIsPresent(By locator) {
        System.out.println("  => Wait for element is present: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), TIME_OUT_ELEMENT_IS_PRESENT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForElementIsVisible(By locator) {
        System.out.println("  => Wait for element is visible: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), TIME_OUT_ELEMENT_IS_PRESENT_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForDisappear(By locator) {
        System.out.println("  => Wait for element is disappear: '" + locator + "'");
        new WebDriverWait(getWrappedDriver(), TIME_OUT_ELEMENT_IS_DISAPPEAR_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitUntilFileDownloaded() throws InterruptedException {
        System.out.println("  => Wait until file downloaded.");
        Thread.sleep(TIME_OUT_FILE_DOWNLOADED_MILISECONDS);
    }

    public void getScreenShot(String filePath) throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(filePath));
    }
}