package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class LetterSentPage {

    private static final By LETTER_SENT_MESSAGE_LOCATOR = By.xpath("//div[text()='Письмо успешно отправлено.']");

    public LetterSentPage waitForLoad() {
        Browser.current().waitForElementIsVisible(LETTER_SENT_MESSAGE_LOCATOR);
        return this;
    }
}
