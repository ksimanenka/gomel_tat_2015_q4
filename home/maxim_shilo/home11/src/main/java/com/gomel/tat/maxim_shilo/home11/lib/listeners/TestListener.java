package com.gomel.tat.maxim_shilo.home11.lib.listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            System.out.println("Test started [" + method.getTestMethod().getDescription() + "]");
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            if (testResult.getStatus() == 1) {
                System.out.println("Test finished [status = DONE]\n");
            } else {
                System.out.println("Test finished [status = FAILED]\n" +
                        "Exception message: " + testResult.getThrowable().getMessage() + "\n");
            }
        }
    }
}
