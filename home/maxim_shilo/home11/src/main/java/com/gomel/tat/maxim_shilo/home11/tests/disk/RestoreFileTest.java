package com.gomel.tat.maxim_shilo.home11.tests.disk;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;


public class RestoreFileTest extends DeleteFileTest {

    @Test(priority = 3, description = "Restore deleted file from trash, check that file restored")
    public void RestoreFile() {
        diskService.restoreFile(randomFile);
        diskService.openDisk();
        assertTrue(diskService.fileIsPresent(randomFile), "File is not restored!");
    }
}
