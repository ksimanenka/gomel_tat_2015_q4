package com.gomel.tat.maxim_shilo.home11.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.openqa.selenium.By;

public class ComposePage {

    private static final By SEND_TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By SUBJECT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");
    private static final By SEND_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By SAVE_BUTTON_LOCATOR = By.xpath("//button[@data-action='dialog.save']");
    private static final By ERROR_MESSAGE_ADDRESS_REQUIRED_LOCATOR =
            By.xpath("//span[contains(@class,'error_required') and not(contains(@class,'hidden'))]");
    private static final By DELETE_COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.delete']");

    private static final int SCROLL_OFFSET = -250;

    public ComposePage waitForLoad() {
        Browser.current().waitForElementIsPresent(MAIL_TEXT_LOCATOR);
        return this;
    }

    public ComposePage typeRecipient(String recipient) {
        Browser.current().writeText(SEND_TO_INPUT_LOCATOR, recipient);
        return this;
    }

    public ComposePage typeSubject(String subject) {
        Browser.current().writeText(SUBJECT_LOCATOR, subject);
        return this;
    }

    public ComposePage typeBody(String bodyText) {
        Browser.current().writeText(MAIL_TEXT_LOCATOR, bodyText);
        return this;
    }

    public LetterSentPage send() {
        Browser.current().click(SEND_BUTTON_LOCATOR);
        return new LetterSentPage();
    }

    public String getErrorMessage() {
        Browser browser = Browser.current();
        if (browser.isElementPresent(ERROR_MESSAGE_ADDRESS_REQUIRED_LOCATOR)) {
            return browser.getText(ERROR_MESSAGE_ADDRESS_REQUIRED_LOCATOR);
        } else {
            throw new RuntimeException("Error message is not present.");
        }
    }

    public boolean dialogWindowIsPresent() {
        return Browser.current().isElementPresent(SAVE_BUTTON_LOCATOR);
    }

    public BaseMailPage clickSaveButton() {
        Browser.current().click(SAVE_BUTTON_LOCATOR);
        return new BaseMailPage();
    }

    public BaseMailPage clickDeleteComposeLetterButton() {
        Browser browser = Browser.current();
        browser.waitForElementIsPresent(DELETE_COMPOSE_BUTTON_LOCATOR);
        browser.scrollWindow(SCROLL_OFFSET);
        browser.click(DELETE_COMPOSE_BUTTON_LOCATOR);
        return new BaseMailPage();
    }
}
