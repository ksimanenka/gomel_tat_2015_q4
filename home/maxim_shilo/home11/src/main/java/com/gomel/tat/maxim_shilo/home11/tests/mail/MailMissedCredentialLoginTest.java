package com.gomel.tat.maxim_shilo.home11.tests.mail;

import com.gomel.tat.maxim_shilo.home11.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home11.lib.feature.mail.service.LoginService;
import com.gomel.tat.maxim_shilo.home11.lib.ui.Browser;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home11.lib.feature.common.AccountFactory.*;
import static org.testng.Assert.assertEquals;

public class MailMissedCredentialLoginTest {

    private LoginService loginService = new LoginService();
    private Account accountWithWrongPassword;
    private Account accountThatNotExisted;

    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS = "Неправильный логин или пароль.";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST = "Нет аккаунта с таким логином.";
    private String actualErrorMessage;

    @BeforeClass()
    public void prepareData() {
        accountWithWrongPassword = getAccountWithWrongPassword();
        accountThatNotExisted = getNonExistedAccount();
    }

    @Test(description = "Check expected error message in case of wrong password")
    public void checkErrorMessageWrongPassword() {
        loginService.loginToMailBox(accountWithWrongPassword);
        actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertEquals(actualErrorMessage, EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS, "Error message is not present or present not correctly!");
    }

    @Test(description = "Check expected error in case of non existed account")
    public void checkErrorMessageNonExistedAccount() {
        loginService.loginToMailBox(accountThatNotExisted);
        actualErrorMessage = loginService.retrieveErrorOnFailedLogin();
        assertEquals(actualErrorMessage, EXPECTED_ERROR_MESSAGE_IN_CASE_ACCOUNT_NOT_EXIST, "Error message is not present or present not correctly!");
    }

    @AfterClass()
    public void clearBrowser() {
        Browser.quit();
    }
}
