package com.gomel.tat.maxim_shilo.home11.lib.util;

import com.gomel.tat.maxim_shilo.home11.lib.feature.disk.RandomFile;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home11.lib.feature.common.CommonConstants.*;

public class RandomFileHelper {

    public static RandomFile createTempFile(RandomFile randomFile) throws IOException {
        File tempTxt = new File(randomFile.getPath());
        if (!tempTxt.exists()) {
            tempTxt.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(
                new FileWriter(tempTxt));
        writer.write(randomFile.getContent());
        writer.close();
        return randomFile;
    }

    public static boolean fileIsPresentOnPC(RandomFile randomFile) throws IOException {
        File fileOnPC = new File(DEFAULT_DOWNLOAD_DIRECTORY + randomFile.getFileName());
        if (fileOnPC.exists()) {
            String content = FileUtils.readFileToString(fileOnPC);
            if (content.equals(randomFile.getContent())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static void deleteTempFile(RandomFile randomFile) {
        File tempFile = new File(randomFile.getPath());
        if (tempFile.exists()) {
            tempFile.delete();
        }
    }

    public static void deleteTempFiles(ArrayList<RandomFile> randomFiles) {
        for (RandomFile file : randomFiles) {
            deleteTempFile(file);
        }
    }
}
