package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service;

import com.gomel.tat.maxim_shilo.home13.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen.BaseMailPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen.LoginFailedPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen.LoginPage;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.getScreenshot;
import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.logger;

public class LoginService {

    private LoginPage loginPage = new LoginPage();

    public void loginToMailBox(Account account) {
        logger().info("Try login to mail box using credentials [" + account.getLogin() + "::" + account.getPassword() + "]");
        loginPage.open()
                .typeLogin(account.getLogin())
                .typePassword(account.getPassword());
        getScreenshot();
        loginPage.submitLoginForm();
    }

    public boolean isLoginSuccess(Account account) {
        logger().info("Check that login success using credentials " + account.getLogin() + "::" + account.getPassword());
        return new BaseMailPage().isOpened();
    }

    public void successLogin(Account account) {
        loginToMailBox(account);
        isLoginSuccess(account);
    }

    public String retrieveErrorOnFailedLogin() {
        logger().info("Retrieving error message.");
        return new LoginFailedPage().getErrorMessage();
    }

    public void loginToDisk(Account account) {
        successLogin(account);
        logger().info("Open yandex disk.");
        new DiskPage().open();
    }
}
