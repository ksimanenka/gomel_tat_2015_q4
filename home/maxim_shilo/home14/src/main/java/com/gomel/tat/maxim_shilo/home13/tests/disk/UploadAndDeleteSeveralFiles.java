package com.gomel.tat.maxim_shilo.home13.tests.disk;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.LoginService;
import org.testng.annotations.*;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.common.AccountFactory.*;
import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileFactory.*;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class UploadAndDeleteSeveralFiles {

    private static final String ASSERT_UPLOAD_FAIL_MESSAGE_PATTERN = "Files [%s] is not presents on disk.";
    private static final String ASSERT_DELETE_FAIL_MESSAGE_PATTERN = "Files [%s] is not presents in trash folder.";

    private ArrayList<YaFile> yandexFiles;
    private DiskService diskService = new DiskService();

    @BeforeClass(description = "Login to yandex disk.")
    public void loginAndPrepareFiles(){
        yandexFiles = getSeveralYandexFiles();
        new LoginService().loginToDisk(getAccount());
    }

    @Test(description = "Upload several files, check that files correctly uploaded.")
    public void UploadSeveralFiles() {
        diskService.uploadFiles(yandexFiles);
        assertThat(format(ASSERT_UPLOAD_FAIL_MESSAGE_PATTERN, yandexFiles), diskService.isFilePresent(yandexFiles));
    }

    @Test(dependsOnMethods = "UploadSeveralFiles", description = "Delete several files to trash, check that files presents in trash.")
    public void DeleteSeveralFiles() {
        diskService.deleteSeveralFiles(yandexFiles);
        diskService.openTrash();
        assertThat(format(ASSERT_DELETE_FAIL_MESSAGE_PATTERN, yandexFiles), diskService.isFilePresent(yandexFiles));
    }
}
