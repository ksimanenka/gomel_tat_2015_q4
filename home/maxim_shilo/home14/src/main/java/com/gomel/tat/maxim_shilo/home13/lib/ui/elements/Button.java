package com.gomel.tat.maxim_shilo.home13.lib.ui.elements;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.*;
import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.logger;

public class Button extends Element {

    public Button(String xpath) {
        super(xpath);
    }

    public void click() {
        logger().debug("Click button: " + getBy());
        browser().findElement(getBy()).click();
        getScreenshot();
    }
}
