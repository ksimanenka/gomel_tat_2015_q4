package com.gomel.tat.maxim_shilo.home13.lib.listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.logger;

public class TestMethodsListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            logger().debug("TEST METHOD STARTED [" + method.getTestMethod().getDescription() + "]");
        } else {
            logger().debug("METHOD STARTED [" + method.getTestMethod().getMethodName() + "]");
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            logger().debug("TEST METHOD FINISHED [" + method.getTestMethod().getDescription() + "]");
        } else {
            logger().debug("METHOD FINISHED [" + method.getTestMethod().getMethodName() + "]");
        }
    }

}
