package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Button;
import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Element;

public class LetterSentPage {

    private Element letterSentMessage = new Element("//div[text()='Message sent successfully.']");
    private Button goToInbox = new Button("//a[contains(@class,'done') and @href='#inbox']");

    public boolean isOpened() {
        return letterSentMessage.isPresent();
    }


    public void backToInbox() {
        goToInbox.click();
    }
}
