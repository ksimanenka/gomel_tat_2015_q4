package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Button;

public class DeletedFileDetailsWindow {

    private static final String RESTORE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.restore' and contains(@data-params,'%s')]";
    private static final String DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.delete' and contains(@data-params,'%s')]";

    private Button deletePermanently;
    private Button restore;

    public DeletedFileDetailsWindow(YaFile yandexFile) {
        deletePermanently = new Button(String.format(DELETE_PERMANENTLY_BUTTON_LOCATOR_PATTERN, yandexFile));
        restore = new Button(String.format(RESTORE_BUTTON_LOCATOR_PATTERN, yandexFile));
    }

    public TrashFolderPage clickDeletePermanentlyButton() {
        deletePermanently.click();
        return new TrashFolderPage();
    }

    public TrashFolderPage clickRestoreFileButton() {
        restore.click();
        return new TrashFolderPage();
    }

    public DeletedFileDetailsWindow waitForPageLoad() {
        restore.waitFor().appear();
        deletePermanently.waitFor().appear();
        return this;
    }
}
