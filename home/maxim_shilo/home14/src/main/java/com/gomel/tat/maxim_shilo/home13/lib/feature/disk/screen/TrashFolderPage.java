package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.browser;

public class TrashFolderPage extends BaseDiskPage {

    private static final String TRASH_PAGE = "https://disk.yandex.com/client/trash";

    public TrashFolderPage open() {
        browser().open(TRASH_PAGE);
        return this;
    }

    public DeletedFileDetailsWindow selectFile(YaFile yaFile) {
        file(yaFile).click();
        return new DeletedFileDetailsWindow(yaFile);
    }
}
