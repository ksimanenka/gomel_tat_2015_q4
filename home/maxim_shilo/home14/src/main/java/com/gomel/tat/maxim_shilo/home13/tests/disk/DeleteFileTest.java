package com.gomel.tat.maxim_shilo.home13.tests.disk;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskService;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service.DiskServiceScripts;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileFactory.*;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DeleteFileTest {

    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "File [%s] is not deleted and not present in trash folder.";

    private YaFile yandexFile;
    private DiskService diskService = new DiskService();

    @BeforeClass(description = "Prepare file on disk for test [deleteFile].")
    public void prepareFile() {
        yandexFile = getYandexFile();
        new DiskServiceScripts().prepareFileOnDisk(yandexFile);
    }

    @Test(description = "Delete file to trash, check that file present in trash.")
    public void deleteFile() {
        diskService.deleteFile(yandexFile);
        diskService.openTrash();
        assertThat(format(ASSERT_FAIL_MESSAGE_PATTERN, yandexFile), diskService.isFilePresent(yandexFile));
    }
}
