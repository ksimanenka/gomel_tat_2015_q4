package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YandexFileUtils.*;
import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.*;
import static com.gomel.tat.maxim_shilo.home13.lib.util.Randomizer.*;

public class YandexFileFactory {

    private static final int DEFAULT_COUNT_OF_FILES = 3;

    public static YaFile getYandexFile() {
        logger().debug("Create file for upload.");
        File yaFile = new File(getDirForFiles() + randomTxtFileName());
        try {
            if (yaFile.createNewFile()) {
                FileUtils.write(yaFile, randomFileContent());
            }
        } catch (IOException e) {
            logger().error("Failed to create or write file.", e);
        }
        return new YaFile(yaFile);
    }

    public static ArrayList<YaFile> getSeveralYandexFiles() {
        ArrayList<YaFile> listOfFiles = new ArrayList<>();
        for (int i = 0; i < DEFAULT_COUNT_OF_FILES; i++) {
            listOfFiles.add(getYandexFile());
        }
        return listOfFiles;
    }
}
