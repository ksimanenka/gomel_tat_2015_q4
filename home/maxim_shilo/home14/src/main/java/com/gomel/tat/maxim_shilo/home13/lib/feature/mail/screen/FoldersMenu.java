package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Link;

public abstract class FoldersMenu extends BaseMailPage {

    public abstract Link getFolder();
}
