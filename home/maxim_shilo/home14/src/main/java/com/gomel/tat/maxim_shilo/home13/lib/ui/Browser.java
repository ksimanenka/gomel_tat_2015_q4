package com.gomel.tat.maxim_shilo.home13.lib.ui;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.gomel.tat.maxim_shilo.home13.config.GlobalConfig.*;
import static com.gomel.tat.maxim_shilo.home13.lib.ui.DriverConfig.driverConfig;
import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.*;
import static java.io.File.*;
import static java.lang.System.*;
import static java.util.concurrent.TimeUnit.*;

public class Browser {

    private static final int DEFAULT_TIMEOUT_SECONDS = 30;
    private static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;

    private WebDriver driver;
    private static Map<Thread, Browser> instances = new HashMap<>();

    public Browser() {
        try {
            createDriver().setup();
        } catch (MalformedURLException e) {
            logger().error(e.getMessage(), e);
        }
    }

    public static void riseBrowser() {
        logger().debug("Open browser.");
        if (instances.get(Thread.currentThread()) != null) {
            instances.get(Thread.currentThread()).closeBrowser();
        }
        instances.put(Thread.currentThread(), new Browser());
    }

    public static synchronized Browser browser() {
        if (instances.get(Thread.currentThread()) == null) {
            throw new SessionNotCreatedException("Browser not opened!");
        }
        return instances.get(Thread.currentThread());
    }

    public static void closeBrowser() {
        logger().debug("Close browser.");
        try {
            WebDriver driver = instances.get(Thread.currentThread()).getWrappedDriver();
            if (driver != null) {
                driver.quit();
            }
        } catch (Exception e) {
            logger().error("Problem with shutting down driver: " + e.getMessage(), e);
        } finally {
            instances.put(Thread.currentThread(), null);
        }
    }

    private Browser createDriver() throws MalformedURLException {
        if (config().getHub() == null) {
            driver = driverConfig().getLocalWebDriver(config().getBrowserType());
        } else {
            driver = driverConfig().getRemoteWebDriver(config().getBrowserType(), config().getHub());
        }
        return this;
    }

    private void setup() {
        driver.manage().timeouts().implicitlyWait(4, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, SECONDS);
        driver.manage().window().maximize();
    }

    private WebDriver getWrappedDriver() {
        return driver;
    }

    public Actions getActionBuilder() {
        return new Actions(browser().getWrappedDriver());
    }

    public void open(String URL) {
        logger().debug("Open page: " + URL);
        driver.get(URL);
    }

    public void refresh() {
        logger().debug("Refresh page.");
        getWrappedDriver().navigate().refresh();
    }

    public WebElement findElement(By locator) {
        try {
            return getWrappedDriver().findElement(locator);
        } catch (StaleElementReferenceException e) {
            return getWrappedDriver().findElement(locator);
        }
    }

    public void dragAndDrop(ArrayList<By> elements, By to) {
        try {
            logger().debug("Drag and drop: [" + elements + "] to: [" + to + "]");
            Actions builder = new Actions(getWrappedDriver());
            builder.keyDown(Keys.CONTROL);
            for (By element : elements) {
                builder.click(findElement(element));
            }
            builder.keyUp(Keys.CONTROL);
            builder.clickAndHold(findElement(elements.get(0)))
                    .moveToElement(findElement(to))
                    .release();
            builder.build().perform();
        } catch (StaleElementReferenceException e) {
            refresh();
            dragAndDrop(elements, to);
        }
    }

    public void scrollWindow(int offset) {
        logger().debug("Scrolling window.");
        ((JavascriptExecutor) getWrappedDriver())
                .executeScript(String.format("window.scrollBy(0,%s)", offset), "");
    }

    public void waitForElementIsPresent(By locator) {
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForElementIsClickable(By locator) {
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitForAppear(By locator) {
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForDisappear(By locator) {
        new WebDriverWait(getWrappedDriver(), DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public static void getScreenshot() {
        File sreenshotFile = new File(config().getReportsDir() + separator + nanoTime() + ".png");
        File scrFile = ((TakesScreenshot) browser().getWrappedDriver()).getScreenshotAs(OutputType.FILE);
        logger().save(sreenshotFile.getName());
        try {
            FileUtils.copyFile(scrFile, sreenshotFile);
        } catch (IOException e) {
            logger().error(e.getMessage(), e);
        }
    }

    public void delay(int milliseconds) {
        logger().debug("Delay for " + milliseconds + " milliseconds.");
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            logger().error(e.getMessage(), e);
        }
    }
}