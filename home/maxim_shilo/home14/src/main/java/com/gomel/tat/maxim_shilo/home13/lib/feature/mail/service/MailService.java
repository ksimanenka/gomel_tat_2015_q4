package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service;

import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo.Letter;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen.*;

import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.*;

public class MailService {

    private BaseMailPage baseMailPage = new BaseMailPage();
    private ComposePage composePage = new ComposePage();

    public void sendMail(Letter letter) {
        logger().info("Send mail.");
        baseMailPage.clickComposeLetterButton();
        composePage.typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody());
        composePage.send();
    }

    public void sendEmptyMail(String recipient) {
        logger().info("Send empty mail.");
        baseMailPage.clickComposeLetterButton();
        composePage.typeRecipient(recipient);
        composePage.send();
    }

    public boolean isLetterSent() {
        logger().info("Check that letter sent.");
        return new LetterSentPage().isOpened();
    }

    public void backToInbox() {
        logger().info("Open inbox folder.");
        new LetterSentPage().backToInbox();
    }

    public void deleteLetter(Letter letter) {
        logger().info("Delete letter [" + letter.getSubject() + "]");
        baseMailPage.openLetter(letter);
        new LetterPage().clickDeleteLetterButton()
                .closeMessageLetterDeleted();
    }

    public void deleteDraftLetter(Letter letter) {
        logger().info("Delete draft letter [" + letter.getSubject() + "]");
        baseMailPage.openLetter(letter);
        new ComposePage().clickDeleteComposeButton()
                .closeMessageLetterDeleted();
    }

    public void openInboxFolder() {
        logger().info("Open inbox folder.");
        baseMailPage.openFolder(new InboxFolderPage());
    }

    public void openOutboxFolder() {
        logger().info("Open outbox folder.");
        baseMailPage.openFolder(new OutboxFolderPage());
    }

    public void openTrashFolder() {
        logger().info("Open trash folder.");
        baseMailPage.openFolder(new TrashFolderPage());
    }

    public boolean isLetterPresent(Letter letter) {
        logger().info("Check that letter [" + letter.getSubject() + "] is present.");
        return baseMailPage.isLetterPresent(letter);
    }

    public boolean isLetterNotPresent(Letter letter) {
        logger().info("Check that letter [" + letter.getSubject() + "] not present.");
        return !baseMailPage.isLetterPresent(letter);
    }

    public String retrieveErrorOnFailedSendMail() {
        logger().info("Retrieving error message.");
        return composePage.getErrorMessage();
    }

    public void saveInDraft(Letter letter) {
        logger().info("Save letter in drafts.");
        baseMailPage.clickComposeLetterButton();
        composePage.typeRecipient(letter.getRecipient())
                .typeSubject(letter.getSubject())
                .typeBody(letter.getBody());
        baseMailPage.openFolder(new DraftFolderPage());
        composePage.saveLetter();
    }
}
