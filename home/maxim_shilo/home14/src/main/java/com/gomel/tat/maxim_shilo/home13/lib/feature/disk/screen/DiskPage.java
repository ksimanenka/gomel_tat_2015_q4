package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.*;
import org.openqa.selenium.*;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.browser;

public class DiskPage extends BaseDiskPage {

    private static final String YANDEX_DISK_PAGE = "https://disk.yandex.com/client/disk";

    private Input upload = new Input("//input[@type='file']");
    private Button closeUploadWindow = new Button("//button[contains(@class,'button-close')]");
    private Link trash = new Link("//div[@data-id='/trash']");

    public DiskPage open() {
        browser().open(YANDEX_DISK_PAGE);
        return this;
    }

    public DiskPage uploadFile(YaFile yaFile) {
        upload.type(yaFile.uploaded().getAbsolutePath());
        return this;
    }

    public DiskPage waitUntilFileUploaded() {
        closeUploadWindow.waitFor().clickable();
        return this;
    }

    public DiskPage closeUploadWindow() {
        closeUploadWindow.click();
        return this;
    }

    public DiskPage dragToTrash(YaFile yaFile) {
        file(yaFile).dragAndDrop(trash);
        return this;
    }

    public FileDetailsWindow selectFile(YaFile yandexFile) {
        file(yandexFile).click();
        return new FileDetailsWindow(yandexFile);
    }

    public DiskPage deleteSeveralFiles(ArrayList<YaFile> yandexFiles) {
        ArrayList<By> elements = new ArrayList<>();
        for (YaFile yandexFile : yandexFiles) {
            elements.add(file(yandexFile).getBy());
        }
        browser().dragAndDrop(elements, trash.getBy());
        return this;
    }
}
