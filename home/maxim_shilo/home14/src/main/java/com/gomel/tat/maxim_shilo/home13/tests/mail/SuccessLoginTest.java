package com.gomel.tat.maxim_shilo.home13.tests.mail;

import com.gomel.tat.maxim_shilo.home13.lib.feature.common.Account;
import com.gomel.tat.maxim_shilo.home13.lib.feature.mail.service.LoginService;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home13.lib.feature.common.AccountFactory.*;
import static java.lang.String.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class SuccessLoginTest {

    private Account account = getAccount();
    private LoginService loginService = new LoginService();
    private static final String ASSERT_FAIL_MESSAGE_PATTERN = "Login failed with credentials:\n%s";

    @Test(description = "Login into mail as valid user.")
    public void login() {
        loginService.loginToMailBox(account);
        assertThat(format(ASSERT_FAIL_MESSAGE_PATTERN, account), loginService.isLoginSuccess(account));
    }
}
