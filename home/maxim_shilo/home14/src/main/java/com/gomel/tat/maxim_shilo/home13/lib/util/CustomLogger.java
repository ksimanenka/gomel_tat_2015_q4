package com.gomel.tat.maxim_shilo.home13.lib.util;

import com.gomel.tat.maxim_shilo.home13.lib.listeners.LoggerLevel;
import com.gomel.tat.maxim_shilo.home13.lib.listeners.TestListener;
import org.apache.log4j.Logger;
import org.testng.xml.XmlSuite;

import static com.gomel.tat.maxim_shilo.home13.config.GlobalConfig.*;
import static org.apache.commons.lang3.StringUtils.*;

public class CustomLogger {

    private static final Logger logger = Logger.getLogger(CustomLogger.class);
    public TestListener testListener = new TestListener();

    private static CustomLogger instance;

    public static CustomLogger logger() {
        if (instance == null) {
            instance = new CustomLogger();
        }
        return instance;
    }

    public void debug(String message) {
        logger.debug(getTestName() + "[DEBUG] " + message);
    }

    public void info(String message) {
        logger.info(getTestName() + " [INFO] " + message);
    }

    public void error(String message, Throwable exception) {
        logger.error(getTestName() + "[ERROR] " + message, exception);
    }

    public void save(String pathToFile) {
        logger.log(LoggerLevel.SAVE, pathToFile);
    }

    private String getTestName() {
        if (config().getParallelMode() == XmlSuite.ParallelMode.FALSE) {
            return EMPTY;
        } else {
            return "[" + testListener.getCurrentTestName() + "]";
        }
    }

}
