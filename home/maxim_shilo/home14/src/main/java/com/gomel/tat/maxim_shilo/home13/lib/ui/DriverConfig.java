package com.gomel.tat.maxim_shilo.home13.lib.ui;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.remote.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.logger;

public class DriverConfig {

    private static final String PATH_TO_CHROME_DRIWER = "./src/main/resources/chromedriver_win.exe";
    private static final String DEFAULT_DOWNLOAD_DIRECTORY = FileUtils.getTempDirectoryPath() + "browser_downloads\\";

    private static DriverConfig instance;

    public static DriverConfig driverConfig() {
        if (instance == null) {
            instance = new DriverConfig();
        }
        return instance;
    }

    public WebDriver getLocalWebDriver(BrowserType browserType) {
        switch (browserType) {
            case CHROME:
                return getChromeLocalWebDriver();
            case FIREFOX:
                return getFirefoxLocalWebDriver();
            default:
                throw new IllegalArgumentException(String.format("Browser type [%s] doesn't exist!", browserType));
        }
    }

    public WebDriver getRemoteWebDriver(BrowserType browserType, String hub) throws MalformedURLException {
        switch (browserType) {
            case CHROME:
                return getChromeRemoteWebDriver(hub);
            case FIREFOX:
                return getFirefoxRemoteWebDriver(hub);
            default:
                throw new IllegalArgumentException(String.format("Browser type [%s] doesn't exist!", browserType));
        }
    }

    public WebDriver getChromeLocalWebDriver() {
        logger().debug("Create local chrome driver.");
        System.setProperty("webdriver.chrome.driver", PATH_TO_CHROME_DRIWER);
        return new ChromeDriver(getChromeOptions());
    }

    public WebDriver getChromeRemoteWebDriver(String hub) throws MalformedURLException {
        logger().debug("Create remote chrome driver.");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, getChromeOptions());
        return new RemoteWebDriver(new URL(hub), capabilities);
    }

    public WebDriver getFirefoxLocalWebDriver() {
        logger().debug("Create local firefox driver.");
        return new FirefoxDriver(getFirefoxProfile());
    }

    public WebDriver getFirefoxRemoteWebDriver(String hub) throws MalformedURLException {
        logger().debug("Create remote firefox driver.");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, getFirefoxProfile());
        return new RemoteWebDriver(new URL(hub), capabilities);
    }

    private ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("download.default_directory", getDownloadDirectory());
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("--lang=en-us");
        return options;
    }

    private FirefoxProfile getFirefoxProfile() {
        FirefoxProfile ffProfile = new FirefoxProfile();
        ffProfile.setPreference("browser.download.folderList", 2);
        ffProfile.setPreference("browser.download.dir", getDownloadDirectory());
        ffProfile.setPreference("browser.download.manager.showWhenStarting", false);
        ffProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        return ffProfile;
    }

    public static String getDownloadDirectory() {
        File downloadDir = new File(DEFAULT_DOWNLOAD_DIRECTORY);
        if (!downloadDir.exists()) {
            logger().debug("Create directory:" + downloadDir.getAbsolutePath());
            downloadDir.mkdirs();
        }
        return downloadDir.getAbsolutePath();
    }
}
