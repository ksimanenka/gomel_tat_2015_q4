package com.gomel.tat.maxim_shilo.home13.lib.ui.elements;

import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.*;
import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.logger;

public class Element {

    private By element;

    public Element(String xpath) {
        element = By.xpath(xpath);
    }

    public By getBy() {
        return element;
    }

    public boolean isPresent() {
        logger().debug("Check that element present: " + getBy());
        try {
            browser().findElement(getBy());
        } catch (RuntimeException e) {
            getScreenshot();
            return false;
        }
        getScreenshot();
        return true;
    }

    public String getText() {
        logger().debug("Get text from element located: " + getBy());
        getScreenshot();
        return browser().findElement(getBy()).getText();
    }

    public void dragAndDrop(Element to) {
        logger().debug("Drag and drop: " + getBy() + ", to: " + to);
        browser().getActionBuilder()
                .dragAndDrop(browser().findElement(getBy()), browser().findElement(to.getBy()))
                .build()
                .perform();
        getScreenshot();
    }

    public ElementWaits waitFor() {
        return new ElementWaits(this);
    }

    @Override
    public String toString() {
        return element.toString();
    }
}
