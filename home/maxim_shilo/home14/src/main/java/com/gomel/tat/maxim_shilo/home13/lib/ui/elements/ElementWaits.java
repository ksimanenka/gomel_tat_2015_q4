package com.gomel.tat.maxim_shilo.home13.lib.ui.elements;

import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Element;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.browser;
import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.logger;

public class ElementWaits {

    private Element element;

    public ElementWaits(Element element) {
        this.element = element;
    }

    public Element presence() {
        logger().debug("Wait for element is present: " + element);
        browser().waitForElementIsPresent(element.getBy());
        return element;
    }

    public Element clickable() {
        logger().debug("Wait for element is clickable: " + element);
        browser().waitForElementIsClickable(element.getBy());
        return element;
    }

    public Element appear() {
        logger().debug("Wait for element is visible: " + element);
        browser().waitForAppear(element.getBy());
        return element;
    }

    public Element disappear() {
        logger().debug("Wait for element is disappear: " + element);
        browser().waitForDisappear(element.getBy());
        return element;
    }
}
