package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Link;

public class InboxFolderPage extends FoldersMenu {

    private Link inboxFolder = new Link("//a[@href='#inbox' and @data-action='move']");

    public Link getFolder() {
        return inboxFolder;
    }
}