package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Button;

public class FileDetailsWindow {

    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.download' and contains(@data-params,'%s')]";

    private Button download;

    public FileDetailsWindow(YaFile yandexFile) {
        download = new Button(String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, yandexFile));
    }

    public FileDetailsWindow clickDownloadButton() {
        download.click();
        return this;
    }

    public FileDetailsWindow waitForPageLoad() {
        download.waitFor().appear();
        return this;
    }
}
