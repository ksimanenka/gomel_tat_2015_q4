package com.gomel.tat.maxim_shilo.home13.lib.ui.elements;

import com.gomel.tat.maxim_shilo.home13.lib.ui.Browser;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.getScreenshot;
import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.logger;

public class Link extends Element {

    public Link(String xpath) {
        super(xpath);
    }

    public void click() {
        logger().debug("Click link: " + getBy());
        Browser.browser().findElement(getBy()).click();
        getScreenshot();
    }
}
