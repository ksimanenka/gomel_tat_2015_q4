package com.gomel.tat.maxim_shilo.home13.lib.ui.elements;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.*;
import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.logger;

public class Input extends Element {

    public Input(String xpath) {
        super(xpath);
    }

    public Input type(String text) {
        logger().debug("Write text: " + text + ", to: " + getBy());
        browser().findElement(getBy()).sendKeys(text);
        return this;
    }
}
