package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Element;
import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Link;

public class BaseDiskPage {

    private static final String UPLOADED_FILE_LOCATOR_PATTERN = "//div[contains(@data-id,'%s')]";

    private Element fileDeletedMessage = new Element("//div[@class='notifications__text js-message']");

    public Link file(YaFile yaFile) {
        return new Link(String.format(UPLOADED_FILE_LOCATOR_PATTERN, yaFile));
    }

    public boolean isFilePresent(YaFile yaFile) {
        return file(yaFile).isPresent();
    }

    public void waitUntilFileDeleted() {
        fileDeletedMessage.waitFor().appear()
                .waitFor().disappear();
    }
}
