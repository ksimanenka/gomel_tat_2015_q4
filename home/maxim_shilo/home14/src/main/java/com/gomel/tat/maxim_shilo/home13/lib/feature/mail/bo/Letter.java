package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.bo;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class Letter {

    private String recipient;
    private String subject;
    private String body;

    private static final String EMPTY_LETTER_SUBJECT_NAME = "(No subject)";

    public Letter(String recipient, String subject, String body){
        this.recipient = recipient;
        this.subject = subject;
        this.body = body;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSubject() {
        if (subject.equals(EMPTY)) {
            return EMPTY_LETTER_SUBJECT_NAME;
        } else {
            return subject;
        }
    }

    public String getBody() {
        return body;
    }
}
