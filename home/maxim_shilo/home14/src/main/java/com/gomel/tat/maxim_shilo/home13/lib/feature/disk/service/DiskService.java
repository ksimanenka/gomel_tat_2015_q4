package com.gomel.tat.maxim_shilo.home13.lib.feature.disk.service;

import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.bo.YaFile;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen.BaseDiskPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home13.lib.feature.disk.screen.TrashFolderPage;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home13.lib.util.CustomLogger.*;

public class DiskService {

    private DiskPage diskPage = new DiskPage();
    private TrashFolderPage trashFolderPage = new TrashFolderPage();
    private BaseDiskPage baseDiskPage = new BaseDiskPage();

    public DiskPage openDisk() {
        logger().info("Open disk.");
        return diskPage.open();
    }

    public TrashFolderPage openTrash() {
        logger().info("Open trash folder.");
        return trashFolderPage.open();
    }

    public void uploadFile(YaFile yandexFile) {
        logger().info("Upload file: " + yandexFile.uploaded().getPath());
        diskPage.uploadFile(yandexFile)
                .waitUntilFileUploaded()
                .closeUploadWindow();
    }

    public void uploadFiles(ArrayList<YaFile> yandexFiles) {
        logger().info("Upload several files.");
        for (YaFile yandexFile : yandexFiles) {
            diskPage.uploadFile(yandexFile);
        }
        diskPage.waitUntilFileUploaded().closeUploadWindow();
    }

    public void downloadFile(YaFile yandexFile) {
        logger().info("Download file[" + yandexFile + "].");
        diskPage.selectFile(yandexFile)
                .waitForPageLoad()
                .clickDownloadButton();
    }

    public void deleteFile(YaFile yaFile) {
        logger().info("Remove file [" + yaFile + "] to trash.");
        diskPage.dragToTrash(yaFile).waitUntilFileDeleted();
    }

    public void deleteFilePermanently(YaFile yandexFile) {
        logger().info("Remove file [" + yandexFile + "] from trash permanently.");
        trashFolderPage.selectFile(yandexFile)
                .waitForPageLoad()
                .clickDeletePermanentlyButton()
                .waitUntilFileDeleted();
    }

    public boolean isFilePresent(YaFile yandexFile) {
        logger().info("Check that file [" + yandexFile + "] is present in opened folder.");
        return baseDiskPage.isFilePresent(yandexFile);
    }

    public boolean isFilePresent(ArrayList<YaFile> yaFiles) {
        logger().info("Check that files [" + yaFiles + "] is presents in opened folder.");
        boolean result = true;
        for (YaFile yaFile : yaFiles) {
            result = result && isFilePresent(yaFile);
        }
        return result;
    }

    public void restoreFile(YaFile yandexFile) {
        logger().info("Restore file [" + yandexFile + "] from trash.");
        trashFolderPage.selectFile(yandexFile)
                .waitForPageLoad()
                .clickRestoreFileButton();
    }

    public void deleteSeveralFiles(ArrayList<YaFile> yandexFiles) {
        logger().info("Delete files [" + yandexFiles + "]");
        diskPage.deleteSeveralFiles(yandexFiles)
                .waitUntilFileDeleted();
    }
}