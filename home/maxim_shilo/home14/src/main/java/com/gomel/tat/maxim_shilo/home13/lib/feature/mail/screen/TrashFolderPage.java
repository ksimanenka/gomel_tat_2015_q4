package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Link;

public class TrashFolderPage extends FoldersMenu {

    private Link trashFolder = new Link("//a[@href='#trash']");

    public Link getFolder() {
        return trashFolder;
    }
}