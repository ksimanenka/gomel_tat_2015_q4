package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Button;
import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Input;

import static com.gomel.tat.maxim_shilo.home13.lib.ui.Browser.browser;

public class LoginPage {

    private static final String MAILBOX_URL = "https://mail.yandex.com";

    private Input login = new Input("//input[@name ='login']");
    private Input password = new Input("//input[@name='passwd']");
    private Button logIn = new Button("//button[contains(@class,'start')]");

    public LoginPage open() {
        browser().open(MAILBOX_URL);
        return this;
    }

    public LoginPage typeLogin(String userName) {
        login.type(userName);
        return this;
    }

    public LoginPage typePassword(String userPassword) {
        password.type(userPassword);
        return this;
    }

    public void submitLoginForm() {
        logIn.click();
    }
}
