package com.gomel.tat.maxim_shilo.home13.lib.feature.mail.screen;

import com.gomel.tat.maxim_shilo.home13.lib.ui.elements.Link;

public class DraftFolderPage extends FoldersMenu {

    private Link draftFolder = new Link("//a[@href='#draft']");

    public Link getFolder() {
        return draftFolder;
    }
}