package com.gomel.tat.maxim_shilo.home8.ui.page.yandex_mail;

import com.gomel.tat.maxim_shilo.home8.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.gomel.tat.maxim_shilo.home8.utils.WebDriverHelper.*;

public class LoginPage extends Page {

    public static final String YANDEX_START_PAGE = "http://ya.ru";
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href,'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSOWRD_INPUT_LOCATOR = By.name("passwd");

    public LoginPage(WebDriver driver){
        super(driver);
    }

    public void open(){
        driver.get(YANDEX_START_PAGE);
        driver.findElement(ENTER_BUTTON_LOCATOR).click();
    }

    public void login(String userLogin, String userPassword) {
        waitForAppear(LOGIN_INPUT_LOCATOR).sendKeys(userLogin);
        WebElement passwordInput = waitForElementIsClickable(PASSOWRD_INPUT_LOCATOR);
        passwordInput.sendKeys(userPassword);
        passwordInput.submit();
    }
}
