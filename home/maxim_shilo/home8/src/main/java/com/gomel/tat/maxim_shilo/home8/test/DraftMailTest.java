package com.gomel.tat.maxim_shilo.home8.test;

import com.gomel.tat.maxim_shilo.home8.ui.page.yandex_mail.*;
import com.gomel.tat.maxim_shilo.home8.utils.Letter;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home8.utils.WebDriverHelper.*;

public class DraftMailTest extends BaseTestLogin{

    private Letter letter;

    @BeforeClass
    public void createLetter(){
        letter = Letter.getRandomLetter();
    }

    @Test()
    public void CreateAndDeleteDraftMail() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.saveInDraft(letter);
        DraftPage draftPage = new DraftPage(driver);
        draftPage.open();
        org.testng.Assert.assertTrue(draftPage.isLetterPresent(letter));
        draftPage.deleteDraftLetter(letter);
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        org.testng.Assert.assertTrue(trashPage.isLetterPresent(letter));
        trashPage.deleteLetter(letter);
        org.testng.Assert.assertFalse(trashPage.isLetterPresent(letter));
    }
}
