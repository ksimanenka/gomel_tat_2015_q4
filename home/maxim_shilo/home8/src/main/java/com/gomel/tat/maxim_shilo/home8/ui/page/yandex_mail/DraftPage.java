package com.gomel.tat.maxim_shilo.home8.ui.page.yandex_mail;

import com.gomel.tat.maxim_shilo.home8.ui.page.Page;
import com.gomel.tat.maxim_shilo.home8.utils.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.maxim_shilo.home8.utils.WebDriverHelper.*;

public class DraftPage extends Page {

    private static final By DRAFT_FOLDER_LOCATOR = By.xpath("//a[@href='#draft']");

    public static final By DELETE_DRAFT_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.delete']");
    public static final By STATUSLINE_LOCATOR = By.xpath("//div[@class='b-statusline']");

    public DraftPage(WebDriver driver){
        super(driver);
    }

    public void open(){
        waitForElementIsClickable(DRAFT_FOLDER_LOCATOR).click();
    }

    public void deleteDraftLetter(Letter letter){
        waitForAppear(letter.getLocator()).click();
        waitForElementIsClickable(DELETE_DRAFT_BUTTON_LOCATOR).click();
        waitForAppear(STATUSLINE_LOCATOR);
    }
}
