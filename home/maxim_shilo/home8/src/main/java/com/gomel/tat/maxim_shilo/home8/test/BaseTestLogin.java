package com.gomel.tat.maxim_shilo.home8.test;

import com.gomel.tat.maxim_shilo.home8.ui.page.yandex_mail.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;

import static com.gomel.tat.maxim_shilo.home8.utils.WebDriverHelper.*;

public class BaseTestLogin {

    public WebDriver driver;
    private static final String login = "Maxim.Shilo.tat2015";
    private static final String password = "passwordfortat2015";

    @BeforeClass(groups = "chrome")
    public void prepareChromeBrowser() throws MalformedURLException {
        driver = getChromeWebDriver();
    }

    @BeforeClass(groups = "firefox")
    public void prepareFirefoxBrowser() throws MalformedURLException {
        driver = getFirefoxWebDriver();
    }

    @Test()
    public void LoginIntoMail() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(login, password);
    }

    @AfterClass()
    public void clearBrowser() {
        shutdownWebDriver();
    }
}
