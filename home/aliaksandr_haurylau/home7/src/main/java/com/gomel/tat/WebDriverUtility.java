package com.gomel.tat;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class WebDriverUtility {

    // AUT data
    public static final String BASE_URL = "http://www.mail.yandex.ru";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By COMPOSE_CANCEL_BUTTON_LOCATOR = By.xpath(".//*[@data-action='compose.close']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    //public static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//button[contains(@class,'item-delete')]");
    public static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.delete']");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final By TRASH_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    public WebDriver driver;

    // Test data
    public static final String YANDEX_START_PAGE = "http://mail.yandex.ru";


      public void firefoxLocalLaunch() {
        driver = new FirefoxDriver();
        driver.get(YANDEX_START_PAGE);
    }

      public void chromeLocalLaunch() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.get(YANDEX_START_PAGE);
    }

      public void firefoxRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444
        standAloneServerLaunch("FF");
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.get(YANDEX_START_PAGE);
    }

      public void chromeRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444 -Dwebdriver.chrome.driver="d:\path\to\chromedriver.exe"
        // java -jar /home/agv/workspace/webdriver/selenium-server-standalone-2.48.2.jar -port4444 -Dwebdriver.chrome.driver=/home/agv/workspace/home7/src/main/resources/chromedriver
        standAloneServerLaunch("CR");

        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome" );
        capability.setPlatform(Platform.LINUX);
        capability.setVersion("3.6");

        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capability);
        driver.get(YANDEX_START_PAGE);
    }

    public void standAloneServerLaunch(String browser) {
        String[] cmd;
        if (browser=="FF"){
            cmd = new String[]{"java", "-jar", "selenium-server-standalone-2.48.2.jar", "-port4444"};
        } else if (browser=="CR") {
            cmd = new String[]{"java", "-jar", "selenium-server-standalone-2.48.2.jar", "-port4444", "-Dwebdriver.chrome.driver=chromedriver"};
        } else { return; }
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void YandexMailLoginRoutine (WebDriver driver, String login, String password){
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.clear();
        loginInput.sendKeys(login);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.clear();
        passInput.sendKeys(password);
        passInput.submit();
    }

    public void YandexSendMessgeRoutine (String address, String subj, String content) {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(address);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(subj);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(content);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

    }

    public void YandexCreateDraftRoutine (String address, String subj, String content) {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(address);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(subj);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(content);
        new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//span[contains(@data-action,'compose-common.save-dropdown')]")));
        WebElement cancelMailButton = driver.findElement(COMPOSE_CANCEL_BUTTON_LOCATOR);
        cancelMailButton.click();
    }

    public void YandexLogoff () {
        By locator = By.xpath(".//*[@class='header-user-name js-header-user-name']");
        WebElement dropdownMenu = driver.findElement(locator);
        dropdownMenu.click();
        locator = By.xpath(".//*[@data-metric='Меню сервисов:Выход']");
        WebElement exitMenu = driver.findElement(locator);
        exitMenu.click();
    }


    public boolean YandexMessageExsistenceCheck (String subj) {
        if (subj=="") subj="(Без темы)";
        By locator = By.xpath(".//*[@title='"+subj+"']");
        //return driver.findElement(locator).isDisplayed();
        return !(0==driver.findElements(locator).size());
    }

    public boolean YandexIsMessageSent () {
        By locator = By.xpath(".//img[contains(@class,'mail-icon_error')]");
        return (0==driver.findElements(locator).size());
        //return !driver.findElement(locator).isDisplayed();
    }

    public boolean YandexIsLoginCheck () {
        return !(0==driver.findElements(INBOX_LINK_LOCATOR).size());
    }

}
