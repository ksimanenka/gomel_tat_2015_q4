package com.gomel.tat.ui;

import com.gomel.tat.baseobj.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LoginPage extends Page {

    public static final String BASE_URL = "https://disk.yandex.ru/";
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("password");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
           driver.get(BASE_URL);
    }

    public YandexDiskPage login(String userLogin, String userPassword) {
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.clear();
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.clear();
        passInput.sendKeys(userPassword);
        passInput.submit();
        return new YandexDiskPage(driver);
    }
}