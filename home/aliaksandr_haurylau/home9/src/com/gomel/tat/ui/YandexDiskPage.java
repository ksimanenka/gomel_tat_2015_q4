package com.gomel.tat.ui;

import com.gomel.tat.baseobj.Page;
import com.gomel.tat.baseobj.TestFile;
import static com.gomel.tat.util.WebDriverUtility.waitForElementIsClickable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;


public class YandexDiskPage extends Page {

    public static final String MAIN_PAGE_URL = "https://disk.yandex.ru/client/disk";
    public static final By TRASH_LOCATOR = By.xpath(".//*[@title='Корзина']");
    public static final By RESTORE_BUTTON_LOCATOR = By.xpath(".//*[@data-click-action='resource.restore']");
    public static final By PERMANENT_DELETE_BUTTON_LOCATOR = By.xpath(".//*[@data-click-action='resource.delete']");
    public static final By UPLOAD_LOCATOR = By.xpath(".//input[@type='file']");
    public static final By UPLOAD_POPUP_CLOSE = By.xpath(".//*[@data-click-action='dialog.close']");

    public YandexDiskPage(WebDriver driver) {
        super(driver);

    }

    public void open() {}

    public void uploadFile(TestFile testFile) {

        WebElement uploadButton = driver.findElement(UPLOAD_LOCATOR);
        uploadButton.sendKeys(testFile.getTestFilePath());
        waitForElementIsClickable(UPLOAD_POPUP_CLOSE);
        WebElement closeButton = driver.findElement(UPLOAD_POPUP_CLOSE);
        closeButton.click();
    }

    public boolean isTestFilePresent(TestFile testFile) {
        List<WebElement> search = driver.findElements(testFileLocator(testFile));
        int i=search.size();
        if (0==i) return false; //if found nothing
        for(WebElement elementN : search) if (elementN.isDisplayed()) return true; //if found displayed element
        return false; //if no displayed elements
    }

    public void moveToTrash(TestFile testFile) {
        WebElement draggable = driver.findElement(testFileLocator(testFile));
        WebElement target = driver.findElement(TRASH_LOCATOR);
        new Actions(driver).dragAndDrop(draggable, target).perform();
    }

    public void restoreFromTrash(TestFile testFile) {
        WebElement file = driver.findElement(testFileLocator(testFile));
        file.click();
        waitForElementIsClickable(RESTORE_BUTTON_LOCATOR);
        WebElement button = driver.findElement(RESTORE_BUTTON_LOCATOR);
        new Actions(driver).click(button).perform();
        pause();
    }

    public void permanentDelete(TestFile testFile) {
        WebElement file = driver.findElement(testFileLocator(testFile));
        file.click();
        waitForElementIsClickable(PERMANENT_DELETE_BUTTON_LOCATOR);
        WebElement button = driver.findElement(PERMANENT_DELETE_BUTTON_LOCATOR);
        new Actions(driver).click(button).perform();
        pause();pause();
    }


    public void openTrashBin() {
        WebElement target = driver.findElement(TRASH_LOCATOR);
        new Actions(driver).doubleClick(target).perform();
    }

    public void openRootFolder() {
        driver.get(MAIN_PAGE_URL);
    }


    private void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public By testFileLocator(TestFile testFile) {
        return By.xpath(".//*[@title='" + testFile.getTestFileName() + "']");
    }


}



