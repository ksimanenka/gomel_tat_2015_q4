package com.gomel.tat.baseobj;

import org.openqa.selenium.WebDriver;

public abstract class Page {

    public static final int TIME_OUT_WEBDRIVER_WAIT_SECONDS = 5;
    protected WebDriver driver;

    public Page (WebDriver driver) {
        this.driver=driver;
    }

}