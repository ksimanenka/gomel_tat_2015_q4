package com.gomel.tat.baseobj;

public class TestFileFactory {

    public static TestFile getTestFile() {
        String fileName = "test_file_"+(long)(Math.random() * 100000000)+".txt";
        String content = "test content " + (long)(Math.random() * 1000000000);
        return new TestFile(fileName,"", content);
    }

    public static TestFile[] getTestFiles(int qty) {
        TestFile[] testFileN = new TestFile[qty];
        for (int i=0;i<qty;i++) {
            String fileName = "test_file_" + (long) (Math.random() * 100000000) + ".txt";
            String content = "test content " + (long) (Math.random() * 1000000000);
            testFileN[i]=new TestFile(fileName, "", content);
        }
        return testFileN;
    }

}
