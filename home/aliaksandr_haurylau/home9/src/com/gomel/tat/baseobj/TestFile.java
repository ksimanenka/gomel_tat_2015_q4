package com.gomel.tat.baseobj;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TestFile {

    private String testFileName;
    private String testFilePath;
    private String testFileContent;


    public TestFile(String fileName, String filePath, String content) {
        this.testFileName = fileName;
        this.testFileContent = content;
        this.testFilePath = "";
    }

    public String getTestFileName() {
        return testFileName;
    }

    public String getTestFilePath() {
        return testFilePath;
    }

    public void setTestFilePath(String path) {
        this.testFilePath = path;
    }

    public String getTestFileContent() {
        return testFileContent;
    }

    public void deleteStoredTestFile() {
        File file = new File(this.getTestFileName());

        try {
            FileUtils.forceDelete(file);
        } catch (IOException e) {
            System.out.print("Error while deleting file!");
            e.printStackTrace();
        }
    }

    public void storeTestFile() {

        File file = new File(this.getTestFileName());
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            try {
                out.print(this.getTestFileContent());
                this.setTestFilePath(file.getAbsolutePath());
            } finally {
                out.close();
            }
        } catch (IOException e) {
            System.out.println("Write file error!");
        }
    }

    public static boolean isTestFileStored(TestFile testFile) throws IOException {

        List<String> lines = new ArrayList<String>();
        File file = new File(testFile.getTestFileName());

        try {
            lines = Files.readAllLines(Paths.get(file.toURI()), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println("Read file error!  ");
            e.printStackTrace();
        }
        if (lines.isEmpty() || lines.size() > 1) {
            return false;
        }
        if (!testFile.getTestFileContent().equals(lines.get(0))) {
            return false;
        }
        return true;
    }

}
