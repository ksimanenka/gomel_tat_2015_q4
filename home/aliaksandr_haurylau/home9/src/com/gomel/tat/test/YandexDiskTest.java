package com.gomel.tat.test;

import com.gomel.tat.ui.YandexDiskPage;
import com.gomel.tat.util.WebDriverUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.gomel.tat.baseobj.TestFile;
import com.gomel.tat.baseobj.TestFileFactory;
import com.gomel.tat.ui.LoginPage;

import static com.gomel.tat.util.WebDriverUtility.*;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import java.io.IOException;


public class YandexDiskTest {

    private static String userLogin = "a22333g";
    private static String userPassword = "cnhfyyjctrhtnysq2";
    private static WebDriver driver;
    private static TestFile testFile;
    private static YandexDiskPage yandexDisk;

    public static final By DOWNLOAD_BUTTON = By.xpath(".//button[@title='Скачать']");
    public static final By ALL_UPLOADS_DONE = By.xpath("//*[text()='Все загрузки завершены']");
    public static final String YDISK_ROOT_FOLDER = "https://disk.yandex.ru/client/disk";


    @Test
    public void testUploadFile() {

        testFile = TestFileFactory.getTestFile();
        testFile.storeTestFile();
        loginRoutine();
        yandexDisk.uploadFile(testFile);
        Assert.assertTrue(yandexDisk.isTestFilePresent(testFile));
        testFile.deleteStoredTestFile();
    }

    @Test
    public void testDownloadFile() {

        testFile = TestFileFactory.getTestFile();
        testFile.storeTestFile();
        loginRoutine();
        yandexDisk.uploadFile(testFile);
        waitForElementIsDisplayed(ALL_UPLOADS_DONE);
        testFile.deleteStoredTestFile();
        WebElement uploadedCheckbox = driver.findElement(By.xpath("//*[@title='" + testFile.getTestFileName() + "']"));
        uploadedCheckbox.click();
        WebElement downloadButton = driver.findElement(DOWNLOAD_BUTTON);
        waitForElementIsClickable(DOWNLOAD_BUTTON);
        downloadButton.click();
        pause();
        boolean fileDownloaded = false;
        try {
            fileDownloaded = TestFile.isTestFileStored(testFile);
        } catch (IOException e) {
        }
        Assert.assertTrue(fileDownloaded);
        testFile.deleteStoredTestFile();
    }

    @Test
    public void testDeleteAndRestoreFile() {

        testFile = TestFileFactory.getTestFile();
        testFile.storeTestFile();
        loginRoutine();
        yandexDisk.uploadFile(testFile);
        waitForElementIsDisplayed(ALL_UPLOADS_DONE);
        testFile.deleteStoredTestFile();
        yandexDisk.moveToTrash(testFile);
        pause();
        yandexDisk.openTrashBin();
        pause();
        yandexDisk.restoreFromTrash(testFile);
        pause();
        yandexDisk.openRootFolder();
        Assert.assertTrue(yandexDisk.isTestFilePresent(testFile));
    }

    @Test
    public void testDeletePermanently() {
        testFile = TestFileFactory.getTestFile();
        testFile.storeTestFile();
        loginRoutine();
        yandexDisk.uploadFile(testFile);
        waitForElementIsDisplayed(ALL_UPLOADS_DONE);
        testFile.deleteStoredTestFile();
        yandexDisk.moveToTrash(testFile);
        pause();
        yandexDisk.openTrashBin();
        pause();
        yandexDisk.permanentDelete(testFile);
        pause();
        Assert.assertTrue(!yandexDisk.isTestFilePresent(testFile));
    }

    @Test
    public void testDeleteSeveralFilesAtOnce() {
        char qty = 3; // number of elements
        TestFile[] testFileN = new TestFile[qty];
        testFileN = TestFileFactory.getTestFiles(qty);
        for (int i = 0; i < qty; i++) testFileN[i].storeTestFile();
        loginRoutine();
        for (int i = 0; i < qty; i++) {
            yandexDisk.uploadFile(testFileN[i]);
            pause();
        }
        waitForElementIsDisplayed(ALL_UPLOADS_DONE);
        Actions action = new Actions(driver);
        for (int i = 0; i < qty; i++) {
            action.keyDown(Keys.CONTROL).click(driver.findElement(yandexDisk.testFileLocator(testFileN[i]))).build().perform();
        }
        pause();
        WebElement draggable = driver.findElement(yandexDisk.testFileLocator(testFileN[0]));
        WebElement target = driver.findElement(YandexDiskPage.TRASH_LOCATOR);
        action.dragAndDrop(draggable, target).perform();
        pause();
        boolean isFiles=true;
        for (int i = 0; i < qty; i++) isFiles&=yandexDisk.isTestFilePresent(testFileN[i]);
        Assert.assertTrue(isFiles);
    }


    private void pause() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void loginRoutine() {
        if (!isLoggedInCheck()) {
            LoginPage loginPage = new LoginPage(driver);
            loginPage.open();
            yandexDisk = loginPage.login(userLogin, userPassword);
        } else driver.get(YDISK_ROOT_FOLDER);
        yandexDisk.open();
    }

    @BeforeSuite
    public void prepeareForTests() {
        driver = WebDriverUtility.getWebDriver("CR");
    } // "FF" for Firefox, "CR" for Chrome

    @AfterSuite
    public void closeWebdriver() {
        WebDriverUtility.shutdownWebDriver();
    }


}
