package com.gomel.tat.ui;

import com.gomel.tat.baseobj.Letter;
import com.gomel.tat.baseobj.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static com.gomel.tat.util.WebDriverUtility.waitForDisappear;
import static com.gomel.tat.util.WebDriverUtility.waitForElementIsDisplayed;

public class ComposePage extends Page {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By YOU_HAVE_GOT_MAIL_MESSAGE = By.xpath("//a[contains(text(),'от Александр Г.')]");
    public static final By SAVE_DIALOG_POPUP_LOCATOR = By.xpath(".//p[text()='Сохранить сделанные изменения?']");
    public static final By SAVE_DIALOG_SAVE_BUTTON_LOCATOR = By.xpath(".//*[@data-action='dialog.save']");
    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
    }

    public String sendLetter(Letter letter) {
        String messageID[], href;

        sendLetterUnchecked(letter);
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
        waitForElementIsDisplayed(YOU_HAVE_GOT_MAIL_MESSAGE);
        WebElement recievedMessage = driver.findElement(By.xpath("//a[contains(text(),'от Александр Г.')]"));
        href=recievedMessage.getAttribute("href");
        messageID=href.split("#message/");
        return messageID[1];
    }

    public void createDraft(Letter letter) {

        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getMailto());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());
        WebElement draftLink = driver.findElement(DRAFT_LINK_LOCATOR);
        draftLink.click();
        waitForElementIsDisplayed(SAVE_DIALOG_POPUP_LOCATOR);
        WebElement saveButton=driver.findElement(SAVE_DIALOG_SAVE_BUTTON_LOCATOR);
        saveButton.click();
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);

    }

    public void sendLetterUnchecked(Letter letter) {

        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getMailto());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }
}
