package com.gomel.tat.ui;

import com.gomel.tat.baseobj.Letter;
import com.gomel.tat.baseobj.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;
import static com.gomel.tat.util.WebDriverUtility.waitForElementIsClickable;

public class InboxPage extends Page {

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";


    public InboxPage(WebDriver driver) {
        super(driver);
    }


    public void open()    {
        waitForElementIsClickable(INBOX_LINK_LOCATOR).click();
    }


    public boolean isLetterPresent(Letter letter) {
       try {
            new WebDriverWait(driver, TIME_OUT_WEBDRIVER_WAIT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isMessageWithIDPresent(String messageID) {
        try {
            new WebDriverWait(driver, TIME_OUT_WEBDRIVER_WAIT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath("//a[contains(@href,'"+messageID+"')]")));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}