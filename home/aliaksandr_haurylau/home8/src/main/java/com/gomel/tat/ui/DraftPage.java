package com.gomel.tat.ui;

import com.gomel.tat.baseobj.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static com.gomel.tat.util.WebDriverUtility.waitForElementIsClickable;
import static com.gomel.tat.util.WebDriverUtility.waitForElementIsDisplayed;

public class DraftPage extends Page{

    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final String DRAFT_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.delete']");

    public DraftPage(WebDriver driver) {
        super(driver);
    }


    public void open()    {
        waitForElementIsClickable(DRAFT_LINK_LOCATOR).click();
    }


    public boolean isLetterPresent(Letter letter) {
        try {
            new WebDriverWait(driver, TIME_OUT_WEBDRIVER_WAIT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(DRAFT_LINK_LOCATOR_PATTERN, letter.getSubject()))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void deleteLetter(Letter letter) {

        WebElement thisLetter = driver.findElement(By.xpath(".//*[@title='"+letter.getSubject()+"']"));
        thisLetter.click();
        waitForElementIsClickable(DELETE_MAIL_BUTTON_LOCATOR);
        WebElement deleteButton= driver.findElement(DELETE_MAIL_BUTTON_LOCATOR);
        deleteButton.click();
        waitForElementIsDisplayed(By.xpath("//span[contains(text(),'1 сообщение удалено.')]"));
    }

}
