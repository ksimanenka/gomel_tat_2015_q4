package com.gomel.tat.ui;

import com.gomel.tat.baseobj.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static com.gomel.tat.util.WebDriverUtility.waitForElementIsClickable;
import static com.gomel.tat.util.WebDriverUtility.waitForElementIsDisplayed;

public class SentboxPage extends Page{

    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final String SENT_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.delete']");
    public static final By DELETED_MAIL_MESSAGE_LOCATOR = By.xpath("//span[contains(text(),'1 сообщение удалено.')]");

    public SentboxPage(WebDriver driver) {
        super(driver);
    }

    public void open()    {
        waitForElementIsClickable(SENT_LINK_LOCATOR).click();
    }


    public boolean isLetterPresent(Letter letter) {
        try {
            new WebDriverWait(driver, TIME_OUT_WEBDRIVER_WAIT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(SENT_LINK_LOCATOR_PATTERN, letter.getSubject()))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isMessageWithIDPresent(String messageID) {
        try {
            new WebDriverWait(driver, TIME_OUT_WEBDRIVER_WAIT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath("//a[contains(@href,'"+messageID+"')]")));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void deleteLetter(Letter letter) {
        WebElement thisLetter = driver.findElement(By.xpath(".//*[@title='"+letter.getSubject()+"']"));
        thisLetter.click();
        waitForElementIsClickable(DELETE_MAIL_BUTTON_LOCATOR);
        WebElement deleteButton= driver.findElement(DELETE_MAIL_BUTTON_LOCATOR);
        deleteButton.click();
        waitForElementIsDisplayed(DELETED_MAIL_MESSAGE_LOCATOR);
    }
}
