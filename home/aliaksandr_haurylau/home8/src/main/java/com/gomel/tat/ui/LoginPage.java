package com.gomel.tat.ui;

import com.gomel.tat.baseobj.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.gomel.tat.util.WebDriverUtility;


public class LoginPage extends Page {

    public static final String BASE_URL = "http://mail.yandex.ru";
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        if (!WebDriverUtility.isLoggedInCheck()) {
            driver.get(BASE_URL);
        }
    }

    public InboxPage login(String userLogin, String userPassword) {
        if (!WebDriverUtility.isLoggedInCheck()) {
            WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
            loginInput.clear();
            loginInput.sendKeys(userLogin);
            WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
            passInput.clear();
            passInput.sendKeys(userPassword);
            passInput.submit();
        }   return new InboxPage(driver);
    }
}
