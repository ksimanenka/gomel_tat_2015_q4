package com.gomel.tat.home11.lib.mail.screen;

import com.gomel.tat.home11.lib.ui.Browser;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;


@FindBy(className = "block-folders")
public class FolderSwitchForm extends HtmlElement {
    @FindBy(css = "[href='#inbox']")
    WebElement inbox;

    @FindBy(css = "[href='#sent']")
    WebElement sent;

    @FindBy(css = "[href='#draft']")
    WebElement draft;

    @FindBy(css = "[href='#trash']")
    WebElement trash;

    @FindBy(css = "[href='#spam']")
    WebElement spam;

    public void toInbox() {
        inbox.click();
    }

    public void toSent() {
        sent.click();
    }

    public void toDraft() {
        draft.click();
    }

    public void toTrash() {
        trash.click();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return ((TakesScreenshot) Browser.current().getWrappedDriver()).getScreenshotAs(outputType);
    }
}