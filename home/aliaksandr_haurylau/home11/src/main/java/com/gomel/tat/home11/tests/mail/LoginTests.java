package com.gomel.tat.home11.tests.mail;

import com.gomel.tat.home11.lib.common.Account;
import com.gomel.tat.home11.lib.common.AccountBuilder;
import com.gomel.tat.home11.lib.mail.service.LoginService;
import com.gomel.tat.home11.lib.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(TestListener.class)
public class LoginTests {

    @Test(description = "Login to mailbox with invalid password")
    public void testLoginNegative() {
        Account account = AccountBuilder.getAccountWithInvalidPassword();
        LoginService loginService = new LoginService();
        Assert.assertTrue(loginService.checkErrorWithWrongLogin(account));
    }

    @Test(description = "Login to mailbox as a valid password")
    public void testLoginPositive() {
        Account account = AccountBuilder.getDefaultAccount();
        LoginService loginService = new LoginService();
        Assert.assertTrue(loginService.checkLoginSuccessfull(account));
    }

    @BeforeClass
    public void prepeareForTests() {
        Browser.rise();
    }

    @AfterClass
    public void closeWebdriver() {
        Browser.current().quit();
    }

}
