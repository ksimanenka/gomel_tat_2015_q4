package com.gomel.tat.home11.lib.common;

import org.openqa.selenium.By;

public interface CommonConstants {
    String DEFAULT_MAIL_USER_LOGIN = "a22333g";
    String DEFAULT_MAIL_USER_PASSWORD = "cnhfyyjctrhtnysq2";
    String DEFAULT_MAIL_TO_SEND = "a22333g@yandex.ru";
    String MAILBOX_URL = "http://mail.yandex.by";

    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(.,'%s')]";
    public static final By SAVE_DIALOG_POPUP_LOCATOR = By.xpath(".//p[text()='Сохранить сделанные изменения?']");
    public static final By SAVE_DIALOG_CANCEL_BUTTON_LOCATOR = By.xpath(".//*[@data-action='dialog.dont_save']");
    public static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.delete']");
    public static final By DELETE_PERMANENT_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    public static final By YOU_HAVE_GOT_MAIL_MESSAGE = By.xpath("//a[contains(text(),'от Александр Г.')]");

}
