package com.gomel.tat.home11.lib.mail;


import com.gomel.tat.home11.lib.common.CommonConstants;
import org.apache.commons.lang3.RandomStringUtils;

public class LetterBuilder {


    public static Letter getTestLetter() {
        Letter letter = new Letter();
        letter.setMailto(CommonConstants.DEFAULT_MAIL_TO_SEND);
        letter.setSubject("Test subject " + RandomStringUtils.randomAlphanumeric(10));
        letter.setBody("Test content " + RandomStringUtils.randomAlphanumeric(100));
        return letter;
    }

    public static Letter getLetterWithWrongAddress() {
        Letter letter = getTestLetter();
        letter.setMailto("NotAMailAddress");
        return letter;
    }

    public static Letter getLetterWithoutAddress() {
        Letter letter = getTestLetter();
        letter.setMailto("");
        return letter;
    }

    public static Letter getLetterWithoutSubj() {
        Letter letter = getTestLetter();
        letter.setSubject("");
        return letter;
    }

    public static Letter getLetterWithoutBody() {
        Letter letter = getTestLetter();
        letter.setBody("");
        return letter;
    }
}
