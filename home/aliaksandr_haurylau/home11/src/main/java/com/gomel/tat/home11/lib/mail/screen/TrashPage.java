package com.gomel.tat.home11.lib.mail.screen;

import com.gomel.tat.home11.lib.common.CommonConstants;
import com.gomel.tat.home11.lib.mail.Letter;
import com.gomel.tat.home11.lib.ui.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TrashPage extends Page {

    public TrashPage() {
        super();
        this.gotoTrash();
    }

    public void deleteLetterBySubj(Letter letter) {

        WebElement thisLetter = Browser.current().getWrappedDriver().findElement(By.xpath(".//*[@title='" + letter.getSubject() + "']"));
        thisLetter.click();
        Browser.current().waitForElementIsClickable(CommonConstants.DELETE_PERMANENT_BUTTON_LOCATOR);
        WebElement deleteButton = Browser.current().getWrappedDriver().findElement(CommonConstants.DELETE_PERMANENT_BUTTON_LOCATOR);
        deleteButton.click();
        Browser.current().waitForElementIsDisplayed(By.xpath("//span[contains(text(),'1 сообщение удалено.')]"));
    }


}