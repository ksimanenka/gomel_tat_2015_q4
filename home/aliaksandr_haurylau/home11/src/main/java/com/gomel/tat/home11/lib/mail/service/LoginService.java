package com.gomel.tat.home11.lib.mail.service;

import com.gomel.tat.home11.lib.common.Account;
import com.gomel.tat.home11.lib.common.AccountBuilder;
import com.gomel.tat.home11.lib.mail.screen.MailLoginPage;
import com.gomel.tat.home11.lib.util.Logger;

public class LoginService {

    private static Logger logger = new Logger();

    public void loginToMailBox() {

        Account account = AccountBuilder.getDefaultAccount();
        MailLoginPage page = new MailLoginPage();
        logger.log(" logging with account '"+account.getLogin()+"'");
        page.login(account);
    }

    public boolean checkLoginSuccessfull(Account account) {

        MailLoginPage page = new MailLoginPage();
        logger.log(" logging with account '"+account.getLogin()+"'");
        page.login(account);
        logger.log(" checking successfull login");
        return page.isLoginSuccessful();
    }

    public boolean checkErrorWithWrongLogin(Account account) {

        MailLoginPage page = new MailLoginPage();
        logger.log(" logging with account '"+account.getLogin()+"'");
        page.login(account);
        logger.log(" checking error message");
        return page.isLoginError();
    }

}
