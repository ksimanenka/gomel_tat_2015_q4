package com.gomel.tat.home11.lib.runner;

import com.gomel.tat.home11.lib.config.GlobalConfig;
import org.apache.commons.cli.*;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        String browserArgument = "";

        Options options = new Options();
        options.addOption("browser_type", true, "select browser type for tests");
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);
            if (line.hasOption("browser_type")) {
                browserArgument = line.getOptionValue("browser_type");
            }
        } catch (ParseException exp) {
        }
        GlobalConfig.setSelectedBrowser(browserArgument);

        runAllTests();

    }

    public static void runAllTests() {

        TestNG testNG = new TestNG();
        TestListenerAdapter testListener = new TestListenerAdapter();
        testNG.addListener(testListener);
        XmlSuite suite = new XmlSuite();
        suite.setName("RefactoredTests");
        List<String> files = new ArrayList<>();
        files.addAll(new ArrayList<String>() {{
            add("./src/main/resources/test-suites/Tests1.xml");
            add("./src/main/resources/test-suites/Tests2.xml");
        }});
        suite.setSuiteFiles(files);
        suite.setThreadCount(5);
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        testNG.setXmlSuites(suites);
        testNG.run();

    }


}

