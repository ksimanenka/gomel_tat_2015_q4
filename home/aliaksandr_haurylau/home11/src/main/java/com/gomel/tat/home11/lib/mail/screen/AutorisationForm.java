package com.gomel.tat.home11.lib.mail.screen;

import com.gomel.tat.home11.lib.ui.Browser;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;


@FindBy(xpath = ".//*[@class='new-auth-form js-new-auth-form']")
public class AutorisationForm extends HtmlElement {
    @FindBy(name = "login")
    WebElement loginField;

    @FindBy(name = "passwd")
    WebElement passwordField;

    @FindBy(css = ".new-auth-form-line button[type='submit']")
    WebElement submitButton;

    public void login(String login, String password) {
        loginField.sendKeys(login);
        passwordField.sendKeys(password);
        submitButton.click();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
       return ((TakesScreenshot)Browser.current().getWrappedDriver()).getScreenshotAs(outputType);
     }

}