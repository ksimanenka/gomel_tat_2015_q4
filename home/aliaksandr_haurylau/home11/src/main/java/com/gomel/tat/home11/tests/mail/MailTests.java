package com.gomel.tat.home11.tests.mail;

import com.gomel.tat.home11.lib.mail.Letter;
import com.gomel.tat.home11.lib.mail.LetterBuilder;
import com.gomel.tat.home11.lib.mail.screen.DraftPage;
import com.gomel.tat.home11.lib.mail.screen.InboxPage;
import com.gomel.tat.home11.lib.mail.screen.SentPage;
import com.gomel.tat.home11.lib.mail.screen.TrashPage;
import com.gomel.tat.home11.lib.mail.service.LetterService;
import com.gomel.tat.home11.lib.mail.service.LoginService;
import com.gomel.tat.home11.lib.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(TestListener.class)
public class MailTests {

    @Test(description = "Sent a letter and check that it appears in Sent and Inbox folders")
    public void testSendMailPositive() {

        Letter letter = LetterBuilder.getTestLetter();
        LetterService letterService = new LetterService();
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isLetterInFolder(new SentPage(), letter));
        Assert.assertTrue(letterService.isLetterInFolder(new InboxPage(), letter));
    }

    @Test(description = "Sent a letter with invalid address and check that Error Message appears")
    public void testSendMailNegative() {

        Letter letter = LetterBuilder.getLetterWithWrongAddress();
        LetterService letterService = new LetterService();
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isErrorWhileSend());
        letterService.cancelCompose();
    }

    @Test(description = "Sent a letter without subject and check that it appears in Sent and Inbox folders")
    public void testSendMailWithoutSubject() {
        Letter letter = LetterBuilder.getLetterWithoutSubj();
        LetterService letterService = new LetterService();
        letter.setMessageID(letterService.sendLetterReturnId(letter));
        Assert.assertTrue(letterService.isLetterInFolderWithId(new SentPage(), letter));
        Assert.assertTrue(letterService.isLetterInFolderWithId(new InboxPage(), letter));
    }

    @Test(description = "Sent a letter without content and check that it appears in Sent and Inbox folders")
    public void testSendMailWithoutContent() {
        Letter letter = LetterBuilder.getLetterWithoutBody();
        LetterService letterService = new LetterService();
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isLetterInFolder(new SentPage(), letter));
        Assert.assertTrue(letterService.isLetterInFolder(new InboxPage(), letter));
    }

    @Test(description = "Create a draft, check that it appears in Draft folder, delete it check that it appears in Trash folder")
    public void testCreateAndDeleteDraft() {
        Letter letter = LetterBuilder.getTestLetter();
        LetterService letterService = new LetterService();
        letterService.createDraft(letter);
        Assert.assertTrue(letterService.isLetterInFolder(new DraftPage(), letter));
        letterService.deleteLetter(new DraftPage(), letter);
        Assert.assertTrue(letterService.isLetterInFolder(new TrashPage(), letter));
    }

    @Test(description = "Create a draft, delete it from Draft folder, delete it from Trash folder and check that it disappears in Trash folder")
    public void testPermanentDeleteDraft() {
        Letter letter = LetterBuilder.getTestLetter();
        LetterService letterService = new LetterService();
        letterService.createDraft(letter);
        letterService.deleteLetter(new DraftPage(), letter);
        letterService.deleteLetter(new TrashPage(), letter);
        Assert.assertTrue(!letterService.isLetterInFolder(new TrashPage(), letter));
    }

    @BeforeClass
    public void prepeareForTests() {
        Browser.rise();
        LoginService loginService = new LoginService();
        loginService.loginToMailBox();
    }

    @AfterClass
    public void closeWebdriver() {
        Browser.current().quit();
    }


}
