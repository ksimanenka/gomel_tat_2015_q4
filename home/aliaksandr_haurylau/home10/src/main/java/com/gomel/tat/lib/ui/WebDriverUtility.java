package com.gomel.tat.lib.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class WebDriverUtility {

    public static final String DOWNLOAD_DIR = ".";
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 10;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 4;

    public static WebDriver driver;

    public static void firefoxLocalLaunch() {
        driver = new FirefoxDriver();
    }

    public static void chromeLocalLaunch() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
    }

    public static WebDriver firefoxRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444

        standAloneServerLaunch("FF");
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", DOWNLOAD_DIR);
        profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.useDownloadDir", true);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        profile.setPreference("browser.privatebrowsing.dont_prompt_on_enter", true);
        profile.setPreference("startup.homepage_welcome_url", "");
        profile.setPreference("startup.homepage_welcome_url.additional", "");
        profile.setEnableNativeEvents(true);
        DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
        firefoxCapabilities.setCapability("firefox_profile", profile);

        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), firefoxCapabilities);
        return driver;
    }

    public static WebDriver chromeRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444 -Dwebdriver.chrome.driver="d:\path\to\chromedriver.exe"

        standAloneServerLaunch("CR");
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome");
        capability.setPlatform(Platform.LINUX);
        capability.setVersion("3.6");
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.prompt_for_download", "false");
        prefs.put("download.default_directory", DOWNLOAD_DIR);
        options.setExperimentalOption("prefs", prefs);
        capability.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capability);
        return driver;
    }

    public static void standAloneServerLaunch(String browser) {
        String[] cmd;
        if (browser == "FF") {
            cmd = new String[]{"java", "-jar", "selenium-server-standalone-2.48.2.jar", "-port4444"};
        } else if (browser == "CR") {
            cmd = new String[]{"java", "-jar", "selenium-server-standalone-2.48.2.jar", "-port4444", "-Dwebdriver.chrome.driver=chromedriver"};
        } else {
            return;
        }
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {
            System.err.println("Problem with launching standalone-server: " + e.getMessage());
        }
    }

    public static WebDriver getWebDriver(String browser) {
        if (driver == null) {
            standAloneServerLaunch(browser); // "FF" for Firefox, "CR" for Chrome
            WebDriver driver = null;
            try {
                if ("CR" == browser) {
                    driver = chromeRemoteLaunch();
                } else {
                    driver = firefoxRemoteLaunch();
                }
            } catch (MalformedURLException e) {
                System.err.println("Problem with launching driver: " + e.getMessage());
            }
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        }
    }

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForElementIsDisplayed(By locator) {
        new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

}