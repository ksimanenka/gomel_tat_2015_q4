package com.gomel.tat.lib.mail.service;

import com.gomel.tat.lib.common.Account;
import com.gomel.tat.lib.common.AccountBuilder;
import com.gomel.tat.lib.mail.screen.MailLoginPage;
import org.openqa.selenium.WebDriver;

public class LoginService {

    private WebDriver driver;

    public LoginService(WebDriver driver) {
        this.driver = driver;
    }

    public void loginToMailBox() {
        Account account = AccountBuilder.getDefaultAccount();
        MailLoginPage page = new MailLoginPage(driver);
        page.login(account);
    }

    public boolean checkLoginSuccessfull(Account account) {
        MailLoginPage page = new MailLoginPage(driver);
        page.login(account);
        return page.isLoginSuccessful();
    }


    public boolean checkErrorWithWrongLogin(Account account) {
        MailLoginPage page = new MailLoginPage(driver);
        page.login(account);
        return page.isLoginError();
    }

}
