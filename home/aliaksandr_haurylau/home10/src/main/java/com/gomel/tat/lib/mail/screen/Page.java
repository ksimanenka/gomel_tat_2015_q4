package com.gomel.tat.lib.mail.screen;


import com.gomel.tat.lib.common.CommonConstants;
import com.gomel.tat.lib.mail.Letter;
import com.gomel.tat.lib.ui.WebDriverUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

import java.util.List;

public abstract class Page {


    protected LettersManageForm lettersManageForm;
    protected FolderSwitchForm folderSwitchForm;

    protected WebDriver driver;

    public Page(WebDriver driver) {
        HtmlElementLoader.populatePageObject(this, driver);
        this.driver = driver;
    }

    public void compose() {
        lettersManageForm.compose();
    }

    public void gotoInbox() {
        folderSwitchForm.toInbox();
    }

    public void gotoSent() {
        folderSwitchForm.toSent();
    }

    public void gotoDraft() {
        folderSwitchForm.toDraft();
    }

    public void gotoTrash() {
        folderSwitchForm.toTrash();
    }

    public boolean isLetterPresent(Letter letter) {

        List<WebElement> search = driver.findElements(By.xpath(String.format(CommonConstants.MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
        int i = search.size();
        if (0 == i) return false;
        for (WebElement elementN : search) if (elementN.isDisplayed()) return true;
        return false;
    }

    public boolean isLetterIDPresent(Letter letter) {

        List<WebElement> search = driver.findElements(By.xpath("//a[contains(@href,'" + letter.getMessageID() + "')]"));
        int i = search.size();
        if (0 == i) return false;
        for (WebElement elementN : search) if (elementN.isDisplayed()) return true;
        return false;
    }


    public void deleteLetterBySubj(Letter letter) {

        WebElement thisLetter = driver.findElement(By.xpath(".//*[@title='" + letter.getSubject() + "']"));
        thisLetter.click();
        WebDriverUtility.waitForElementIsClickable(CommonConstants.DELETE_MAIL_BUTTON_LOCATOR);
        WebElement deleteButton = driver.findElement(CommonConstants.DELETE_MAIL_BUTTON_LOCATOR);
        deleteButton.click();
        WebDriverUtility.waitForElementIsDisplayed(By.xpath("//span[contains(text(),'1 сообщение удалено.')]"));
    }

}
