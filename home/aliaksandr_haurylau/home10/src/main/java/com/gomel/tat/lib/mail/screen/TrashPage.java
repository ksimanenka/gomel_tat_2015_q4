package com.gomel.tat.lib.mail.screen;

import com.gomel.tat.lib.common.CommonConstants;
import com.gomel.tat.lib.mail.Letter;
import com.gomel.tat.lib.ui.WebDriverUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TrashPage extends Page {

    public TrashPage(WebDriver driver) {
        super(driver);
        this.gotoTrash();
    }

    public void deleteLetterBySubj(Letter letter) {

        WebElement thisLetter = driver.findElement(By.xpath(".//*[@title='" + letter.getSubject() + "']"));
        thisLetter.click();
        WebDriverUtility.waitForElementIsClickable(CommonConstants.DELETE_PERMANENT_BUTTON_LOCATOR);
        WebElement deleteButton = driver.findElement(CommonConstants.DELETE_PERMANENT_BUTTON_LOCATOR);
        deleteButton.click();
        WebDriverUtility.waitForElementIsDisplayed(By.xpath("//span[contains(text(),'1 сообщение удалено.')]"));
    }


}