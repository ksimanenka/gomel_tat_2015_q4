package com.gomel.tat.lib.mail.screen;

import com.gomel.tat.lib.common.CommonConstants;
import com.gomel.tat.lib.mail.Letter;
import com.gomel.tat.lib.ui.WebDriverUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ComposePage extends Page {
    private ComposeForm composeForm;

    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final By SAVE_DIALOG_POPUP_LOCATOR = By.xpath(".//p[text()='Сохранить сделанные изменения?']");
    public static final By SAVE_DIALOG_SAVE_BUTTON_LOCATOR = By.xpath(".//*[@data-action='dialog.save']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");


    public ComposePage(WebDriver driver) {
        super(driver);
        this.compose();
    }

    public void sendLetter(Letter letter) {
        composeForm.send(letter);
        if (!isErrorSendingLetter()) WebDriverUtility.waitForDisappear(CommonConstants.SEND_MAIL_BUTTON_LOCATOR);
    }


    public String sendLetterReturnId(Letter letter) {
        String messageID[], href;
        composeForm.send(letter);
        WebDriverUtility.waitForElementIsDisplayed(CommonConstants.YOU_HAVE_GOT_MAIL_MESSAGE);
        WebElement recievedMessage = driver.findElement(By.xpath("//a[contains(text(),'от Александр Г.')]"));
        href = recievedMessage.getAttribute("href");
        messageID = href.split("#message/");
        return messageID[1];
    }


    public void cancelCompose() {
        composeForm.cancelCompose();
        WebDriverUtility.waitForElementIsDisplayed(CommonConstants.SAVE_DIALOG_POPUP_LOCATOR);
        WebElement cancelButton = driver.findElement(CommonConstants.SAVE_DIALOG_CANCEL_BUTTON_LOCATOR);
        cancelButton.click();
    }

    public void createDraft(Letter letter) {
        composeForm.createDraft(letter);

        WebElement draftLink = driver.findElement(DRAFT_LINK_LOCATOR);
        draftLink.click();
        WebDriverUtility.waitForElementIsDisplayed(SAVE_DIALOG_POPUP_LOCATOR);
        WebElement saveButton = driver.findElement(SAVE_DIALOG_SAVE_BUTTON_LOCATOR);
        saveButton.click();
        WebDriverUtility.waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

    public boolean isErrorSendingLetter() {

        List<WebElement> search = driver.findElements(By.xpath("//*[contains(text(),'Некорректный адрес электронной почты')]"));
        int i = search.size();
        if (0 == i) return false; //if found nothing
        for (WebElement elementN : search) if (elementN.isDisplayed()) return true; //if found displayed element
        return false; //if no displayed elements
    }


}
