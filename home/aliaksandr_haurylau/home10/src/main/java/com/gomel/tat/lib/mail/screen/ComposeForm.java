package com.gomel.tat.lib.mail.screen;

import com.gomel.tat.lib.mail.Letter;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;


@FindBy(xpath = "//*[@class='compose-form']")
public class ComposeForm extends HtmlElement {
    @FindBy(xpath = "//*[@data-params='field=to']//ancestor::tr//input[@type='text']")
    WebElement mailTo;

    @FindBy(id = "compose-subj")
    WebElement subject;

    @FindBy(id = "compose-send")
    WebElement content;

    @FindBy(id = "compose-submit")
    WebElement submitButton;

    @FindBy(xpath = ".//*[@data-action='compose.close']")
    WebElement cancelButton;

    public void send(Letter letter) {
        mailTo.sendKeys(letter.getMailto());
        subject.sendKeys(letter.getSubject());
        content.sendKeys(letter.getBody());
        submitButton.click();
    }

    public void createDraft(Letter letter) {
        mailTo.sendKeys(letter.getMailto());
        subject.sendKeys(letter.getSubject());
        content.sendKeys(letter.getBody());
    }

    public void cancelCompose()
    {
        cancelButton.click();
     }



    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }
}