package com.gomel.tat.lib.mail.screen;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;


@FindBy(xpath = "//*[@class='b-toolbar__block b-toolbar__block_chevron']")
public class LettersManageForm extends HtmlElement {
    @FindBy(css = "[href='#compose']")
    WebElement composeButton;

    @FindBy(xpath = "//a[contains(@class,'__item_delete')]")
    WebElement deleteButton;

    @FindBy(xpath = "//a[contains(@class,'__item_forward')]")
    WebElement forwardButton;

    public void compose() {
        composeButton.click();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }
}