package com.gomel.tat.lib.mail.service;

import com.gomel.tat.lib.mail.Letter;
import com.gomel.tat.lib.mail.screen.ComposePage;
import com.gomel.tat.lib.mail.screen.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LetterService {
    protected WebDriver driver;
    private ComposePage composePage;


    public LetterService(WebDriver driver) {
        this.driver = driver;
    }

    public void sendLetter(Letter letter) {
        composePage = new ComposePage(driver);
        composePage.sendLetter(letter);
        new WebDriverWait(driver, 5, 1000);
    }

    public String sendLetterReturnId(Letter letter) {
        composePage = new ComposePage(driver);
        return composePage.sendLetterReturnId(letter);
    }


    public void createDraft(Letter letter) {
        composePage = new ComposePage(driver);
        composePage.createDraft(letter);
    }

    public void deleteLetter(Page page, Letter letter) {
        page.deleteLetterBySubj(letter);
        new WebDriverWait(driver, 6, 1000);
    }

    public void cancelCompose() {
        composePage.cancelCompose();
    }

    public boolean isLetterInFolder(Page page, Letter letter) {
        return page.isLetterPresent(letter);
    }

    public boolean isLetterInFolderWithId(Page page, Letter letter) {
        return page.isLetterIDPresent(letter);
    }

    public boolean isErrorWhileSend() {
        return composePage.isErrorSendingLetter();
    }

}
