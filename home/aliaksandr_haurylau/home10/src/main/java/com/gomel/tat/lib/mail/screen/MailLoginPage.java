package com.gomel.tat.lib.mail.screen;


import com.gomel.tat.lib.common.Account;
import com.gomel.tat.lib.common.CommonConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

public class MailLoginPage {
    private AutorisationForm autorisationForm;
    private WebDriver driver;

    public static final By ERROR_ON_LOGIN_LOCATOR = By.xpath("//*[ @class='error-msg']");


    public MailLoginPage(WebDriver driver) {
        driver.get(CommonConstants.MAILBOX_URL);
        HtmlElementLoader.populatePageObject(this, driver);
        this.driver = driver;
    }

    public void login(Account account) {
        autorisationForm.login(account.getLogin(), account.getPassword());
    }

    public boolean isLoginSuccessful() {
        return !(0 == driver.findElements(CommonConstants.INBOX_LINK_LOCATOR).size());
    }

    public boolean isLoginError() {
        return !(0 == driver.findElements(ERROR_ON_LOGIN_LOCATOR).size());
    }

}
