package com.gomel.tat.tests.mail;

import com.gomel.tat.lib.mail.Letter;
import com.gomel.tat.lib.mail.LetterBuilder;
import com.gomel.tat.lib.mail.screen.DraftPage;
import com.gomel.tat.lib.mail.screen.InboxPage;
import com.gomel.tat.lib.mail.screen.SentPage;
import com.gomel.tat.lib.mail.screen.TrashPage;
import com.gomel.tat.lib.mail.service.LetterService;
import com.gomel.tat.lib.mail.service.LoginService;
import com.gomel.tat.lib.ui.WebDriverUtility;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class MailTests {

    protected static WebDriver driver;


    @Test
    public void testSendMailPositive() {

        Letter letter = LetterBuilder.getTestLetter();
        LetterService letterService = new LetterService(driver);
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isLetterInFolder(new SentPage(driver), letter));
        Assert.assertTrue(letterService.isLetterInFolder(new InboxPage(driver), letter));
    }

    @Test
    public void testSendMailNegative() {
        Letter letter = LetterBuilder.getLetterWithWrongAddress();
        LetterService letterService = new LetterService(driver);
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isErrorWhileSend());
        letterService.cancelCompose();
    }

    @Test
    public void testSendMailWithoutSubject() {
        Letter letter = LetterBuilder.getLetterWithoutSubj();
        LetterService letterService = new LetterService(driver);
        letter.setMessageID(letterService.sendLetterReturnId(letter));
        Assert.assertTrue(letterService.isLetterInFolderWithId(new SentPage(driver), letter));
        Assert.assertTrue(letterService.isLetterInFolderWithId(new InboxPage(driver), letter));
    }

    @Test
    public void testSendMailWithoutContent() {
        Letter letter = LetterBuilder.getLetterWithoutBody();
        LetterService letterService = new LetterService(driver);
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isLetterInFolder(new SentPage(driver), letter));
        Assert.assertTrue(letterService.isLetterInFolder(new InboxPage(driver), letter));
    }

    @Test
    public void testCreateAndDeleteDraft() {
        Letter letter = LetterBuilder.getTestLetter();
        LetterService letterService = new LetterService(driver);
        letterService.createDraft(letter);
        Assert.assertTrue(letterService.isLetterInFolder(new DraftPage(driver), letter));
        letterService.deleteLetter(new DraftPage(driver), letter);
        Assert.assertTrue(letterService.isLetterInFolder(new TrashPage(driver), letter));
    }

    @Test
    public void testPermanentDeleteDraft() {
        Letter letter = LetterBuilder.getTestLetter();
        LetterService letterService = new LetterService(driver);
        letterService.createDraft(letter);
        letterService.deleteLetter(new DraftPage(driver), letter);
        letterService.deleteLetter(new TrashPage(driver), letter);
        Assert.assertTrue(!letterService.isLetterInFolder(new TrashPage(driver), letter));
    }

    @BeforeClass
    public void prepeareForTests() {
        driver = WebDriverUtility.getWebDriver("CR");
        LoginService loginService = new LoginService(driver);
        loginService.loginToMailBox();
    }

    @AfterClass
    public void closeWebdriver() {
        WebDriverUtility.shutdownWebDriver();
    }


}
