package com.gomel.tat.tests.mail;

import com.gomel.tat.lib.common.Account;
import com.gomel.tat.lib.common.AccountBuilder;
import com.gomel.tat.lib.mail.service.LoginService;
import com.gomel.tat.lib.ui.WebDriverUtility;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class LoginTests {

    protected static WebDriver driver;


    @Test
    public void testLoginNegative() {
        Account account = AccountBuilder.getAccountWithInvalidPassword();
        LoginService loginService = new LoginService(driver);
        Assert.assertTrue(loginService.checkErrorWithWrongLogin(account));
    }

    @Test
    public void testLoginPositive() {
        Account account = AccountBuilder.getDefaultAccount();
        LoginService loginService = new LoginService(driver);
        Assert.assertTrue(loginService.checkLoginSuccessfull(account));
    }

    @BeforeClass
    public void prepeareForTests() {
        driver = WebDriverUtility.getWebDriver("CR");
    }

    @AfterClass
    public void closeWebdriver() {
        WebDriverUtility.shutdownWebDriver();
    }

}
