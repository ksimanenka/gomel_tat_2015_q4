package com.gomel.tat.lib.mail.screen;

import org.openqa.selenium.WebDriver;

public class DraftPage extends Page {

    public DraftPage(WebDriver driver) {
        super(driver);
        this.gotoDraft();
    }
}