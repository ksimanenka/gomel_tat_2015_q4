package com.gomel.tat.lib.mail.screen;

import org.openqa.selenium.WebDriver;


public class SentPage extends Page{

    public SentPage(WebDriver driver) {
        super(driver);
        this.gotoSent();
    }
}