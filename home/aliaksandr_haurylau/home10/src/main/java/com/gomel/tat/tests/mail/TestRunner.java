package com.gomel.tat.tests.mail;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import java.util.ArrayList;
import java.util.List;

   public class TestRunner {

        public static void main(String[] args) {

            TestNG testNG = new TestNG();
            XmlSuite suite = new XmlSuite();
            suite.setName("RefactoredTests");
            List<String> files = new ArrayList<>();
            files.addAll(new ArrayList<String>() {{
                add("./src/main/java/com/gomel/tat/tests/suites/Tests1.xml");
                add("./src/main/java/com/gomel/tat/tests/suites/Tests2.xml");
            }});
            suite.setSuiteFiles(files);
            suite.setThreadCount(5);
            List<XmlSuite> suites = new ArrayList<>();
            suites.add(suite);
            testNG.setXmlSuites(suites);
            testNG.run();

        }

    }

