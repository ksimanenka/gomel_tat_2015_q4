package com.gomel.tat.tests;

import org.testng.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class MyTestListener extends  TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult result) {
        String message[]=new String[3];

        message[0]="Test failed: " + result.getName()+"\n";
        message[1]="Message is: " + result.getThrowable().getMessage()+"\n";
        message[2]="Stack trace is: " + result.getThrowable().getStackTrace()+"\n";

        File file = new File("TestFail.txt");
        try {
              if(!file.exists()){
               file.createNewFile();
            }
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            try {
                out.print(message[0]+message[1]+message[2]);
            } finally {
                out.close();
            }
        } catch(IOException e) {
            }

    }










}