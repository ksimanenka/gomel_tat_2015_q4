package com.gomel.tat.home5;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.*;
import java.nio.file.*;
import org.mozilla.universalchardet.UniversalDetector;
import java.io.FileInputStream;

public class WordList {

    public Map<String, Integer> values = new HashMap<String, Integer>();

    public static void main(String[] args) {

        String baseDirectory = "/home/agv/workspace/TAT5/gomel_tat_2015_q4/home/";
        String fileNamePattern = "*hello.txt";

        WordList newWordList = new WordList();
      //  newWordList.getFilesFromRepository(); For running tests in test fixture
        newWordList.getWords(baseDirectory, fileNamePattern);
        newWordList.printStatistics();

    }


    public void getWords(String baseDirectory, String fileNamePattern) {

        List<File> Myfiles = this.getListOfFiles(baseDirectory, fileNamePattern);
        for (File file : Myfiles) {
            getWordsFromFile(file,getFileEncodingType(file.getPath()));
        }

    }

    public void printStatistics() {

        for (Map.Entry entry : values.entrySet()) {
            System.out.println("Word: '" + entry.getKey() + "'  has inclusions: " + entry.getValue());
        }
    }


    public List<File> getListOfFiles(String directory, String filePattern) {

        List<File> selectedFiles = new ArrayList<File>();
        File dir;
        dir = new File(directory);
        if (!dir.exists()||!dir.isDirectory()) {
            System.out.println("Invalid baseDirectory "+directory);
            return selectedFiles;
        }
        IOFileFilter fileFilter = new WildcardFileFilter("*.*"); //getting all file to check match with pattern
        IOFileFilter dirFilter = TrueFileFilter.TRUE; // including subdirectories
        Collection<java.io.File> files = FileUtils.listFiles(dir, fileFilter, dirFilter);
        FileSystem fileSystem = FileSystems.getDefault();
        String globeFilePattern = "regex:" + filePattern.replace("*", ".*");
        PathMatcher pathMatcher;
        pathMatcher = fileSystem.getPathMatcher(globeFilePattern);
        for (File file : files) {
            Path path = file.toPath();
            if (pathMatcher.matches(path)) {
                selectedFiles.add(file);
            }
        }
        return selectedFiles;
    }

    public void getWordsFromFile(File file, String encoding) {
        List<String> lines = new ArrayList<String>();
        if (!file.exists()) {return; }
        Charset charset = Charset.forName(encoding);
        try {
            lines = Files.readAllLines(Paths.get(file.toURI()),charset);
        }
        catch (IOException e)
        {
            System.out.println("File read Error");
             e.printStackTrace();
        }

        for (String line : lines) {
            line = line.replaceAll("[^а-яА-Яa-zA-Z]", " ");
            String[] words = line.split(" ");
            for (String word : words) {
                if (!word.trim().isEmpty()) {
                    if (this.values.containsKey(word)) {
                        int count = this.values.get(word);
                        this.values.put(word, count + 1);
                    } else {
                        this.values.put(word, 1);
                    }
                }
            }
        }
    }

    public String getFileEncodingType(String filePath) {
        byte[] buf = new byte[3];
        UniversalDetector detector = new UniversalDetector(null);
        int bufNum;
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(filePath);
            while ((bufNum = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, bufNum);
            }
        } catch (IOException e) {
             //e.printStackTrace();
        }

        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        detector.reset();
        if (encoding==null) encoding="UTF8";
        return encoding;
    }


    private void getFilesFromRepository() {

        String osName = System.getProperty("os.name" ).toLowerCase();
        String[] cmd1= new String[]{""};
        String[] cmd2= new String[]{""};

        if (osName.contains("nux")) {
            cmd1 = new String[]{"git", "init"};
            cmd2 = new String[]{"git", "clone", "https://AlexAGV@bitbucket.org/AlexAGV/gomel_tat_2015_q4.git"};
        } /*else {
            if (osName.contains("win")) {
                cmd1 = new String[]{"cmd.exe", "/C", "git init"};
                cmd2 = new String[]{"cmd.exe", "/C", "git clone https://AlexAGV@bitbucket.org/AlexAGV/gomel_tat_2015_q4.git"};
            }*/ else {
                System.out.println("Sorry, unsupported OS");
                return;
            }

        try {
            Runtime.getRuntime().exec(cmd1);
            Process p = Runtime.getRuntime().exec(cmd2);
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}