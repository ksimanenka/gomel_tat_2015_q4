package com.gomel.tat.home5.test.testng;

import com.gomel.tat.home5.WordList;
import com.gomel.tat.home5.test.testng.listeners.CustomListener;
import org.testng.annotations.*;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.List;
@Listeners (CustomListener.class)
public class BaseWordListTest {

    protected WordList wordList;
    protected Collection<File> files;
    protected List<String> values;
//    protected String files_content;

    @BeforeSuite
    public void bs() {
        System.out.println("bs");
    }

    @BeforeClass
    public void bc() { System.out.println("bc"); }

    @BeforeMethod
    public void bm() {
        System.out.println("bm");
        files = wordList.getFileList("d:\\EPAM\\Automation\\dev\\GOMEL_TAT_2015_Q4", "pavel_*home3\\hello.txt");
        values = wordList.getWords("d:\\EPAM\\Automation\\dev\\GOMEL_TAT_2015_Q4", "konstantsin_simanenka\\*.txt");
//        files_content = wordList.filesToString(files);
    }

    @BeforeGroups(value = "a")
    public void bg() {
        System.out.println("bg");
    }

    @BeforeClass(groups = "a")
    public void setUp() {
        wordList = new WordList();
        System.out.println("Config1");
    }

    @AfterMethod
    public void am() {
        System.out.println("am");
    }

    @AfterSuite
    public void as() {
        System.out.println("as");
    }

    @AfterClass
    public void ac() {
        System.out.println("ac");
    }

    protected void checkTime() {
        System.out.println("Current time: " + new Date(System.currentTimeMillis()));
    }
}
