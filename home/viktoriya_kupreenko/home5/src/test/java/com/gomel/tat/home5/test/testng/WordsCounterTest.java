package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class WordsCounterTest extends BaseWordListTest {
    private List<String> values;
    private HashMap<String, Integer> expectedLibrary;

    @Factory(dataProvider = "dataProviderWordsCounter")
    public WordsCounterTest(List<String> values, HashMap<String, Integer> expectedLibrary) {
        this.values = values;
        this.expectedLibrary = expectedLibrary;
    }

    @Test
    public void wordsCounter() {
        HashMap<String, Integer> library = wordList.wordsCounter(values);
        Assert.assertEquals(library, expectedLibrary, "Invalid result");
    }

    @DataProvider(name = "dataProviderWordsCounter")
    public static Object[][] dataProvider() {
        List<String> values1 = Arrays.asList("слов", "слов", "список");
        List<String> values2 = Arrays.asList("cat", "dog", "pig", "dog");
        List<String> values3 = Arrays.asList("имя", "name");
        HashMap<String, Integer> expectedLibrary1 = new HashMap<String, Integer>();
        expectedLibrary1.put("слов", 2);
        expectedLibrary1.put("список", 1);
        HashMap<String, Integer> expectedLibrary2 = new HashMap<String, Integer>();
        expectedLibrary2.put("cat", 1);
        expectedLibrary2.put("dog", 2);
        expectedLibrary2.put("pig", 1);
        HashMap<String, Integer> expectedLibrary3 = new HashMap<String, Integer>();
        expectedLibrary3.put("имя", 1);
        expectedLibrary3.put("name", 1);
        return new Object[][]{
                {values1, expectedLibrary1},
                {values2, expectedLibrary2},
                {values3, expectedLibrary3}
        };
    }
}
