package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

public class WordsInFileContentTest extends BaseWordListTest {
    @Test(groups = "get words")
    public void wordsInFileContent() {
        String[] file_content_arr = wordList.wordsInFileContent("список слов 1");
        String[] expectedArray = {"список", "слов"};
        Assert.assertEquals(file_content_arr, expectedArray, "Invalid result array");
    }
}
