package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

public class GetFileListTest extends BaseWordListTest {
    @Test(groups = "get words")
    public void getFileList() {
        Collection<File> file = wordList.getFileList("D:\\EPAM\\Automation\\dev\\GOMEL_TAT_2015_Q4\\home\\aleh_kurnitsou\\home3", "hello.txt");
        Collection<File> expectedFile = new ArrayList<File>();
        expectedFile.add(new File("D:\\EPAM\\Automation\\dev\\GOMEL_TAT_2015_Q4\\home\\aleh_kurnitsou\\home3\\hello.txt"));
        Assert.assertEquals(file, expectedFile, "Invalid file");
    }
}

