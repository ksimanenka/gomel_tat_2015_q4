package com.epam.gomel.tat2015.home10.tests.mail;


import com.epam.gomel.tat2015.home10.lib.common.Account;
import com.epam.gomel.tat2015.home10.lib.common.AccountBuilder;
import com.epam.gomel.tat2015.home10.lib.mail.Letter;
import com.epam.gomel.tat2015.home10.lib.mail.LetterBuilder;
import com.epam.gomel.tat2015.home10.lib.mail.service.MailService;
import com.epam.gomel.tat2015.home10.lib.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendMailTest extends BaseMailTest {

    LoginService loginService = new LoginService();
    MailService mailServise = new MailService();

    @Test
    public void sendMailTest() {
        Letter letter = LetterBuilder.buildLetter();
        Account account = AccountBuilder.getDefaultAccount();
        loginService.loginToMailbox(account);
        mailServise.sendLetter(account, letter);
        Assert.assertTrue(mailServise.isLetterExistInInbox(account, letter));
    }
}
