package com.epam.gomel.tat2015.home10.tests.mail;

import com.epam.gomel.tat2015.home10.lib.common.Account;
import com.epam.gomel.tat2015.home10.lib.common.AccountBuilder;
import com.epam.gomel.tat2015.home10.lib.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseMailTest {

    LoginService loginService = new LoginService();

    @Test
    public void loginSuccessTest() {
        Account account = AccountBuilder.getDefaultAccount();
        loginService.loginToMailbox(account);
        Assert.assertTrue(loginService.checkSuccessLogin(), "Login was not success");
    }

    @Test
    public void loginFailedTest() {
        Account account = AccountBuilder.getAccountWithWrongPass();
        loginService.loginToMailbox(account);
        Assert.assertTrue(loginService.checkErrorOnFailedLogin(), "Login was not failed");
    }
}


