package com.epam.gomel.tat2015.home10.lib.mail;

import static com.epam.gomel.tat2015.home10.lib.common.CommonConstants.*;

public class LetterBuilder {

    public static Letter buildLetter() {
        Letter letter = new Letter();
        letter.setRecipient(DEFAULT_MAIL_USER_LOGIN);
        letter.setSubject(emailSubject);
        letter.setBody(emailBodyText);
        return letter;
    }

}
