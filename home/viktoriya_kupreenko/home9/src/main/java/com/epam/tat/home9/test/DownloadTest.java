package com.epam.tat.home9.test;

import com.epam.tat.home9.ui.page.yandexdisk.DiskPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.epam.tat.home9.bo.FileFactory.*;

public class DownloadTest extends UploadTest {

    @Test(priority = 3)
    public void testDownload() throws InterruptedException, IOException {
        DiskPage diskPage = new DiskPage(driver);
        diskPage.selectFile();
        diskPage.downloadFile();
        Thread.sleep(2000);
        String actualContent = diskPage.checkContent();
        Assert.assertEquals(actualContent, FILE_CONTENT, "File does not have expected content");

    }
}
