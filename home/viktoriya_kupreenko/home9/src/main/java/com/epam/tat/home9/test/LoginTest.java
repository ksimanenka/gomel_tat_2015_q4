package com.epam.tat.home9.test;

import com.epam.tat.home9.ui.page.yandexdisk.LoginPage;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    private String userLogin = "tat-test-user@yandex.ru"; // ACCOUNT
    private String userPassword = "tat-123qwe"; // ACCOUNT

    @Test(priority = 0)
    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin, userPassword);

    }

}


