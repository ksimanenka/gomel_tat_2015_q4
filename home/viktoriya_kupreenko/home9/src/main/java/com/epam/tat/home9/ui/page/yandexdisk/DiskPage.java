package com.epam.tat.home9.ui.page.yandexdisk;

import com.epam.tat.home9.ui.page.Page;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

import static com.epam.tat.home9.bo.FileFactory.*;
import static com.epam.tat.home9.util.WebDriverHelper.waitForElementIsClickable;


public class DiskPage extends Page {

    public static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//input[@class='button__attach']");
    public static final By CLOSE_UPLOAD_WINDOW_LINK_LOCATOR = By.xpath("//a[@data-click-action='dialog.close']");
    public static final By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='resource.download']");
    public static final By TRASH_BOX_LOCATOR = By.xpath(" //div[@data-id='/trash']/div");
    public static final By YANDEX_DISK_LINK_LOCATOR = By.xpath("//a[@class='b-crumbs__root _link ns-action']");
    public static final String FILE_LOCATOR_PATTERN = " //div[@data-id='/disk/%s']";

    public DiskPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.findElement(YANDEX_DISK_LINK_LOCATOR).click();
    }

    public void uploadFile() throws IOException {
        createFile();
        driver.findElement(UPLOAD_BUTTON_LOCATOR).sendKeys(FILE_PATH_UPLOAD + FILE_NAME);
        waitForElementIsClickable(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME)));
    }

    public void uploadSeveralFiles() throws IOException {
        createFiles();
        driver.findElement(UPLOAD_BUTTON_LOCATOR).sendKeys(FILE_PATH_UPLOAD + FILE_NAME1);
        driver.findElement(UPLOAD_BUTTON_LOCATOR).sendKeys(FILE_PATH_UPLOAD + FILE_NAME2);
        driver.findElement(UPLOAD_BUTTON_LOCATOR).sendKeys(FILE_PATH_UPLOAD + FILE_NAME3);
        waitForElementIsClickable(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME1)));
        waitForElementIsClickable(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME2)));
        waitForElementIsClickable(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME3)));
    }

    public void closeUploadWindow() {
        driver.findElement(CLOSE_UPLOAD_WINDOW_LINK_LOCATOR).click();
    }

    public boolean isFilePresent() {
        try {
            new WebDriverWait(driver, TIME_OUT_FILE_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME))));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public void selectFile() {
        driver.findElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME))).click();
    }

    public void selectSeveralElements() {
        WebElement file = driver.findElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME1)));
        WebElement file1 = driver.findElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME2)));
        WebElement file2 = driver.findElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME3)));
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.CONTROL)
                .click(file)
                .click(file1)
                .click(file2)
                .keyUp(Keys.CONTROL)
                .build()
                .perform();
    }

    public void downloadFile() {
        waitForElementIsClickable(DOWNLOAD_BUTTON_LOCATOR);
        driver.findElement(DOWNLOAD_BUTTON_LOCATOR).click();
    }

    public void removeFile() {
        WebElement file = driver.findElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME)));
        WebElement trashBox = driver.findElement(TRASH_BOX_LOCATOR);
        Action action = new Actions(driver).dragAndDrop(file, trashBox).build();
        action.perform();
    }

    public void removeFiles() {
        WebElement file = driver.findElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME1)));
        WebElement trashBox = driver.findElement(TRASH_BOX_LOCATOR);
        Action action = new Actions(driver).dragAndDrop(file, trashBox).build();
        action.perform();
    }

    public String checkContent() throws IOException {
        File file = new File(FILE_PATH_DOWNLOAD + FILE_NAME);
        String actualContent = FileUtils.readFileToString(file);
        return actualContent;
    }

}
