package com.epam.tat.home9.test;

import com.epam.tat.home9.ui.page.yandexdisk.DiskPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class UploadTest extends LoginTest {

    @Test(priority = 1)
    public void testUpload() throws InterruptedException, IOException {
        DiskPage diskPage = new DiskPage(driver);
        diskPage.uploadFile();
        diskPage.closeUploadWindow();
        Assert.assertTrue(diskPage.isFilePresent());
    }

}