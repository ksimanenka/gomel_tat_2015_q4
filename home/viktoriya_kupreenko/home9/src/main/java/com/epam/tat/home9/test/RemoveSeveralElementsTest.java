package com.epam.tat.home9.test;

import com.epam.tat.home9.ui.page.yandexdisk.DiskPage;
import com.epam.tat.home9.ui.page.yandexdisk.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class RemoveSeveralElementsTest extends LoginTest {

    @Test(priority = 1)
    public void testRemoveSeveralElements() throws InterruptedException, IOException {
        DiskPage diskPage = new DiskPage(driver);
        diskPage.uploadSeveralFiles();
        diskPage.closeUploadWindow();
        diskPage.selectSeveralElements();
        diskPage.removeFiles();
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        Assert.assertTrue(trashPage.areFilesPresent());
    }
}
