package com.epam.tat.home9.ui.page.yandexdisk;

import com.epam.tat.home9.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.tat.home9.bo.FileFactory.*;
import static com.epam.tat.home9.bo.FileFactory.FILE_NAME;
import static com.epam.tat.home9.util.WebDriverHelper.waitForDisappear;
import static com.epam.tat.home9.util.WebDriverHelper.waitForElementIsClickable;

public class TrashPage extends Page {

    public static final By TRASH_BOX_LOCATOR = By.xpath(" //div[@data-id='/trash']/div");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath(" //button[@data-click-action='resource.delete']");
    public static final By RESTORE_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='resource.restore']");
    public static final String TRASH_FILE_LOCATOR_PATTERN = " //div[@data-id='/trash/%s']";

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(TRASH_BOX_LOCATOR);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(TRASH_BOX_LOCATOR)).doubleClick().build().perform();
    }

    public boolean isFilePresent() {
        try {
            new WebDriverWait(driver, TIME_OUT_FILE_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, FILE_NAME))));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean isFilePresent(String FILE_NAME) {
        try {
            new WebDriverWait(driver, TIME_OUT_FILE_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, FILE_NAME))));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean areFilesPresent() {
        if (!isFilePresent(FILE_NAME1)) return false;
        if (!isFilePresent(FILE_NAME2)) return false;
        if (!isFilePresent(FILE_NAME3)) return false;
        return true;
    }

    public void selectFile() {
        waitForElementIsClickable(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, FILE_NAME)));
        driver.findElement(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, FILE_NAME))).click();
    }

    public void removePermanently() {
        waitForElementIsClickable(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, FILE_NAME)));
        driver.findElement(DELETE_BUTTON_LOCATOR).click();
        waitForDisappear(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, FILE_NAME)));
    }

    public void restore() {
        driver.findElement(RESTORE_BUTTON_LOCATOR).click();
    }

}
