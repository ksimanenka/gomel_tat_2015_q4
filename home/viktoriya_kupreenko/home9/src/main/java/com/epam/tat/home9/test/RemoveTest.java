package com.epam.tat.home9.test;

import com.epam.tat.home9.ui.page.yandexdisk.DiskPage;
import com.epam.tat.home9.ui.page.yandexdisk.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RemoveTest extends UploadTest {

    @Test(priority = 2)
    public void testRemove() {
        DiskPage diskPage = new DiskPage(driver);
        diskPage.removeFile();
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        Assert.assertTrue(trashPage.isFilePresent());
    }
}
