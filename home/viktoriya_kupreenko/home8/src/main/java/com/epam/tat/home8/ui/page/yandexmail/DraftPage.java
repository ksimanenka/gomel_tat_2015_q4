package com.epam.tat.home8.ui.page.yandexmail;

import com.epam.tat.home8.bo.Letter;
import com.epam.tat.home8.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import static com.epam.tat.home8.util.WebDriverHelper.*;

public class DraftPage extends Page {

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 15;
    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final String CHECKBOX_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]/ancestor::span/preceding-sibling::label/input";
    public static final By DELETE_BUTTON_LOCATOR = By.xpath(" //a[@data-action='delete']");

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
//        driver.get("https://mail.yandex.by/?#draft");
        waitForElementIsClickable(DRAFT_LINK_LOCATOR).click();
    }

    public void clickCheckbox(Letter letter) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);

        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(
                        By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, letter.getSubject()))));

        WebElement checkboxInput = driver.findElement(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, letter.getSubject())));
        waitForElementIsClickable(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, letter.getSubject())));
        checkboxInput.click();
    }

    public void deleteMail() {


        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(DELETE_BUTTON_LOCATOR));

        WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR);
        waitForElementIsClickable(DELETE_BUTTON_LOCATOR);
        deleteButton.click();

    }

    public boolean isLetterPresent(Letter letter) {
        try {
            waitForDisappear(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
            driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }
}
