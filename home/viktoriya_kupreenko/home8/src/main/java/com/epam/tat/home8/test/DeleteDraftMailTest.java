package com.epam.tat.home8.test;

import com.epam.tat.home8.bo.Letter;
import com.epam.tat.home8.bo.LetterFactory;
import com.epam.tat.home8.ui.page.yandexmail.*;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DeleteDraftMailTest extends LoginTest {
    private Letter letter;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test(description = "Create draft mail", priority = 1)
    public void draftMail() {

        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.createDraftMail(letter);
        Assert.assertEquals(composePage.savedInDraft(), true, "Mail was not saved in Draft");
    }

    @Test(description = "Delete draft mail", priority = 2)
    public void deleteMailFromDraft() {

        DraftPage draftPage = new DraftPage(driver);
        draftPage.open();
        draftPage.clickCheckbox(letter);
        draftPage.deleteMail();
        Assert.assertFalse(draftPage.isLetterPresent(letter), "Mail was not deleted from Draft");
    }

    @Test(description = "Delete draft mail", priority = 3)
    public void deleteMailFromTrash() {

        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        trashPage.clickCheckbox(letter);
        trashPage.deleteMail();
        Assert.assertFalse(trashPage.isLetterPresent(letter), "Mail was not deleted from Trash");
    }

}
