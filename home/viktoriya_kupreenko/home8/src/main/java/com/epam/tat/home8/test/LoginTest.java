package com.epam.tat.home8.test;

import com.epam.tat.home8.ui.page.yandexmail.LoginPage;
import org.testng.annotations.Test;

public class LoginTest extends BaseYandexMailTest {

    private String userLogin = "tat-test-user@yandex.ru"; // ACCOUNT
    private String userPassword = "tat-123qwe"; // ACCOUNT

    @Test(priority = 0)
    public void login() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin, userPassword);
    }

}


