package com.epam.tat.home8.test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import static com.epam.tat.home8.util.WebDriverHelper.getWebDriver;
import static com.epam.tat.home8.util.WebDriverHelper.shutdownWebDriver;

public class BaseYandexMailTest {
    protected WebDriver driver;

    @BeforeClass
    public void prepareBrowser() {
        driver = getWebDriver();
    }

    @AfterClass
    public void shutdown() {
        shutdownWebDriver();
    }

}
