package com.epam.tat.home8.ui.page.yandexmail;

import com.epam.tat.home8.bo.Letter;
import com.epam.tat.home8.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.epam.tat.home8.util.WebDriverHelper.waitForDisappear;
import static com.epam.tat.home8.util.WebDriverHelper.waitForElementIsClickable;


public class ComposePage extends Page {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By MAIL_SENT_ERROR = By.xpath("//span[@class='b-notification b-notification_error b-notification_error_required']");
    public static final By SEND_MAIL_MASSAGE_LOCATOR = By.xpath("//div[@class='b-statusline']//a[@class='b-statusline__link']");
    public static final By SAVED_IN_DRAFT = By.xpath("(//span[@class='b-compose-message__actions__helper b-compose-message__actions__helper_saved'])[2]");
    private String[] splited_href;

    public static final int TIME_OUT_ERROR_MESSAGE_SECONDS = 5;

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
//        driver.get("https://mail.yandex.by/?#compose");
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR).click();
    }

    public void sendLetter(Letter letter) {
        new WebDriverWait(driver, TIME_OUT_ERROR_MESSAGE_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(TO_INPUT_LOCATOR));

        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

    public void sendLetterWithoutAddress(Letter letter) {
        new WebDriverWait(driver, TIME_OUT_ERROR_MESSAGE_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(SUBJECT_INPUT_LOCATOR));

        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
    }

    public boolean errorMessageIsPresent() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);

        try {
            new WebDriverWait(driver, TIME_OUT_ERROR_MESSAGE_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(MAIL_SENT_ERROR));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void sendLetterWithoutSubjectBody(Letter letter) {
        new WebDriverWait(driver, TIME_OUT_ERROR_MESSAGE_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(TO_INPUT_LOCATOR));

        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
        waitForElementIsClickable(SEND_MAIL_MASSAGE_LOCATOR);
        String href = driver.findElement(SEND_MAIL_MASSAGE_LOCATOR).getAttribute("href");
        splited_href = href.split("#");
    }

    public String[] getSplited_href() {
        return splited_href;
    }


    public void createDraftMail(Letter letter) {
        new WebDriverWait(driver, TIME_OUT_ERROR_MESSAGE_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(TO_INPUT_LOCATOR));

        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());


    }

    public boolean savedInDraft() {
        waitForElementIsClickable(SAVED_IN_DRAFT);

        try {
            new WebDriverWait(driver, TIME_OUT_ERROR_MESSAGE_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(SAVED_IN_DRAFT));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
