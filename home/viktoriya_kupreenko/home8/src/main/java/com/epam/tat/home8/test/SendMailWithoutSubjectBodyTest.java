package com.epam.tat.home8.test;

import com.epam.tat.home8.bo.Letter;
import com.epam.tat.home8.bo.LetterFactory;
import com.epam.tat.home8.ui.page.yandexmail.ComposePage;
import com.epam.tat.home8.ui.page.yandexmail.InboxPage;
import com.epam.tat.home8.ui.page.yandexmail.SentPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

public class SendMailWithoutSubjectBodyTest extends LoginTest {

    private Letter letter;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }


    @Test(description = "Send email without subject and body")
    public void sendMailWithoutSubjectBody() {

        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetterWithoutSubjectBody(letter);

        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresentWithoutSubject(composePage.getSplited_href()), "Letter should be present in inbox folder");

        SentPage sentPage = new SentPage(driver);
        sentPage.open();
        Assert.assertTrue(sentPage.isLetterPresentWithoutSubject(composePage.getSplited_href()), "Letter should be present in sent folder");

    }

}
