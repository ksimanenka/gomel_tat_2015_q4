package com.epam.tat.home8.test;

import com.epam.tat.home8.bo.Letter;
import com.epam.tat.home8.bo.LetterFactory;
import com.epam.tat.home8.ui.page.yandexmail.ComposePage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SendMailWithoutAddressTest extends LoginTest {

    private Letter letter;

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test(description = "Send email without filled address, check error is present")
    public void errorSendMail() {

        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetterWithoutAddress(letter);
        Assert.assertEquals(composePage.errorMessageIsPresent(), true, "Error message was not shown");

    }

}

