package com.epam.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ErrorSendMailTest extends LoginTest {

    // UI data
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By MAIL_SENT_ERROR = By.xpath("//span[@class='b-notification b-notification_error b-notification_error_required']");
    public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");

    // Test data

    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @Test(description = "Check error send email without filled address", dependsOnMethods = "login")
    public void sendMailWithoutAddress() {
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        waitForElementIsClickable(SUBJECT_INPUT_LOCATOR);

        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        waitForElementIsClickable(MAIL_TEXT_LOCATOR);

        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
//        WebElement attachFileButton = driver.findElement(ATTACH_LOCATOR);
//        attachFileButton.sendKeys("D:\\LU28ACmNKIU.jpg");
        waitForElementIsClickable(SEND_MAIL_BUTTON_LOCATOR);

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForElementIsClickable(MAIL_SENT_ERROR);
        List<WebElement> actualRes = driver.findElements(MAIL_SENT_ERROR);
        Assert.assertEquals(!actualRes.isEmpty(), true, "Error was not shown");

    }
}
