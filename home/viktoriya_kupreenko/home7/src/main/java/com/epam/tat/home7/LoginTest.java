package com.epam.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;


public class LoginTest extends BaseYandexMailTest {

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[@href='https://mail.yandex.by']");
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");

    // Test data
    private String userLogin = "tat-test-user@yandex.ru"; // ACCOUNT
    private String userPassword = "tat-123qwe"; // ACCOUNT

    @Test(description = "Mail login")
    public void login() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        waitForElementIsClickable(LOGIN_INPUT_LOCATOR);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
        List<WebElement> actualRes = driver.findElements(COMPOSE_BUTTON_LOCATOR);
        Assert.assertEquals(!actualRes.isEmpty(), true, "LoginTest is not success");
    }

}


