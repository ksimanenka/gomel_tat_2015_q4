package com.epam.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseYandexMailTest {

    protected WebDriver driver;

    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;

    // AUT data
    public static final String BASE_URL = "http://yandex.by";

    // UI data
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");

    //    @BeforeClass
//    public void prepareBrowser() throws MalformedURLException {
//        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
////        driver.manage().window().maximize();
//        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
//        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
//    }
    @BeforeClass
    public void firefoxRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
    }

    @BeforeClass
    public void bc() {
    }

    @BeforeMethod
    public void bm() {
    }

    @BeforeGroups()
    public void bg() {
    }

    @AfterMethod
    public void am() {
    }

    @AfterClass
    public void ac() {
    }

    @AfterClass
    public void clearBrowser() {
        driver.close();
        driver.quit();
    }

    public WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 10000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

}
