package com.epam.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class SendMailTest extends LoginTest {


    // UI data
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@ class='b-folders__folder__link' and @href='#inbox']");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By MAIL_SENT_SUCCESS = By.xpath("(//div[@class='b-done']/div)[1]");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;


    // Test data
    private String mailTo = "tat-test-user@yandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM


    @Test(description = "Send email", dependsOnMethods = "login")
    public void sendMail() {
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        waitForElementIsClickable(TO_INPUT_LOCATOR);
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        waitForElementIsClickable(SUBJECT_INPUT_LOCATOR);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        waitForElementIsClickable(MAIL_TEXT_LOCATOR);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
//        WebElement attachFileButton = driver.findElement(ATTACH_LOCATOR);
//        attachFileButton.sendKeys("D:\\LU28ACmNKIU.jpg");
        waitForElementIsClickable(SEND_MAIL_BUTTON_LOCATOR);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);

        waitForElementIsClickable(MAIL_SENT_SUCCESS);
        List<WebElement> actualRes1 = driver.findElements(MAIL_SENT_SUCCESS);
        Assert.assertEquals(!actualRes1.isEmpty(), true, "Mail was not sent");
    }

    @Test(description = "Mail exists in folder Inbox",dependsOnMethods = "sendMail")
    public void inboxFolder() {
//      driver.get("https://mail.yandex.by/?#inbox");
        waitForElementIsClickable(INBOX_LINK_LOCATOR);
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))));

//        waitForElementIsClickable(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        List<WebElement> actualRes2 = driver.findElements(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        Assert.assertEquals(!actualRes2.isEmpty(), true, "Mail does not exist in folder Inbox");
    }

    @Test(description = "Mail exists in folder Sent",dependsOnMethods = "sendMail")
    public void sentFolder() {
        waitForElementIsClickable(SENT_LINK_LOCATOR);
        WebElement sentLink = driver.findElement(SENT_LINK_LOCATOR);
        sentLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))));
        waitForElementIsClickable(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        List<WebElement> actualRes3 = driver.findElements(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        Assert.assertEquals(!actualRes3.isEmpty(), true, "Mail does not exist in folder Inbox");
    }

}
