package home5task;

import home4taskFilesForTest.WordList;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created by Shaman on 17.12.2015.
 */
public class WorkTesting extends BaseAppTest {

    @Test(groups = "a")
    public void checkOneHelloFileParsingCountWords() {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + "test1", fileMask);
        int result = WList.getValues().size();
        Assert.assertEquals(result, 25, "Wrong count");
    }

    @Test(dependsOnMethods = "checkOneHelloFileParsingCountWords", groups = "a")
    public void checkSomeHelloFilesParsingCountWords() {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + "test2", fileMask);
        int result = WList.getValues().size();
        Assert.assertEquals(result, 25, "Wrong count");
    }

    @Test(groups = "b")
    public void checkRusFilesParsingCountWords() {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + "test6", fileMask);
        int result = WList.getValues().size();
        Assert.assertEquals(result, 12, "Wrong count");
    }

    @Test(groups = "b")
    public void checkUtfFilesParsingCountWords() {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + "test5", fileMask);
        int result = WList.getValues().size();
        Assert.assertEquals(result, 8, "Wrong count");
    }

    @Test(dataProvider = "create", dataProviderClass = DataProviderClass.class, groups = "b")
    public void checkSomeHelloFilesForDataProvider(String folderName, int countResult) {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + folderName, fileMask);
        int result = WList.getValues().size();
        Assert.assertEquals(result, countResult, "Wrong count");
    }

}
