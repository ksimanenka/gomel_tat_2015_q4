package home5task;

import home4taskFilesForTest.WordList;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 22.12.2015.
 */
public class DataProviderTest extends BaseAppTest {
    @Test(dataProvider = "create", dataProviderClass = DataProviderClass.class, groups = "b")
    public void checkSomeHelloFilesForDataProvider(String folderName, int countResult) {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + folderName, fileMask);
        int result = WList.getValues().size();
        Assert.assertEquals(result, countResult, "Wrong count");
    }
}
