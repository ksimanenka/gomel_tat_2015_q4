package home5task.runner;

import home5task.listeners.FailTestListener;
import home5task.listeners.Home5SuitListener;
import home5task.listeners.Home5TestListener;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shaman on 16.12.2015.
 */
public class Runner {

    public static void main(String[] args) {
        TestListenerAdapter tla = new TestListenerAdapter();
        TestNG tng = new TestNG();
        tng.addListener(tla);
        tng.addListener(new Home5SuitListener());
        tng.addListener(new Home5TestListener());
        tng.addListener(new FailTestListener());

        XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");
        List<String> files = new ArrayList<>();
        files.addAll(new ArrayList<String>() {{
            add("./src/main/resources/Suites/Home5-1.xml");
            add("./src/main/resources/Suites/Home5-2.xml");
        }});
        suite.setSuiteFiles(files);
        suite.setParallel(XmlSuite.ParallelMode.METHODS);
        suite.setThreadCount(4);
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        tng.setXmlSuites(suites);
        tng.run();
    }
}
