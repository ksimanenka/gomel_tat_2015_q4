package home4taskFilesForTest;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Shaman on 21.12.2015.
 */
public class FilesPreparation {

    public void prepare(WordList wordListTemp) {
        //Download and unzip repo
        File zip = new File(wordListTemp.getDestination());
        File workFolder = new File(wordListTemp.getDestinationFolderDel());
        if (!zip.exists() || !workFolder.exists()) {
            try {
                FileUtils.deleteDirectory(new File(wordListTemp.getDestinationFolder()));
                FileUtils.forceMkdir(new File(wordListTemp.getDestinationFolder()));
                URL downloadUrl = new URL(wordListTemp.getSource());
                File downloadFile = new File(wordListTemp.getDestination());
                FileUtils.copyURLToFile(downloadUrl, downloadFile);
                try {
                    ZipFile zipFile = new ZipFile(wordListTemp.getDestination());
                    zipFile.extractAll(wordListTemp.getDestinationFolder());
                } catch (ZipException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
