package home4taskFilesForTest;

/**
 * Created by Shaman on 21.12.2015.
 */
public class FolderPreparation {

    String zipName = "9b7a63db8498";
    String source =
            "https://bitbucket.org/ksimanenka/gomel_tat_2015_q4/get/"+zipName+".zip";
    String destination = "d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\downloadFile.zip";
    String destinationFolder = "d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4";
    String destinationFolderDel =
            "d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\ksimanenka-gomel_tat_2015_q4-"+zipName;
    String fileMask = "hel*.t?t";


    public String getZipName() {
        return zipName;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public String getDestinationFolder() {
        return destinationFolder;
    }

    public String getDestinationFolderDel() {
        return destinationFolderDel;
    }

    public String getFileMask() {
        return fileMask;
    }

}
