package home4taskFilesForTest;

/**
 * class HomeWork4 is main class
 *
 * @version
1.0 14 Dec 2015
 * @author
MIkalai Kabzar
 */

public class HomeWork4 {

    public static void main(String[] args) {
        WordList engine = new WordList();
        FolderPreparation folderPrepare = new FolderPreparation();
        engine.setZipName(folderPrepare.getZipName());
        engine.setSource(folderPrepare.getSource());
        engine.setDestinationFolder(folderPrepare.getDestinationFolder());
        engine.setDestination(folderPrepare.getDestination());
        engine.setDestinationFolderDel(folderPrepare.getDestinationFolderDel());
        engine.setFileMask(folderPrepare.getFileMask());
        FilesPreparation fPrepare = new FilesPreparation();
        fPrepare.prepare(engine);
        //Find and parse files
        engine.getWords(engine.getDestinationFolder(),engine.getFileMask());
        //Print result
        engine.printStatistics();
    }
}
