package com.gomel.tat.home4task;

import org.apache.commons.io.FileUtils;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.util.HashMap;
import java.util.*;
import java.io.IOException;
import java.net.URL;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.core.ZipFile;

/**
 * class WordList count nomber of inclusionf of words in texts
 *
 * @version
1.0 14 Dec 2015
 * @author
MIkalai Kabzar
 */

public class WordList {

    private List<String> address = new ArrayList<String>();
    private List<File> files = new ArrayList<File>();
    private TreeMap<String, Integer> values = new TreeMap<String, Integer>();

    public void run(){
        String zipName = "9b7a63db8498";
        String source =
            "https://bitbucket.org/ksimanenka/gomel_tat_2015_q4/get/"+zipName+".zip";
        String destination = "d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\downloadFile.zip";
        String destinationFolder = "d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4";
        String destinationFolderDel =
            "d:\\TAT\\TAT 2015 Q4\\gomel_tat_2015_q4\\ksimanenka-gomel_tat_2015_q4-"+zipName;
        String fileMask = "hello.txt";
        String password = "";
        //Download and unzip repo
        File zip = new File(destination);
        File workFolder = new File(destinationFolderDel);
        if (!zip.exists() || !workFolder.exists()){
            try {
                deleteFolderWithFiles(workFolder);
                deleteFolderWithFiles(zip);
                URL downloadUrl = new URL(source);
                File downloadFile = new File(destination);
                FileUtils.copyURLToFile(downloadUrl, downloadFile);
                try {
                    ZipFile zipFile = new ZipFile(destination);
                    if (zipFile.isEncrypted()) {
                        zipFile.setPassword(password);
                    }
                    zipFile.extractAll(destinationFolder);
                } catch (ZipException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //Find and parse files
        getWords(destinationFolder,fileMask);
        //Print result
        System.out.println("Result:");
        printStatistics(values);
    }

    public void fillTreeMap(List<File> files){
        for (int i = 0; i < files.size(); i++) {
            addInfoFromFile(files.get(i));
        }
    }

    public void deleteFolderWithFiles(File file) {
        if(!file.exists())
            return;
        if(file.isDirectory()) {
            for(File f : file.listFiles()){
                deleteFolderWithFiles(f);
            }
            file.delete();
        } else {
            file.delete();
        }
    }

    public void addInfoFromFile(File file){
        String s = "";
        try {
            s = FileUtils.readFileToString(file).toLowerCase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String sa[] = s.split("[^а-яa-z]");
        int tempInteger = 0;
        for (String saa : sa ){
            if ((saa.length()>1) || (saa.equals("a")) || (saa.equals("а"))||
               (saa.equals("в")) || (saa.equals("и")) || (saa.equals("к")) ||
               (saa.equals("о"))){
                if (values.containsKey(saa)){
                    tempInteger = values.get(saa)+1;
                    values.put(saa,tempInteger);
                } else {
                    values.put(saa,1);
                }
            }
            if (saa.length() == 1){
                if (saa.equals("a")/*a = article*/ || (saa.equals("а"))||//rus
                        (saa.equals("в")) || (saa.equals("и")) ||
                        (saa.equals("к")) || (saa.equals("о")) ||
                        (saa.equals("у")) || (saa.equals("я"))){
                    if (values.containsKey(saa)){
                        tempInteger = values.get(saa)+1;
                        values.put(saa,tempInteger);
                    } else {
                        values.put(saa,1);
                    }
                }
            }
        }
    }

    public void printStatistics(TreeMap<String, Integer> hm){
        for (HashMap.Entry entry : hm.entrySet()) {
            System.out.printf("%-30s%-30s%n","Word: " + entry.getKey(),"Quantity: "
                    + entry.getValue());
        }
    }

    public void getWords(String baseDirectory, String fileNamePattern) {
        String s;
        s = new String(baseDirectory);
        File f = new File(s);
        if (!f.exists()) {
            System.out.println("\nNot found: " + s);
        }
        if (!f.isDirectory()) {
            System.out.println("\nNot directory: " + s);
        }
        list(s, fileNamePattern);
        fillTreeMap(files);
    }

    /**recursive method of searching the files in folders and subfolders*/
    void list(String szDir, String fileNamePattern) {
        int i;
        File f = new File(szDir);
        String[] sDirList = f.list();
        for (i = 0; i < sDirList.length; i++) {
            File f1 = new File(szDir +
                    File.separator + sDirList[i]);
            if (f1.isFile()) {
                if (f1.getName().equals(fileNamePattern)) {
                    address.add(szDir + File.separator + sDirList[i]);
                    files.add(f1);
                }
            } else {
                list(szDir + File.separator + sDirList[i], fileNamePattern);
            }
        }
    }
}




