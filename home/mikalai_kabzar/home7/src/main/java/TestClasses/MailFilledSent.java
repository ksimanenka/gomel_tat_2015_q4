package TestClasses;

import Instance.WebDriverInstanceSetup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 24.12.2015.
 */
public class MailFilledSent extends LoginPositive {

    private boolean letterIsInInbox = false;
    private boolean letterIsInSent = false;
    protected String letterId = "";

    public void realizeMailFilledSent(String browserName) {
        realizeLoginPositive(browserName);
        WebElement composeButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.TO_INPUT_LOCATOR);
        toInput.sendKeys(webDriverInstance.mailTo);
        WebElement subjectInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(webDriverInstance.mailSubject);
        WebElement mailContentText = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(webDriverInstance.mailContent);
        WebElement sentMailButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.SEND_MAIL_BUTTON_LOCATOR);
        sentMailButton.click();
        sleep(3);
        String urlTemp = webDriverInstance.getDriver().
                findElement(By.xpath("//a[@class = 'b-statusline__link']"))
                .getAttribute("href");
        WebElement inboxFolderButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_INCOMING_FOLDER_LOCATOR);
        inboxFolderButton.click();
        webDriverInstance.getDriver().get(urlTemp);
        String letterIdTemp = webDriverInstance.getDriver()
                .findElement(By.xpath("//div[@class = 'b-mail-dropdown__item " +
                        "b-mail-dropdown__item_with-icon']" +
                        "/a[contains(@href,'#thread')]")).
                        getAttribute("href");
        letterId = letterIdTemp.substring(letterIdTemp.lastIndexOf("/") + 1);

        WebElement inboxFolderButtonNew = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_INCOMING_FOLDER_LOCATOR);
        WebElement sentFolderButtonNew = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_OUTCOMING_FOLDER_LOCATOR);
        inboxFolderButtonNew.click();
        sentFolderButtonNew.click();
        inboxFolderButtonNew.click();
        letterIsInInbox = webDriverInstance.getDriver().
                findElement(By.xpath("//div[@class = 'b-messages b-messages_threaded']" +
                        "/div[@data-id = 't" + letterId + "']")).isEnabled();
        //sentFolderButtonNew.click();
        // Some change, and this one bring mistake in Chrome(Firefox is Ok)
        webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_OUTCOMING_FOLDER_LOCATOR).click();
        letterIsInSent = webDriverInstance.getDriver().
                findElement(By.xpath("//div[@class = 'b-messages b-messages_threaded']" +
                        "/div[@data-id = 't" + letterId + "']")).isEnabled();
    }

    public boolean isLetterIsInInbox() {
        return letterIsInInbox;
    }

    public boolean isLetterIsInSent() {
        return letterIsInSent;
    }


}
