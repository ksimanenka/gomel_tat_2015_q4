package TestClasses.ClassForChrome;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import Instance.WebDriverInstanceSetup;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 23.12.2015.
 */
public class WebDriverInstanceSetupChrome extends WebDriverInstanceSetup {

    private static String browser = "Chrome";

    @BeforeClass(description = "Prepare Chrome browser")
    public void prepareBrowser() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:5555/wd/hub"),
                DesiredCapabilities.chrome());
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
    }

    @AfterClass(description = "Close Chrome browser")
    public void clearBrowser() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver " + e.getMessage());
        } finally {
            driver = null;
        }
    }

    public static String getBrowser() {
        return browser;
    }
}
