package TestClasses.ClassForChrome;

import TestClasses.WrongAddressName;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 27.12.2015.
 */
public class WrongAddressNameTest {
    @Test(description = "Chrome wrong address name test")
    public void realizeWrongAddressNameTest() {
        WrongAddressName wrongAddress = new WrongAddressName();
        wrongAddress.realizeWrongAddressName(WebDriverInstanceSetupChrome.
                getBrowser());
        boolean result = wrongAddress.isWrongAddressAllert();
        Assert.assertEquals(result, true, "You should write letter with wrong address." +
                " Mistake.");
    }
}
