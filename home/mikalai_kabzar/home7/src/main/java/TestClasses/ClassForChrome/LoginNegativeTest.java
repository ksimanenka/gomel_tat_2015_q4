package TestClasses.ClassForChrome;

import TestClasses.LoginNegative;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginNegativeTest {
    @Test(description = "Chrome login negative test")
    public void realizeLoginNegativeTest() {
        LoginNegative loginN = new LoginNegative();
        loginN.realizeLoginNegative(WebDriverInstanceSetupChrome.
                getBrowser());
        boolean result = false;
        if (loginN.getWebDriverInstance().getDriver().getCurrentUrl().
                indexOf("passport.yandex") != -1) {
            result = true;
        }
        Assert.assertEquals(result, true, "You should login with fakse password. " +
                "Mistake.");
    }
}
