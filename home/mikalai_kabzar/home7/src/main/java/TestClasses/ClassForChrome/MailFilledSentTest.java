package TestClasses.ClassForChrome;

import TestClasses.MailFilledSent;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class MailFilledSentTest {

    @Test(description = "Chrome mail filled send test")
    public void realizeMailFilledSentTest() {
        MailFilledSent mailSentFill = new MailFilledSent();
        mailSentFill.realizeMailFilledSent(WebDriverInstanceSetupChrome.
                getBrowser());
        boolean result = false;
        if (mailSentFill.isLetterIsInInbox() && mailSentFill.isLetterIsInSent()) {
            result = true;
        }
        Assert.assertEquals(result, true, "Mail is not exist in folder Sent and in folder Inbox" +
                " Mistake.");
    }
}
