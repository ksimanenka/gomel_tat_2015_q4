package TestClasses;

import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 25.12.2015.
 */
public class LoginNegative extends EnterButton {

    public void realizeLoginNegative(String browserName) {
        realizeEnterButton(browserName);
        WebElement loginInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(webDriverInstance.userLogin);
        WebElement passInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(webDriverInstance.userPassword + "123");//Wrong password
        passInput.submit();
    }
}