package TestClasses;

import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginPositive extends EnterButton {

    public void realizeLoginPositive(String browserName) {
        realizeEnterButton(browserName);
        WebElement loginInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(webDriverInstance.userLogin);
        WebElement passInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(webDriverInstance.userPassword);
        passInput.submit();
    }
}
