package TestClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 27.12.2015.
 */
public class EmptyAddressName extends LoginPositive {

    protected boolean emptyAddressAllert = false;

    public void realizeEmptyAddressName(String browserName) {
        realizeLoginPositive(browserName);
        WebElement composeButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.TO_INPUT_LOCATOR);
        toInput.sendKeys("");
        //empty e-mail
        WebElement subjectInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(webDriverInstance.mailSubject);
        WebElement mailContentText = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(webDriverInstance.mailContent);
        WebElement sendMailButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        emptyAddressAllert = webDriverInstance.getDriver().
                findElement(By.xpath("//span[@class = 'b-notification b-notification_" +
                        "error b-notification_error_required']")).isEnabled();
    }

    public boolean isEmptyAddressAllert() {
        return emptyAddressAllert;
    }
}
