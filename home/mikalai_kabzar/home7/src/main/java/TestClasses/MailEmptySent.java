package TestClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 27.12.2015.
 */
public class MailEmptySent extends LoginPositive {

    private boolean letterIsInInbox = false;
    private boolean letterIsInSent = false;
    protected String letterId ="";

    public void realizeMailEmptySent(String browserName) {
        realizeLoginPositive(browserName);
        WebElement composeButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.TO_INPUT_LOCATOR);
        toInput.sendKeys(webDriverInstance.mailTo);
        WebElement subjectInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys("");
        WebElement mailContentText = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys("");
        WebElement sentMailButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.SEND_MAIL_BUTTON_LOCATOR);
        sentMailButton.click();
        sleep(3);
        String urlTemp = webDriverInstance.getDriver().
                findElement(By.xpath("//a[@class = 'b-statusline__link']"))
                .getAttribute("href");
        WebElement inboxFolderButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_INCOMING_FOLDER_LOCATOR);
        inboxFolderButton.click();
        letterId = urlTemp.substring(urlTemp.lastIndexOf("/")+1);
        letterIsInInbox = webDriverInstance.getDriver().
                findElement(By.xpath("//div[@class = 'b-messages b-messages_threaded']" +
                        "/div[@data-id = '"+letterId+"']")).isEnabled();
        WebElement sentFolderButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_OUTCOMING_FOLDER_LOCATOR);
        sentFolderButton.click();
        letterIsInSent = webDriverInstance.getDriver().
                findElement(By.xpath("//div[@class = 'b-messages b-messages_threaded']" +
                        "/div[@data-id = '"+letterId+"']")).isEnabled();
    }

    public boolean isLetterIsInInbox() {
        return letterIsInInbox;
    }

    public boolean isLetterIsInSent() {
        return letterIsInSent;
    }


}
