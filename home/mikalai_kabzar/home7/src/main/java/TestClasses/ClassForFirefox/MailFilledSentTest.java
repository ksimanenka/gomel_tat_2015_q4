package TestClasses.ClassForFirefox;

import TestClasses.MailFilledSent;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class MailFilledSentTest {

    @Test(description = "Firefox mail filled send test")
    public void realizeMailFilledSentTest() {
        MailFilledSent mailSentFill = new MailFilledSent();
        mailSentFill.realizeMailFilledSent(WebDriverInstanceSetupFirefox.
                getBrowser());
        boolean result = false;
        if (mailSentFill.isLetterIsInInbox() && mailSentFill.isLetterIsInSent()) {
            result = true;
        }
        Assert.assertEquals(result, true, "Mail is not exist in folder Sent and in folder Inbox" +
                " Mistake.");
    }

}
