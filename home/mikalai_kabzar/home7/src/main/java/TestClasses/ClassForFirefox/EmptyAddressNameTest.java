package TestClasses.ClassForFirefox;

import TestClasses.EmptyAddressName;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 27.12.2015.
 */
public class EmptyAddressNameTest {

    @Test(description = "Firefox empty address name test")
    public void realizeEmptyAddressNameTest() {
        EmptyAddressName emptyAddress = new EmptyAddressName();
        emptyAddress.realizeEmptyAddressName(WebDriverInstanceSetupFirefox.
                getBrowser());
        boolean result = emptyAddress.isEmptyAddressAllert();
        Assert.assertEquals(result, true, "You should write letter with empty address." +
                " Mistake.");
    }
}
