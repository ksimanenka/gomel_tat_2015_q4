package TestClasses.ClassForFirefox;

import TestClasses.MailDraftTrashDelete;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 28.12.2015.
 */
public class DraftLetterTrashDeletedTest {

    @Test(description = "Firefox mail draft, trash and delete test")
    public void realizeMailDraftTrashDeleteTest() {
        MailDraftTrashDelete mailDraftTrashDel = new MailDraftTrashDelete();
        mailDraftTrashDel.realizeMailDraftTrashDelete(WebDriverInstanceSetupFirefox.
                getBrowser());
        boolean result = false;
        if (mailDraftTrashDel.isLetterIsInDraft() && mailDraftTrashDel.isLetterIsInTrash() && mailDraftTrashDel.isLetterDeleted()) {
            result = true;
        }
        Assert.assertEquals(result, true, "Mail is not exist in folder Draft or Trash or not delete" +
                " Mistake.");
    }
}
