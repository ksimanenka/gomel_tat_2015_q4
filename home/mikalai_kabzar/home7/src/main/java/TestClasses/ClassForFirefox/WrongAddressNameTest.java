package TestClasses.ClassForFirefox;

import TestClasses.LoginPositive;
import TestClasses.WrongAddressName;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 27.12.2015.
 */
public class WrongAddressNameTest {
    @Test(description = "Firefox wrong address name test")
    public void realizeWrongAddressNameTest() {
        WrongAddressName wrongAddress = new WrongAddressName();
        wrongAddress.realizeWrongAddressName(WebDriverInstanceSetupFirefox.
                getBrowser());
        boolean result = wrongAddress.isWrongAddressAllert();
        Assert.assertEquals(result, true, "You should write letter with wrong address." +
                " Mistake.");
    }
}
