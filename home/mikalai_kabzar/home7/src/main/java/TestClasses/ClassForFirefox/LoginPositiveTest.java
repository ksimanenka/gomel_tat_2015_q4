package TestClasses.ClassForFirefox;

import TestClasses.LoginPositive;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginPositiveTest {

    @Test(description = "Firefox login positive test")
    public void realizeLoginPositiveTest() {
        LoginPositive loginP = new LoginPositive();
        loginP.realizeLoginPositive(WebDriverInstanceSetupFirefox.
                getBrowser());
        boolean result = false;
        if (loginP.getWebDriverInstance().getDriver().getCurrentUrl().
                indexOf("passport.yandex") == -1) {
            result = true;
        }
        Assert.assertEquals(result, true, "You should login with true password." +
                " Mistake.");
    }
}
