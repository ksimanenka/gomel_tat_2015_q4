package Page.YandexMail;

import Util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginPage extends LaunchPage {

    public static final String USER_LOGIN = "LokiEngine@yandex.ru"; // ACCOUNT
    public static final String USER_PASSWORD = "TheBestPassword"; // ACCOUNT
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.xpath("//*[@id='compose-send']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By DRAFT_MAIL_FOLDER_LOCATOR = By.xpath("//a[@class = 'b-folders__folder__link' and @href = '#draft']");
    public static final By TRASH_MAIL_FOLDER_LOCATOR = By.xpath("//a[@class = 'b-folders__folder__link' and @href = '#trash']");
    public static final By MAIL_INCOMING_FOLDER_LOCATOR = By.xpath("//a[@class = 'b-folders__folder__link' and @href = '#inbox']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void login() {
        launch();
        WebElement loginInput = WebDriverHelper.waitForElementIsClickable(driver, LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(USER_LOGIN);
        WebElement passInput = WebDriverHelper.waitForElementIsClickable(driver, PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(USER_PASSWORD);
        passInput.submit();
    }

    public void loginError() {
        launch();
        WebElement loginInput = WebDriverHelper.waitForElementIsClickable(driver, LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(USER_LOGIN);
        WebElement passInput = WebDriverHelper.waitForElementIsClickable(driver, PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(USER_PASSWORD + "123");
        passInput.submit();
    }

    public boolean checkLoginPositive() {
        if (WebDriverHelper.elementIsOnThisPage(driver, MAIL_INCOMING_FOLDER_LOCATOR)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkLoginNegative() {
        if (driver.getCurrentUrl().
                indexOf("passport.yandex") != -1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkLetterIsHere(String letterId) {
        return WebDriverHelper.elementIsOnThisPage(driver, By.xpath("//div[@class = 'b-messages b-messages_threaded']/div[@data-id = 't" + letterId + "']"));
    }

    public boolean checkEmptyLetterIsHere(String letterId) {
        return WebDriverHelper.elementIsOnThisPage(driver, By.xpath("//div[@class = 'b-messages b-messages_threaded']/div[@data-id = '" + letterId + "']"));
    }

    public boolean checkDeleteLetterIsHere(String letterId) {
        return WebDriverHelper.elementIsOnThisPage(driver, By.xpath("//div[@data-id = '" + letterId + "']"));
    }

    public void open() {
    }

    public void goToLetter() {
        String urlTemp = driver.findElement(By.xpath("//a[@class = 'b-statusline__link']")).getAttribute("href");
        open();
        driver.get(urlTemp);
    }

}
