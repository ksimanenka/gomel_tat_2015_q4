package Page.YandexMail;

import Util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 05.01.2016.
 */
public class DraftPage extends LoginPage {

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebDriverHelper.waitForElementIsClickable(driver, DRAFT_MAIL_FOLDER_LOCATOR).click();
    }

    public void goToLetter(String letterId) {
        WebElement draftLetter = WebDriverHelper.waitForElementIsClickable(driver, By.xpath("//div[contains(@class,'" + letterId + "')]"));
        draftLetter.click();
    }

    public void deleteLetter() {
        WebElement deleteButton = WebDriverHelper.waitForElementIsClickable(driver, By.xpath("//a[@data-action = 'compose.delete']"));
        deleteButton.click();
    }
}
