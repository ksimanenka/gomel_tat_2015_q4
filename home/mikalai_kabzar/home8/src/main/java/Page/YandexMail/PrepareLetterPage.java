package Page.YandexMail;

import Letter.Letter;
import Util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 04.01.2016.
 */
public class PrepareLetterPage extends LoginPage {

    public PrepareLetterPage(WebDriver driver) {
        super(driver);
    }

    public static void prepareSentLetter(WebDriver driver, Letter letter) {
        WebElement composeButton = WebDriverHelper.waitForElementIsClickable(driver, COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = WebDriverHelper.waitForElementIsClickable(driver, TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = WebDriverHelper.waitForElementIsClickable(driver, SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = WebDriverHelper.waitForElementIsClickable(driver, MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());
    }

    public static void sentLetter(WebDriver driver) {
        WebElement sentMailButton = WebDriverHelper.waitForElementIsClickable(driver, SEND_MAIL_BUTTON_LOCATOR);
        sentMailButton.click();
    }

    public static String waitForDraftLetter(WebDriver driver) {
        String tempUrl = driver.getCurrentUrl();
        String tempUrlChange = tempUrl;
        int timer = 0;
        while (tempUrl.equals(tempUrlChange) && timer < 100) {
            sleep(1);
            tempUrlChange = driver.getCurrentUrl();
            timer++;
        }
        return tempUrlChange;
    }

    public static String findLetterIdDraft(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }

}
