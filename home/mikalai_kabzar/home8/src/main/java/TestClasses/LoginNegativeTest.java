package TestClasses;

import Page.YandexMail.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginNegativeTest extends BaseTestClass {

    @Test(description = "Login negative test")
    public void realizeLoginNegativeTest() {
        LoginPage loginNegative = new LoginPage(driver);
        loginNegative.loginError();
        boolean result = loginNegative.checkLoginNegative();
        Assert.assertEquals(result, true, "You should login with fakse password. " +
                "Mistake.");
    }
}
