package TestClasses;

import Page.YandexMail.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginPositiveTest extends BaseTestClass {

    @Test(description = "Login positive test")
    public void realizeLoginPositiveTest() {
        LoginPage loginPositive = new LoginPage(driver);
        loginPositive.login();
        boolean result = loginPositive.checkLoginPositive();
        Assert.assertEquals(result, true, "You should login with true password." +
                " Mistake.");
    }
}
