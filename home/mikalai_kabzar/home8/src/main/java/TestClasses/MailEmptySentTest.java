package TestClasses;

import Letter.LetterFactory;
import Page.YandexMail.IncomingPage;
import Page.YandexMail.OutcomingPage;
import Page.YandexMail.PrepareLetterPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 27.12.2015.
 */
public class MailEmptySentTest extends BaseTestClass {

    private boolean letterIsInInbox = false;
    private boolean letterIsInSent = false;

    @Test(description = "Mail empty sent test")
    public void realizeMailEmptySentTest() {
        IncomingPage incomingPage = new IncomingPage(driver);
        incomingPage.login();
        PrepareLetterPage.prepareSentLetter(driver, LetterFactory.getEmptyLetter());
        PrepareLetterPage.sentLetter(driver);
        incomingPage.sleep(3);
        incomingPage.findEmptyLetterId();
        letterIsInInbox = incomingPage.checkEmptyLetterIsHere(incomingPage.getLetterId());
        OutcomingPage outcomingPage = new OutcomingPage(driver);
        outcomingPage.open();
        letterIsInSent = outcomingPage.checkEmptyLetterIsHere(incomingPage.getLetterId());
        boolean result;
        if (letterIsInInbox && letterIsInSent) {
            result = true;
        } else {
            result = false;
        }
        Assert.assertEquals(result, true, "Mail is not exist in folder Sent and in folder Inbox" +
                " Mistake.");
    }
}
