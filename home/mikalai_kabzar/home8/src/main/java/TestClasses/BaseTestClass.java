package TestClasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Shaman on 05.01.2016.
 */
public class BaseTestClass {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;

    public WebDriver driver;

    @BeforeClass(groups = "Firefox")
    public WebDriver prepareBrowserFirefox() {
        if (driver != null) {
            shutdownWebDriver();
        }
        try {
            driver = new RemoteWebDriver(new URL("http://localhost:5555/wd/hub"),
                    DesiredCapabilities.firefox());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        return driver;
    }

    @BeforeClass(groups = "Chrome")
    public WebDriver prepareBrowserChrome() {
        if (driver != null) {
            shutdownWebDriver();
        }
        try {
            driver = new RemoteWebDriver(new URL("http://localhost:5555/wd/hub"),
                    DesiredCapabilities.chrome());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        return driver;
    }

    @AfterClass
    public void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            driver = null;
        }
    }

}
