package TestClasses;

import Letter.LetterFactory;
import Page.YandexMail.IncomingPage;
import Page.YandexMail.OutcomingPage;
import Page.YandexMail.PrepareLetterPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class MailFilledSentTest extends BaseTestClass {

    private boolean letterIsInInbox = false;
    private boolean letterIsInSent = false;

    @Test(description = "Mail filled send test")
    public void realizeMailFilledSentTest() {
        IncomingPage incomingPage = new IncomingPage(driver);
        incomingPage.login();
        PrepareLetterPage.prepareSentLetter(driver, LetterFactory.getRandomLetter());
        PrepareLetterPage.sentLetter(driver);
        incomingPage.sleep(3);
        incomingPage.goToLetter();
        incomingPage.sleep(3);
        incomingPage.findLetterId();
        incomingPage.open();
        letterIsInInbox = incomingPage.checkLetterIsHere(incomingPage.getLetterId());
        OutcomingPage outcomingPage = new OutcomingPage(driver);
        outcomingPage.open();
        letterIsInSent = outcomingPage.checkLetterIsHere(incomingPage.getLetterId());
        boolean result;
        if (letterIsInInbox && letterIsInSent) {
            result = true;
        } else {
            result = false;
        }
        Assert.assertEquals(result, true, "Mail is not exist in folder Sent and in folder Inbox" +
                " Mistake.");
    }
}
