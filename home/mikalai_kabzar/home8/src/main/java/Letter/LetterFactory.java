package Letter;

import java.util.Date;

/**
 * Created by Shaman on 03.01.2016.
 */
public class LetterFactory {

    public static Letter getRandomLetter() {
        String mailTo = "LokiEngine@yandex.ru";
        String mailSubject = "test subject " + new Date(System.currentTimeMillis());
        String mailContent = "mail content " + new Date(System.currentTimeMillis());
        return new Letter(mailTo, mailSubject, mailContent);
    }

    public static Letter getWrongAddressLetter() {
        String mailTo = "LokiEngine@yandex.ru@";
        String mailSubject = "test subject " + new Date(System.currentTimeMillis());
        String mailContent = "mail content " + new Date(System.currentTimeMillis());
        return new Letter(mailTo, mailSubject, mailContent);
    }

    public static Letter getEmptyAddressLetter() {
        String mailTo = "";
        String mailSubject = "test subject " + new Date(System.currentTimeMillis());
        String mailContent = "mail content " + new Date(System.currentTimeMillis());
        return new Letter(mailTo, mailSubject, mailContent);
    }

    public static Letter getEmptyLetter() {
        String mailTo = "LokiEngine@yandex.ru";
        String mailSubject = "";
        String mailContent = "";
        return new Letter(mailTo, mailSubject, mailContent);
    }


}
