package Util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Shaman on 09.01.2016.
 */
public class Content {

    private static HashMap<String, String> fileContent =
            new HashMap<String, String>();
    private static List<File> files = new ArrayList<File>();

    public static void fillContentMap(String baseDirectory, String fileNamePattern) {
        if ((baseDirectory.isEmpty()) || (baseDirectory == null)) {
            throw new IllegalArgumentException("Base directory is wrong");
        }
        searchinFiles(baseDirectory, fileNamePattern);
        for (int i = 0; i < files.size(); i++) {
            fileContent.put(files.get(i).getName(), readFile(files.get(i)));
        }
    }

    public static String readFile(File file) {
        String stringFile = "";
        try {
            stringFile = FileUtils.readFileToString(file, Util.CommonFileUtils.
                    getFileEncodingType(file.getPath())).toLowerCase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringFile;
    }

    private static void searchinFiles(String szDir, String fileNamePattern) {
        String[] sDirList = new File(szDir).list();
        for (int i = 0; i < sDirList.length; i++) {
            File tempFile = new File(szDir +
                    File.separator + sDirList[i]);
            if (tempFile.isFile()) {
                if (FilenameUtils.wildcardMatch(tempFile.getName(), fileNamePattern)) {
                    files.add(tempFile);
                }
            } else {
                searchinFiles(szDir + File.separator + sDirList[i], fileNamePattern);
            }
        }
    }

    public static String getContentByFileName(String fileName) {
        return fileContent.get(fileName);
    }
}
