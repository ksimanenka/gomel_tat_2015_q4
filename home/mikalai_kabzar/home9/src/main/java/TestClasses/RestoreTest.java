package TestClasses;

import Page.YandexMail.YandexDiskPage;
import Page.YandexMail.YandexTrashPage;
import Util.WebDriverHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 09.01.2016.
 */
public class RestoreTest extends BaseTestClass {

    @Test(description = "Restore test")
    public void realizeRestoreTest() {
        YandexDiskPage yandexDiskPage = new YandexDiskPage(driver);
        yandexDiskPage.login();
        yandexDiskPage.open();
        YandexTrashPage yandexTrashPage = new YandexTrashPage(driver);
        yandexTrashPage.clearYandexDiskTrash();
        Assert.assertTrue(yandexDiskPage.upload("TestFile№1.txt"));
        Assert.assertTrue(yandexDiskPage.upload("TestFile№2.txt"));
        Assert.assertTrue(yandexDiskPage.upload("TestFile№3.txt"));
        Assert.assertTrue(yandexDiskPage.upload("TestFile№4.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№5.txt"));
        yandexDiskPage.refresh();
        Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№1.txt"));
        Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№2.txt"));
        Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№3.txt"));
        //Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№4.txt"));
        //Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№5.txt"));
        yandexTrashPage.open();
        Assert.assertTrue(yandexTrashPage.restoreFile("TestFile№1.txt"));
        Assert.assertTrue(yandexTrashPage.restoreFile("TestFile№2.txt"));
        Assert.assertTrue(yandexTrashPage.restoreFile("TestFile№3.txt"));
        //Assert.assertTrue(yandexTrashPage.restoreFile("TestFile№4.txt"));
        //Assert.assertTrue(yandexTrashPage.restoreFile("TestFile№5.txt"));
        yandexDiskPage.openFiles();
        Assert.assertTrue(WebDriverHelper.elementIsOnThisPage
                (driver, yandexDiskPage.getFileXpath("TestFile№1.txt")));
        Assert.assertTrue(WebDriverHelper.elementIsOnThisPage
                (driver, yandexDiskPage.getFileXpath("TestFile№2.txt")));
        Assert.assertTrue(WebDriverHelper.elementIsOnThisPage
                (driver, yandexDiskPage.getFileXpath("TestFile№3.txt")));
        //Assert.assertTrue(WebDriverHelper.elementIsOnThisPage
        //        (driver, yandexDiskPage.getFileXpath("TestFile№4.txt")));
        //Assert.assertTrue(WebDriverHelper.elementIsOnThisPage
        //        (driver, yandexDiskPage.getFileXpath("TestFile№5.txt")));
    }
}
