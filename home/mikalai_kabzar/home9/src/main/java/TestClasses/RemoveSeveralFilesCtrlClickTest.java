package TestClasses;

import Page.YandexMail.YandexDiskPage;
import Page.YandexMail.YandexTrashPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shaman on 10.01.2016.
 */
public class RemoveSeveralFilesCtrlClickTest extends BaseTestClass {

    private static List<String> filesForTest = new ArrayList<String>();

    @BeforeClass
    public void prepareFilesAndFolders() {
        YandexDiskPage yandexDiskPage = new YandexDiskPage(driver);
        yandexDiskPage.login();
        yandexDiskPage.open();
        YandexTrashPage yandexTrashPage = new YandexTrashPage(driver);
        yandexTrashPage.clearYandexDiskTrash();
        Assert.assertTrue(yandexDiskPage.upload("TestFile№1.txt"));
        Assert.assertTrue(yandexDiskPage.upload("TestFile№2.txt"));
        Assert.assertTrue(yandexDiskPage.upload("TestFile№3.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№4.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№5.txt"));
        yandexDiskPage.refresh();
    }

    @Test(description = "Remove several files with 'ctrl+click' test")
    public void realizeRemoveSeveralFilesCtrlClickTest() {
        YandexDiskPage yandexDiskPage = new YandexDiskPage(driver);
        filesForTest.add("TestFile№1.txt");
        //filesForTest.add("TestFile№2.txt");
        filesForTest.add("TestFile№3.txt");
        //filesForTest.add("TestFile№4.txt");
        //filesForTest.add("TestFile№5.txt");
        Assert.assertTrue(yandexDiskPage.deleteFilesCtrlClick(filesForTest));
        YandexTrashPage yandexTrashPage = new YandexTrashPage(driver);
        yandexTrashPage.open();
        Assert.assertTrue(yandexTrashPage.deletedFilesCtrlClickIsHere(filesForTest));
    }
}
