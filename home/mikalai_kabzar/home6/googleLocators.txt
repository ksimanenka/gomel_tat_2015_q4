Xpath

		---Google search page---

queryInputEditXpathLocator  	//input[@id='lst-ib'] 
searchInGoogleButtonXpathLocator//input[@name='btnK'] 
iAmLuckyButtonXpathLocator	//input[@name='btnI']
SearchResultsXpathLocator	//*[@id='rso']//child::a/parent::div[@class = '_Icb _kk _wI' or @class = '_I2']/a | //child::a/parent::h3[@class = 'r']//a
			@class = '_Icb _kk _wI' - allows you to find link in "picture" category
			@class = '_I2' - allows you to find link in "news" category
_nThOLetterXpathLocator		//*[@id='nav']//child::span/parent::td[@class = 'cur' and text() = '4']//span | //child::span/parent::a[@class = 'fl' and text() = '4']//child::span
			Element (4 for example)
		
CSS 

		---Google search page--- 

queryInputEditCSSLocator  	#lst-ib 
searchInGoogleButtonCSSLocator 	input[name='btnK']
iAmLuckyButtonCSSLocator	input[name='btnI']
SearchResultsCSSLocator		.r>a
				._Icb._kk._wI>a
				._I2>a
_nThOLetterCSSLocator		#nav tr td:nth-child(n+1) span		
			n=4 is equal 5th letter for example

